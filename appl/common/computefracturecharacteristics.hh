// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Functionality to characterize fracture networks.
 */
#ifndef DUMUX_COMPUTE_FRACTURE_CHARACTERISTICS
#define DUMUX_COMPUTE_FRACTURE_CHARACTERISTICS

#include <dumux/discretization/elementsolution.hh>

namespace Dumux {
namespace FractureNetworks {

template<class Scalar>
struct VolumeCharacteristics
{
    Scalar volume;
    Scalar meanAperture;

    VolumeCharacteristics()
    : volume(0.0), meanAperture(0.0)
    {}
};

//! Computes the volumetric characterization of a network
template<class FVGridGeometry, class Problem, class SolutionVector>
typename FractureNetworks::VolumeCharacteristics<typename SolutionVector::field_type>
computeVolumeCharacteristics(const FVGridGeometry& fvGridGeometry,
                             const Problem& problem,
                             const SolutionVector& x)
{
    static constexpr int dim = FVGridGeometry::GridView::dimension;
    static constexpr int dimWorld = FVGridGeometry::GridView::dimensionworld;
    static_assert(dim < dimWorld, "Characterization makes sense only for surface grids");

    using Scalar = typename SolutionVector::field_type;
    using ReturnType = FractureNetworks::VolumeCharacteristics<Scalar>;

    ReturnType result;
    for (const auto& element : elements(fvGridGeometry.gridView()))
    {
        const auto elemSol = elementSolution(element, x, fvGridGeometry);

        auto fvGeometry = localView(fvGridGeometry);
        fvGeometry.bindElement(element);

        for (const auto& scv : scvs(fvGeometry))
        {
            const auto aperture = problem.extrusionFactor(element, scv, elemSol);
            result.volume += scv.volume()*aperture;
            result.meanAperture += aperture;
        }
    }

    result.meanAperture /= fvGridGeometry.numScv();
    return result;
}

} // end namespace FractureNetworks
} // end namespace Dumux

#endif // DUMUX_COMPUTE_FRACTURE_CHARACTERISTICS
