// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Computes the velocities at the cell centers for a given grid and solution vector.
 */
#ifndef DUMUX_FACETCOUPLING_COMMON_COMPUTEVELOCITIES_HH
#define DUMUX_FACETCOUPLING_COMMON_COMPUTEVELOCITIES_HH

#include <dune/common/indices.hh>

#include <dumux/common/properties.hh>
#include <dumux/discretization/method.hh>
#include <dumux/discretization/evalgradients.hh>
#include <dumux/discretization/elementsolution.hh>


namespace Dumux {

//! Function to compute velocities in a single-phase context
template<class TypeTag, std::size_t id,
         class Assembler, class CouplingManager,
         class FVGridGeometry, class GridVariables,
         class SolutionVector, class VelocityVector>
void computeSinglePhaseVelocities(Dune::index_constant<id> domainId,
                                  const Assembler& assembler,
                                  CouplingManager& couplingManager,
                                  const FVGridGeometry& fvGridGeometry,
                                  const GridVariables& gridVars,
                                  const SolutionVector& x,
                                  VelocityVector& velocities)
{
    using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
    VelocityOutput velOutput(gridVars);

    velocities.resize(fvGridGeometry.gridView().size(0));
    for (const auto& element : elements(fvGridGeometry.gridView()))
    {
        auto fvGeometry = localView(fvGridGeometry);
        auto elemVolVars = localView(gridVars.curGridVolVars());
        auto elemFluxVarsCache = localView(gridVars.gridFluxVarsCache());

        couplingManager.bindCouplingContext(domainId, element, assembler);
        fvGeometry.bind(element);
        elemVolVars.bind(element, fvGeometry, x);

        if (FVGridGeometry::discMethod != DiscretizationMethod::box)
        {
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);
            velOutput.calculateVelocity(velocities, element, fvGeometry, elemVolVars, elemFluxVarsCache, 0);
        }
        else
        {
            double volume = 0.0;
            typename VelocityVector::value_type vel(0.0);
            const auto elemSol = elementSolution(element, x, fvGridGeometry);
            for (const auto& scv : scvs(fvGeometry))
            {
                auto velScv = evalGradients(element,
                                            element.geometry(),
                                            fvGridGeometry,
                                            elemSol,
                                            scv.center())[0];
                velScv *= elemVolVars[scv].permeability();
                velScv *= elemVolVars[scv].mobility(0);
                velScv *= -1.0*scv.volume();
                vel += velScv;
                volume += scv.volume();
            }
            vel /= volume;
            velocities[fvGridGeometry.elementMapper().index(element)] = vel;
        }
    }
}

//! Function to compute velocities in a two-phase context
template<class TypeTag, std::size_t id,
         class Assembler, class CouplingManager,
         class FVGridGeometry, class GridVariables,
         class SolutionVector, class VelocityVector>
void computeTwoPhaseVelocities(Dune::index_constant<id> domainId,
                               const Assembler& assembler,
                               CouplingManager& couplingManager,
                               const FVGridGeometry& fvGridGeometry,
                               const GridVariables& gridVars,
                               const SolutionVector& x,
                               VelocityVector& velocitiesPhase0,
                               VelocityVector& velocitiesPhase1)
{
    using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
    VelocityOutput velOutput(gridVars);

    velocitiesPhase0.resize(fvGridGeometry.gridView().size(0));
    velocitiesPhase1.resize(fvGridGeometry.gridView().size(0));
    for (const auto& element : elements(fvGridGeometry.gridView()))
    {
        auto fvGeometry = localView(fvGridGeometry);
        auto elemVolVars = localView(gridVars.curGridVolVars());
        auto elemFluxVarsCache = localView(gridVars.gridFluxVarsCache());

        couplingManager.bindCouplingContext(domainId, element, assembler);
        fvGeometry.bind(element);
        elemVolVars.bind(element, fvGeometry, x);

        if (FVGridGeometry::discMethod != DiscretizationMethod::box)
        {
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);
            velOutput.calculateVelocity(velocitiesPhase0, element, fvGeometry, elemVolVars, elemFluxVarsCache, 0);
            velOutput.calculateVelocity(velocitiesPhase1, element, fvGeometry, elemVolVars, elemFluxVarsCache, 1);
        }
        else
        {
            double volume = 0.0;
            typename VelocityVector::value_type vel_w(0.0);
            const auto elemSol = elementSolution(element, x, fvGridGeometry);
            for (const auto& scv : scvs(fvGeometry))
            {
                auto vel = evalGradients(element,
                                         element.geometry(),
                                         fvGridGeometry,
                                         elemSol,
                                         scv.center())[0];
                vel *= elemVolVars[scv].permeability();
                vel *= elemVolVars[scv].mobility(0);
                vel *= -1.0*scv.volume();
                vel_w += vel;
                volume += scv.volume();
            }
            vel_w /= volume;

            // compute vector of non-wetting phase pressures
            using MaterialLaw = typename GridVariables::GridVolumeVariables::Problem::SpatialParams::MaterialLaw;
            auto elemSol_n = elemSol;
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& params = gridVars.curGridVolVars().problem().spatialParams().materialLawParams(element, scv, elemSol_n);
                elemSol_n[scv.localDofIndex()][0] += MaterialLaw::pc(params, 1.0 - elemSol_n[scv.localDofIndex()][1]);
            }

            typename VelocityVector::value_type vel_n(0.0);
            for (const auto& scv : scvs(fvGeometry))
            {
                auto vel = evalGradients(element,
                                         element.geometry(),
                                         fvGridGeometry,
                                         elemSol_n,
                                         scv.center())[0];
                vel *= elemVolVars[scv].permeability();
                vel *= elemVolVars[scv].mobility(1);
                vel *= -1.0*scv.volume();
                vel_n += vel;
            }
            vel_n /= volume;

            velocitiesPhase0[fvGridGeometry.elementMapper().index(element)] = vel_w;
            velocitiesPhase1[fvGridGeometry.elementMapper().index(element)] = vel_n;
        }
    }
}

} // end namespace Dumux

#endif
