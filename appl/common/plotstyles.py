import matplotlib.pyplot as plt

# plot adjustments
plt.style.use('ggplot')

plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = 'Ubuntu'
plt.rcParams['text.usetex'] = True
plt.rcParams['font.weight'] = 'light'
plt.rcParams['font.size'] = 13
plt.rcParams['axes.labelsize'] = 28
plt.rcParams['axes.labelweight'] = 'light'
plt.rcParams['xtick.labelsize'] = 15
plt.rcParams['ytick.labelsize'] = 15
plt.rcParams['legend.fontsize'] = 13
plt.rcParams['figure.titlesize'] = 15
plt.rcParams.update( {'mathtext.default': 'regular' } )
plt.rcParams["text.latex.preamble"] += r"\usepackage{stmaryrd}\n"
plt.rcParams["text.latex.preamble"] += r"\usepackage{siunitx}\n"
plt.rcParams["text.latex.preamble"] += r"\usepackage{amsmath}\n"
plt.rcParams["text.latex.preamble"] += r"\usepackage{cmbright}\n"
