// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Spatial params to the reference solution of the two-phase
 *        crossing fracture test case.
 */
#ifndef DUMUX_TWOP_CROSS_REFERENCE_SPATIAL_PARAMS_HH
#define DUMUX_TWOP_CROSS_REFERENCE_SPATIAL_PARAMS_HH

#include <dumux/io/grid/griddata.hh>

#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux {

/*!
 * \brief Spatial params to the reference solution of the two-phase
 *        crossing fracture test case.
 */
template<class FVGridGeometry, class Scalar>
class TwoPTestSpatialParams
: public FVSpatialParams<FVGridGeometry, Scalar, TwoPTestSpatialParams<FVGridGeometry, Scalar>>
{
    using GridView = typename FVGridGeometry::GridView;
    using Grid = typename GridView::Grid;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using ThisType = TwoPTestSpatialParams<FVGridGeometry, Scalar>;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, ThisType>;

    static constexpr int dimWorld = GridView::dimensionworld;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using EffectiveLaw = RegularizedVanGenuchten<Scalar>;

public:
    using MaterialLaw = EffToAbsLaw<EffectiveLaw>;
    using MaterialLawParams = typename MaterialLaw::Params;
    using PermeabilityType = Scalar;

    TwoPTestSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                          std::shared_ptr<const Dumux::GridData<Grid>> gridData)
    : ParentType(fvGridGeometry)
    , gridDataPtr_(gridData)
    {
        matrixPerm_ = getParamFromGroup<Scalar>("Matrix", "SpatialParams.Permeability");
        matrixPoro_ = getParamFromGroup<Scalar>("Matrix", "SpatialParams.Porosity");
        fracturePerm_ = getParamFromGroup<Scalar>("Fracture", "SpatialParams.Permeability");
        fracturePoro_ = getParamFromGroup<Scalar>("Fracture", "SpatialParams.Porosity");

        // residual saturations
        matrixMaterialLawParams_.setSwr(getParamFromGroup<Scalar>("Matrix", "SpatialParams.Swr"));
        matrixMaterialLawParams_.setSnr(getParamFromGroup<Scalar>("Matrix", "SpatialParams.Snr"));
        fractureMaterialLawParams_.setSwr(getParamFromGroup<Scalar>("Fracture", "SpatialParams.Swr"));
        fractureMaterialLawParams_.setSnr(getParamFromGroup<Scalar>("Fracture", "SpatialParams.Snr"));

        // parameters for the Van Genuchten law
        // alpha and n
        matrixMaterialLawParams_.setVgAlpha(getParamFromGroup<Scalar>("Matrix", "SpatialParams.VGAlpha"));
        matrixMaterialLawParams_.setVgn(getParamFromGroup<Scalar>("Matrix", "SpatialParams.VGN"));
        fractureMaterialLawParams_.setVgAlpha(getParamFromGroup<Scalar>("Fracture", "SpatialParams.VGAlpha"));
        fractureMaterialLawParams_.setVgn(getParamFromGroup<Scalar>("Fracture", "SpatialParams.VGN"));
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     *        In this test, we use element-wise distributed permeabilities.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return permeability
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {

        // do not use a less permeable lens in the test with inverted wettability
        if (isInFracture(element))
            return fracturePerm_;
        return matrixPerm_;
    }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param globalPos The global position
     */
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    {
        if (isInFracture(element))
            return fracturePoro_;
        return matrixPoro_;
    }

    /*!
     * \brief Returns the parameter object for the Brooks-Corey material law.
     *        In this test, we use element-wise distributed material parameters.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return the material parameters object
     */
    template<class ElementSolution>
    const MaterialLawParams& materialLawParams(const Element& element,
                                               const SubControlVolume& scv,
                                               const ElementSolution& elemSol) const
    {
        if (isInFracture(element))
            return fractureMaterialLawParams_;
        return matrixMaterialLawParams_;
    }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \return the wetting phase index
     * \param globalPos The global position
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::phase0Idx; }

    // Returns true if a given position is inside the fracture
    bool isInFracture(const Element& element) const
    { return gridDataPtr_->getElementDomainMarker(element) == 2; }

private:
    //! pointer to the grid data (contains domain markers)
    std::shared_ptr<const Dumux::GridData<Grid>> gridDataPtr_;

    Scalar matrixPerm_;
    Scalar matrixPoro_;
    Scalar fracturePerm_;
    Scalar fracturePoro_;
    MaterialLawParams matrixMaterialLawParams_;
    MaterialLawParams fractureMaterialLawParams_;
};

} // end namespace Dumux

#endif
