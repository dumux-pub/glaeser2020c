// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Solver to the reference solution of the
 *        two-phase crossing fracture test case.
 */
#ifndef DUMUX_TWOP_CROSS_REFERENCE_SOLVER_HH
#define DUMUX_TWOP_CROSS_REFERENCE_SOLVER_HH

#include <config.h>

#include <ctime>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>

#include "problem.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/linear/amgbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>

// return type of solveReference(), containing the
// solutions and the grid views on which they were computed
template<class TypeTag>
struct ReferenceSolutionStorage
{
private:
    using Grid = Dumux::GetPropType<TypeTag, Dumux::Properties::Grid>;
    using FVGG = Dumux::GetPropType<TypeTag, Dumux::Properties::FVGridGeometry>;
    using GridVars = Dumux::GetPropType<TypeTag, Dumux::Properties::GridVariables>;
    using Problem = Dumux::GetPropType<TypeTag, Dumux::Properties::Problem>;

public:
    Dumux::GridManager<Grid> gridManager;
    std::shared_ptr<FVGG> fvGridGeometry;
    std::shared_ptr<GridVars> gridVariables;
    std::shared_ptr<Problem> problem;
    Dumux::GetPropType<TypeTag, Dumux::Properties::SolutionVector> solution;
};

template<class TypeTag>
ReferenceSolutionStorage<TypeTag> solveReference(const std::string& gridParamGroup = "")
{
    using namespace Dumux;

    // try to create a grid (from the given grid file or the input file)
    ReferenceSolutionStorage<TypeTag> result;
    auto& gridManager = result.gridManager;
    gridManager.init(gridParamGroup);

    // grid data for domain markers
    auto gridData = gridManager.getGridData();

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    auto fvGridGeometry = std::make_shared<FVGridGeometry>(leafGridView);
    fvGridGeometry->update();

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using SpatialParams = typename Problem::SpatialParams;
    auto spatialParams = std::make_shared<SpatialParams>(fvGridGeometry, gridData);
    auto problem = std::make_shared<Problem>(fvGridGeometry, spatialParams);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x(fvGridGeometry->numDofs());
    problem->applyInitialSolution(x);
    auto xOld = x;

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, fvGridGeometry);
    gridVariables->init(x);

    // get some time loop parameters
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // instantiate time loop
    auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(0.0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // intialize the vtk output module
    using VtkOutputFields = GetPropType<TypeTag, Properties::VtkOutputFields>;

    // use non-conforming output for the test with interface solver
    VtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, getParamFromGroup<std::string>("Reference", "VtkFile"));
    VtkOutputFields::initOutputModule(vtkWriter); //!< Add model specific output fields
    vtkWriter.write(0.0);

    // the assembler with time loop for instationary problem
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, fvGridGeometry, gridVariables, timeLoop);

    // the linear solver
    using LinearSolver = AMGBackend<TypeTag>;
    auto linearSolver = std::make_shared<LinearSolver>(leafGridView, fvGridGeometry->dofMapper());

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    // use checkpoints for output
    timeLoop->setPeriodicCheckPoint(maxDt);

    // output file for masses in the different layers
    std::ofstream file;
    file.open(problem->name() + ".log", std::ios::out);

    // time loop
    timeLoop->start(); do
    {
        // set previous solution for storage evaluations
        assembler->setPreviousSolution(xOld);

        // solve the non-linear system with time step control
        nonLinearSolver.solve(x, *timeLoop);

        // make the new solution the old solution
        xOld = x;
        gridVariables->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // add mass distributions etc to output file
        problem->writeOutput(file, *gridVariables, x, timeLoop->time());

        // write vtk output
        if (timeLoop->isCheckPoint())
            vtkWriter.write(timeLoop->time());

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by the Newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

    } while (!timeLoop->finished());

    // finalize
    nonLinearSolver.report();
    timeLoop->finalize(leafGridView.comm());
    file.close();

    result.fvGridGeometry = fvGridGeometry;
    result.gridVariables = gridVariables;
    result.problem = problem;
    result.solution = x;

    return result;
}

#endif
