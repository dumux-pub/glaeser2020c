// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 * \brief Problem to the reference solution of the two-phase
 *        crossing fracture test case.
 */
#ifndef DUMUX_TWOP_CROSS_REFERENCE_PROBLEM_HH
#define DUMUX_TWOP_CROSS_REFERENCE_PROBLEM_HH

#include <dune/alugrid/grid.hh>

#include <dumux/discretization/ccmpfa.hh>

#include <dumux/material/components/trichloroethene.hh>
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/2pimmiscible.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p/model.hh>
#include <dumux/porousmediumflow/2p/incompressiblelocalresidual.hh>

#include "spatialparams.hh"

namespace Dumux {
// forward declarations
template<class TypeTag> class TwoPTestProblem;

namespace Properties {
namespace TTag {

struct TwoPFractureMpfa { using InheritsFrom = std::tuple<TwoP, CCMpfaModel>; };

} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TwoPFractureMpfa> { using type = Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>; };
// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::TwoPFractureMpfa> { using type = TwoPTestProblem<TypeTag>; };
// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TwoPFractureMpfa>
{
    using type = TwoPTestSpatialParams< GetPropType<TypeTag, Properties::FVGridGeometry>,
                                        GetPropType<TypeTag, Properties::Scalar> >;
};

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPFractureMpfa>
{
    using type = FluidSystems::TwoPImmiscible< GetPropType<TypeTag, Properties::Scalar>,
                                               FluidSystems::OnePLiquid<GetPropType<TypeTag, Properties::Scalar>, Components::SimpleH2O<GetPropType<TypeTag, Properties::Scalar>>>,
                                               FluidSystems::OnePLiquid<GetPropType<TypeTag, Properties::Scalar>, Components::Trichloroethene<GetPropType<TypeTag, Properties::Scalar>>> >;
};

// Enable caching
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::TwoPFractureMpfa>
{ static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::TwoPFractureMpfa>
{ static constexpr bool value = true; };
template<class TypeTag>
struct EnableFVGridGeometryCache<TypeTag, TTag::TwoPFractureMpfa>
{ static constexpr bool value = false; };
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::TwoPFractureMpfa>
{ static constexpr bool value = false; };
} // end namespace Properties

/*!
 * \brief Problem to the reference solution of the two-phase
 *        crossing fracture test case.
 */
template<class TypeTag>
class TwoPTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum {
        pressureH2OIdx = Indices::pressureIdx,
        saturationDNAPLIdx = Indices::saturationIdx,
        contiDNAPLEqIdx = Indices::conti0EqIdx + FluidSystem::comp1Idx,
        waterPhaseIdx = FluidSystem::phase0Idx,
        dnaplPhaseIdx = FluidSystem::phase1Idx
    };

public:

    //! pull up spatial parameters type
    using typename ParentType::SpatialParams;

    //! The constructor
    TwoPTestProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                    std::shared_ptr<SpatialParams> spatialParams,
                    const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , deltaP_(getParamFromGroup<Scalar>(paramGroup, "Problem.DeltaP"))
    , boundarySat_(getParamFromGroup<Scalar>(paramGroup, "Problem.BoundarySaturation"))
    {
        // initialize the fluid system, i.e. the tabulation
        // of water properties. Use the default p/T ranges.
        FluidSystem::init();
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param values Stores the value of the boundary type
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        if (isOnInlet(globalPos) || isOnOutlet(globalPos))
            values.setAllDirichlet();
        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet
     *        boundary segment
     *
     * \param values Stores the Dirichlet values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variable} ] \f$
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        if (isOnInlet(globalPos))
            return initialAtPos(globalPos) + PrimaryVariables({deltaP_, boundarySat_});
        return initialAtPos(globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values Stores the Neumann values for the conservation equations in
     *               \f$ [ \textnormal{unit of conserved quantity} / (m^(dim-1) \cdot s )] \f$
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    { return NumEqVector(0.0); }

    /*!
     * \brief Evaluates the initial values for a control volume
     *
     * \param values Stores the initial values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variables} ] \f$
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        values[pressureH2OIdx] = 1e5;
        values[saturationDNAPLIdx] = 0.0;
        return values;
    }

    /*!
     * \brief Returns the temperature \f$\mathrm{[K]}\f$ for an isothermal problem.
     *
     * This is not specific to the discretization. By default it just
     * throws an exception so it must be overloaded by the problem if
     * no energy equation is used.
     */
    Scalar temperature() const
    { return 283.15; /*10°C*/ }

    //! returns true if position is on inlet
    bool isOnInlet(const GlobalPosition& globalPos) const
    { return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + 1e-8 && globalPos[1] < -0.25 + 1e-8; }

    //! returns true if position is on outlet
    bool isOnOutlet(const GlobalPosition& globalPos) const
    { return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - 1e-8 && globalPos[1] > 0.25 - 1e-8; }

    //! write the mass distributions for a time step
    template<class Outputfile, class GridVariables, class SolutionVector>
    void writeOutput(Outputfile& outputFile,
                     const GridVariables& gridVariables,
                     const SolutionVector& x,
                     Scalar time) const
    {
        Scalar massMatrix = 0.0;
        Scalar massFracture = 0.0;

        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            auto elemVolVars = localView(gridVariables.curGridVolVars());

            fvGeometry.bindElement(element);
            elemVolVars.bindElement(element, fvGeometry, x);

            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& vv = elemVolVars[scv];
                const auto scvMass = vv.saturation(dnaplPhaseIdx)*vv.density(dnaplPhaseIdx)*vv.porosity()*scv.volume();

                if (this->spatialParams().isInFracture(element))
                    massFracture += scvMass;
                else
                    massMatrix += scvMass;
            }
        }

        outputFile << time << "," << massMatrix << "," << massFracture << std::endl;
    }

private:
    Scalar deltaP_;
    Scalar boundarySat_;
};

} // end namespace Dumux

#endif
