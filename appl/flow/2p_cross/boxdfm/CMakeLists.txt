dune_add_test(NAME test_2p_cross_boxdfm
             SOURCES test_2p_cross_boxdfm.cc
             CMAKE_GUARD dune-foamgrid_FOUND
             CMAKE_GUARD dune-alugrid_FOUND
             COMPILE_DEFINITIONS FRACTUREGRIDTYPE=Dune::FoamGrid<1,2>)

set(CMAKE_BUILD_TYPE Release)
