// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \brief Problem to the boxdfm solution of the two-phase
 *        crossing fracture test case.
 */
#ifndef DUMUX_TWOP_CROSS_BOXDFM_PROBLEM_HH
#define DUMUX_TWOP_CROSS_BOXDFM_PROBLEM_HH

#include <iostream>

#include <dune/alugrid/grid.hh>
#include <dune/grid/uggrid.hh>

#include <dumux/material/components/trichloroethene.hh>
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/2pimmiscible.hh>

#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/evalgradients.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p/model.hh>
#include <dumux/porousmediumflow/boxdfm/model.hh>
#include <dumux/porousmediumflow/2p/incompressiblelocalresidual.hh>

#include "spatialparams.hh"

namespace Dumux {

// forward declarations
template<class TypeTag> class TwoPBoxDfmTestProblem;

namespace Properties {
namespace TTag {
struct TwoPDfm { using InheritsFrom = std::tuple<BoxDfmModel, TwoP>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TwoPDfm> { using type = Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>; };
// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::TwoPDfm> { using type = TwoPBoxDfmTestProblem<TypeTag>; };
// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TwoPDfm>
{
    using type = TwoPBoxDfmTestSpatialParams< GetPropType<TypeTag, Properties::FVGridGeometry>,
                                              GetPropType<TypeTag, Properties::Scalar> >;
};

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPDfm>
{
    using type = FluidSystems::TwoPImmiscible< GetPropType<TypeTag, Properties::Scalar>,
                                               FluidSystems::OnePLiquid<GetPropType<TypeTag, Properties::Scalar>, Components::SimpleH2O<GetPropType<TypeTag, Properties::Scalar>>>,
                                               FluidSystems::OnePLiquid<GetPropType<TypeTag, Properties::Scalar>, Components::Trichloroethene<GetPropType<TypeTag, Properties::Scalar>>> >;
};

// Disable caching
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::TwoPDfm>
{ static constexpr bool value = false; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::TwoPDfm>
{ static constexpr bool value = false; };
template<class TypeTag>
struct EnableFVGridGeometryCache<TypeTag, TTag::TwoPDfm>
{ static constexpr bool value = false; };

// Enable the box-interface solver
template<class TypeTag>
struct EnableBoxInterfaceSolver<TypeTag, TTag::TwoPDfm>
{ static constexpr bool value = true; };
} // end namespace Properties

/*!
 * \brief Problem to the boxdfm solution of the two-phase
 *        crossing fracture test case.
 */
template<class TypeTag>
class TwoPBoxDfmTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;

    // some indices for convenience
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum
    {
        pressureH2OIdx = Indices::pressureIdx,
        saturationDNAPLIdx = Indices::saturationIdx,
        contiDNAPLEqIdx = Indices::conti0EqIdx + FluidSystem::comp1Idx,
        waterPhaseIdx = FluidSystem::phase0Idx,
        dnaplPhaseIdx = FluidSystem::phase1Idx
    };

    static constexpr Scalar eps_ = 1e-6;
    static constexpr int dimWorld = GridView::dimensionworld;

public:

    //! The constructor
    TwoPBoxDfmTestProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                          const std::string& paramGroup = "")
    : ParentType(fvGridGeometry)
    , deltaP_(getParamFromGroup<Scalar>(paramGroup, "Problem.DeltaP"))
    , boundarySat_(getParamFromGroup<Scalar>(paramGroup, "Problem.BoundarySaturation"))
    {
        // initialize the fluid system, i.e. the tabulation
        // of water properties. Use the default p/T ranges.
        FluidSystem::init();
    }

    /*!
     * \brief Return how much the domain is extruded at a given sub-control volume.
     *
     * Here, we extrude the fracture scvs by half the aperture
     */
    template<class ElementSolution>
    Scalar extrusionFactor(const Element& element,
                           const SubControlVolume& scv,
                           const ElementSolution& elemSol) const
    {
        // In the box-scheme, we compute fluxes etc element-wise,
        // thus per element we compute only half a fracture !!!
        static const Scalar aHalf = getParam<Scalar>("Fracture.SpatialParams.Aperture")/2.0;
        if (scv.isOnFracture())
            return aHalf;
        return 1.0;
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param values Stores the value of the boundary type
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet
     *        boundary segment
     * \param globalPos The global position
     */
     PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
     {
         if (isOnInlet(globalPos))
             return initialAtPos(globalPos) + PrimaryVariables({deltaP_, boundarySat_});
         return initialAtPos(globalPos);
     }

    //! write the mass distributions for a time step
    template<class Outputfile, class GridVariables, class SolutionVector>
    void writeMassDistributions(Outputfile& outputFile,
                                const GridVariables& gridVariables,
                                const SolutionVector& x,
                                Scalar time) const
    {
        Scalar massMatrix = 0.0;
        Scalar massFracture = 0.0;

        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            auto elemVolVars = localView(gridVariables.curGridVolVars());

            fvGeometry.bindElement(element);
            elemVolVars.bindElement(element, fvGeometry, x);

            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& vv = elemVolVars[scv];
                const auto scvMass = vv.saturation(dnaplPhaseIdx)*vv.density(dnaplPhaseIdx)*vv.porosity()*scv.volume();

                if (scv.isOnFracture())
                    massFracture += scvMass*vv.extrusionFactor();
                else
                    massMatrix += scvMass;
            }
        }

        outputFile << time << "," << massMatrix << "," << massFracture << std::endl;
    }

    //! evaluates the Neumann boundary condition for a given position
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        const auto globalPos = scvf.ipGlobal();

        // incorporate Dirichlet Bcs weakly
        if ( isOnOutlet(globalPos) || isOnInlet(globalPos) )
        {
            // modify element solution to carry inlet/outlet head
            auto elemSol = elementSolution(element, elemVolVars, fvGeometry);
            for (const auto& curScvf : scvfs(fvGeometry))
            {
                if (curScvf.boundary())
                {
                    if (isOnOutlet(curScvf.ipGlobal()) || isOnInlet(curScvf.ipGlobal()))
                    {
                        const auto diriValues = dirichletAtPos(curScvf.ipGlobal());
                        const auto& insideScv = fvGeometry.scv(curScvf.insideScvIdx());
                        elemSol[insideScv.localDofIndex()] = diriValues;
                    }
                }
            }

            // evaluate gradients using this element solution
            auto gradHead = evalGradients(element,
                                          element.geometry(),
                                          fvGeometry.fvGridGeometry(),
                                          elemSol,
                                          scvf.ipGlobal())[0];

            // compute the flux
            const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
            const auto& volVars = elemVolVars[insideScv];
            Scalar flux_0 = gradHead * scvf.unitOuterNormal();
            flux_0 *= -1.0*volVars.permeability();
            flux_0 *= volVars.density(FluidSystem::phase0Idx);

            // evaluate mobility depending on flux sign
            using SpatialParams = typename ParentType::SpatialParams;
            using MaterialLaw = typename SpatialParams::MaterialLaw;
            const auto& matParams = this->spatialParams().materialLawParams(element, insideScv, elemSol);

            if (isOnInlet(globalPos))
                flux_0 *= MaterialLaw::krw(matParams, (1.0 - elemSol[insideScv.localDofIndex()][1]))
                          /volVars.viscosity(FluidSystem::phase0Idx);
            else
                flux_0 *= volVars.mobility(FluidSystem::phase0Idx);

            // add capillary pressure to evaluate non-wetting phase gradients
            for (const auto& scv : scvs(fvGeometry))
                elemSol[scv.localDofIndex()][0] += MaterialLaw::pc(matParams, (1.0 - elemSol[scv.localDofIndex()][1]));

            gradHead = evalGradients(element,
                                     element.geometry(),
                                     fvGeometry.fvGridGeometry(),
                                     elemSol,
                                     scvf.ipGlobal())[0];

            Scalar flux_1 = gradHead * scvf.unitOuterNormal();
            flux_1 *= -1.0 *volVars.permeability();
            flux_1 *= volVars.density(FluidSystem::phase1Idx);

            if (isOnInlet(globalPos))
                flux_1 *= MaterialLaw::krn(matParams, (1.0 - elemSol[insideScv.localDofIndex()][1]))
                          /volVars.viscosity(FluidSystem::phase1Idx);
            else
                flux_1 *= volVars.mobility(FluidSystem::phase1Idx);

            return NumEqVector({flux_0, flux_1});
        }

        return NumEqVector(0.0);
    }

    /*!
     * \brief Evaluates the initial values for a control volume
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;
        values[pressureH2OIdx] = 1e5;
        values[saturationDNAPLIdx] = 0.0;
        return values;
    }

    //! Returns the temperature \f$\mathrm{[K]}\f$ for an isothermal problem.
    Scalar temperature() const
    { return 293.15; /* 10°C */ }

    //! returns true if position is on inlet
    bool isOnInlet(const GlobalPosition& globalPos) const
    { return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + 1e-8 && globalPos[1] < -0.25 + 1e-8; }

    //! returns true if position is on outlet
    bool isOnOutlet(const GlobalPosition& globalPos) const
    { return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - 1e-8 && globalPos[1] > 0.25 - 1e-8; }


private:
    Scalar deltaP_;
    Scalar boundarySat_;
};

} // end namespace Dumux

#endif
