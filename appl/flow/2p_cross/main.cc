// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Two-phase crossing fractures test
 */
#include <config.h>
#include <iostream>

#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/subgrid/subgrid.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/istl/bvector.hh>

#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/dumuxmessage.hh>

#include "reference/solver.hh"
#include "facet/solver.hh"
#include "boxdfm/solver.hh"

#include <dumux/io/vtk/vtkreader.hh>
#include <dumux/io/grid/gridmanager_sub.hh>

#include <dumux/discretization/functionspacebasis.hh>
#include <dumux/common/integrate.hh>
#include <dumux/multidomain/glue.hh>
#include <dumux/discretization/projection/projector.hh>

// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // initialize parameter tree
    Parameters::init(argc, argv);

    // the type tag used for the reference solution
    using RefTypeTag = Dumux::Properties::TTag::TwoPFractureMpfa;
    using ReferenceGrid = GetPropType<RefTypeTag, Properties::Grid>;
    using ReferenceGridManager = Dumux::GridManager<ReferenceGrid>;

    using RefScalar = GetPropType<RefTypeTag, Properties::Scalar>;
    using RefBlockType = Dune::FieldVector<RefScalar, 1>;
    using RefSolution = Dune::BlockVector<RefBlockType>;

    using SubGrid = Dune::SubGrid<ReferenceGrid::dimension, ReferenceGrid>;
    using SubGridManager = Dumux::GridManager<SubGrid>;
    using SubFVGridGeometry = Dumux::CCTpfaFVGridGeometry<typename SubGrid::LeafGridView>;

    // compute and parse reference sol + geometry
    ReferenceGridManager refGridManager;
    SubGridManager subGridManagerMatrix, subGridManagerFracture;
    RefSolution matrixPressure, matrixSaturation, fracturePressure, fractureSaturation;
    std::shared_ptr<SubFVGridGeometry> matrixFvGridGeometry, fractureFvGridGeometry;

    // run reference
    const std::string problemNameBody = getParam<std::string>("Problem.Name");
    auto assignRefName = [&problemNameBody] (auto& paramTree)
                           {
                               paramTree["Problem.Name"] = problemNameBody + "_ref";
                           };
    Parameters::init(assignRefName);
    {
        // run simulation for reference solution
        auto refData = solveReference<RefTypeTag>("Reference");

        // create sub-grids for matrix & fracture
        const auto& refProblem = *refData.problem;

        auto matrixElemSelector = [&refProblem] (const auto& element) -> bool
        { return !refProblem.spatialParams().isInFracture(element); };

        auto fractureElemSelector = [&refProblem] (const auto& element) -> bool
        { return refProblem.spatialParams().isInFracture(element); };

        std::cout << "\nMaking sub-grids" << std::endl;
        refGridManager = refData.gridManager;
        auto& refGrid = refGridManager.grid();
        subGridManagerMatrix.init(refGrid, matrixElemSelector);
        subGridManagerFracture.init(refGrid, fractureElemSelector);

        const auto& matrixGridView = subGridManagerMatrix.grid().leafGridView();
        const auto& fractureGridView = subGridManagerFracture.grid().leafGridView();

        // project solution onto the sub-grids
        std::cout << "Making sub-grid geometries" << std::endl;
        matrixFvGridGeometry = std::make_shared<SubFVGridGeometry>(matrixGridView);
        fractureFvGridGeometry = std::make_shared<SubFVGridGeometry>(fractureGridView);

        matrixFvGridGeometry->update();
        fractureFvGridGeometry->update();

        std::cout << "Making projectors" << std::endl;
        const auto matrixGlue = makeGlue(*refData.fvGridGeometry, *matrixFvGridGeometry);
        const auto fractureGlue = makeGlue(*refData.fvGridGeometry, *fractureFvGridGeometry);

        const auto& refBasis = getFunctionSpaceBasis(*refData.fvGridGeometry);
        const auto& matrixBasis = getFunctionSpaceBasis(*matrixFvGridGeometry);
        const auto& fractureBasis = getFunctionSpaceBasis(*fractureFvGridGeometry);

        const auto matrixProjector = Dumux::makeProjector(refBasis, matrixBasis, matrixGlue);
        const auto fractureProjector = Dumux::makeProjector(refBasis, fractureBasis, fractureGlue);

        auto params = matrixProjector.defaultParams();
        params.residualReduction = 1e-16;

        const auto matrixSolution = matrixProjector.project(refData.solution, params);
        const auto fractureSolution = fractureProjector.project(refData.solution, params);

        matrixPressure.resize(matrixSolution.size()); matrixSaturation.resize(matrixSolution.size());
        fracturePressure.resize(fractureSolution.size()); fractureSaturation.resize(fractureSolution.size());

        for (std::size_t i = 0; i < matrixSolution.size(); ++i)
        { matrixPressure[i] = matrixSolution[i][0]; matrixSaturation[i] = matrixSolution[i][1]; }

        for (std::size_t i = 0; i < fractureSolution.size(); ++i)
        { fracturePressure[i] = fractureSolution[i][0]; fractureSaturation[i] = fractureSolution[i][1]; }

        std::cout << "Finished projection" << std::endl;
    }

    const auto& matrixBasis = getFunctionSpaceBasis(*matrixFvGridGeometry);
    const auto& fractureBasis = getFunctionSpaceBasis(*fractureFvGridGeometry);

    // Make grid functions for pressure in both matrix & fracture
    using namespace Dune::Functions;
    auto gfMatrixPressure = makeDiscreteGlobalBasisFunction<RefBlockType>(matrixBasis, matrixPressure);
    auto gfMatrixSaturation = makeDiscreteGlobalBasisFunction<RefBlockType>(matrixBasis, matrixSaturation);

    // average fracture solution
    using AverageGrid = Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>;
    using AverageGridGeometry = Dumux::CCTpfaFVGridGeometry<typename AverageGrid::LeafGridView>;
    Dumux::GridManager<AverageGrid> avgGridManager;
    avgGridManager.init("FractureAverage");

    auto& avgGrid = avgGridManager.grid();
    AverageGridGeometry avgGridGeometry(avgGrid.leafGridView());
    const auto& avgFractureBasis  = getFunctionSpaceBasis(avgGridGeometry);

    std::cout << "Computing averaged fracture solution" << std::endl;
    const auto avgGlue = makeGlue(*fractureFvGridGeometry, avgGridGeometry);
    const auto avgProjector = Dumux::makeProjector(fractureBasis, avgFractureBasis, avgGlue);
    auto params = avgProjector.defaultParams();
    params.residualReduction = 1e-16;

    const auto avgFracPressure = avgProjector.project(fracturePressure, params);
    const auto avfFracSaturation = avgProjector.project(fractureSaturation, params);
    auto gfFracAvgPressure = makeDiscreteGlobalBasisFunction<RefBlockType>(avgFractureBasis, avgFracPressure);
    auto gfFracAvgSaturation = makeDiscreteGlobalBasisFunction<RefBlockType>(avgFractureBasis, avfFracSaturation);

    // write out (averaged) fracture solution and matrix solution separately
    Dune::VTKWriter< std::decay_t<decltype(avgFractureBasis.gridView())> > avgFracSolWriter(avgFractureBasis.gridView());
    avgFracSolWriter.addCellData(avgFracPressure, "pw_avg");
    avgFracSolWriter.addCellData(avfFracSaturation, "Sn_avg");
    avgFracSolWriter.write(getParamFromGroup<std::string>("Reference", "VtkFile") + "_fracture_average");

    Dune::VTKWriter< std::decay_t<decltype(matrixBasis.gridView())> > matrixSolWriter(matrixBasis.gridView());
    matrixSolWriter.addCellData(matrixPressure, "pw");
    matrixSolWriter.addCellData(matrixSaturation, "Sn");
    matrixSolWriter.write(getParamFromGroup<std::string>("Reference", "VtkFile") + "_matrix");

    // integrate matrix function to define relative errors later
    const int intOrder = getParam<int>("L2Norm.IntegrationOrder");
    const auto aperture = getParam<double>("Fracture.SpatialParams.Aperture");
    const auto integralMatrixPressure = Dumux::integrateGridFunction(matrixBasis.gridView(), gfMatrixPressure, intOrder);
    const auto integralMatrixSaturation = Dumux::integrateGridFunction(matrixBasis.gridView(), gfMatrixSaturation, intOrder);
    const auto integralFracturePressure = Dumux::integrateGridFunction(avgFractureBasis.gridView(), gfFracAvgPressure, intOrder)/aperture;
    const auto integralFractureSaturation = Dumux::integrateGridFunction(avgFractureBasis.gridView(), gfFracAvgSaturation, intOrder)/aperture;

    std::cout << "Integral matrix (pw / Sn): " << integralMatrixPressure << " / " << integralMatrixSaturation << std::endl;
    std::cout << "Integral fracture (pw / Sn): " << integralFracturePressure << " / " << integralFractureSaturation << std::endl;

    ////////////////////////////
    // Compute L2 error norms //
    ////////////////////////////
    const auto l2FileBody = getParam<std::string>("L2Norm.OutputFile");
    std::ofstream l2ErrorsMatrixPressure(l2FileBody + "_matrix_p.txt", std::ios::out);
    std::ofstream l2ErrorsMatrixSaturation(l2FileBody + "_matrix_s.txt", std::ios::out);
    std::ofstream l2ErrorsFracturePressure(l2FileBody + "_fracture_p.txt", std::ios::out);
    std::ofstream l2ErrorsFractureSaturation(l2FileBody + "_fracture_s.txt", std::ios::out);

    // run tpfa
    int refinementCount = 0;
    auto assignTpfaNames = [&problemNameBody] (auto& paramTree)
                           {
                               paramTree["Problem.Name"] = problemNameBody + "_tpfa";
                               paramTree["Bulk.Problem.Name"] = problemNameBody + "_bulk_tpfa";
                               paramTree["LowDim.Problem.Name"] = problemNameBody + "_lowdim_tpfa";
                           };
    Parameters::init(assignTpfaNames);
    while (hasParam("Refinement" + std::to_string(refinementCount) + ".Grid.File"))
    {
        std::cout << "\n\n"
                  << "################################\n"
                  << "# Solving Tpfa for refinement " << refinementCount << "#\n\n";

        const std::string gridParamGroup = "Refinement" + std::to_string(refinementCount);

        using namespace Dumux::Properties::TTag;
        const auto storage = solveFacet<MatrixProblemTpfaTypeTag, FractureProblemTpfaTypeTag>(gridParamGroup, refinementCount);

        // intersect this and the reference grid
        std::cout << "\nIntersecting the grids" << std::endl;
        const auto bulkGlue = makeGlue(*matrixFvGridGeometry, *storage.bulkFvGridGeometry);
        std::cout << "\nIntersecting the grids" << std::endl;
        const auto lowDimGlue = makeGlue(*fractureFvGridGeometry, *storage.lowDimFvGridGeometry);

        const auto& bulkBasis = getFunctionSpaceBasis(*storage.bulkFvGridGeometry);
        const auto& lowDimBasis = getFunctionSpaceBasis(*storage.lowDimFvGridGeometry);

        std::cout << "\nProjecting reference solutions" << std::endl;
        // we project the bulk solution into the matrix solution space
        // and the fracture reference solution onto the low dim solution space
        const auto bulkProjector = Dumux::makeProjectorPair(matrixBasis, bulkBasis, bulkGlue).second;
        const auto lowDimProjector = Dumux::makeProjectorPair(fractureBasis, lowDimBasis, lowDimGlue).first;

        auto params = bulkProjector.defaultParams();
        params.residualReduction = 1e-16;

        // extract pressure/saturation solutions
        RefSolution bulkPressure(storage.bulkSolution.size());
        RefSolution bulkSaturation(storage.bulkSolution.size());
        RefSolution lowDimPressure(storage.lowDimSolution.size());
        RefSolution lowDimSaturation(storage.lowDimSolution.size());

        for (std::size_t i = 0; i < storage.bulkSolution.size(); ++i)
        { bulkPressure[i] = storage.bulkSolution[i][0]; bulkSaturation[i] = storage.bulkSolution[i][1]; }

        for (std::size_t i = 0; i < storage.lowDimSolution.size(); ++i)
        { lowDimPressure[i] = storage.lowDimSolution[i][0]; lowDimSaturation[i] = storage.lowDimSolution[i][1]; }

        auto projectedBulkPressureSol = bulkProjector.project(bulkPressure, params);
        auto projectedBulkSaturationSol = bulkProjector.project(bulkSaturation, params);
        auto projectedFracturePressureSol = lowDimProjector.project(fracturePressure, params);
        auto projectedFractureSaturationSol = lowDimProjector.project(fractureSaturation, params);

        std::cout << "\nComputing discrete l2 error norm" << std::endl;

        // grid function for bulk solution on matrix grid
        auto gfBulkMatrixPressure = makeDiscreteGlobalBasisFunction<RefBlockType>(matrixBasis, projectedBulkPressureSol);
        auto gfBulkMatrixSaturation = makeDiscreteGlobalBasisFunction<RefBlockType>(matrixBasis, projectedBulkSaturationSol);
        // grid function for ref fracture sol on low dim Grid
        auto gfFractureLowDimPressure = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, projectedFracturePressureSol);
        auto gfFractureLowDimSaturation = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, projectedFractureSaturationSol);
        // grid function for solution on low dim grid
        auto gfLowDimPressure = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, lowDimPressure);
        auto gfLowDimSaturation = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, lowDimSaturation);

        // compute l2 norms
        auto matrixL2normPressure = Dumux::integrateL2Error(matrixBasis.gridView(), gfMatrixPressure, gfBulkMatrixPressure, intOrder);
        auto matrixL2normSaturation = Dumux::integrateL2Error(matrixBasis.gridView(), gfMatrixSaturation, gfBulkMatrixSaturation, intOrder);
        auto fractureL2normPressure = Dumux::integrateL2Error(lowDimBasis.gridView(), gfLowDimPressure, gfFractureLowDimPressure, intOrder);
        auto fractureL2normSaturation = Dumux::integrateL2Error(lowDimBasis.gridView(), gfLowDimSaturation, gfFractureLowDimSaturation, intOrder);
        std::cout << "\t -> matrix: " << matrixL2normPressure << ", " << matrixL2normSaturation << "\n"
                  << "\t -> fracture: " << fractureL2normPressure << ", " << fractureL2normSaturation << std::endl;

        // write projection of bulk solution in reference grid
        Dune::VTKWriter< std::decay_t<decltype(matrixBasis.gridView())> > matrixProjectionWriter(matrixBasis.gridView());
        matrixProjectionWriter.addCellData(gfBulkMatrixPressure, Dune::VTK::FieldInfo("p_proj", Dune::VTK::FieldInfo::Type::scalar, 1));
        matrixProjectionWriter.addCellData(gfBulkMatrixSaturation, Dune::VTK::FieldInfo("s_proj", Dune::VTK::FieldInfo::Type::scalar, 1));
        matrixProjectionWriter.write(getParam<std::string>("Bulk.Problem.Name") + "_projection" + "_" + std::to_string(refinementCount));

        // write projection of fracture solution in low dim grid
        Dune::VTKWriter< std::decay_t<decltype(lowDimBasis.gridView())> > fractureProjectionWriter(lowDimBasis.gridView());
        fractureProjectionWriter.addCellData(gfFractureLowDimPressure, Dune::VTK::FieldInfo("pRef_proj", Dune::VTK::FieldInfo::Type::scalar, 1));
        fractureProjectionWriter.addCellData(gfFractureLowDimSaturation, Dune::VTK::FieldInfo("sRef_proj", Dune::VTK::FieldInfo::Type::scalar, 1));
        fractureProjectionWriter.write(getParam<std::string>("LowDim.Problem.Name") + "_projection" + "_" + std::to_string(refinementCount));

        if (refinementCount > 0) { l2ErrorsMatrixPressure << ","; l2ErrorsMatrixSaturation << ","; l2ErrorsFracturePressure << ","; l2ErrorsFractureSaturation << ","; }
        l2ErrorsMatrixPressure << matrixL2normPressure/integralMatrixPressure;
        l2ErrorsMatrixSaturation << matrixL2normSaturation/integralMatrixSaturation;
        l2ErrorsFracturePressure << fractureL2normPressure/integralFracturePressure;
        l2ErrorsFractureSaturation << fractureL2normSaturation/integralFractureSaturation;

        refinementCount++;
    }

    // go to next line in error norm file
    l2ErrorsMatrixPressure << std::endl; l2ErrorsMatrixSaturation << std::endl;
    l2ErrorsFracturePressure << std::endl; l2ErrorsFractureSaturation << std::endl;

    // run mpfa
    refinementCount = 0;
    auto assignMpfaNames = [&problemNameBody] (auto& paramTree)
                           {
                               paramTree["Problem.Name"] = problemNameBody + "_mpfa";
                               paramTree["Bulk.Problem.Name"] = problemNameBody + "_bulk_mpfa";
                               paramTree["LowDim.Problem.Name"] = problemNameBody + "_lowdim_mpfa";
                           };
    Parameters::init(assignMpfaNames);
    while (hasParam("Refinement" + std::to_string(refinementCount) + ".Grid.File"))
    {
        std::cout << "\n\n"
                  << "################################\n"
                  << "# Solving Mpfa for refinement " << refinementCount << "#\n\n";

        const std::string gridParamGroup = "Refinement" + std::to_string(refinementCount);

        using namespace Dumux::Properties::TTag;
        const auto storage = solveFacet<MatrixProblemMpfaTypeTag, FractureProblemMpfaTypeTag>(gridParamGroup, refinementCount);

        // intersect this and the reference grid
        std::cout << "\nIntersecting the grids" << std::endl;
        const auto bulkGlue = makeGlue(*matrixFvGridGeometry, *storage.bulkFvGridGeometry);
        std::cout << "\nIntersecting the grids" << std::endl;
        const auto lowDimGlue = makeGlue(*fractureFvGridGeometry, *storage.lowDimFvGridGeometry);

        const auto& bulkBasis = getFunctionSpaceBasis(*storage.bulkFvGridGeometry);
        const auto& lowDimBasis = getFunctionSpaceBasis(*storage.lowDimFvGridGeometry);

        std::cout << "\nProjecting reference solutions" << std::endl;
        // we project the bulk solution into the matrix solution space
        // and the fracture reference solution onto the low dim solution space
        const auto bulkProjector = Dumux::makeProjectorPair(matrixBasis, bulkBasis, bulkGlue).second;
        const auto lowDimProjector = Dumux::makeProjectorPair(fractureBasis, lowDimBasis, lowDimGlue).first;

        auto params = bulkProjector.defaultParams();
        params.residualReduction = 1e-16;

        // extract pressure/saturation solutions
        RefSolution bulkPressure(storage.bulkSolution.size());
        RefSolution bulkSaturation(storage.bulkSolution.size());
        RefSolution lowDimPressure(storage.lowDimSolution.size());
        RefSolution lowDimSaturation(storage.lowDimSolution.size());

        for (std::size_t i = 0; i < storage.bulkSolution.size(); ++i)
        { bulkPressure[i] = storage.bulkSolution[i][0]; bulkSaturation[i] = storage.bulkSolution[i][1]; }

        for (std::size_t i = 0; i < storage.lowDimSolution.size(); ++i)
        { lowDimPressure[i] = storage.lowDimSolution[i][0]; lowDimSaturation[i] = storage.lowDimSolution[i][1]; }

        auto projectedBulkPressureSol = bulkProjector.project(bulkPressure, params);
        auto projectedBulkSaturationSol = bulkProjector.project(bulkSaturation, params);
        auto projectedFracturePressureSol = lowDimProjector.project(fracturePressure, params);
        auto projectedFractureSaturationSol = lowDimProjector.project(fractureSaturation, params);

        std::cout << "\nComputing discrete l2 error norm" << std::endl;

        // grid function for bulk solution on matrix grid
        auto gfBulkMatrixPressure = makeDiscreteGlobalBasisFunction<RefBlockType>(matrixBasis, projectedBulkPressureSol);
        auto gfBulkMatrixSaturation = makeDiscreteGlobalBasisFunction<RefBlockType>(matrixBasis, projectedBulkSaturationSol);
        // grid function for ref fracture sol on low dim Grid
        auto gfFractureLowDimPressure = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, projectedFracturePressureSol);
        auto gfFractureLowDimSaturation = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, projectedFractureSaturationSol);
        // grid function for solution on low dim grid
        auto gfLowDimPressure = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, lowDimPressure);
        auto gfLowDimSaturation = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, lowDimSaturation);

        // compute l2 norms
        auto matrixL2normPressure = Dumux::integrateL2Error(matrixBasis.gridView(), gfMatrixPressure, gfBulkMatrixPressure, intOrder);
        auto matrixL2normSaturation = Dumux::integrateL2Error(matrixBasis.gridView(), gfMatrixSaturation, gfBulkMatrixSaturation, intOrder);
        auto fractureL2normPressure = Dumux::integrateL2Error(lowDimBasis.gridView(), gfLowDimPressure, gfFractureLowDimPressure, intOrder);
        auto fractureL2normSaturation = Dumux::integrateL2Error(lowDimBasis.gridView(), gfLowDimSaturation, gfFractureLowDimSaturation, intOrder);
        std::cout << "\t -> matrix: " << matrixL2normPressure << ", " << matrixL2normSaturation << "\n"
                  << "\t -> fracture: " << fractureL2normPressure << ", " << fractureL2normSaturation << std::endl;

        // write projection of bulk solution in reference grid
        Dune::VTKWriter< std::decay_t<decltype(matrixBasis.gridView())> > matrixProjectionWriter(matrixBasis.gridView());
        matrixProjectionWriter.addCellData(gfBulkMatrixPressure, Dune::VTK::FieldInfo("p_proj", Dune::VTK::FieldInfo::Type::scalar, 1));
        matrixProjectionWriter.addCellData(gfBulkMatrixSaturation, Dune::VTK::FieldInfo("s_proj", Dune::VTK::FieldInfo::Type::scalar, 1));
        matrixProjectionWriter.write(getParam<std::string>("Bulk.Problem.Name") + "_projection" + "_" + std::to_string(refinementCount));

        // write projection of fracture solution in low dim grid
        Dune::VTKWriter< std::decay_t<decltype(lowDimBasis.gridView())> > fractureProjectionWriter(lowDimBasis.gridView());
        fractureProjectionWriter.addCellData(gfFractureLowDimPressure, Dune::VTK::FieldInfo("pRef_proj", Dune::VTK::FieldInfo::Type::scalar, 1));
        fractureProjectionWriter.addCellData(gfFractureLowDimSaturation, Dune::VTK::FieldInfo("sRef_proj", Dune::VTK::FieldInfo::Type::scalar, 1));
        fractureProjectionWriter.write(getParam<std::string>("LowDim.Problem.Name") + "_projection" + "_" + std::to_string(refinementCount));

        if (refinementCount > 0) { l2ErrorsMatrixPressure << ","; l2ErrorsMatrixSaturation << ","; l2ErrorsFracturePressure << ","; l2ErrorsFractureSaturation << ","; }
        l2ErrorsMatrixPressure << matrixL2normPressure/integralMatrixPressure;
        l2ErrorsMatrixSaturation << matrixL2normSaturation/integralMatrixSaturation;
        l2ErrorsFracturePressure << fractureL2normPressure/integralFracturePressure;
        l2ErrorsFractureSaturation << fractureL2normSaturation/integralFractureSaturation;

        refinementCount++;
    }

    // go to next line in error norm file
    l2ErrorsMatrixPressure << std::endl; l2ErrorsMatrixSaturation << std::endl;
    l2ErrorsFracturePressure << std::endl; l2ErrorsFractureSaturation << std::endl;

    // run box
    refinementCount = 0;
    auto assignBoxNames = [&problemNameBody] (auto& paramTree)
                           {
                               paramTree["Problem.Name"] = problemNameBody + "_box";
                               paramTree["Bulk.Problem.Name"] = problemNameBody + "_bulk_box";
                               paramTree["LowDim.Problem.Name"] = problemNameBody + "_lowdim_box";
                           };
    Parameters::init(assignBoxNames);
    while (hasParam("Refinement" + std::to_string(refinementCount) + ".Grid.File"))
    {
        std::cout << "\n\n"
                  << "################################\n"
                  << "# Solving Box for refinement " << refinementCount << "##\n\n";

        const std::string gridParamGroup = "Refinement" + std::to_string(refinementCount);

        using namespace Dumux::Properties::TTag;
        const auto storage = solveFacet<MatrixProblemBoxTypeTag, FractureProblemBoxTypeTag>(gridParamGroup, refinementCount);

        // first, average solution in each element -> create DG0 space coefficients
        const auto& bulkFvGridGeometry = *storage.bulkFvGridGeometry;
        const auto& bulkGridView = bulkFvGridGeometry.gridView();

        RefSolution bulkPressureAveraged(bulkGridView.size(0));
        RefSolution bulkSaturationAveraged(bulkGridView.size(0));
        for (const auto& element : elements(bulkGridView))
        {
            const auto eg = element.geometry();
            const auto elemSol = elementSolution(element, storage.bulkSolution, bulkFvGridGeometry);

            Dune::FieldVector<double, 2> avgSol(0.0);
            for (auto&& qp : Dune::QuadratureRules<double, 2>::rule(eg.type(), 2))
            {
                auto tmp = evalSolution(element, eg, elemSol, eg.global(qp.position()));
                tmp *= qp.weight()*eg.integrationElement(qp.position());
                avgSol += tmp;
            }
            avgSol /= eg.volume();

            bulkPressureAveraged[bulkFvGridGeometry.elementMapper().index(element)] = avgSol[0];
            bulkSaturationAveraged[bulkFvGridGeometry.elementMapper().index(element)] = avgSol[1];
        }

        const auto& lowDimFvGridGeometry = *storage.lowDimFvGridGeometry;
        const auto& lowDimGridView = lowDimFvGridGeometry.gridView();

        RefSolution lowDimPressureAveraged(lowDimGridView.size(0));
        RefSolution lowDimSaturationAveraged(lowDimGridView.size(0));
        for (const auto& element : elements(lowDimGridView))
        {
            const auto eg = element.geometry();
            const auto elemSol = elementSolution(element, storage.lowDimSolution, lowDimFvGridGeometry);

            Dune::FieldVector<double, 2> avgSol(0.0);
            for (auto&& qp : Dune::QuadratureRules<double, 1>::rule(eg.type(), 2))
            {
                auto tmp = evalSolution(element, eg, elemSol, eg.global(qp.position()));
                tmp *= qp.weight()*eg.integrationElement(qp.position());
                avgSol += tmp;
            }
            avgSol /= eg.volume();

            lowDimPressureAveraged[lowDimFvGridGeometry.elementMapper().index(element)] = avgSol[0];
            lowDimSaturationAveraged[lowDimFvGridGeometry.elementMapper().index(element)] = avgSol[1];
        }

        // intersect this and the reference grid
        std::cout << "\nIntersecting the grids" << std::endl;
        const auto bulkGlue = makeGlue(*matrixFvGridGeometry, *storage.bulkFvGridGeometry);
        std::cout << "\nIntersecting the grids" << std::endl;
        const auto lowDimGlue = makeGlue(*fractureFvGridGeometry, *storage.lowDimFvGridGeometry);

        // create DG0 Basis for the two grids
        Dune::Functions::LagrangeBasis<std::decay_t<decltype(bulkGridView)>, 0> bulkBasis(bulkGridView);
        Dune::Functions::LagrangeBasis<std::decay_t<decltype(lowDimGridView)>, 0> lowDimBasis(lowDimGridView);

        std::cout << "\nProjecting reference solutions" << std::endl;
        // we project the bulk solution into the matrix solution space
        // and the fracture reference solution onto the low dim solution space
        const auto bulkProjector = Dumux::makeProjectorPair(matrixBasis, bulkBasis, bulkGlue).second;
        const auto lowDimProjector = Dumux::makeProjectorPair(fractureBasis, lowDimBasis, lowDimGlue).first;

        auto params = bulkProjector.defaultParams();
        params.residualReduction = 1e-16;

        auto projectedBulkPressureSol = bulkProjector.project(bulkPressureAveraged, params);
        auto projectedBulkSaturationSol = bulkProjector.project(bulkSaturationAveraged, params);
        auto projectedFracturePressureSol = lowDimProjector.project(fracturePressure, params);
        auto projectedFractureSaturationSol = lowDimProjector.project(fractureSaturation, params);

        std::cout << "\nComputing discrete l2 error norm" << std::endl;

        // grid function for bulk solution on matrix grid
        auto gfBulkMatrixPressure = makeDiscreteGlobalBasisFunction<RefBlockType>(matrixBasis, projectedBulkPressureSol);
        auto gfBulkMatrixSaturation = makeDiscreteGlobalBasisFunction<RefBlockType>(matrixBasis, projectedBulkSaturationSol);
        // grid function for ref fracture sol on low dim Grid
        auto gfFractureLowDimPressure = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, projectedFracturePressureSol);
        auto gfFractureLowDimSaturation = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, projectedFractureSaturationSol);
        // grid function for solution on low dim grid
        auto gfLowDimPressure = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, lowDimPressureAveraged);
        auto gfLowDimSaturation = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, lowDimSaturationAveraged);

        // compute l2 norms
        auto matrixL2normPressure = Dumux::integrateL2Error(matrixBasis.gridView(), gfMatrixPressure, gfBulkMatrixPressure, intOrder);
        auto matrixL2normSaturation = Dumux::integrateL2Error(matrixBasis.gridView(), gfMatrixSaturation, gfBulkMatrixSaturation, intOrder);
        auto fractureL2normPressure = Dumux::integrateL2Error(lowDimBasis.gridView(), gfLowDimPressure, gfFractureLowDimPressure, intOrder);
        auto fractureL2normSaturation = Dumux::integrateL2Error(lowDimBasis.gridView(), gfLowDimSaturation, gfFractureLowDimSaturation, intOrder);
        std::cout << "\t -> matrix: " << matrixL2normPressure << ", " << matrixL2normSaturation << "\n"
                  << "\t -> fracture: " << fractureL2normPressure << ", " << fractureL2normSaturation << std::endl;

        // write projection of bulk solution in reference grid
        Dune::VTKWriter< std::decay_t<decltype(matrixBasis.gridView())> > matrixProjectionWriter(matrixBasis.gridView());
        matrixProjectionWriter.addCellData(gfBulkMatrixPressure, Dune::VTK::FieldInfo("p_proj", Dune::VTK::FieldInfo::Type::scalar, 1));
        matrixProjectionWriter.addCellData(gfBulkMatrixSaturation, Dune::VTK::FieldInfo("s_proj", Dune::VTK::FieldInfo::Type::scalar, 1));
        matrixProjectionWriter.write(getParam<std::string>("Bulk.Problem.Name") + "_projection" + "_" + std::to_string(refinementCount));

        // write projection of fracture solution in low dim grid
        Dune::VTKWriter< std::decay_t<decltype(lowDimBasis.gridView())> > fractureProjectionWriter(lowDimBasis.gridView());
        fractureProjectionWriter.addCellData(gfFractureLowDimPressure, Dune::VTK::FieldInfo("pRef_proj", Dune::VTK::FieldInfo::Type::scalar, 1));
        fractureProjectionWriter.addCellData(gfFractureLowDimSaturation, Dune::VTK::FieldInfo("sRef_proj", Dune::VTK::FieldInfo::Type::scalar, 1));
        fractureProjectionWriter.write(getParam<std::string>("LowDim.Problem.Name") + "_projection" + "_" + std::to_string(refinementCount));

        if (refinementCount > 0) { l2ErrorsMatrixPressure << ","; l2ErrorsMatrixSaturation << ","; l2ErrorsFracturePressure << ","; l2ErrorsFractureSaturation << ","; }
        l2ErrorsMatrixPressure << matrixL2normPressure/integralMatrixPressure;
        l2ErrorsMatrixSaturation << matrixL2normSaturation/integralMatrixSaturation;
        l2ErrorsFracturePressure << fractureL2normPressure/integralFracturePressure;
        l2ErrorsFractureSaturation << fractureL2normSaturation/integralFractureSaturation;

        refinementCount++;
    }

    // go to next line in error norm file
    l2ErrorsMatrixPressure << std::endl; l2ErrorsMatrixSaturation << std::endl;
    l2ErrorsFracturePressure << std::endl; l2ErrorsFractureSaturation << std::endl;

    // run box-dfm
    refinementCount = 0;
    auto assignBoxDfmNames = [&problemNameBody] (auto& paramTree)
                           {
                               paramTree["Problem.Name"] = problemNameBody + "_boxdfm";
                               paramTree["Bulk.Problem.Name"] = problemNameBody + "_bulk_boxdfm";
                               paramTree["LowDim.Problem.Name"] = problemNameBody + "_lowdim_boxdfm";
                           };
    Parameters::init(assignBoxDfmNames);
    while (hasParam("Refinement" + std::to_string(refinementCount) + ".Grid.File"))
    {
        std::cout << "\n\n"
                  << "###################################\n"
                  << "# Solving BoxDfm for refinement " << refinementCount << "##\n\n";

        const std::string gridParamGroup = "Refinement" + std::to_string(refinementCount);

        using namespace Dumux::Properties::TTag;
        const auto storage = solveBoxDfm<TwoPDfm>(gridParamGroup, refinementCount);

        // first, average solution in each element -> create DG0 space coefficients
        const auto& bulkFvGridGeometry = *storage.fvGridGeometry;
        const auto& bulkGridView = bulkFvGridGeometry.gridView();

        RefSolution bulkPressureAveraged(bulkGridView.size(0));
        RefSolution bulkSaturationAveraged(bulkGridView.size(0));
        for (const auto& element : elements(bulkGridView))
        {
            const auto eg = element.geometry();
            const auto elemSol = elementSolution(element, storage.solution, bulkFvGridGeometry);

            Dune::FieldVector<double, 2> avgSol(0.0);
            for (auto&& qp : Dune::QuadratureRules<double, 2>::rule(eg.type(), 2))
            {
                auto tmp = evalSolution(element, eg, elemSol, eg.global(qp.position()));
                tmp *= qp.weight()*eg.integrationElement(qp.position());
                avgSol += tmp;
            }
            avgSol /= eg.volume();

            bulkPressureAveraged[bulkFvGridGeometry.elementMapper().index(element)] = avgSol[0];
            bulkSaturationAveraged[bulkFvGridGeometry.elementMapper().index(element)] = avgSol[1];
        }

        // read in fracture solution from vtk file
        const std::string vtpFile = getParam<std::string>("Problem.Name") + "_" + std::to_string(refinementCount) + "_fracture-00001.vtp";

        auto vtkReader = VTKReader(vtpFile);
        using FractureGrid = Dune::FoamGrid<1, 2>;
        using FractureGridView = typename FractureGrid::LeafGridView;

        VTKReader::Data cellData, pointData;
        Dune::GridFactory<FractureGrid> gridFactory;
        auto fractureGrid = vtkReader.readGrid(gridFactory, cellData, pointData, /*verbose=*/true);
        const auto& fractureGridView = fractureGrid->leafGridView();
        CCTpfaFVGridGeometry<FractureGridView> fractureGridGeometry(fractureGridView);

        // to write out we need to reorder as the simple addCellData interface expects MCMG mapper indices
        Dune::MultipleCodimMultipleGeomTypeMapper<FractureGrid::LeafGridView> elementMapper(fractureGridView, Dune::mcmgElementLayout());
        Dune::MultipleCodimMultipleGeomTypeMapper<FractureGrid::LeafGridView> vertexMapper(fractureGridView, Dune::mcmgVertexLayout());
        std::vector<std::size_t> vertexIndex(fractureGridView.size(FractureGrid::dimension));
        for (const auto& v : vertices(fractureGridView))
            vertexIndex[gridFactory.insertionIndex(v)] = vertexMapper.index(v);

        Dumux::VTKReader::Data reorderedPointData = pointData;
        for (const auto& data : pointData)
        {
            auto& reorderedData = reorderedPointData[data.first];
            for (unsigned int i = 0; i < data.second.size(); ++i)
                reorderedData[vertexIndex[i]] = data.second[i];
        }

        RefSolution lowDimPressureAveraged(fractureGridView.size(0));
        RefSolution lowDimSaturationAveraged(fractureGridView.size(0));
        for (const auto& element : elements(fractureGridView))
        {
            const auto eg = element.geometry();
            const auto elemSolP = elementSolution(element, reorderedPointData["p_aq"], fractureGridGeometry);
            const auto elemSolS = elementSolution(element, reorderedPointData["S_napl"], fractureGridGeometry);

            double avgSolP(0.0);
            double avgSolS(0.0);
            for (auto&& qp : Dune::QuadratureRules<double, 1>::rule(eg.type(), 2))
            {
                auto tmpP = evalSolution(element, eg, elemSolP, eg.global(qp.position()));
                auto tmpS = evalSolution(element, eg, elemSolS, eg.global(qp.position()));

                tmpP *= qp.weight()*eg.integrationElement(qp.position());
                tmpS *= qp.weight()*eg.integrationElement(qp.position());

                avgSolP += tmpP;
                avgSolS += tmpS;
            }

            avgSolP /= eg.volume();
            avgSolS /= eg.volume();

            lowDimPressureAveraged[fractureGridGeometry.elementMapper().index(element)] = avgSolP;
            lowDimSaturationAveraged[fractureGridGeometry.elementMapper().index(element)] = avgSolS;
        }

        // intersect this and the reference grid
        std::cout << "\nIntersecting the grids" << std::endl;
        const auto bulkGlue = makeGlue(*matrixFvGridGeometry, *storage.fvGridGeometry);
        std::cout << "\nIntersecting the grids" << std::endl;
        const auto lowDimGlue = makeGlue(*fractureFvGridGeometry, fractureGridGeometry);

        // create DG0 Basis for the two grids
        Dune::Functions::LagrangeBasis<std::decay_t<decltype(bulkGridView)>, 0> bulkBasis(bulkGridView);
        Dune::Functions::LagrangeBasis<std::decay_t<decltype(fractureGridView)>, 0> lowDimBasis(fractureGridView);

        std::cout << "\nProjecting reference solutions" << std::endl;
        // we project the bulk solution into the matrix solution space
        // and the fracture reference solution onto the low dim solution space
        const auto bulkProjector = Dumux::makeProjectorPair(matrixBasis, bulkBasis, bulkGlue).second;
        const auto lowDimProjector = Dumux::makeProjectorPair(fractureBasis, lowDimBasis, lowDimGlue).first;

        auto params = bulkProjector.defaultParams();
        params.residualReduction = 1e-16;

        auto projectedBulkPressureSol = bulkProjector.project(bulkPressureAveraged, params);
        auto projectedBulkSaturationSol = bulkProjector.project(bulkSaturationAveraged, params);
        auto projectedFracturePressureSol = lowDimProjector.project(fracturePressure, params);
        auto projectedFractureSaturationSol = lowDimProjector.project(fractureSaturation, params);

        std::cout << "\nComputing discrete l2 error norm" << std::endl;

        // grid function for bulk solution on matrix grid
        auto gfBulkMatrixPressure = makeDiscreteGlobalBasisFunction<RefBlockType>(matrixBasis, projectedBulkPressureSol);
        auto gfBulkMatrixSaturation = makeDiscreteGlobalBasisFunction<RefBlockType>(matrixBasis, projectedBulkSaturationSol);
        // grid function for ref fracture sol on low dim Grid
        auto gfFractureLowDimPressure = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, projectedFracturePressureSol);
        auto gfFractureLowDimSaturation = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, projectedFractureSaturationSol);
        // grid function for solution on low dim grid
        auto gfLowDimPressure = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, lowDimPressureAveraged);
        auto gfLowDimSaturation = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, lowDimSaturationAveraged);

        // compute l2 norms
        auto matrixL2normPressure = Dumux::integrateL2Error(matrixBasis.gridView(), gfMatrixPressure, gfBulkMatrixPressure, intOrder);
        auto matrixL2normSaturation = Dumux::integrateL2Error(matrixBasis.gridView(), gfMatrixSaturation, gfBulkMatrixSaturation, intOrder);
        auto fractureL2normPressure = Dumux::integrateL2Error(lowDimBasis.gridView(), gfLowDimPressure, gfFractureLowDimPressure, intOrder);
        auto fractureL2normSaturation = Dumux::integrateL2Error(lowDimBasis.gridView(), gfLowDimSaturation, gfFractureLowDimSaturation, intOrder);
        std::cout << "\t -> matrix: " << matrixL2normPressure << ", " << matrixL2normSaturation << "\n"
                  << "\t -> fracture: " << fractureL2normPressure << ", " << fractureL2normSaturation << std::endl;

        // write projection of bulk solution in reference grid
        Dune::VTKWriter< std::decay_t<decltype(matrixBasis.gridView())> > matrixProjectionWriter(matrixBasis.gridView());
        matrixProjectionWriter.addCellData(gfBulkMatrixPressure, Dune::VTK::FieldInfo("p_proj", Dune::VTK::FieldInfo::Type::scalar, 1));
        matrixProjectionWriter.addCellData(gfBulkMatrixSaturation, Dune::VTK::FieldInfo("s_proj", Dune::VTK::FieldInfo::Type::scalar, 1));
        matrixProjectionWriter.write(getParam<std::string>("Bulk.Problem.Name") + "_projection" + "_" + std::to_string(refinementCount));

        // write projection of fracture solution in low dim grid
        Dune::VTKWriter< std::decay_t<decltype(lowDimBasis.gridView())> > fractureProjectionWriter(lowDimBasis.gridView());
        fractureProjectionWriter.addCellData(gfFractureLowDimPressure, Dune::VTK::FieldInfo("pRef_proj", Dune::VTK::FieldInfo::Type::scalar, 1));
        fractureProjectionWriter.addCellData(gfFractureLowDimSaturation, Dune::VTK::FieldInfo("sRef_proj", Dune::VTK::FieldInfo::Type::scalar, 1));
        fractureProjectionWriter.write(getParam<std::string>("LowDim.Problem.Name") + "_projection" + "_" + std::to_string(refinementCount));

        if (refinementCount > 0) { l2ErrorsMatrixPressure << ","; l2ErrorsMatrixSaturation << ","; l2ErrorsFracturePressure << ","; l2ErrorsFractureSaturation << ","; }
        l2ErrorsMatrixPressure << matrixL2normPressure/integralMatrixPressure;
        l2ErrorsMatrixSaturation << matrixL2normSaturation/integralMatrixSaturation;
        l2ErrorsFracturePressure << fractureL2normPressure/integralFracturePressure;
        l2ErrorsFractureSaturation << fractureL2normSaturation/integralFractureSaturation;

        refinementCount++;
    }

    l2ErrorsMatrixPressure.close();
    l2ErrorsMatrixSaturation.close();

    l2ErrorsFracturePressure.close();
    l2ErrorsFractureSaturation.close();

    // print dumux good bye message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/false);

    return 0;
}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
