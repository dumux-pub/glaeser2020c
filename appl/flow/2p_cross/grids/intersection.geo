a = 1e-3;
sizeMatrix = 15*a;
sizeFracture = a;

Point(1) = {0.5, -0.5, 0.0, sizeMatrix};
Point(2) = {0.5, 0.25, 0.0, sizeMatrix};
Point(3) = {0.5, 0.5, 0.0, sizeMatrix};
Point(4) = {-0.5, 0.5, 0.0, sizeMatrix};
Point(5) = {-0.5, -0.25, 0.0, sizeMatrix};
Point(6) = {-0.5, -0.5, 0.0, sizeMatrix};

// fracture support points
df = 0.25;
Point(7) = {0.0, df, 0.0, sizeFracture};
Point(8) = {df, 0.0, 0.0, sizeFracture};
Point(9) = {0.0, -df, 0.0, sizeFracture};
Point(10) = {-df, 0.0, 0.0, sizeFracture};
Point(11) = { 0.0, 0.0, 0.0, sizeFracture};

// fracture lines
Line(1) = {7, 11};
Line(2) = {8, 11};
Line(3) = {9, 11};
Line(4) = {10, 11};

// domain outline
Line(13) = {1, 2};
Line(14) = {2, 3};
Line(15) = {3, 4};
Line(16) = {4, 5};
Line(17) = {5, 6};
Line(18) = {6, 1};
Curve Loop(1) = {13, 14, 15 ,16 ,17, 18};
Plane Surface(1) = {1};
Physical Surface(1) = {1};

Line{1:4} In Surface{1};
Physical Line(1) = {1:4};

Recombine Surface "*";
