numElementsInFracture = 4;
a = 1e-3;
size = a/numElementsInFracture;
sizeMatrix = 50.0*size;

Point(1) = {-0.5,  -0.5, 0.0, sizeMatrix};
Point(2) = {0.5,  -0.5, 0.0, sizeMatrix};
Point(3) = {0.5,  0.25, 0.0, sizeMatrix};
Point(4) = {0.5,  0.5, 0.0, sizeMatrix};
Point(5) = {-0.5, 0.5, 0.0, sizeMatrix};
Point(6) = {-0.5, -0.25, 0.0, sizeMatrix};

// fracture support points
dx = a/2.0;
df = 0.25;
Point(7) = {-dx, -df, 0.0, size};
Point(8) = {dx, -df, 0.0, size};
Point(9) = {dx, -dx, 0.0, size};
Point(10) = {df, -dx, 0.0, size};
Point(11) = {df, dx, 0.0, size};
Point(12) = {dx, dx, 0.0, size};
Point(13) = {dx, df, 0.0, size};
Point(14) = {-dx, df, 0.0, size};
Point(15) = {-dx, dx, 0.0, size};
Point(16) = {-df, dx, 0.0, size};
Point(17) = {-df, -dx, 0.0, size};
Point(18) = {-dx, -dx, 0.0, size};

// fracture outlines
Line(1) = {16, 17};
Line(2) = {17, 18};
Line(3) = {18, 7};
Line(4) = {7, 8};
Line(5) = {8, 9};
Line(6) = {9, 10};
Line(7) = {10, 11};
Line(8) = {11, 12};
Line(9) = {12, 13};
Line(10) = {13, 14};
Line(11) = {14, 15};
Line(12) = {15, 16};
Line(13) = {15, 18};
Line(14) = {18, 9};
Line(15) = {9, 12};
Line(16) = {12, 15};

// fracture planes
Curve Loop(1) = {1, 2, -13, 12};
Plane Surface(1) = {1};

Curve Loop(2) = {16, -11, -10, -9};
Plane Surface(2) = {2};

Curve Loop(3) = {15, -8, -7, -6};
Plane Surface(3) = {3};

Curve Loop(4) = {14, -5, -4, -3};
Plane Surface(4) = {4};

Curve Loop(5) = {14, 15, 16, 13};
Plane Surface(5) = {5};

// fracture planes receive physical index 2
Physical Surface(2) = {1, 2, 3, 4, 5};

// domain outline and plane
Line(17) = {5, 6};
Line(18) = {6, 1};
Line(19) = {1, 2};
Line(20) = {2, 3};
Line(21) = {3, 4};
Line(22) = {4, 5};
Curve Loop(6) = {17, 18, 19, 20, 21, 22};
Plane Surface(6) = {6, 1, 2, 3, 4, 5};
Physical Surface(1) = {6};
Recombine Surface "*";
