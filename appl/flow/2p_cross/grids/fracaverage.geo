a = 1e-3;
size = a/2.0;

// fracture support points
dx = a/2.0;
df = 0.25;
Point(7) = {-dx, -df, 0.0, size};
Point(8) = {dx, -df, 0.0, size};
Point(9) = {dx, -dx, 0.0, size};
Point(10) = {df, -dx, 0.0, size};
Point(11) = {df, dx, 0.0, size};
Point(12) = {dx, dx, 0.0, size};
Point(13) = {dx, df, 0.0, size};
Point(14) = {-dx, df, 0.0, size};
Point(15) = {-dx, dx, 0.0, size};
Point(16) = {-df, dx, 0.0, size};
Point(17) = {-df, -dx, 0.0, size};
Point(18) = {-dx, -dx, 0.0, size};

// fracture outlines
Line(1) = {16, 17};
Line(2) = {17, 18};
Line(3) = {18, 7};
Line(4) = {7, 8};
Line(5) = {8, 9};
Line(6) = {9, 10};
Line(7) = {10, 11};
Line(8) = {11, 12};
Line(9) = {12, 13};
Line(10) = {13, 14};
Line(11) = {14, 15};
Line(12) = {15, 16};
Line(13) = {15, 18};
Line(14) = {18, 9};
Line(15) = {9, 12};
Line(16) = {12, 15};

Transfinite Line{4, 14, 16, 15, 13, 10, 7, 1} = 2;
Transfinite Line{12, 2, 8, 6, 9, 11, 5, 3} = Round((0.25-dx)/size)+1;

// fracture planes
Curve Loop(1) = {1, 2, -13, 12};
Plane Surface(1) = {1};

Curve Loop(2) = {16, -11, -10, -9};
Plane Surface(2) = {2};

Curve Loop(3) = {15, -8, -7, -6};
Plane Surface(3) = {3};

Curve Loop(4) = {14, -5, -4, -3};
Plane Surface(4) = {4};

Curve Loop(5) = {14, 15, 16, 13};
Plane Surface(5) = {5};

Transfinite Surface "*";
Recombine Surface "*";
