// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The sub-problem for the matrix domain in the
 *        two-phase crossing fractures example.
 */
#ifndef DUMUX_TWOP_CROSS_FACET_MATRIX_PROBLEM_HH
#define DUMUX_TWOP_CROSS_FACET_MATRIX_PROBLEM_HH

#include <dune/alugrid/grid.hh>

#include <dumux/material/fluidsystems/2pimmiscible.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/components/trichloroethene.hh>

#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/evalgradients.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p/model.hh>
#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>
#include <dumux/multidomain/facet/cellcentered/mpfa/properties.hh>
#include <dumux/multidomain/facet/box/properties.hh>

#include "matrixspatialparams.hh"

namespace Dumux {

// forward declaration of the problem class
template<class TypeTag> class MatrixSubProblem;

namespace Properties {
namespace TTag {

struct MatrixProblemTypeTag { using InheritsFrom = std::tuple<TwoP>; };
struct MatrixProblemTpfaTypeTag { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, MatrixProblemTypeTag>; };
struct MatrixProblemMpfaTypeTag { using InheritsFrom = std::tuple<CCMpfaFacetCouplingModel, MatrixProblemTypeTag>; };
struct MatrixProblemBoxTypeTag { using InheritsFrom = std::tuple<BoxFacetCouplingModel, MatrixProblemTypeTag>; };

} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::MatrixProblemTypeTag> { using type = Dune::ALUGrid<2, 2, Dune::cube, Dune::nonconforming>; };
// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::MatrixProblemTypeTag> { using type = MatrixSubProblem<TypeTag>; };
// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::MatrixProblemTypeTag>
{
    using type = MatrixSpatialParams< GetPropType<TypeTag, Properties::FVGridGeometry>,
                                      GetPropType<TypeTag, Properties::Scalar> >;
};

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::MatrixProblemTypeTag>
{
    using type = FluidSystems::TwoPImmiscible< GetPropType<TypeTag, Properties::Scalar>,
                                               FluidSystems::OnePLiquid<GetPropType<TypeTag, Properties::Scalar>, Components::SimpleH2O<GetPropType<TypeTag, Properties::Scalar>>>,
                                               FluidSystems::OnePLiquid<GetPropType<TypeTag, Properties::Scalar>, Components::Trichloroethene<GetPropType<TypeTag, Properties::Scalar>>> >;
};

template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::MatrixProblemTypeTag>
{ static constexpr bool value = false; };

} // end namespace Properties

/*!
 * \brief The sub-problem for the matrix domain in the
 *        two-phase crossing fractures example.
 */
template<class TypeTag>
class MatrixSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;

    // some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum
    {
        pressureIdx = Indices::pressureIdx,
        saturationIdx = Indices::saturationIdx
    };

public:
    //! The constructor
    MatrixSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                     std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                     const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , deltaP_(getParamFromGroup<Scalar>(paramGroup, "Problem.DeltaP"))
    , boundarySat_(getParamFromGroup<Scalar>(paramGroup, "Problem.BoundarySaturation"))
    {
        // initialize the fluid system, i.e. the tabulation
        // of water properties. Use the default p/T ranges.
        FluidSystem::init();
    }

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        if ( FVGridGeometry::discMethod != DiscretizationMethod::box
             && (isOnInlet(globalPos) || isOnOutlet(globalPos)) )
            values.setAllDirichlet();
        return values;
    }

    //! Specifies the type of interior boundary condition to be used on a sub-control volume face
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        static const auto useInteriorDirichletBC = getParamFromGroup<bool>(this->paramGroup(), "Problem.UseInteriorDirichletBCs");
        if (useInteriorDirichletBC)
            values.setAllDirichlet();
        else
            values.setAllNeumann();
        return values;
    }

    //! evaluates the Neumann boundary condition for a given position
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        const auto globalPos = scvf.ipGlobal();

        // for box, incorporate Dirichlet Bcs weakly
        if ( FVGridGeometry::discMethod == DiscretizationMethod::box
             && (isOnOutlet(globalPos) || isOnInlet(globalPos)) )
        {
            // modify element solution to carry inlet/outlet head
            auto elemSol = elementSolution(element, elemVolVars, fvGeometry);
            for (const auto& curScvf : scvfs(fvGeometry))
            {
                if (curScvf.boundary())
                {
                    if (isOnOutlet(curScvf.ipGlobal()) || isOnInlet(curScvf.ipGlobal()))
                    {
                        const auto diriValues = dirichletAtPos(curScvf.ipGlobal());
                        const auto& insideScv = fvGeometry.scv(curScvf.insideScvIdx());
                        elemSol[insideScv.localDofIndex()] = diriValues;
                    }
                }
            }

            // evaluate gradients using this element solution
            auto gradHead = evalGradients(element,
                                          element.geometry(),
                                          fvGeometry.fvGridGeometry(),
                                          elemSol,
                                          scvf.ipGlobal())[0];

            // compute the flux
            const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
            const auto& volVars = elemVolVars[insideScv];
            Scalar flux_0 = gradHead * scvf.unitOuterNormal();
            flux_0 *= -1.0*volVars.permeability();
            flux_0 *= volVars.density(FluidSystem::phase0Idx);

            // evaluate mobility depending on flux sign
            using SpatialParams = typename ParentType::SpatialParams;
            using MaterialLaw = typename SpatialParams::MaterialLaw;
            const auto& matParams = this->spatialParams().materialLawParamsAtPos(globalPos);

            if (isOnInlet(globalPos))
                flux_0 *= MaterialLaw::krw(matParams, (1.0 - elemSol[insideScv.localDofIndex()][1]))
                          /volVars.viscosity(FluidSystem::phase0Idx);
            else
                flux_0 *= volVars.mobility(FluidSystem::phase0Idx);

            // add capillary pressure to evaluate non-wetting phase gradients
            for (const auto& scv : scvs(fvGeometry))
                elemSol[scv.localDofIndex()][0] += MaterialLaw::pc(matParams, (1.0 - elemSol[scv.localDofIndex()][1]));

            gradHead = evalGradients(element,
                                     element.geometry(),
                                     fvGeometry.fvGridGeometry(),
                                     elemSol,
                                     scvf.ipGlobal())[0];

            Scalar flux_1 = gradHead * scvf.unitOuterNormal();
            flux_1 *= -1.0 *volVars.permeability();
            flux_1 *= volVars.density(FluidSystem::phase1Idx);

            if (isOnInlet(globalPos))
                flux_1 *= MaterialLaw::krn(matParams, (1.0 - elemSol[insideScv.localDofIndex()][1]))
                          /volVars.viscosity(FluidSystem::phase1Idx);
            else
                flux_1 *= volVars.mobility(FluidSystem::phase1Idx);

            return NumEqVector({flux_0, flux_1});
        }

        return NumEqVector(0.0);
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        if (isOnInlet(globalPos))
            return initialAtPos(globalPos) + PrimaryVariables({deltaP_, boundarySat_});
        return initialAtPos(globalPos);
    }

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;
        values[pressureIdx] = 1e5;
        values[saturationIdx] = 0.0;
        return values;
    }

    //! write the mass distributions to an output file
    template<class GridVariables, class SolutionVector>
    Scalar computeMass(const GridVariables& gridVariables,
                       const SolutionVector& x) const
    {
        Scalar mass = 0.0;

        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            auto elemVolVars = localView(gridVariables.curGridVolVars());

            fvGeometry.bindElement(element);
            elemVolVars.bindElement(element, fvGeometry, x);

            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& vv = elemVolVars[scv];
                const auto scvMass = vv.porosity()*scv.volume()
                                     *vv.saturation(FluidSystem::phase1Idx)
                                     *vv.density(FluidSystem::phase1Idx);
                mass += scvMass;
            }
        }

        return mass;
    }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! returns true if position is on inlet
    bool isOnInlet(const GlobalPosition& globalPos) const
    { return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + 1e-8 && globalPos[1] < -0.25 + 1e-8; }

    //! returns true if position is on outlet
    bool isOnOutlet(const GlobalPosition& globalPos) const
    { return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - 1e-8 && globalPos[1] > 0.25 - 1e-8; }

    //! sets the pointer to the coupling manager.
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    Scalar deltaP_;
    Scalar boundarySat_;
};

} // end namespace Dumux

#endif
