// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
 /*!
  * \file
  * \brief The sub-problem for the fracture domain in the
  *        two-phase crossing fractures example.
  */
#ifndef DUMUX_TWOP_CROSS_FACET_FRACTURE_PROBLEM_HH
#define DUMUX_TWOP_CROSS_FACET_FRACTURE_PROBLEM_HH

#include <dune/foamgrid/foamgrid.hh>

#include <dumux/material/fluidsystems/2pimmiscible.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/components/trichloroethene.hh>

#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>
#include <dumux/discretization/box.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p/model.hh>

#include "fracturespatialparams.hh"

namespace Dumux {
// forward declarations
template<class TypeTag> class FractureSubProblem;

namespace Properties {
namespace TTag {

struct FractureProblemTypeTag { using InheritsFrom = std::tuple<TwoP>; };
struct FractureProblemTpfaTypeTag { using InheritsFrom = std::tuple<FractureProblemTypeTag, CCTpfaModel>; };
struct FractureProblemMpfaTypeTag { using InheritsFrom = std::tuple<FractureProblemTypeTag, CCTpfaModel>; };
struct FractureProblemBoxTypeTag { using InheritsFrom = std::tuple<FractureProblemTypeTag, BoxModel>; };

} // end namespace TTag


// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::FractureProblemTypeTag> { using type = Dune::FoamGrid<1, 2>; };
// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::FractureProblemTypeTag> { using type = FractureSubProblem<TypeTag>; };
// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::FractureProblemTypeTag>
{
    using type = FractureSpatialParams< GetPropType<TypeTag, Properties::FVGridGeometry>,
                                        GetPropType<TypeTag, Properties::Scalar> >;
};

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::FractureProblemTypeTag>
{
    using type = FluidSystems::TwoPImmiscible< GetPropType<TypeTag, Properties::Scalar>,
                                               FluidSystems::OnePLiquid<GetPropType<TypeTag, Properties::Scalar>, Components::SimpleH2O<GetPropType<TypeTag, Properties::Scalar>>>,
                                               FluidSystems::OnePLiquid<GetPropType<TypeTag, Properties::Scalar>, Components::Trichloroethene<GetPropType<TypeTag, Properties::Scalar>>> >;
};

template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::FractureProblemTypeTag>
{ static constexpr bool value = false; };

} // end namespace Properties

/*!
 * \brief The sub-problem for the fracture domain in the
 *        two-phase crossing fractures example.
 */
template<class TypeTag>
class FractureSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;

    // some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum
    {
        pressureIdx = Indices::pressureIdx,
        saturationIdx = Indices::saturationIdx
    };

public:
    //! The constructor
    FractureSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                       std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                       const std::string& paramGroup)
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , aperture_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Aperture"))
    {
        // initialize the fluid system, i.e. the tabulation
        // of water properties. Use the default p/T ranges.
        FluidSystem::init();
    }

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    //! Evaluate the source term in a sub-control volume of an element
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        // evaluate sources from bulk domain using the function in the coupling manager
        auto source = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);

        // these sources are in kg/s, divide by volume and extrusion to have it in kg/s/m³
        source /= scv.volume()*elemVolVars[scv].extrusionFactor();
        return source;
    }

    //! Set the aperture as extrusion factor.
    Scalar extrusionFactorAtPos(const GlobalPosition& globalPos) const
    { return aperture_; }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return initialAtPos(globalPos); }

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;
        values[pressureIdx] = 1e5;
        values[saturationIdx] = 0.0;
        return values;
    }

    //! write the mass distributions to an output file
    template<class GridVariables, class SolutionVector>
    Scalar computeMass(const GridVariables& gridVariables, const SolutionVector& x) const
    {
        Scalar mass = 0.0;

        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            auto elemVolVars = localView(gridVariables.curGridVolVars());

            fvGeometry.bindElement(element);
            elemVolVars.bindElement(element, fvGeometry, x);

            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& vv = elemVolVars[scv];
                const auto scvMass = vv.porosity()*scv.volume()*vv.extrusionFactor()
                                     *vv.saturation(FluidSystem::phase1Idx)
                                     *vv.density(FluidSystem::phase1Idx);
                mass += scvMass;
            }
        }

        return mass;
    }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! sets the pointer to the coupling manager.
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManagerPtr_ = cm; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    Scalar aperture_;
};

} // end namespace Dumux

#endif
