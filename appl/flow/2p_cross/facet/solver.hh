// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Solver to the facet solutions of the
 *        two-phase crossing fracture test case.
 */
#ifndef DUMUX_TWOP_CROSS_FACET_SOLVER_HH
#define DUMUX_TWOP_CROSS_FACET_SOLVER_HH

#include <config.h>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>

#include "matrixproblem.hh"
#include "fractureproblem.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/timeloop.hh>

#include <dumux/assembly/diffmethod.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/traits.hh>

#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>
#include <dumux/multidomain/facet/codimonegridadapter.hh>

#include <dumux/io/vtkoutputmodule.hh>

// obtain/define some types to be used below in the property definitions and in main
template< class BulkTypeTag, class LowDimTypeTag >
class TestTraits
{
    using BulkFVGridGeometry = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::FVGridGeometry>;
    using LowDimFVGridGeometry = Dumux::GetPropType<LowDimTypeTag, Dumux::Properties::FVGridGeometry>;
public:
    using MDTraits = Dumux::MultiDomainTraits<BulkTypeTag, LowDimTypeTag>;
    using CouplingMapper = Dumux::FacetCouplingMapper<BulkFVGridGeometry, LowDimFVGridGeometry>;
    using CouplingManager = Dumux::FacetCouplingManager<MDTraits, CouplingMapper>;
};

// set the coupling manager property for all available type tags
namespace Dumux {
namespace Properties {

// set cm property for the tests
using BoxTraits = TestTraits<TTag::MatrixProblemBoxTypeTag, TTag::FractureProblemBoxTypeTag>;
using TpfaTraits = TestTraits<TTag::MatrixProblemTpfaTypeTag, TTag::FractureProblemTpfaTypeTag>;
using MpfaTraits = TestTraits<TTag::MatrixProblemMpfaTypeTag, TTag::FractureProblemMpfaTypeTag>;

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::MatrixProblemBoxTypeTag> { using type = typename BoxTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::FractureProblemBoxTypeTag> { using type = typename BoxTraits::CouplingManager; };

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::MatrixProblemTpfaTypeTag> { using type = typename TpfaTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::FractureProblemTpfaTypeTag> { using type = typename TpfaTraits::CouplingManager; };

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::MatrixProblemMpfaTypeTag> { using type = typename MpfaTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::FractureProblemMpfaTypeTag> { using type = typename MpfaTraits::CouplingManager; };

} // end namespace Properties
} // end namespace Dumux

// updates the finite volume grid geometry. This is necessary as the finite volume
// grid geometry for the box scheme with facet coupling requires additional data for
// the update. The reason is that we have to create additional faces on interior
// boundaries, which wouldn't be created in the standard scheme.
template< class FVGridGeometry,
          class GridManager,
          class FractureGridView,
          std::enable_if_t<FVGridGeometry::discMethod == Dumux::DiscretizationMethod::box, int> = 0 >
void updateMatrixFVGridGeometry(FVGridGeometry& fvGridGeometry,
                                const GridManager& gridManager,
                                const FractureGridView& lowDimGridView)
{
    using BulkFacetGridAdapter = Dumux::CodimOneGridAdapter<typename GridManager::Embeddings>;
    BulkFacetGridAdapter facetGridAdapter(gridManager.getEmbeddings());
    fvGridGeometry.update(lowDimGridView, facetGridAdapter);
}

// specialization for cell-centered schemes
template< class FVGridGeometry,
          class GridManager,
          class FractureGridView,
          std::enable_if_t<FVGridGeometry::discMethod != Dumux::DiscretizationMethod::box, int> = 0 >
void updateMatrixFVGridGeometry(FVGridGeometry& fvGridGeometry,
                                const GridManager& gridManager,
                                const FractureGridView& fractureGidView)
{
    fvGridGeometry.update();
}

// return type of solveFacet(), containing the
// solutions and the grid views on which they were computed
template<class MatrixTypeTag, class FractureTypeTag>
struct FacetSolutionStorage
{
private:
    using BulkGrid = Dumux::GetPropType<MatrixTypeTag,  Dumux::Properties::Grid>;
    using LowDimGrid = Dumux::GetPropType<FractureTypeTag,  Dumux::Properties::Grid>;
    using BulkFVGG = Dumux::GetPropType<MatrixTypeTag, Dumux::Properties::FVGridGeometry>;
    using LowDimFVGG = Dumux::GetPropType<FractureTypeTag, Dumux::Properties::FVGridGeometry>;

public:
    Dumux::FacetCouplingGridManager<BulkGrid, LowDimGrid> gridManager;
    std::shared_ptr<BulkFVGG> bulkFvGridGeometry;
    std::shared_ptr<LowDimFVGG> lowDimFvGridGeometry;
    Dumux::GetPropType<MatrixTypeTag, Dumux::Properties::SolutionVector> bulkSolution;
    Dumux::GetPropType<FractureTypeTag, Dumux::Properties::SolutionVector> lowDimSolution;
};

// run the simulation for given combo of type tags
template<class MatrixTypeTag, class FractureTypeTag>
FacetSolutionStorage<MatrixTypeTag, FractureTypeTag>
solveFacet(const std::string& gridParamGroup = "", int refinementLevel = 0)
{
    using namespace Dumux;

    using MatrixFVGridGeometry = Dumux::GetPropType<MatrixTypeTag, Dumux::Properties::FVGridGeometry>;
    using FractureFVGridGeometry = Dumux::GetPropType<FractureTypeTag, Dumux::Properties::FVGridGeometry>;
    using TheMultiDomainTraits = typename TestTraits<MatrixTypeTag, FractureTypeTag>::MDTraits;
    using TheCouplingMapper = typename TestTraits<MatrixTypeTag, FractureTypeTag>::CouplingMapper;
    using TheCouplingManager = typename TestTraits<MatrixTypeTag, FractureTypeTag>::CouplingManager;

    FacetSolutionStorage<MatrixTypeTag, FractureTypeTag> result;
    auto& gridManager = result.gridManager;
    gridManager.init(gridParamGroup);
    gridManager.loadBalance();

    // the grid ids correspond to the order of the grids passed to the manager (see above)
    static constexpr std::size_t matrixGridId = 0;
    static constexpr std::size_t fractureGridId = 1;
    const auto& matrixGridView = gridManager.template grid<matrixGridId>().leafGridView();
    const auto& fractureGridView = gridManager.template grid<fractureGridId>().leafGridView();

    // create the finite volume grid geometries
    auto matrixFvGridGeometry = std::make_shared<MatrixFVGridGeometry>(matrixGridView);
    auto fractureFvGridGeometry = std::make_shared<FractureFVGridGeometry>(fractureGridView);
    updateMatrixFVGridGeometry(*matrixFvGridGeometry, gridManager, fractureGridView);
    fractureFvGridGeometry->update();

    // the problems (boundary/initial conditions etc)
    using MatrixProblem = GetPropType<MatrixTypeTag, Properties::Problem>;
    using FractureProblem = GetPropType<FractureTypeTag, Properties::Problem>;

    auto matrixSpatialParams = std::make_shared<typename MatrixProblem::SpatialParams>(matrixFvGridGeometry, "Matrix");
    auto matrixProblem = std::make_shared<MatrixProblem>(matrixFvGridGeometry, matrixSpatialParams, "Matrix");

    auto fractureSpatialParams = std::make_shared<typename FractureProblem::SpatialParams>(fractureFvGridGeometry, "Fracture");
    auto fractureProblem = std::make_shared<FractureProblem>(fractureFvGridGeometry, fractureSpatialParams, "Fracture");

    // the solution vector
    using SolutionVector = typename TheMultiDomainTraits::SolutionVector;
    SolutionVector x, xOld;

    // The domain ids within the multi-domain framework.
    static const auto matrixDomainId = typename TheMultiDomainTraits::template SubDomain<0>::Index();
    static const auto fractureDomainId = typename TheMultiDomainTraits::template SubDomain<1>::Index();

    // resize the solution vector and write initial solution to it
    x[matrixDomainId].resize(matrixFvGridGeometry->numDofs());
    x[fractureDomainId].resize(fractureFvGridGeometry->numDofs());
    matrixProblem->applyInitialSolution(x[matrixDomainId]);
    fractureProblem->applyInitialSolution(x[fractureDomainId]);

    // instantiate the class holding the coupling maps between the domains
    const auto embeddings = gridManager.getEmbeddings();
    auto couplingMapper = std::make_shared<TheCouplingMapper>();
    couplingMapper->update(*matrixFvGridGeometry, *fractureFvGridGeometry, embeddings);

    // the grid variables
    using MatrixGridVariables = GetPropType<MatrixTypeTag, Properties::GridVariables>;
    using FractureGridVariables = GetPropType<FractureTypeTag, Properties::GridVariables>;
    auto matrixGridVariables = std::make_shared<MatrixGridVariables>(matrixProblem, matrixFvGridGeometry);
    auto fractureGridVariables = std::make_shared<FractureGridVariables>(fractureProblem, fractureFvGridGeometry);
    xOld = x;
    matrixGridVariables->init(x[matrixDomainId]);
    fractureGridVariables->init(x[fractureDomainId]);

    // the coupling manager (needs the coupling mapper)
    auto couplingManager = std::make_shared<TheCouplingManager>();
    couplingManager->init(matrixProblem, fractureProblem, couplingMapper, x);

    // we have to set coupling manager pointer in sub-problems
    // they also have to be made accessible in them (see e.g. matrixproblem.hh)
    matrixProblem->setCouplingManager(couplingManager);
    fractureProblem->setCouplingManager(couplingManager);

    // intialize the vtk output modules
    const auto bulkDM = MatrixFVGridGeometry::discMethod == DiscretizationMethod::box ? Dune::VTK::nonconforming : Dune::VTK::conforming;
    using MatrixVtkWriter = VtkOutputModule<MatrixGridVariables, GetPropType<MatrixTypeTag, Properties::SolutionVector>>;
    using FractureVtkWriter = VtkOutputModule<FractureGridVariables, GetPropType<FractureTypeTag, Properties::SolutionVector>>;
    MatrixVtkWriter matrixVtkWriter(*matrixGridVariables, x[matrixDomainId], matrixProblem->name() + "_" + std::to_string(refinementLevel), "Matrix", bulkDM);
    FractureVtkWriter fractureVtkWriter(*fractureGridVariables, x[fractureDomainId], fractureProblem->name() + "_" + std::to_string(refinementLevel), "Fracture");

    // Add model specific output fields
    using MatrixVtkOutputFields = GetPropType<MatrixTypeTag, Properties::VtkOutputFields>;
    using FractureVtkOutputFields = GetPropType<FractureTypeTag, Properties::VtkOutputFields>;
    MatrixVtkOutputFields::initOutputModule(matrixVtkWriter);
    FractureVtkOutputFields::initOutputModule(fractureVtkWriter);

    // write out initial solution
    matrixVtkWriter.write(0.0);
    fractureVtkWriter.write(0.0);

    // get some time loop parameters
    const auto tEnd = getParam<double>("TimeLoop.TEnd");
    const auto maxDt = getParam<double>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<double>("TimeLoop.DtInitial");

    // instantiate time loop
    auto timeLoop = std::make_shared< CheckPointTimeLoop<double> >(/*startTime*/0.0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // the assembler for the coupled problem
    using Assembler = MultiDomainFVAssembler<TheMultiDomainTraits, TheCouplingManager, DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>( std::make_tuple(matrixProblem, fractureProblem),
                                                  std::make_tuple(matrixFvGridGeometry, fractureFvGridGeometry),
                                                  std::make_tuple(matrixGridVariables, fractureGridVariables),
                                                  couplingManager,
                                                  timeLoop);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, TheCouplingManager>;
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);

    // use checkpoints for output
    timeLoop->setPeriodicCheckPoint(maxDt);

    // output file for masses in the different layers
    std::ofstream file;
    std::string fileName = matrixProblem->name();
    std::size_t found = fileName.find("_matrix");
    if (found == std::string::npos)
        DUNE_THROW(Dune::InvalidStateException, "Unexpected name for matrix problem");
    fileName.erase(found, 7);
    file.open(fileName + ".log", std::ios::out);

    // time loop
    timeLoop->start(); do
    {
        // set previous solution for storage evaluations
        assembler->setPreviousSolution(xOld);

        // solve the non-linear system with time step control
        newtonSolver->solve(x, *timeLoop);

        // make the new solution the old solution
        xOld = x;
        matrixGridVariables->advanceTimeStep();
        fractureGridVariables->advanceTimeStep();

        // advance the time loop to the next step
        timeLoop->advanceTimeStep();

        // compute mass distribution
        const auto massMatrix = matrixProblem->computeMass(*matrixGridVariables, x[matrixDomainId]);
        const auto massFracture = fractureProblem->computeMass(*fractureGridVariables, x[fractureDomainId]);
        file << timeLoop->time() << "," << massMatrix << "," << massFracture << std::endl;

        // write vtk output
        if (timeLoop->isCheckPoint())
        {
            matrixVtkWriter.write(timeLoop->time());
            fractureVtkWriter.write(timeLoop->time());
        }

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by the Newton solver
        timeLoop->setTimeStepSize(newtonSolver->suggestTimeStepSize(timeLoop->timeStepSize()));

    } while (!timeLoop->finished());

    // output some Newton statistics
    newtonSolver->report();

    // report time loop statistics
    timeLoop->finalize();

    // close output file
    file.close();

    result.bulkFvGridGeometry = matrixFvGridGeometry;
    result.lowDimFvGridGeometry = fractureFvGridGeometry;
    result.bulkSolution = x[matrixDomainId];
    result.lowDimSolution = x[fractureDomainId];

    return result;
}

#endif
