// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Solver to the boxdfm solution of the two-phase
 *        flow, 3d experiment test case.
 */
#ifndef DUMUX_TWOP_EXPERIMENT_BOXDFM_SOLVER_HH
#define DUMUX_TWOP_EXPERIMENT_BOXDFM_SOLVER_HH

#include <config.h>

#include <ctime>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/foamgrid/foamgrid.hh>

#include "problem.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/linear/amgbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>
#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/codimonegridadapter.hh>

#include <dumux/porousmediumflow/boxdfm/vtkoutputmodule.hh>

// return type of solveBoxDfm(), containing the
// solutions and the grid views on which they were computed
template<class TypeTag>
struct BoxDfmSolutionStorage
{
private:
    using Grid = Dumux::GetPropType<TypeTag,  Dumux::Properties::Grid>;
    using FVGG = Dumux::GetPropType<TypeTag, Dumux::Properties::FVGridGeometry>;
    using FractureGrid = Dune::FoamGrid<1, 2>;

public:
    Dumux::FacetCouplingGridManager<Grid, FractureGrid> gridManager;
    std::shared_ptr<FVGG> fvGridGeometry;
    Dumux::GetPropType<TypeTag, Dumux::Properties::SolutionVector> solution;
};

// run the simulation for given combo of type tags
template<class TypeTag>
BoxDfmSolutionStorage<TypeTag>
solveBoxDfm(const std::string& gridParamGroup = "", int refinementLevel = 0)
{
    using namespace Dumux;

    using Grid = Dumux::GetPropType<TypeTag,  Dumux::Properties::Grid>;
    using FractureGrid = Dune::FoamGrid<1, 2>;
    using GridManager = Dumux::FacetCouplingGridManager<Grid, FractureGrid>;

    BoxDfmSolutionStorage<TypeTag> result;
    auto& gridManager = result.gridManager;
    gridManager.init(gridParamGroup);

    // use the grid adapter from the facet coupling framework to
    // identify the grid facets that coincide with a fracture.
    // For instantiation we extract the info on the embeddings from
    // the grid manager (info is read from the grid file)
    using MatrixFractureGridAdapter = CodimOneGridAdapter<typename GridManager::Embeddings>;
    MatrixFractureGridAdapter fractureGridAdapter(gridManager.getEmbeddings());

    // matrix grid view is the first one (index 0) inside the manager
    const auto& leafGridView = gridManager.template grid<0>().leafGridView();

    // create the finite volume grid geometry
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    auto fvGridGeometry = std::make_shared<FVGridGeometry>(leafGridView);
    fvGridGeometry->update(fractureGridAdapter);

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(fvGridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x(fvGridGeometry->numDofs());
    problem->applyInitialSolution(x);
    auto xOld = x;

    // update the interface parameters
    problem->spatialParams().updateMaterialInterfaceParams(x);

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, fvGridGeometry);
    gridVariables->init(x);

    // get some time loop parameters
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // intialize the vtk output module
    using VtkOutputModule = BoxDfmVtkOutputModule<GridVariables, SolutionVector, FractureGrid>;
    using VtkOutputFields = GetPropType<TypeTag, Properties::VtkOutputFields>;
    VtkOutputModule vtkWriter(*gridVariables, x, problem->name() + "_" + std::to_string(refinementLevel), fractureGridAdapter, "", Dune::VTK::nonconforming);
    VtkOutputFields::initOutputModule(vtkWriter); //!< Add model specific output fields
    vtkWriter.write(0.0);

    // instantiate time loop
    auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(0.0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // the assembler with time loop for instationary problem
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, fvGridGeometry, gridVariables, timeLoop);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    // use checkpoints for output
    timeLoop->setPeriodicCheckPoint(maxDt);

    // output file for masses in the different layers
    std::ofstream file;
    file.open(problem->name() + ".log", std::ios::out);

    // time loop
    timeLoop->start(); do
    {
        // set previous solution for storage evaluations
        assembler->setPreviousSolution(xOld);

        // solve the non-linear system with time step control
        nonLinearSolver.solve(x, *timeLoop);

        // make the new solution the old solution
        xOld = x;
        gridVariables->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // add mass distributions etc to output file
        problem->writeMassDistributions(file, *gridVariables, x, timeLoop->time());

        // write vtk output
        if (timeLoop->isCheckPoint())
            vtkWriter.write(timeLoop->time());

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by the Newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

    } while (!timeLoop->finished());

    timeLoop->finalize(leafGridView.comm());
    nonLinearSolver.report();
    file.close();

    result.fvGridGeometry = fvGridGeometry;
    result.solution = x;

    return result;
}

#endif
