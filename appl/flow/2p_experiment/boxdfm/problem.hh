// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \brief Problem to the boxdfm solution of the two-phase
 *        flow, threedimensional experiment test case.
 */
#ifndef DUMUX_TWOP_EXPERIMENT_BOXDFM_PROBLEM_HH
#define DUMUX_TWOP_EXPERIMENT_BOXDFM_PROBLEM_HH

#include <iostream>

#include <dune/alugrid/grid.hh>
#include <dune/grid/uggrid.hh>

#include <dumux/material/fluidsystems/2pimmiscible.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/1pgas.hh>
#include <dumux/material/components/h2.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/tabulatedcomponent.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p/model.hh>
#include <dumux/porousmediumflow/boxdfm/model.hh>
#include <dumux/porousmediumflow/2p/incompressiblelocalresidual.hh>

#include "spatialparams.hh"

namespace Dumux {

// forward declarations
template<class TypeTag> class TwoPBoxDfmTestProblem;

namespace Properties {
namespace TTag {
struct TwoPDfm { using InheritsFrom = std::tuple<BoxDfmModel, TwoP>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TwoPDfm> { using type = Dune::ALUGrid<3, 3, Dune::simplex, Dune::nonconforming>; };
// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::TwoPDfm> { using type = TwoPBoxDfmTestProblem<TypeTag>; };
// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TwoPDfm>
{
    using type = TwoPBoxDfmTestSpatialParams< GetPropType<TypeTag, Properties::FVGridGeometry>,
                                              GetPropType<TypeTag, Properties::Scalar> >;
};

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPDfm>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Hydrogen = Components::H2<Scalar>;
    using Water = Components::H2O<Scalar>;
    using TabulatedWater = Components::TabulatedComponent<Water>;
    using WaterPhase = FluidSystems::OnePLiquid<Scalar, TabulatedWater>;
    using GasPhase = FluidSystems::OnePGas<Scalar, Hydrogen>;

public:
    using type = Dumux::FluidSystems::TwoPImmiscible< Scalar, WaterPhase, GasPhase>;
};

// Disable caching
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::TwoPDfm>
{ static constexpr bool value = false; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::TwoPDfm>
{ static constexpr bool value = false; };
template<class TypeTag>
struct EnableFVGridGeometryCache<TypeTag, TTag::TwoPDfm>
{ static constexpr bool value = false; };

// Enable the box-interface solver
template<class TypeTag>
struct EnableBoxInterfaceSolver<TypeTag, TTag::TwoPDfm>
{ static constexpr bool value = true; };
} // end namespace Properties

/*!
 * \brief Problem to the boxdfm solution of the two-phase
 *        flow, threedimensional experiment test case.
 */
template<class TypeTag>
class TwoPBoxDfmTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;

    // some indices for convenience
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum
    {
        pressureH2OIdx = Indices::pressureIdx,
        saturationDNAPLIdx = Indices::saturationIdx,
        contiDNAPLEqIdx = Indices::conti0EqIdx + FluidSystem::comp1Idx,
        waterPhaseIdx = FluidSystem::phase0Idx,
        dnaplPhaseIdx = FluidSystem::phase1Idx
    };

    static constexpr Scalar eps_ = 1e-6;
    static constexpr int dimWorld = GridView::dimensionworld;

public:

    //! The constructor
    TwoPBoxDfmTestProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                          const std::string& paramGroup = "")
    : ParentType(fvGridGeometry)
    {
        extractionPressure_ = getParam<Scalar>("Problem.ExtractionPressure");
        injectionPhaseLength_ = getParam<Scalar>("Injection.InjectionPhaseLength");
        injectionRate_ = getParam<Scalar>("Injection.InjectionRate");

        FluidSystem::init(/*tMin*/263.15, /*tMax*/283.15, /*n*/100,
                          /*pMin*/extractionPressure_*0.9, /*pMax*/extractionPressure_*10.0, /*n*/100);
    }

    /*!
     * \brief Return how much the domain is extruded at a given sub-control volume.
     *
     * Here, we extrude the fracture scvs by half the aperture
     */
    template<class ElementSolution>
    Scalar extrusionFactor(const Element& element,
                           const SubControlVolume& scv,
                           const ElementSolution& elemSol) const
    {
        // In the box-scheme, we compute fluxes etc element-wise,
        // thus per element we compute only half a fracture !!!
        static const Scalar aHalf = getParam<Scalar>("TwoPFacet.SpatialParams.Aperture")/2.0;
        if (scv.isOnFracture())
            return aHalf;
        return 1.0;
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param values Stores the value of the boundary type
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        if (isOnOutlet_(globalPos))
            values.setAllDirichlet();
        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet
     *        boundary segment
     * \param globalPos The global position
     */
     PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
     { return initialAtPos(globalPos); }

    //! write the mass distributions for a time step
    template<class Outputfile, class GridVariables, class SolutionVector>
    void writeMassDistributions(Outputfile& outputFile,
                                const GridVariables& gridVariables,
                                const SolutionVector& x,
                                Scalar time) const
    {
        Scalar volumeFracture = 0.0;
        Scalar volumeMatrix = 0.0;
        Scalar massMatrix = 0.0;
        Scalar massFracture = 0.0;

        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            auto elemVolVars = localView(gridVariables.curGridVolVars());

            fvGeometry.bindElement(element);
            elemVolVars.bindElement(element, fvGeometry, x);

            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& vv = elemVolVars[scv];
                const auto scvMass = vv.saturation(dnaplPhaseIdx)*vv.density(dnaplPhaseIdx)*vv.porosity()*scv.volume();

                if (scv.isOnFracture())
                {
                    volumeFracture += scv.volume()*vv.extrusionFactor();
                    massFracture += scvMass*vv.extrusionFactor();
                }
                else
                {
                    volumeMatrix += scv.volume();
                    massMatrix += scvMass;
                }
            }
        }

        outputFile << time << "," << volumeMatrix << "," << massMatrix << "," << volumeFracture << "," << massFracture << std::endl;
    }

    //! evaluates the Neumann boundary condition for a given position
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        const auto globalPos = scvf.ipGlobal();

        if (isOnInlet_(globalPos) && time_ <= injectionPhaseLength_)
            return NumEqVector({0.0, -1.0*injectionRate_});
        return NumEqVector(0.0);
    }

    /*!
     * \brief Evaluates the initial values for a control volume
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values;
        values[pressureH2OIdx] = 1e5;
        values[saturationDNAPLIdx] = 0.0;
        return values;
    }

    //! Returns the temperature \f$\mathrm{[K]}\f$ for an isothermal problem.
    Scalar temperature() const
    { return 293.15; /* 10°C */ }

    //! returns true if position is on inlet
    bool isOnInlet(const GlobalPosition& globalPos) const
    { return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + 1e-8 && globalPos[1] < -0.25 + 1e-8; }

    //! returns true if position is on outlet
    bool isOnOutlet(const GlobalPosition& globalPos) const
    { return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - 1e-8 && globalPos[1] > 0.25 - 1e-8; }

    //! set the time of the following time integration
    void setTime(Scalar t)
    { time_ = t; }

private:
    //! The inlet is on the left side of the domain
    bool isOnInlet_(const GlobalPosition& globalPos) const
    { return globalPos[2] < this->fvGridGeometry().bBoxMin()[2] + 1e-6; }

    //! The outlet is on the right side of the domain
    bool isOnOutlet_(const GlobalPosition& globalPos) const
    { return globalPos[2] > this->fvGridGeometry().bBoxMax()[2] - 1e-6; }

    Scalar extractionPressure_;
    Scalar injectionPhaseLength_;
    Scalar injectionRate_;

    // keep track of time
    Scalar time_;
};

} // end namespace Dumux

#endif
