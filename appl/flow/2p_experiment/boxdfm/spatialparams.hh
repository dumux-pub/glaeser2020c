// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Problem to the boxdfm solution of the two-phase
 *        flow, 3d experiment fracture test case.
 */
#ifndef DUMUX_TWOP_EXPERIMENT_BOXDFM_SPATIALPARAMS_HH
#define DUMUX_TWOP_EXPERIMENT_BOXDFM_SPATIALPARAMS_HH

#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

#include <dumux/porousmediumflow/2p/boxmaterialinterfaceparams.hh>

namespace Dumux {

/*!
 * \brief Problem to the boxdfm solution of the two-phase
 *        crossing fracture test case.
 */
template<class FVGridGeometry, class Scalar>
class TwoPBoxDfmTestSpatialParams
: public FVSpatialParams< FVGridGeometry, Scalar, TwoPBoxDfmTestSpatialParams<FVGridGeometry, Scalar> >
{
    using ThisType = TwoPBoxDfmTestSpatialParams<FVGridGeometry, Scalar>;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, ThisType>;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;

    static constexpr int dimWorld = GridView::dimensionworld;

public:
    using MaterialLaw = EffToAbsLaw< RegularizedVanGenuchten<Scalar> >;
    using MaterialLawParams = typename MaterialLaw::Params;
    using PermeabilityType = Scalar;

    TwoPBoxDfmTestSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry) : ParentType(fvGridGeometry)
    {
        matrixPerm_ = getParamFromGroup<Scalar>("TwoPBulk", "SpatialParams.Permeability");
        matrixPoro_ = getParamFromGroup<Scalar>("TwoPBulk", "SpatialParams.Porosity");
        fracturePerm_ = getParamFromGroup<Scalar>("TwoPFacet", "SpatialParams.Permeability");
        fracturePoro_ = getParamFromGroup<Scalar>("TwoPFacet", "SpatialParams.Porosity");

        // residual saturations
        matrixMaterialParams_.setSwr(getParamFromGroup<Scalar>("TwoPBulk", "SpatialParams.Swr"));
        matrixMaterialParams_.setSnr(getParamFromGroup<Scalar>("TwoPBulk", "SpatialParams.Snr"));
        fractureMaterialParams_.setSwr(getParamFromGroup<Scalar>("TwoPFacet", "SpatialParams.Swr"));
        fractureMaterialParams_.setSnr(getParamFromGroup<Scalar>("TwoPFacet", "SpatialParams.Snr"));

        // parameters for the Van Genuchten law
        // alpha and n
        matrixMaterialParams_.setVgAlpha(getParamFromGroup<Scalar>("TwoPBulk", "SpatialParams.VGAlpha"));
        matrixMaterialParams_.setVgn(getParamFromGroup<Scalar>("TwoPBulk", "SpatialParams.VGN"));
        fractureMaterialParams_.setVgAlpha(getParamFromGroup<Scalar>("TwoPFacet", "SpatialParams.VGAlpha"));
        fractureMaterialParams_.setVgn(getParamFromGroup<Scalar>("TwoPFacet", "SpatialParams.VGN"));
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     *        In this test, we use element-wise distributed permeabilities.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return permeability
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        if (scv.isOnFracture())
            return fracturePerm_;
        else
            return matrixPerm_;
    }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param globalPos The global position
     */
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    {
        if (scv.isOnFracture())
            return fracturePoro_;
        else
            return matrixPoro_;
    }

    /*!
     * \brief Returns the parameter object for the Brooks-Corey material law.
     *        In this test, we use element-wise distributed material parameters.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return the material parameters object
     */
    template<class ElementSolution>
    const MaterialLawParams& materialLawParams(const Element& element,
                                               const SubControlVolume& scv,
                                               const ElementSolution& elemSol) const
    {
        if (scv.isOnFracture())
            return fractureMaterialParams_;
        else
            return matrixMaterialParams_;
    }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \return the wetting phase index
     * \param globalPos The global position
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::phase0Idx; }

    //! Updates the map of which material parameters are associated with a nodal dof.
    template<class SolutionVector>
    void updateMaterialInterfaceParams(const SolutionVector& x)
    {
        materialInterfaceParams_.update(this->fvGridGeometry(), *this, x);
    }

    //! Returns the material parameters associated with a nodal dof
    const BoxMaterialInterfaceParams<ThisType>& materialInterfaceParams() const
    { return materialInterfaceParams_; }

private:
    Scalar matrixPerm_;
    Scalar matrixPoro_;
    Scalar fracturePerm_;
    Scalar fracturePoro_;
    MaterialLawParams matrixMaterialParams_;
    MaterialLawParams fractureMaterialParams_;

    // Determines the parameters associated with the dofs at material interfaces
    BoxMaterialInterfaceParams<ThisType> materialInterfaceParams_;
};

} // end namespace Dumux

#endif
