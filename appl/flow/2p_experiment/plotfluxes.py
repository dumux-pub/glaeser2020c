import matplotlib.pyplot as plt
import numpy as np
import sys

# use plot style
sys.path.append("./../../common")
import plotstyles

prop_cycle = list(plt.rcParams['axes.prop_cycle'])

for i in [0, 1, 2, 3]:
    if i == 0:
        fileName = "./conduitcase_tpfa.csv"
        scheme = "TPFA"
    elif i == 1:
        fileName = "./conduitcase_box.csv"
        scheme = "BOX"
    elif i == 2:
        fileName = "./boxdfm/conduitcase_boxdfm.csv"
        scheme = "BOX-CONT"
    else:
        fileName = "./nofractures/conduitcase_nofractures_mpfa.csv"
        scheme = "no fractures"

    masses = np.loadtxt(fileName, delimiter=',')

    # define indices for the data (last one is no fracture case)
    if i != 3:
        timeIdx = 0
        matrixVolume = 1
        matrixGasMass = 2
        fractureVolume = 3
        fractureGasMass = 4
    else:
        timeIdx = 0
        matrixVolume = 1
        matrixGasMass = 2

    # compute contained/injected/produced gas
    containedGas = []
    injectedGas = []
    producedGas = []
    for idx in range(0, len(masses)):
        curGas = masses[idx, matrixGasMass]
        if i != 3:
            curGas += masses[idx, fractureGasMass]

        deltaT = masses[idx][timeIdx]
        if idx > 0:
            deltaT -= masses[idx-1][timeIdx]

        injGas = 5e-6*np.pi*0.1*0.1*deltaT
        if idx > 0:
            injGas += injectedGas[len(injectedGas)-1]

        containedGas.append(curGas)
        injectedGas.append(injGas)
        producedGas.append(injGas-curGas)

    plt.figure(1)

    plt.plot(masses[:, timeIdx], containedGas, color = prop_cycle[i]['color'], label=scheme)
    plt.plot(masses[:, timeIdx], producedGas, color = prop_cycle[i]['color'], alpha=0.75, linestyle='--')

    plt.xlabel(r'$t \,\, [\si{\hour}]$')
    plt.ylabel(r'$m_{H_2} \,\, [\si{\kilogram}]$')
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.xlim([0, 60])
    plt.ylim([0, 8e-6])
    plt.legend()

plt.savefig("massvstime.pdf", bbox_inches='tight')
