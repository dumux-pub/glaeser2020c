// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for the (d-1)-dimensional facet domain in the
 *        two-phase facet coupling test.
 */
#ifndef DUMUX_TWOP_EXPERIMENT_FACET_FLOW_PROBLEM_HH
#define DUMUX_TWOP_EXPERIMENT_FACET_FLOW_PROBLEM_HH

#include <dune/foamgrid/foamgrid.hh>

#include <dumux/material/fluidsystems/2pimmiscible.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/1pgas.hh>
#include <dumux/material/components/h2.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/tabulatedcomponent.hh>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>

#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>
#include <dumux/multidomain/facet/cellcentered/mpfa/properties.hh>
#include <dumux/multidomain/facet/box/properties.hh>

#include <dumux/discretization/method.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p/model.hh>

#include "spatialparams.hh"

namespace Dumux {
// forward declarations
template<class TypeTag> class TwoPFacetProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct TwoPFacet { using InheritsFrom = std::tuple<TwoP>; };
struct TwoPFacetTpfa { using InheritsFrom = std::tuple<TwoPFacet, CCTpfaModel>; };
struct TwoPFacetMpfa { using InheritsFrom = std::tuple<TwoPFacet, CCMpfaModel>; };
struct TwoPFacetBox { using InheritsFrom = std::tuple<TwoPFacet, BoxModel>; };

// type tags for the case of another lower-dimensional intersection domain
struct TwoPThreeDomainFacetTpfa { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, TwoPFacet>; };
struct TwoPThreeDomainFacetMpfa { using InheritsFrom = std::tuple<CCMpfaFacetCouplingModel, TwoPFacet>; };
struct TwoPThreeDomainFacetBox { using InheritsFrom = std::tuple<BoxFacetCouplingModel, TwoPFacet>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TwoPFacet> { using type = Dune::FoamGrid<2, 3>; };
// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::TwoPFacet> { using type = TwoPFacetProblem<TypeTag>; };
// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TwoPFacet>
{
private:
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

public:
    using type = TwoPSpatialParams<FVGridGeometry, Scalar>;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPFacet>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Hydrogen = Components::H2<Scalar>;
    using Water = Components::H2O<Scalar>;
    using TabulatedWater = Components::TabulatedComponent<Water>;
    using WaterPhase = FluidSystems::OnePLiquid<Scalar, TabulatedWater>;
    using GasPhase = FluidSystems::OnePGas<Scalar, Hydrogen>;

public:
    using type = Dumux::FluidSystems::TwoPImmiscible< Scalar, WaterPhase, GasPhase>;
};

// permeability is constant
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::TwoPFacet>
{ static constexpr bool value = false; };

} // end namespace Properties

/*!
 * \brief The problem for the (d-1)-dimensional facet domain in the
 *        two-phase facet coupling test.
 */
template<class TypeTag>
class TwoPFacetProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    static constexpr bool isBox = FVGridGeometry::discMethod == DiscretizationMethod::box;

public:
    TwoPFacetProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                     std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                     std::shared_ptr<CouplingManager> couplingManagerPtr,
                     const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManagerPtr)
    {
        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");

        aperture_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Aperture");
        extractionPressure_ = getParam<Scalar>("Problem.ExtractionPressure");
        injectionPhaseLength_ = getParam<Scalar>("Injection.InjectionPhaseLength");
        injectionRate_ = getParam<Scalar>("Injection.InjectionRate");

        // initialize the fluid system (tabulation)
        GetPropType<TypeTag, Properties::FluidSystem>::init(/*tMin*/263.15, /*tMax*/303.15, /*n*/100,
                           /*pMin*/extractionPressure_*0.9, /*pMax*/extractionPressure_*10.0, /*n*/100);
    }

    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    { return problemName_; }

    //! Specifies the kind of boundary condition at a boundary position.
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    /*!
     * \brief Specifies which kind of interior boundary condition should be
     *        used for which equation on a given sub-control volume face
     *        that couples to a facet element.
     *
     * \param element The finite element the scvf is embedded in
     * \param scvf The sub-control volume face
     */
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        static const bool useDirichlet = getParam<bool>("Problem.UseInteriorDirichlet");
        if (useDirichlet)
            values.setAllDirichlet();
        else
            values.setAllNeumann();

        return values;
    }

    //! evaluates the Neumann boundary condition for a given position
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        const auto globalPos = scvf.ipGlobal();

        if (isOnInlet_(globalPos) && time_ <= injectionPhaseLength_)
            return NumEqVector({0.0, -1.0*injectionRate_});
        return NumEqVector(0.0);
    }

    /*!
     * \brief Evaluates the source term for all phases within a given
     *        sub-control volume.
     */
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        // obtain the sources stemming from the bulk flow domain
        auto source = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);
        source /= scv.volume()*elemVolVars[scv].extrusionFactor();
        return source;
    }

    //! Sets the aperture as extrusion factor.
    template<class ElementSolution>
    Scalar extrusionFactor(const Element& element,
                           const SubControlVolume& scv,
                           const ElementSolution& elemSol) const
    { return aperture_; }

    //! Evaluates the initial conditions.
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables({extractionPressure_, 0.0}); }

    //! Evaluates the Dirichlet boundary conditions at a given position.
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables({extractionPressure_, 0.0}); }

    //! Returns the temperature \f$\mathrm{[K]}\f$ for an isothermal problem.
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! Returns const reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    //! set the time of the following time integration
    void setTime(Scalar t)
    { time_ = t; }

private:
    //! The inlet is on the bottom side of the domain
    bool isOnInlet_(const GlobalPosition& globalPos) const
    {
        static constexpr auto matrixFlowId = Dune::index_constant<0>();
        const auto zMin = couplingManager().problem(matrixFlowId).fvGridGeometry().bBoxMin()[2];
        return globalPos[2] < zMin + 1e-6;
    }

    //! The outlet is on the right side of the domain
    bool isOnOutlet_(const GlobalPosition& globalPos) const
    {
        static constexpr auto matrixFlowId = Dune::index_constant<0>();
        const auto zMax = couplingManager().problem(matrixFlowId).fvGridGeometry().bBoxMax()[2];
        return globalPos[2] > zMax - 1e-6;
    }

    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    std::string problemName_;

    Scalar aperture_;
    Scalar extractionPressure_;
    Scalar injectionPhaseLength_;
    Scalar injectionRate_;

    // keep track of time
    Scalar time_;
};

} // end namespace Dumux

#endif
