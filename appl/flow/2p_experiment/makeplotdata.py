import sys
import os

try:
    from paraview.simple import *
except ImportError:
    print("`paraview.simple` not found. Make sure using pvpython instead of python.")

# creates the data of a plot on a provided vtu file
if len(sys.argv) != 3:
    sys.stderr.write("Please provide vtu and csv file names")
    sys.exit(1)

vtuFileName = sys.argv[1]
csvFileName = sys.argv[2]

# define plots specifics
point1 = [0.0, 0.0, 0.0]
point2 = [0.0, 0.0, 0.3]
resolution = 1000

if not os.path.exists(vtuFileName):
    sys.stderr.write("Provided vtu file \"" + vtuFileName + "\" does not exist")
    sys.exit(1)

print("Reading solution file " + vtuFileName)
vtkFile = XMLUnstructuredGridReader(FileName=vtuFileName)
SetActiveSource(vtkFile)

plotOverLine1 = PlotOverLine(Source="High Resolution Line Source")
plotOverLine1.Source.Resolution = resolution
plotOverLine1.Source.Point1 = point1
plotOverLine1.Source.Point2 = point2

# write output
print("Writing csv file")
writer = CreateWriter(csvFileName, plotOverLine1, Precision=10)
writer.UpdatePipeline()
