// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for the two-phase test.
 */
#ifndef DUMUX_TWOP_EXPERIMENT_FLOW_PROBLEM_HH
#define DUMUX_TWOP_EXPERIMENT_FLOW_PROBLEM_HH

#include <dune/alugrid/grid.hh>

#include <dumux/material/fluidsystems/2pimmiscible.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/1pgas.hh>
#include <dumux/material/components/h2.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/tabulatedcomponent.hh>

#include <dumux/discretization/ccmpfa.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p/model.hh>

#include "../spatialparams.hh"

namespace Dumux {
// forward declarations
template<class TypeTag> class TwoPBulkProblem;

namespace Properties {
// create the type tag nodes
namespace TTag {
struct TwoPTest { using InheritsFrom = std::tuple<TwoP>; };
struct TwoPMpfa { using InheritsFrom = std::tuple<TwoPTest, CCMpfaModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TwoPTest> { using type = Dune::ALUGrid<3, 3, Dune::simplex, Dune::nonconforming>; };
// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::TwoPTest> { using type = TwoPBulkProblem<TypeTag>; };
// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TwoPTest>
{
private:
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

public:
    using type = TwoPSpatialParams<FVGridGeometry, Scalar>;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPTest>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Hydrogen = Components::H2<Scalar>;
    using Water = Components::H2O<Scalar>;
    using TabulatedWater = Components::TabulatedComponent<Water>;
    using WaterPhase = FluidSystems::OnePLiquid<Scalar, TabulatedWater>;
    using GasPhase = FluidSystems::OnePGas<Scalar, Hydrogen>;

public:
    using type = Dumux::FluidSystems::TwoPImmiscible< Scalar, WaterPhase, GasPhase>;
};

// Enable caches
template<class TypeTag>
struct EnableFVGridGeometryCache<TypeTag, TTag::TwoPTest>
{ static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::TwoPTest>
{ static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::TwoPTest>
{ static constexpr bool value = true; };

// solution-independent permeability
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::TwoPTest>
{ static constexpr bool value = false; };

} // end namespace Properties

/*!
 * \brief The problem for the two-phase test.
 */
template<class TypeTag>
class TwoPBulkProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;

public:
    //! The constructor
    TwoPBulkProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                    std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                    const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    {
        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");

        extractionPressure_ = getParam<Scalar>("Problem.ExtractionPressure");
        injectionPhaseLength_ = getParam<Scalar>("Injection.InjectionPhaseLength");
        injectionRate_ = getParam<Scalar>("Injection.InjectionRate");

        // initialize the fluid system (tabulation)
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        FluidSystem::init(/*tMin*/263.15, /*tMax*/283.15, /*n*/100,
                          /*pMin*/extractionPressure_*0.9, /*pMax*/extractionPressure_*10.0, /*n*/100);
    }

    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    { return problemName_; }

    //! Specifies the kind of boundary condition on a given boundary position.
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;

        values.setAllNeumann();
        if (isOnOutlet_(globalPos))
            values.setAllDirichlet();

        return values;
    }

    /*!
     * \brief Specifies which kind of interior boundary condition should be
     *        used for which equation on a given sub-control volume face
     *        that couples to a facet element.
     *
     * \param element The finite element the scvf is embedded in
     * \param scvf The sub-control volume face
     */
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    //! Specifies the Neumann BCs for a given position
    NumEqVector neumannAtPos(const GlobalPosition& globalPos) const
    {
        if (isOnInlet_(globalPos) && time_ <= injectionPhaseLength_)
            return NumEqVector({0.0, -1.0*injectionRate_});
        else
            return NumEqVector(0.0);
    }

    //! Evaluates the Dirichlet boundary conditions at a given position.
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables({extractionPressure_, 0.0}); }

    //! Evaluates the initial conditions.
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables({extractionPressure_, 0.0}); }

    //! Returns the temperature in \f$\mathrm{[K]}\f$ in the domain.
    Scalar temperature() const
    { return 283.15; /*10°C*/ }

    //! set the time of the following time integration
    void setTime(Scalar t)
    { time_ = t; }

    //! add data to the output file
    template<class SolutionVector>
    void writeToOutputFile(std::ofstream& outputFile,
                           const GridVariables& gridVariables,
                           const SolutionVector& x) const
    {
        Scalar volume = 0.0;
        Scalar massGas = 0.0;

        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            auto elemVolVars = localView(gridVariables.curGridVolVars());

            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);

            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& vv = elemVolVars[scv];
                volume += scv.volume()*vv.extrusionFactor();
                massGas += vv.saturation(1)*vv.density(1)*vv.porosity()*scv.volume()*vv.extrusionFactor();
            }
        }

        outputFile << time_ << "," << volume << "," << massGas << std::endl;
    }

private:
    //! The inlet is on the left side of the domain
    bool isOnInlet_(const GlobalPosition& globalPos) const
    { return globalPos[2] < this->fvGridGeometry().bBoxMin()[2] + 1e-6; }

    //! The outlet is on the right side of the domain
    bool isOnOutlet_(const GlobalPosition& globalPos) const
    { return globalPos[2] > this->fvGridGeometry().bBoxMax()[2] - 1e-6; }

    std::string problemName_;

    Scalar extractionPressure_;
    Scalar injectionPhaseLength_;
    Scalar injectionRate_;

    // keep track of time
    Scalar time_;
};

} // end namespace Dumux

#endif
