import matplotlib.pyplot as plt
import subprocess
import numpy as np
import sys
import os

# use plot style
sys.path.append("./../../common")
import plotstyles

prop_cycle = list(plt.rcParams['axes.prop_cycle'])

# return file name for i-th simulation
def getFileName(i):
    if i == 0:
        return "./conduitcase_tpfa.csv"
    elif i == 1:
        return "./conduitcase_box.csv"
    elif i == 2:
        return "./boxdfm/conduitcase_boxdfm.csv"
    else:
        return "./nofractures/conduitcase_nofractures_mpfa.csv"

# return scheme name for i-th simulation
def getSchemeName(i):
    if i == 0:
        return "TPFA"
    elif i == 1:
        return "BOX"
    elif i == 2:
        return "BOX-CONT"
    else:
        return "no fractures"

# parse data arrays
times = []
containedGases = []
injectedGases = []
producedGases = []

for i in [0, 1, 2, 3]:
    fileName = getFileName(i)
    scheme = getSchemeName(i)

    # load data
    masses = np.loadtxt(fileName, delimiter=',')

    # define indices for the data (last one is no fracture case)
    timeIdx = 0
    matrixGasMass = 2
    fractureGasMass = 4

    # compute contained/injected/produced gas
    time = []
    containedGas = []
    injectedGas = []
    producedGas = []
    for idx in range(0, len(masses)):
        curGas = masses[idx, matrixGasMass]
        if i != 3:
            curGas += masses[idx, fractureGasMass]

        deltaT = masses[idx][timeIdx]
        if idx > 0:
            deltaT -= masses[idx-1][timeIdx]

        injGas = 5e-6*np.pi*0.1*0.1*deltaT
        if idx > 0:
            injGas += injectedGas[len(injectedGas)-1]

        time.append(masses[idx][timeIdx])
        containedGas.append(curGas)
        injectedGas.append(injGas)
        producedGas.append(injGas-curGas)

    times.append(time)
    containedGases.append(containedGas)
    injectedGases.append(injectedGas)
    producedGases.append(producedGas)

# make animation from the data
if os.path.exists('tmp'):
    print "deleting previous temporary folder"
    subprocess.call('rm -rf tmp/', shell=True)

print "creating temporary folder"
subprocess.call(['mkdir', 'tmp'])

print "creating frames"
start = 0.0
end = 60.0
frameTimes = np.linspace(start, end, num=31, endpoint=True)

for t in frameTimes:
    plt.clf()
    for i in range(0, 4):
        time = []
        contained = []
        produced = []
        for idx in range(0, len(times[i])):
            if times[i][idx] <= t or (t == end and times[i][idx-1] < end):
                time.append(times[i][idx])
                contained.append(containedGases[i][idx])
                produced.append(producedGases[i][idx])

        plt.plot(time, contained, color = prop_cycle[i]['color'], label=getSchemeName(i))
        plt.plot(time, produced, color = prop_cycle[i]['color'], alpha=0.75, linestyle='--')
        plt.xlabel(r'$t \,\, [\si{\hour}]$')
        plt.ylabel(r'$m_{H_2} \,\, [\si{\kilogram}]$')
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
        plt.xlim([0, 60])
        plt.ylim([0, 8e-6])
        plt.legend(loc='upper left')

    plt.savefig('./tmp/tmp-{:02d}.png'.format(int(t)), dpi=300, bbox_inches='tight')

# make animation using imagemagick
print "creating animation"
subprocess.call(['convert', '-delay', '10', '-loop', '3', 'tmp/tmp*', 'massvstime.gif'])

print "deleting temporary folder"
subprocess.call('rm -rf tmp/', shell=True)
