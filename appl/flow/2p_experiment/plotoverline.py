import matplotlib.pyplot as plt
import subprocess as sp
import numpy as np
import sys
import os

# use plot style
sys.path.append("./../../common")
import plotstyles

# define stuff for the sequence
start = 0.0
end = 60.0
numFrames = 31
frameTimes = np.linspace(start, end, num=numFrames, endpoint=True)

def getDataArrayFileName(schemeIdx):
    if schemeIdx == 0:
        return "conduitcase_tpfa.csv"
    elif schemeIdx == 1:
        return "conduitcase_box.csv"
    elif schemeIdx == 2:
        return "boxdfm/conduitcase_boxdfm.csv"
    elif schemeIdx == 3:
        return "nofractures/conduitcase_nofractures_mpfa.csv"
    else:
        sys.stderr.write("Invalid scheme idx")
        sys.exit(1)

def getFormattedIdx(timeStepIdx):
    return "{:05d}".format(timeStepIdx)

def getVtuFileName(schemeIdx, timeStepIdx):
    idx = getFormattedIdx(timeStepIdx)
    if schemeIdx == 0:
        return "test_2p_experiment_conduitcase_bulk_tpfa-" + idx + ".vtu"
    elif schemeIdx == 1:
        return "test_2p_experiment_conduitcase_bulk_box-" + idx + ".vtu"
    elif schemeIdx == 2:
        return "boxdfm/test_2p_experiment_boxdfm-" + idx + ".vtu"
    elif schemeIdx == 3:
        return "nofractures/test_2p_experiment_conduitcase_nofractures_mpfa-" + idx + ".vtu"
    else:
        sys.stderr.write("Invalid scheme idx")
        sys.exit(1)

def getSchemeLabel(schemeIdx):
    if i == 0:
        return "TPFA"
    elif i == 1:
        return "BOX"
    elif i == 2:
        return "BOX-CONT"
    elif i == 3:
        return "no fractures"
    else:
        sys.stderr.write("Invalid scheme idx")
        sys.exit(1)

# read data of all schemes (to get time steps)
data = []
data.append(np.loadtxt(getDataArrayFileName(0), delimiter=','))
data.append(np.loadtxt(getDataArrayFileName(1), delimiter=','))
data.append(np.loadtxt(getDataArrayFileName(2), delimiter=','))
data.append(np.loadtxt(getDataArrayFileName(3), delimiter=','))

snIdx = 0
pwIdx = 5

if not os.path.exists("plots"):
    sp.call(['mkdir', 'plots'])

for frameIdx in range(0, len(frameTimes)):
    t = frameTimes[frameIdx]

    plt.clf()

    # plot this time step
    for i in range(0, 4):
        if t == 0:
            sp.call(['pvpython', 'makeplotdata.py', getVtuFileName(i, 0), "tmp.csv"])
        else:
            for j in range(0, len(data[i])):
                tCur = data[i][j][0]
                if tCur == t or (j < len(data[i])-1 and tCur < t and data[i][j+1][0] > t):
                    sp.call(['pvpython', 'makeplotdata.py', getVtuFileName(i, j), "tmp.csv"])
                    break

        schemePlotData = np.loadtxt("tmp.csv", skiprows=1, delimiter=',')
        zIdx = len(schemePlotData[0])-1

        plt.figure(1)
        plt.plot(schemePlotData[:,zIdx], schemePlotData[:,pwIdx]/1e5, label=getSchemeLabel(i))
        plt.xlabel(r"$z \,\, \left[ \si{\meter} \right]$")
        plt.ylabel(r"$p_{H_2O} \,\, \left[ \si{\bar} \right]$")
        plt.ylim([1, 2.1])
        plt.legend()

        plt.figure(2)
        plt.plot(schemePlotData[:,zIdx], schemePlotData[:,snIdx], label=getSchemeLabel(i))
        plt.xlabel(r"$z \,\, \left[ \si{\meter} \right]$")
        plt.ylabel(r"$S_{H_2} \,\, \left[-\right]$")
        plt.ylim([0.0, 0.25])
        plt.legend()

        sp.call(['rm', 'tmp.csv'])

    plt.figure(1)
    plt.savefig("plots/pw-" + getFormattedIdx(frameIdx), bbox_inches='tight', dpi=300)
    plt.clf()
    plt.figure(2)
    plt.savefig("plots/sn-" + getFormattedIdx(frameIdx), bbox_inches='tight', dpi=300)
    plt.clf()

sp.call('convert -delay 12 -loop 3 plots/pw-* pw_conduit.gif', shell=True)
sp.call('convert -delay 12 -loop 3 plots/sn-* sn_conduit.gif', shell=True)
