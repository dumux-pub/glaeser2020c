// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for the lower-dimensional domain in the single-phase
 *        test mimicking a NAGRA experiment using the facet coupling models.
 */
#ifndef DUMUX_ONEP_NAGRAEXPERIMENT_FACET_EDGE_PROBLEM_HH
#define DUMUX_ONEP_NAGRAEXPERIMENT_FACET_EDGE_PROBLEM_HH

#include <dune/foamgrid/foamgrid.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/multilineargeometry.hh>

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>

#include <dumux/multidomain/facet/box/properties.hh>
#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>
#include <dumux/multidomain/facet/cellcentered/mpfa/properties.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/1p/model.hh>

#include "spatialparams.hh"

namespace Dumux {
// forward declarations
template<class TypeTag> class OnePEdgeProblem;

namespace Properties {
namespace TTag {

// create the type tag nodes
struct OnePEdge { using InheritsFrom = std::tuple<OneP>; };
struct OnePEdgeTpfa { using InheritsFrom = std::tuple<OnePEdge, CCTpfaModel>; };
struct OnePEdgeMpfa { using InheritsFrom = std::tuple<OnePEdge, CCTpfaModel>; };
struct OnePEdgeBox { using InheritsFrom = std::tuple<OnePEdge, BoxModel>; };

} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePEdge> { using type = Dune::FoamGrid<1, 3>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::OnePEdge> { using type = OnePEdgeProblem<TypeTag>; };

// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePEdge>
{
private:
    using FVGridGeometry = GetPropType<TypeTag, FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Scalar>;

public:
    using type = OnePSpatialParams< FVGridGeometry, Scalar >;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePEdge>
{
private:
    using Scalar = GetPropType<TypeTag, Scalar>;

public:
    using type = FluidSystems::OnePLiquid< Scalar, Components::SimpleH2O< Scalar> >;
};

// permeability is constant
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::OnePEdge> { static constexpr bool value = false; };

} // end namespace Properties

/*!
 * \brief The problem for the lower-dimensional domain in the single-phase
 *        test mimicking a NAGRA experiment using the facet coupling models.
 */
template<class TypeTag>
class OnePEdgeProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr bool isBox = FVGridGeometry::discMethod == DiscretizationMethod::box;

public:
    OnePEdgeProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                    std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                    std::shared_ptr<CouplingManager> couplingManager,
                    const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManager)
    , crossSectionArea_(getParamFromGroup<Scalar>(paramGroup, "Problem.CrossSectionArea"))
    {}

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        if ( !isBox && (isOnInlet(globalPos) || isOnOutlet(globalPos)))
            values.setAllDirichlet();

        return values;
    }

    //! Evaluate the source term at a given position
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        // evaluate sources from bulk domain
        auto source = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);
        source /= scv.volume()*elemVolVars[scv].extrusionFactor();
        return source;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        static const Scalar deltaP = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.DeltaP");
        if (isOnInlet(globalPos))
            return initialAtPos(globalPos) + PrimaryVariables(deltaP);
        return initialAtPos(globalPos);
    }

    //! Evaluate the Neumann boundary conditions
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        // for box, incorporate Dirichlet Bcs weakly
        if (isBox && (isOnOutlet(scvf.ipGlobal()) || isOnInlet(scvf.ipGlobal())))
        {
            // modify element solution to carry inlet/outlet head
            auto elemSol = elementSolution(element, elemVolVars, fvGeometry);
            for (const auto& curScvf : scvfs(fvGeometry))
            {
                if (curScvf.boundary())
                {
                    if (isOnOutlet(curScvf.ipGlobal()) || isOnInlet(curScvf.ipGlobal()))
                    {
                        const auto diriValues = dirichletAtPos(curScvf.ipGlobal());
                        const auto& insideScv = fvGeometry.scv(curScvf.insideScvIdx());
                        elemSol[insideScv.localDofIndex()][0] = diriValues[0];
                    }
                }
            }

            // evaluate gradients using this element solution
            const auto gradHead = evalGradients(element,
                                                element.geometry(),
                                                fvGeometry.fvGridGeometry(),
                                                elemSol,
                                                scvf.ipGlobal())[0];

            // compute the flux
            const auto& volVars = elemVolVars[scvf.insideScvIdx()];
            Scalar flux = -1.0*vtmv(gradHead, volVars.permeability(), scvf.unitOuterNormal());
            flux *= volVars.density()*volVars.mobility();
            return NumEqVector(flux);
        }

        return NumEqVector(0.0);
    }

    //! Set the aperture as extrusion factor.
    Scalar extrusionFactorAtPos(const GlobalPosition& globalPos) const
    { return crossSectionArea_; }

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    //! returns true if position is on inlet
    bool isOnInlet(const GlobalPosition& globalPos) const
    {
        static constexpr auto bulkId = Dune::index_constant<0>();
        const auto& bulkFvGridGeometry = couplingManager().problem(bulkId).fvGridGeometry();
        return globalPos[2] < bulkFvGridGeometry.bBoxMin()[2] + 1e-6;
    }

    //! returns true if position is on outlet
    bool isOnOutlet(const GlobalPosition& globalPos) const
    {
        static constexpr auto bulkId = Dune::index_constant<0>();
        const auto& bulkFvGridGeometry = couplingManager().problem(bulkId).fvGridGeometry();
        return globalPos[2] > bulkFvGridGeometry.bBoxMax()[2] - 1e-6;
    }

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    Scalar crossSectionArea_;
};

} // end namespace Dumux

#endif
