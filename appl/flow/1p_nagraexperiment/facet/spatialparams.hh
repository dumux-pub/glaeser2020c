// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The spatial parameters for the bulk domain in the single-phase
 *        real case test using the facet coupling models.
 */
#ifndef DUMUX_ONEP_REAL_SPATIALPARAMS_HH
#define DUMUX_ONEP_REAL_SPATIALPARAMS_HH

#include <cmath>
#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux {

/*!
 * \brief The spatial parameters for the bulk domain in the single-phase
 *        tensorial test using the facet coupling models.
 */
template< class FVGridGeometry, class Scalar >
class OnePSpatialParams
: public FVSpatialParamsOneP< FVGridGeometry, Scalar, OnePSpatialParams<FVGridGeometry, Scalar> >
{
    using ThisType = OnePSpatialParams< FVGridGeometry, Scalar >;
    using ParentType = FVSpatialParamsOneP< FVGridGeometry, Scalar, ThisType >;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    //! export the type used for permeabilities
    using PermeabilityType = Scalar;

    //! the constructor
    OnePSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry, const std::string& paramGroup)
    : ParentType(fvGridGeometry)
    , permeability_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability"))
    {}

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    { return permeability_; }

    //! Return the porosity
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return 1.0; }

private:
    PermeabilityType permeability_;
};

} // end namespace Dumux

#endif
