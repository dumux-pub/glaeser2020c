// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief One-phase "real" test case using facet coupling models
 */
#include <config.h>
#include <iostream>
#include <limits>
#include <algorithm>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/type.hh>
#include <dune/istl/matrixmarket.hh>

#include "bulkproblem.hh"
#include "lowdimproblem.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/geometry/diameter.hh>

#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/evalsolution.hh>
#include <dumux/linear/seqsolverbackend.hh>

#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/traits.hh>

#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>
#include <dumux/multidomain/facet/codimonegridadapter.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <appl/common/computevelocities.hh>

// updates the finite volume grid geometry. This is necessary as the finite volume
// grid geometry for the box scheme with facet coupling requires additional data for
// the update. The reason is that we have to create additional faces on interior
// boundaries, which wouldn't be created in the standard scheme.
template< class FVGridGeometry,
       class GridManager,
       class LowDimGridView,
       std::enable_if_t<FVGridGeometry::discMethod == Dumux::DiscretizationMethod::box, int> = 0 >
void updateBulkFVGridGeometry(FVGridGeometry& fvGridGeometry,
                           const GridManager& gridManager,
                           const LowDimGridView& lowDimGridView)
{
    using BulkFacetGridAdapter = Dumux::CodimOneGridAdapter<typename GridManager::Embeddings>;
    BulkFacetGridAdapter facetGridAdapter(gridManager.getEmbeddings());
    fvGridGeometry.update(lowDimGridView, facetGridAdapter);
}

// specialization for cell-centered schemes
template< class FVGridGeometry,
       class GridManager,
       class LowDimGridView,
       std::enable_if_t<FVGridGeometry::discMethod != Dumux::DiscretizationMethod::box, int> = 0 >
void updateBulkFVGridGeometry(FVGridGeometry& fvGridGeometry,
                           const GridManager& gridManager,
                           const LowDimGridView& lowDimGridView)
{
    fvGridGeometry.update();
}

//! Function to compute the outflux across the right boundary
template<class TypeTag, std::size_t id, class CM, class A, class P, class GV, class Sol>
std::array<double, 4> computeInAndOutflow(Dune::index_constant<id> domainId,
                                          const A& assembler,
                                          CM& couplingManager,
                                          const P& problem,
                                          const GV& gridVariables,
                                          const Sol& x)
{
    using GridGeometry = std::decay_t<decltype(problem.fvGridGeometry())>;
    static constexpr bool isBox = GridGeometry::discMethod == Dumux::DiscretizationMethod::box;

    auto upwindTerm = [] (const auto& volVars) { return 1.0; };
    std::array<double, 4> result; std::fill(result.begin(), result.end(), 0.0);
    for (const auto& element : elements(problem.fvGridGeometry().gridView()))
    {
        if (!element.hasBoundaryIntersections())
            continue;

        auto fvGeometry = localView(problem.fvGridGeometry());
        auto elemVolVars = localView(gridVariables.curGridVolVars());
        auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());

        couplingManager.bindCouplingContext(domainId, element, assembler);
        fvGeometry.bind(element);
        elemVolVars.bind(element, fvGeometry, x);
        elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

        using FluxVariables = Dumux::GetPropType<TypeTag, Dumux::Properties::FluxVariables>;
        for (const auto& scvf : scvfs(fvGeometry))
        {
            if (!scvf.boundary())
                continue;

            const auto isOutlet = problem.isOnOutlet(scvf.ipGlobal());
            const auto isInlet = problem.isOnInlet(scvf.ipGlobal());
            if (isInlet || isOutlet)
            {
                if (!isBox)
                {
                    FluxVariables fluxVars;
                    fluxVars.init(problem, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
                    if (isInlet) { result[0] += fluxVars.advectiveFlux(/*phaseIdx*/0, upwindTerm); result[2] += scvf.area()*elemVolVars[scvf.insideScvIdx()].extrusionFactor(); }
                    else { result[1] += fluxVars.advectiveFlux(/*phaseIdx*/0, upwindTerm); result[3] += scvf.area()*elemVolVars[scvf.insideScvIdx()].extrusionFactor(); }
                }
                else
                {
                    const auto bcTypes = problem.boundaryTypes(element, fvGeometry.scv(scvf.insideScvIdx()));
                    if (!bcTypes.hasOnlyNeumann())
                        DUNE_THROW(Dune::InvalidStateException, "Expected boundary fluxes to be incorporated weakly!");

                    // obtain neumann fluxes (scale with density to get volume flux)
                    auto scvfFlux = problem.neumann(element, fvGeometry, elemVolVars, scvf)[0];
                    scvfFlux *= elemVolVars[scvf.insideScvIdx()].extrusionFactor();
                    scvfFlux /= elemVolVars[scvf.insideScvIdx()].density();
                    scvfFlux /= elemVolVars[scvf.insideScvIdx()].mobility();
                    scvfFlux *= scvf.area();

                    if (isInlet) { result[0] += scvfFlux; result[2] += scvf.area()*elemVolVars[scvf.insideScvIdx()].extrusionFactor(); }
                    else { result[1] += scvfFlux; result[3] += scvf.area()*elemVolVars[scvf.insideScvIdx()].extrusionFactor(); }
                }
            }
        }
    }

    return result;
}

// obtain/define some types to be used below in the property definitions and in main
template< class BulkTypeTag, class LowDimTypeTag >
class TestTraits
{
    using BulkFVGridGeometry = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::FVGridGeometry>;
    using LowDimFVGridGeometry = Dumux::GetPropType<LowDimTypeTag, Dumux::Properties::FVGridGeometry>;
public:
    using MDTraits = Dumux::MultiDomainTraits<BulkTypeTag, LowDimTypeTag>;
    using CouplingMapper = Dumux::FacetCouplingMapper<BulkFVGridGeometry, LowDimFVGridGeometry>;
    using CouplingManager = Dumux::FacetCouplingManager<MDTraits, CouplingMapper>;
};

// set the coupling manager property for all available type tags
namespace Dumux {
namespace Properties {

// set cm property for the tests
using BoxTraits = TestTraits<TTag::OnePBulkBox, TTag::OnePLowDimBox>;
using TpfaTraits = TestTraits<TTag::OnePBulkTpfa, TTag::OnePLowDimTpfa>;
using MpfaTraits = TestTraits<TTag::OnePBulkMpfa, TTag::OnePLowDimMpfa>;

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePBulkBox> { using type = typename BoxTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePLowDimBox> { using type = typename BoxTraits::CouplingManager; };

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePBulkTpfa> { using type = typename TpfaTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePLowDimTpfa> { using type = typename TpfaTraits::CouplingManager; };

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePBulkMpfa> { using type = typename MpfaTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePLowDimMpfa> { using type = typename MpfaTraits::CouplingManager; };

} // end namespace Properties
} // end namespace Dumux

// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // initialize parameter tree
    Parameters::init(argc, argv);

    // get type tags from CMakeLists
    using BulkProblemTypeTag = Dumux::Properties::TTag::BULKTYPETAG;     // (must be declared in CMakeLists.txt)
    using LowDimProblemTypeTag = Dumux::Properties::TTag::LOWDIMTYPETAG; // (must be declared in CMakeLists.txt)

    //////////////////////////////////////////////////////
    // try to create the grids (from the given grid file)
    //////////////////////////////////////////////////////
    using BulkGrid = Dumux::GetPropType<BulkProblemTypeTag,  Dumux::Properties::Grid>;
    using LowDimGrid = Dumux::GetPropType<LowDimProblemTypeTag,  Dumux::Properties::Grid>;

    Dumux::FacetCouplingGridManager<BulkGrid, LowDimGrid> gridManager;
    gridManager.init();
    gridManager.loadBalance();

    ////////////////////////////////////////////////////////////
    // run stationary, non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid views
    const auto& bulkGridView = gridManager.template grid<0>().leafGridView();
    const auto& lowDimGridView = gridManager.template grid<1>().leafGridView();

    // create the finite volume grid geometries
    using BulkFVGridGeometry = GetPropType<BulkProblemTypeTag, Properties::FVGridGeometry>;
    using LowDimFVGridGeometry = GetPropType<LowDimProblemTypeTag, Properties::FVGridGeometry>;
    auto bulkFvGridGeometry = std::make_shared<BulkFVGridGeometry>(bulkGridView);
    auto lowDimFvGridGeometry = std::make_shared<LowDimFVGridGeometry>(lowDimGridView);
    updateBulkFVGridGeometry(*bulkFvGridGeometry, gridManager, lowDimGridView);
    lowDimFvGridGeometry->update();

    // the coupling mapper
    using TestTraits = TestTraits<BulkProblemTypeTag, LowDimProblemTypeTag>;
    auto couplingMapper = std::make_shared<typename TestTraits::CouplingMapper>();
    couplingMapper->update(*bulkFvGridGeometry, *lowDimFvGridGeometry, gridManager.getEmbeddings());

    // the coupling manager
    using CouplingManager = typename TestTraits::CouplingManager;
    auto couplingManager = std::make_shared<CouplingManager>();

    // the problems (boundary conditions)
    using BulkProblem = GetPropType<BulkProblemTypeTag, Properties::Problem>;
    using LowDimProblem = GetPropType<LowDimProblemTypeTag, Properties::Problem>;
    auto bulkSpatialParams = std::make_shared<typename BulkProblem::SpatialParams>(bulkFvGridGeometry, "Bulk");
    auto bulkProblem = std::make_shared<BulkProblem>(bulkFvGridGeometry, bulkSpatialParams, couplingManager, "Bulk");
    auto lowDimSpatialParams = std::make_shared<typename LowDimProblem::SpatialParams>(lowDimFvGridGeometry, "LowDim");
    auto lowDimProblem = std::make_shared<LowDimProblem>(lowDimFvGridGeometry, lowDimSpatialParams, couplingManager, "LowDim");

    // the solution vector
    using MDTraits = typename TestTraits::MDTraits;
    using SolutionVector = typename MDTraits::SolutionVector;
    SolutionVector x;

    static const auto bulkId = typename MDTraits::template SubDomain<0>::Index();
    static const auto lowDimId = typename MDTraits::template SubDomain<1>::Index();
    x[bulkId].resize(bulkFvGridGeometry->numDofs());
    x[lowDimId].resize(lowDimFvGridGeometry->numDofs());
    bulkProblem->applyInitialSolution(x[bulkId]);
    lowDimProblem->applyInitialSolution(x[lowDimId]);

    // initialize coupling manager
    couplingManager->init(bulkProblem, lowDimProblem, couplingMapper, x);

    // the grid variables
    using BulkGridVariables = GetPropType<BulkProblemTypeTag, Properties::GridVariables>;
    using LowDimGridVariables = GetPropType<LowDimProblemTypeTag, Properties::GridVariables>;
    auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkProblem, bulkFvGridGeometry);
    auto lowDimGridVariables = std::make_shared<LowDimGridVariables>(lowDimProblem, lowDimFvGridGeometry);
    bulkGridVariables->init(x[bulkId]);
    lowDimGridVariables->init(x[lowDimId]);

    // intialize the vtk output module
    using BulkVtkWriter = VtkOutputModule<BulkGridVariables, GetPropType<BulkProblemTypeTag, Properties::SolutionVector>>;
    using LowDimVtkWriter = VtkOutputModule<LowDimGridVariables, GetPropType<LowDimProblemTypeTag, Properties::SolutionVector>>;
    const auto bulkDM = BulkFVGridGeometry::discMethod == DiscretizationMethod::box ? Dune::VTK::nonconforming : Dune::VTK::conforming;
    BulkVtkWriter bulkVtkWriter(*bulkGridVariables, x[bulkId], bulkProblem->name(), "Bulk", bulkDM);
    LowDimVtkWriter lowDimVtkWriter(*lowDimGridVariables, x[lowDimId], lowDimProblem->name(), "LowDim");

    // Add model specific output fields
    using BulkVtkOutputFields = GetPropType<BulkProblemTypeTag, Properties::VtkOutputFields>;
    using LowDimVtkOutputFields = GetPropType<LowDimProblemTypeTag, Properties::VtkOutputFields>;
    BulkVtkOutputFields::initOutputModule(bulkVtkWriter);
    LowDimVtkOutputFields::initOutputModule(lowDimVtkWriter);


    // the assembler
    using Assembler = MultiDomainFVAssembler<MDTraits, CouplingManager, DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>( std::make_tuple(bulkProblem, lowDimProblem),
                                                  std::make_tuple(bulkFvGridGeometry, lowDimFvGridGeometry),
                                                  std::make_tuple(bulkGridVariables, lowDimGridVariables),
                                                  couplingManager);

    // add velocities
    using VelocityVector = std::vector< Dune::FieldVector<double, 3> >;
    VelocityVector bulkVelocities, lowDimVelocities;

    // write initial solution
    Dumux::computeSinglePhaseVelocities<BulkProblemTypeTag>(bulkId, *assembler, *couplingManager, *bulkFvGridGeometry, *bulkGridVariables, x[bulkId], bulkVelocities);
    Dumux::computeSinglePhaseVelocities<LowDimProblemTypeTag>(lowDimId, *assembler, *couplingManager, *lowDimFvGridGeometry, *lowDimGridVariables, x[lowDimId], lowDimVelocities);
    bulkVtkWriter.addField(bulkVelocities, "velocity", BulkVtkWriter::FieldType::element);
    lowDimVtkWriter.addField(lowDimVelocities, "velocity", LowDimVtkWriter::FieldType::element);

    bulkVtkWriter.write(0.0);
    lowDimVtkWriter.write(0.0);

    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);
    newtonSolver->solve(x);

    // update grid variables for output
    bulkGridVariables->update(x[bulkId]);
    lowDimGridVariables->update(x[lowDimId]);
    Dumux::computeSinglePhaseVelocities<BulkProblemTypeTag>(bulkId, *assembler, *couplingManager, *bulkFvGridGeometry, *bulkGridVariables, x[bulkId], bulkVelocities);
    Dumux::computeSinglePhaseVelocities<LowDimProblemTypeTag>(lowDimId, *assembler, *couplingManager, *lowDimFvGridGeometry, *lowDimGridVariables, x[lowDimId], lowDimVelocities);

    // write vtk output
    bulkVtkWriter.write(1.0);
    lowDimVtkWriter.write(1.0);

    // compute in & outflows
    const auto inAndOutflowBulk = computeInAndOutflow<BulkProblemTypeTag>(bulkId, *assembler, *couplingManager, *bulkProblem, *bulkGridVariables, x[bulkId]);
    const auto inAndOutflowLowDim = computeInAndOutflow<LowDimProblemTypeTag>(lowDimId, *assembler, *couplingManager, *lowDimProblem, *lowDimGridVariables, x[lowDimId]);
    const auto avgInOutletArea = 0.5*(inAndOutflowBulk[2]+inAndOutflowBulk[3]+inAndOutflowLowDim[2]+inAndOutflowLowDim[3]);
    std::cout << "Average area: " << avgInOutletArea << std::endl;
    std::cout << "Influxes:  " << inAndOutflowBulk[0] << ", " << inAndOutflowLowDim[0] << ", " << inAndOutflowBulk[0]+inAndOutflowLowDim[0] << std::endl;
    std::cout << "Outfluxes: " << inAndOutflowBulk[1] << ", " << inAndOutflowLowDim[1] << ", " << inAndOutflowBulk[1]+inAndOutflowLowDim[1] << std::endl;
    std::cout << "Effective permeability: " << (inAndOutflowBulk[1]+inAndOutflowLowDim[1])*0.025/avgInOutletArea/getParam<double>("Problem.DeltaP") << std::endl;

    std::ofstream permFile(getParam<std::string>("EffectivePermeability.OutputFile"), std::ios::app);
    permFile << (inAndOutflowBulk[1]+inAndOutflowLowDim[1])*0.025/avgInOutletArea/getParam<double>("Problem.DeltaP");
    if (getParam<bool>("EffectivePermeability.WriteDelimiter")) permFile << ",";

    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
