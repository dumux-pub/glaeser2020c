import os
import sys
import matplotlib.pyplot as plt
import subprocess
import numpy as np
from utility import *

# use plot style
sys.path.append("./../../common")
import plotstyles

# defines the array of apertures to consider
apertures = np.logspace(-8, -3, 2, endpoint=True);
apertures_is = []
for a in apertures:
    if a > 9e-6:
        apertures_is.append(a)

# function to run a scheme for one aperture
def runScheme(schemeAcronym, schemeSubFolder, apertureIdx, aperture, outputFileName, writeDelimiter=True):

    fractureK = aperture*aperture/12
    crossSection = aperture*aperture
    crossSectionK = crossSection*aperture/12

    exeName = "test_1p_nagraexperiment_" + schemeAcronym
    print("\n\nCalling " + exeName)
    print(" With a, epsilon, k_f & k_e: ", aperture, crossSection, fractureK, crossSectionK)
    subprocess.call([schemeSubFolder + "/" + exeName,
                     '-EffectivePermeability.OutputFile', outputFileName,
                     '-Problem.Name', schemeAcronym + "_" + str(apertureIdx),
                     '-Bulk.Problem.Name', schemeAcronym + "_bulk_" + str(apertureIdx),
                     '-LowDim.Problem.ExtrusionFactor', str(aperture),
                     '-LowDim.SpatialParams.Permeability', str(fractureK),
                     '-LowDim.Problem.Name', schemeAcronym + "_facet_" + str(apertureIdx),
                     '-Edge.Problem.CrossSectionArea', str(crossSection),
                     '-Edge.SpatialParams.Permeability', str(crossSectionK),
                     '-Edge.Problem.Name', schemeAcronym + "_edge_" + str(apertureIdx),
                     '-EffectivePermeability.WriteDelimiter', str(writeDelimiter)])


for schemeIdx in range(0, numSchemes):

    outputFileName = "effk_" + getSchemeAcronym(schemeIdx) + ".csv"
    if os.path.isfile(outputFileName):
        print("Found and removed previous permeability data for scheme " + getSchemeAcronym(schemeIdx))
        subprocess.call(['rm', outputFileName])

    for i in range(0, len(apertures)):
        if i  == len(apertures)-1:
            runScheme(getSchemeAcronym(schemeIdx), getSchemeSubFolder(schemeIdx), i, apertures[i], outputFileName, False)
        else:
            runScheme(getSchemeAcronym(schemeIdx), getSchemeSubFolder(schemeIdx), i, apertures[i], outputFileName)
    k_scheme = np.loadtxt(outputFileName, delimiter=',')

    plt.figure(schemeIdx)
    plt.semilogx(apertures, k_scheme, label=getSchemeLabel(schemeIdx))
    plt.xlabel(r"$a \,\, \left[ \si{\meter} \right]$")
    plt.ylabel(r"$k_{\mathrm{eff}} \,\, \left[ \si{\meter^2} \right]$")

    # (maybe) compute effective permeability also for the case of no fractures present
    if (hasIntersectionCase(schemeIdx) == True):
        outputFileName = "effk_nofractures_" + getSchemeAcronym(schemeIdx) + ".csv"
        if os.path.isfile(outputFileName):
            print("Found and removed previous permeability data for scheme nofractures_" + getSchemeAcronym(schemeIdx))
            subprocess.call(['rm', outputFileName])

        runScheme("nofractures_" + getSchemeAcronym(schemeIdx), "nofractures", 0, apertures[0], outputFileName, False)
        k_nofrac = np.loadtxt(outputFileName, delimiter=',')
        k_nofrac_array = []
        for j in range(0, len(apertures)):
            k_nofrac_array.append(k_nofrac)
        plt.semilogx(apertures, k_nofrac_array, label=getSchemeLabel(schemeIdx) + " (no fractures)", color='k', alpha = 0.5)

    # (maybe) compute eff K also for intersection flow being considered
    if (hasIntersectionCase(schemeIdx) == True):
        outputFileName = "effk_" + getIntersectionSchemeAcronym(schemeIdx) + ".csv"
        if os.path.isfile(outputFileName):
            print("Found and removed previous permeability data for scheme " + getIntersectionSchemeAcronym(schemeIdx))
            subprocess.call(['rm', outputFileName])

        for i in range(0, len(apertures_is)):
            if i  == len(apertures_is)-1:
                runScheme(getIntersectionSchemeAcronym(schemeIdx), getSchemeSubFolder(schemeIdx), i, apertures_is[i], outputFileName, False)
            else:
                runScheme(getIntersectionSchemeAcronym(schemeIdx), getSchemeSubFolder(schemeIdx), i, apertures_is[i], outputFileName)
        k_scheme_is = np.loadtxt(outputFileName, delimiter=',')

        plt.semilogx(apertures_is, k_scheme_is, label=getIntersectionSchemeLabel(schemeIdx))


    if not os.path.exists("plots"):
        subprocess.call(['mkdir', 'plots'])

    plt.legend()
    plt.savefig("plots/" + getSchemeAcronym(schemeIdx) + ".pdf", bbox_inches='tight')
