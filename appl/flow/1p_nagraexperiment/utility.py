import math

# considered schemes are tpfa, mpfa, box, boxdfm
numSchemes = 4

# some stuff used by plot functions
lineStyleArray = ["-","--","-.",":"]

# returns a string with the scheme acronym being given an index
def getSchemeAcronym(schemeIdx):
    if schemeIdx == 0:
        return "tpfa"
    elif schemeIdx == 1:
        return "mpfa"
    elif schemeIdx == 2:
        return "box"
    elif schemeIdx == 3:
        return "boxdfm"

# returns true if scheme has a variant with intersections
def hasIntersectionCase(schemeIdx):
    if schemeIdx < 3:
        return True
    else:
        return False

# returns the acronym for the case with intersections
def getIntersectionSchemeAcronym(schemeIdx):
    if schemeIdx == 0:
        return "is_tpfa"
    elif schemeIdx == 1:
        return "is_mpfa"
    elif schemeIdx == 2:
        return "is_box"

# returns a string with the plot label used for a scheme with intersections
def getIntersectionSchemeLabel(schemeIdx):
    if schemeIdx  == 0:
        return "TPFA (with is)"
    elif schemeIdx  == 1:
        return "MPFA (wth is)"
    elif schemeIdx == 2:
        return "BOX (with is)"

# returns a string with the plot label used for a scheme
def getSchemeLabel(schemeIdx):
    if schemeIdx == 0:
        return "TPFA"
    elif schemeIdx == 1:
        return "MPFA"
    elif schemeIdx == 2:
        return "BOX"
    elif schemeIdx == 3:
        return "BOX-CONT"

# returns the sub-folder in which the executable of a scheme lies
def getSchemeSubFolder(schemeIdx):
    if schemeIdx < 3:
        return "facet"
    elif schemeIdx == 3:
        return "boxdfm"
    elif schemeIdx == 4:
        return "nofractures"
    elif schemeIdx > 4:
        return "facet"
