import os
import sys
import matplotlib.pyplot as plt
import subprocess
import numpy as np
from utility import *

# use plot style
sys.path.append("./../../common")
import plotstyles

prop_cycle = list(plt.rcParams['axes.prop_cycle'])
lineStyles = ['solid', 'dotted', 'dashed', 'dashdot']

# defines the array of apertures to consider
apertures = np.logspace(-8, -3, 5, endpoint=True);
numTypeRealizations = 4
numTypes = 3

for i in range(1, numTypes+1):
    type = "type" + str(i)

    for j in range(1, numTypeRealizations+1):
        subFolder = type + "_" + str(j)
        effK = np.loadtxt(subFolder + "/effk_box.csv", delimiter=',')
        effK_nofrac = np.loadtxt(subFolder + "/effk_nofractures_box.csv", delimiter=',')
        effK_nofrac_array = []
        for k in range(0, len(apertures)):
            effK_nofrac_array.append(effK_nofrac)

        plt.figure(1)
        if i == 1 and j == 1:
            # plt.semilogx(apertures, effK_nofrac_array, label=r"$k_m$", color='k', alpha = 0.5)
            plt.semilogx(apertures, effK_nofrac_array, color='k', alpha = 0.5)
        plt.semilogx(apertures, effK, label=r"$\mathcal{F}_{" + str(i) + str(j) + "}$", color=prop_cycle[i-1]['color'], linestyle=lineStyles[j-1])
        plt.xlabel(r"$a \,\, \left[ \si{\meter} \right]$")
        plt.ylabel(r"$k_{\mathrm{eff}} \,\, \left[ \si{\meter^2} \right]$")

plt.legend(ncol=2)
plt.savefig("allcurves.pdf", bbox_inches='tight')
