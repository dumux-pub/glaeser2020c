// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Problem implementation of the reference solution of
 *        the 1-phase tensorial test case
 */
#ifndef DUMUX_ONEP_TENSORIAL_REFERENCE_PROBLEM_HH
#define DUMUX_ONEP_TENSORIAL_REFERENCE_PROBLEM_HH

#include <dune/alugrid/grid.hh>
#include <dune/common/exceptions.hh>

#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/1p/model.hh>

#include "spatialparams.hh"

namespace Dumux {
// forward declarations
template<class TypeTag> class OnePProblem;

namespace Properties {
namespace TTag {

// create the type tag nodes
struct OnePReference { using InheritsFrom = std::tuple<OneP>; };
struct OnePTpfa { using InheritsFrom = std::tuple<OnePReference, CCTpfaModel>; };
struct OnePMpfa { using InheritsFrom = std::tuple<OnePReference, CCMpfaModel>; };
struct OnePBox { using InheritsFrom = std::tuple<OnePReference, BoxModel>; };

} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePReference> { using type = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::OnePReference> { using type = OnePProblem<TypeTag>; };

// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePReference>
{
private:
    using FVGridGeometry = GetPropType<TypeTag, FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Scalar>;

public:
    using type = OnePSpatialParams< FVGridGeometry, Scalar >;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePReference>
{
private:
    using Scalar = GetPropType<TypeTag, Scalar>;

public:
    using type = FluidSystems::OnePLiquid< Scalar, Components::Constant<1, Scalar> >;
};

// enable caches
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::OnePReference> { static constexpr bool value = true; };

// permeability is constant
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::OnePReference> { static constexpr bool value = false; };

} // end namespace Properties

/*!
 * \brief Problem implementation of the reference solution of
 *        the 1-phase tensorial test case
 */
template<class TypeTag>
class OnePProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

public:
    OnePProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, paramGroup)
    , a_(getParamFromGroup<Scalar>("LowDim", "Problem.ExtrusionFactor"))
    , deltaP_(getParamFromGroup<Scalar>(paramGroup, "Problem.DeltaP"))
    {}

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();

        if ( isOnInlet(globalPos) || isOnOutlet(globalPos) )
            values.setAllDirichlet();

        return values;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        if (isOnInlet(globalPos))
            return initialAtPos(globalPos) + PrimaryVariables(deltaP_);
        return initialAtPos(globalPos);
    }

    //! evaluates the Neumann boundary condition for a given position
    NumEqVector neumannAtPos(const GlobalPosition& globalPos) const
    { return NumEqVector(0.0); }

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! returns true if position is on inlet
    bool isOnInlet(const GlobalPosition& globalPos) const
    { return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + 1e-8 && globalPos[1] < -0.25 + 1e-8; }

    //! returns true if position is on outlet
    bool isOnOutlet(const GlobalPosition& globalPos) const
    { return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - 1e-8 && globalPos[1] > 0.25 - 1e-8; }

    // writes the transfer fluxes to disk
    template<class GridVariables, class SolutionVector>
    void writeTransferFluxes(const GridVariables& gridVars, const SolutionVector& x) const
    {
        // this makes no sense for the box scheme
        if (FVGridGeometry::discMethod == DiscretizationMethod::box)
            DUNE_THROW(Dune::InvalidStateException, "Transfer flux computation doesn't work for box");

        Scalar flux_lo = 0.0;
        Scalar flux_hi = 0.0;

        std::ofstream fluxPlot_lo(this->name() + "_plot_lo.csv", std::ios::out);
        std::ofstream fluxPlot_hi(this->name() + "_plot_hi.csv", std::ios::out);

        using FluxVars = GetPropType<TypeTag, Properties::FluxVariables>;
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            auto elemVolVars = localView(gridVars.curGridVolVars());
            auto elemFluxVarsCache = localView(gridVars.gridFluxVarsCache());

            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            for (const auto& scvf : scvfs(fvGeometry))
            {
                if (scvf.boundary())
                    continue;

                const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
                const auto& outsideScv = fvGeometry.scv(scvf.outsideScvIdx());

                const bool insideInFracture = this->spatialParams().isInFracture(insideScv.center());
                const bool outsideInFracture = this->spatialParams().isInFracture(outsideScv.center());
                const bool isInterface = !insideInFracture && outsideInFracture;

                if (!isInterface)
                    continue;

                const bool isLowerInterface = scvf.unitOuterNormal()[1] > 0.9;
                const bool isUpperInterface = scvf.unitOuterNormal()[1] < -0.9;

                FluxVars fv;
                fv.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

                auto up = [] (const auto& vv) { return vv.mobility()*vv.density(); };
                Scalar flux = fv.advectiveFlux(/*phaseIdx*/0, up);

                if (isLowerInterface) { flux_lo += flux; fluxPlot_lo << scvf.center()[0] << "," << flux/scvf.area() << "\n"; }
                else if (isUpperInterface) { flux_hi += flux; fluxPlot_hi << scvf.center()[0] << "," << flux/scvf.area() << "\n"; }
            }
        }

        std::ofstream transferFluxFile(this->name() + ".csv", std::ios::out);
        transferFluxFile << flux_lo << "," << flux_hi << std::endl;
        transferFluxFile.close();
        fluxPlot_lo.close();
        fluxPlot_hi.close();
    }

private:
    Scalar a_;
    Scalar deltaP_;
};

} // end namespace Dumux

#endif
