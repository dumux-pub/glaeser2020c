// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Spatial parameters implementation of the reference solution of
 *        the 1-phase tensorial test case
 */
#ifndef DUMUX_ONEP_TENSORIAL_REFERENCE_SPATIALPARAMS_HH
#define DUMUX_ONEP_TENSORIAL_REFERENCE_SPATIALPARAMS_HH

#include <cmath>
#include <dune/common/fmatrix.hh>
#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux {

/*!
 * \brief Spatial parameters implementation of the reference solution of
 *        the 1-phase tensorial test case
 */
template< class FVGridGeometry, class Scalar >
class OnePSpatialParams
: public FVSpatialParamsOneP< FVGridGeometry, Scalar, OnePSpatialParams<FVGridGeometry, Scalar> >
{
    using ThisType = OnePSpatialParams< FVGridGeometry, Scalar >;
    using ParentType = FVSpatialParamsOneP< FVGridGeometry, Scalar, ThisType >;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    static constexpr int dimWorld = GridView::dimensionworld;

public:
    //! export the type used for permeabilities
    using PermeabilityType = Dune::FieldMatrix<Scalar, dimWorld, dimWorld>;

    //! the constructor
    OnePSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry, const std::string& paramGroup = "")
    : ParentType(fvGridGeometry)
    , a_(getParamFromGroup<Scalar>("LowDim", "Problem.ExtrusionFactor"))
    {
        const auto kf = getParamFromGroup<Scalar>("LowDim", "SpatialParams.Permeability");
        const auto kh = getParamFromGroup<Scalar>("Bulk", "SpatialParams.Permeability");
        const auto ratio = getParamFromGroup<Scalar>("Bulk", "SpatialParams.PermeabilityAnisotropyRatio");
        const auto angle = getParamFromGroup<Scalar>("Bulk", "SpatialParams.PermeabilityAngle");
        const auto kv = kh/ratio;

        Scalar cost = cos(angle);
        Scalar sint = sin(angle);

        permeability_ = 0.0;
        permeability_[0][0] = cost*cost*kh + sint*sint*kv;
        permeability_[1][1] = sint*sint*kh + cost*cost*kv;
        permeability_[0][1] = permeability_[1][0] = cost*sint*(kh - kv);

        permeabilityFracture_ = 0.0;
        permeabilityFracture_[0][0] = kf;
        permeabilityFracture_[1][1] = kf;
    }

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    {
        if ( isInFracture(globalPos) )
            return permeabilityFracture_;
        else
            return permeability_;
    }

    //! Return the porosity
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return 1.0; }

    //! returns true if a position is inside the fracture
    bool isInFracture(const GlobalPosition& globalPos) const
    { return globalPos[1] > -1.0*a_/2.0 && globalPos[1] < a_/2.0; }

private:
    Scalar a_;
    PermeabilityType permeability_;
    PermeabilityType permeabilityFracture_;
};

} // end namespace Dumux

#endif
