// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Solver routine for the reference solution of
 *        the 1-phase tensorial test case
 */
#ifndef DUMUX_ONEP_TENSORIAL_REFERENCE_SOLVER_HH
#define DUMUX_ONEP_TENSORIAL_REFERENCE_SOLVER_HH

#include <config.h>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/type.hh>
#include <dune/istl/matrixmarket.hh>

#include "problem.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/geometry/diameter.hh>

#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/vtkoutputmodule.hh>

#include <dumux/assembly/diffmethod.hh>
#include <dumux/assembly/fvassembler.hh>

#include <dumux/discretization/method.hh>
#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/evalsolution.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

// return type of solveReference(), containing the
// solutions and the grid views on which they were computed
template<class TypeTag>
struct ReferenceSolutionStorage
{
private:
    using Grid = Dumux::GetPropType<TypeTag, Dumux::Properties::Grid>;
    using FVGG = Dumux::GetPropType<TypeTag, Dumux::Properties::FVGridGeometry>;
    using GridVars = Dumux::GetPropType<TypeTag, Dumux::Properties::GridVariables>;
    using Problem = Dumux::GetPropType<TypeTag, Dumux::Properties::Problem>;

public:
    Dumux::GridManager<Grid> gridManager;
    std::shared_ptr<FVGG> fvGridGeometry;
    std::shared_ptr<GridVars> gridVariables;
    std::shared_ptr<Problem> problem;
    Dumux::GetPropType<TypeTag, Dumux::Properties::SolutionVector> solution;
};

// run the simulation for given type tag
template<class TypeTag>
ReferenceSolutionStorage<TypeTag>
solveReference(const std::string& gridParamGroup = "")
{
    using namespace Dumux;

    //////////////////////////////////////////////////////
    // try to create the grids (from the given grid file)
    //////////////////////////////////////////////////////
    ReferenceSolutionStorage<TypeTag> result;
    auto& gridManager = result.gridManager;
    gridManager.init(gridParamGroup);
    gridManager.loadBalance();

    ////////////////////////////////////////////////////////////
    // run stationary, non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    auto fvGridGeometry = std::make_shared<FVGridGeometry>(leafGridView);
    fvGridGeometry->update();

    // the problem (boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(fvGridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x(fvGridGeometry->numDofs());

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, fvGridGeometry);
    gridVariables->init(x);

    // intialize the vtk output module
    VtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, getParamFromGroup<std::string>("ReferenceSolution", "VtkFile"));
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    IOFields::initOutputModule(vtkWriter); // Add model specific output fields
    vtkWriter.write(0.0);

    // the assembler
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, fvGridGeometry, gridVariables);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    // linearize & solve
    nonLinearSolver.solve(x);

    // update grid variables for output
    gridVariables->update(x);

    // write vtk output
    vtkWriter.write(1.0);

    // write transfer fluxes into file
    problem->writeTransferFluxes(*gridVariables, x);

    result.fvGridGeometry = fvGridGeometry;
    result.gridVariables = gridVariables;
    result.problem = problem;
    result.solution = x;
    return result;
}

#endif
