// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Computes the cross-section averaged solution for the fracture domain.
 */
#ifndef DUMUX_ONEP_TENSORIAL_COMPUTEFRACTUREAVERAGE_HH
#define DUMUX_ONEP_TENSORIAL_COMPUTEFRACTUREAVERAGE_HH

namespace Dumux {
namespace Impl {

struct CrossSectionData
{
    double x;
    double area;
    double pIntegrated;
    std::vector<std::size_t> elementIndices;
};

} // end namespace Impl

template<class FractureFVGridGeometry, class Solution>
Solution computeAveragedFractureSolution(const FractureFVGridGeometry& fvGridGeom,
                                         const Solution sol)
{
    std::vector<typename Impl::CrossSectionData> crossSectionData;
    for (const auto& element : elements(fvGridGeom.gridView()))
    {
        const auto eg = element.geometry();
        const auto area = eg.volume();
        const auto eIdx = fvGridGeom.elementMapper().index(element);
        const auto x = eg.center()[0];
        const auto p = sol[eIdx];

        auto it = std::find_if( crossSectionData.begin(),
                                crossSectionData.end(),
                                [&] (const auto& cs) { return cs.x > x - 1e-7 && cs.x < x + 1e-7; } );

        if (it != crossSectionData.end())
        {
            (*it).area += area;
            (*it).pIntegrated += p*area;
            (*it).elementIndices.push_back(eIdx);
        }
        else
        {
            typename Impl::CrossSectionData cs;
            cs.x = x;
            cs.area = area;
            cs.pIntegrated = p*area;
            cs.elementIndices.push_back(eIdx);
            crossSectionData.emplace_back(std::move(cs));
        }
    }

    auto result = sol;
    for (const auto& cs : crossSectionData)
    {
        double pAvg = cs.pIntegrated/cs.area;
        for (auto idx : cs.elementIndices)
            result[idx] = pAvg;
    }

    return result;
}

} // end namespace Dumux

#endif
