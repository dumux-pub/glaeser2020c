import subprocess
import os
import sys
import math
import numpy as np
import matplotlib.pyplot as plt
from utility import *

# use plot style
sys.path.append("./../../common")
import plotstyles

prop_cycle = list(plt.rcParams['axes.prop_cycle'])
markers = ['o', '^', 'x', '.', ',', 's']

# adds data to a plot
def addDataToPlot(plotIdx, data, xIdx, yIdx, color, legend):
    plt.figure(plotIdx)
    plt.plot(data[:, xIdx], data[:, yIdx], c=color, label=legend)
    plt.xlabel(r'$x \,\, [\si{\meter}]$')
    plt.ylabel(r'$p \,\, [\si{\pascal}]$')
    plt.legend()

####################
# clears all plots #
####################
def clearSolutionPlots():
    plt.figure(getCombinedMatrixPressurePlotIdx())
    plt.clf()
    plt.figure(getCombinedFracturePressurePlotIdx())
    plt.clf()

    for schemeIdx in range(0, numSchemes):
        matrixPlotIdx = getMatrixPressurePlotIdx(schemeIdx)
        fracturePlotIdx = getFracturePressurePlotIdx(schemeIdx)

        plt.figure(matrixPlotIdx)
        plt.clf()
        plt.figure(fracturePlotIdx)
        plt.clf()

        if getSchemeAcronym(schemeIdx) != "boxdfm":
            loFluxPlotIdx = getLowTransferFluxPlotIdx(schemeIdx)
            hiFluxPlotIdx = getHighTransferFluxPlotIdx(schemeIdx)

            plt.figure(loFluxPlotIdx)
            plt.clf()
            plt.figure(hiFluxPlotIdx)
            plt.clf()

def clearErrorNormPlots():
    for schemeIdx in range(0, numSchemes):
        matrixErrorPlotIdx = getMatrixErrorPlotIdx(schemeIdx)
        fractureErrorPlotIdx = getFractureErrorPlotIdx(schemeIdx)
        plt.figure(matrixErrorPlotIdx)
        plt.clf()
        plt.figure(fractureErrorPlotIdx)
        plt.clf()

#################################################
# plots the schemes' solutions vs the reference #
#################################################
def plotSolutions(kf, angle, dxArray, vtuFolder):

    csvFolder = vtuFolder + "/csv"
    if not os.path.exists(csvFolder):
        sys.stderr.write("Could not find folder with the csv files!")
        sys.exit(1)

    # indices in the csv files created by paraview
    xIdx = 4
    pressureIdx = 0

    # reference plot in matrix
    csvFileReference_matrix_1 = csvFolder + "/ref_k_" + str(kf) + "_angle_" + str(to_degrees(angle)) + "_matrix_1.csv"
    csvFileReference_matrix_2 = csvFolder + "/ref_k_" + str(kf) + "_angle_" + str(to_degrees(angle)) + "_matrix_2.csv"
    csvFileReference_fracture = csvFolder + "/ref_k_" + str(kf) + "_angle_" + str(to_degrees(angle)) + "_fracture.csv"

    csvFileReference_flux_lo = vtuFolder + "/tensorial_ref_k_" + str(kf) + "_angle_" + str(to_degrees(angle)) + "_plot_lo.csv"
    csvFileReference_flux_hi = vtuFolder + "/tensorial_ref_k_" + str(kf) + "_angle_" + str(to_degrees(angle)) + "_plot_hi.csv"

    ref_matrixPlot1Data = np.loadtxt(csvFileReference_matrix_1, delimiter=',', skiprows=1)
    ref_matrixPlot2Data = np.loadtxt(csvFileReference_matrix_2, delimiter=',', skiprows=1)
    ref_fracturePlotData = np.loadtxt(csvFileReference_fracture, delimiter=',', skiprows=1)

    ref_flux_file_lo = vtuFolder + "/tensorial_k_" + str(kf) + "_angle_" + str(to_degrees(angle)) + "_ref_plot_lo.csv"
    ref_flux_file_hi = vtuFolder + "/tensorial_k_" + str(kf) + "_angle_" + str(to_degrees(angle)) + "_ref_plot_hi.csv"
    ref_flux_lo_data = np.loadtxt(ref_flux_file_lo, delimiter=",")
    ref_flux_hi_data = np.loadtxt(ref_flux_file_hi, delimiter=",")

    ref_order_lo = np.argsort(ref_flux_lo_data[:, 0])
    ref_x_flux_lo = np.array(ref_flux_lo_data[:, 0])[ref_order_lo]
    ref_q_flux_lo = np.array(ref_flux_lo_data[:, 1])[ref_order_lo]

    ref_order_hi = np.argsort(ref_flux_hi_data[:, 0])
    ref_x_flux_hi = np.array(ref_flux_hi_data[:, 0])[ref_order_hi]
    ref_q_flux_hi = np.array(ref_flux_hi_data[:, 1])[ref_order_hi]

    # add reference solution to the desired plots
    for schemeIdx in range(0, numSchemes):
        matrixPlotIdx = getMatrixPressurePlotIdx(schemeIdx)
        fracturePlotIdx = getFracturePressurePlotIdx(schemeIdx)
        hiTransferPlotIdx = getHighTransferFluxPlotIdx(schemeIdx)
        loTransferlPlotIdx = getLowTransferFluxPlotIdx(schemeIdx)

        plt.figure(matrixPlotIdx)
        addDataToPlot(matrixPlotIdx, ref_matrixPlot1Data, 3, pressureIdx, prop_cycle[0]['color'], "reference")
        addDataToPlot(matrixPlotIdx, ref_matrixPlot2Data, 3, pressureIdx, prop_cycle[0]['color'], "")

        plt.figure(fracturePlotIdx)
        addDataToPlot(fracturePlotIdx, ref_fracturePlotData, 3, pressureIdx, prop_cycle[0]['color'], "reference")

        plt.figure(loTransferlPlotIdx)
        plt.plot(ref_x_flux_lo, ref_q_flux_lo, c=prop_cycle[0]['color'], label="reference")
        plt.figure(hiTransferPlotIdx)
        plt.plot(ref_x_flux_hi, ref_q_flux_hi, c=prop_cycle[0]['color'], label="reference")

    combinedMatrixPlotIdx = getCombinedMatrixPressurePlotIdx()
    combinedFracturePlotIdx = getCombinedFracturePressurePlotIdx()

    plt.figure(combinedMatrixPlotIdx)
    addDataToPlot(combinedMatrixPlotIdx, ref_matrixPlot1Data, 3, pressureIdx, prop_cycle[0]['color'], "reference")
    addDataToPlot(combinedMatrixPlotIdx, ref_matrixPlot2Data, 3, pressureIdx, prop_cycle[0]['color'], "")

    plt.figure(combinedFracturePlotIdx)
    addDataToPlot(combinedFracturePlotIdx, ref_fracturePlotData, 3, pressureIdx, prop_cycle[0]['color'], "reference")

    # make plot data for all schemes & discretizations
    for schemeIdx in range(0, numSchemes):
        for count in range(0, len(dxArray)):

            schemeName = getSchemeAcronym(schemeIdx)
            schemeLabel = getSchemeLabel(schemeIdx)
            matrixPlotIdx = getMatrixPressurePlotIdx(schemeIdx)
            fracturePlotIdx = getFracturePressurePlotIdx(schemeIdx)
            dxString = "1/" + str(int(1.0/dxArray[count]))

            # index is different for fracture solution in boxdfm
            if schemeName == "boxdfm":
                fractureXIdx = xIdx - 1
            else:
                fractureXIdx = xIdx

            csvFile_matrix_1 = csvFolder + "/" + schemeName + "_k_" + str(kf) + "_angle_" + str(to_degrees(angle)) + "_" + str(count) + "_matrix_1.csv"
            csvFile_matrix_2 = csvFolder + "/" + schemeName + "_k_" + str(kf) + "_angle_" + str(to_degrees(angle)) + "_" + str(count) + "_matrix_2.csv"
            csvFile_fracture = csvFolder + "/" + schemeName + "_k_" + str(kf) + "_angle_" + str(to_degrees(angle)) + "_" + str(count) + "_fracture.csv"

            matrixPlot1Data = np.loadtxt(csvFile_matrix_1, delimiter=',', skiprows=1)
            matrixPlot2Data = np.loadtxt(csvFile_matrix_2, delimiter=',', skiprows=1)
            fracturePlotData = np.loadtxt(csvFile_fracture, delimiter=',', skiprows=1)

            addDataToPlot(matrixPlotIdx, matrixPlot1Data, xIdx, pressureIdx, prop_cycle[count+1]['color'], "h = " + dxString)
            addDataToPlot(matrixPlotIdx, matrixPlot2Data, xIdx, pressureIdx, prop_cycle[count+1]['color'], "")
            plt.title(schemeLabel)
            addDataToPlot(fracturePlotIdx, fracturePlotData, fractureXIdx, pressureIdx, prop_cycle[count+1]['color'], "h = " + dxString)
            plt.title(schemeLabel)
            if count == len(dxArray)-1:
                addDataToPlot(combinedMatrixPlotIdx, matrixPlot1Data, xIdx, pressureIdx, prop_cycle[schemeIdx+1]['color'], schemeLabel + ", h = " + dxString)
                addDataToPlot(combinedMatrixPlotIdx, matrixPlot2Data, xIdx, pressureIdx, prop_cycle[schemeIdx+1]['color'], "")
                addDataToPlot(combinedFracturePlotIdx, fracturePlotData, fractureXIdx, pressureIdx, prop_cycle[schemeIdx+1]['color'], schemeLabel + ", h = " + dxString)

            # flux plots
            if schemeName != "boxdfm":
                flux_lo_csvFile = vtuFolder + "/tensorial_k_" + str(kf) + "_angle_" + str(to_degrees(angle)) + "_bulk_" + schemeName + "_" + str(count) + "_plot_lo.csv"
                flux_hi_csvFile = vtuFolder + "/tensorial_k_" + str(kf) + "_angle_" + str(to_degrees(angle)) + "_bulk_" + schemeName + "_" + str(count) + "_plot_hi.csv"
                flux_lo_data = np.loadtxt(flux_lo_csvFile, delimiter=',', skiprows=0)
                flux_hi_data = np.loadtxt(flux_hi_csvFile, delimiter=',', skiprows=0)

                order_lo = np.argsort(flux_lo_data[:, 0])
                x_flux_lo = np.array(flux_lo_data[:, 0])[order_lo]
                q_flux_lo = np.array(flux_lo_data[:, 1])[order_lo]

                order_hi = np.argsort(flux_hi_data[:, 0])
                x_flux_hi = np.array(flux_hi_data[:, 0])[order_hi]
                q_flux_hi = np.array(flux_hi_data[:, 1])[order_hi]

                plotIdx_lo = getLowTransferFluxPlotIdx(schemeIdx)
                plotIdx_hi = getHighTransferFluxPlotIdx(schemeIdx)
                plt.figure(plotIdx_lo)
                plt.plot(x_flux_lo, q_flux_lo, c=prop_cycle[count+1]['color'], label="h = " + dxString)
                plt.xlabel(r'$x \,\, [\si{\meter}]$')
                plt.ylabel(r'$F_2 \,\, [\si{\kilogram/\meter/\second}]$')
                plt.title(schemeLabel)
                plt.legend()
                plt.figure(plotIdx_hi)
                plt.plot(x_flux_hi, q_flux_hi, c=prop_cycle[count+1]['color'], label="h = " + dxString)
                plt.xlabel(r'$x \,\, [\si{\meter}]$')
                plt.ylabel(r'$F_2 \,\, [\si{\kilogram/\meter/\second}]$')
                plt.title(schemeLabel)
                plt.legend()

##################################################
# plots the convergence behaviour of the schemes #
##################################################
def plotErrorNorms(kf, angle, a, dxArray, vtuFolder, angleCount):

    normFileMatrix = open("norms_k_" + str(kf) + "_angle_" + str(to_degrees(angle)) + "_matrix.txt", "r")
    normFileFracture = open("norms_k_" + str(kf) + "_angle_" + str(to_degrees(angle)) + "_fracture.txt", "r")
    normDataMatrix = np.loadtxt(normFileMatrix, delimiter=',')
    normDataFracture = np.loadtxt(normFileFracture, delimiter=',')

    angleLabel = "$\Phi = 0.0$"
    if angleCount == 1:
        angleLabel = r"$\Phi = \pi/4$"
    elif angleCount == 2:
        angleLabel = r"$\Phi = -\pi/4$"

    xAxis = []
    for dx in dxArray:
        xAxis.append(a/dx)

    if len(normDataMatrix) != len(normDataFracture):
        sys.stderr.write("Norm data size mismatch!")
        sys.exit(1)

    if len(normDataMatrix) != numSchemes:
        sys.stderr.write("No error data for all schemes!")
        sys.exit(1)

    for schemeIdx in range(0, len(normDataMatrix)):
        matrixErrorPlotIdx = getMatrixErrorPlotIdx(schemeIdx)
        fractureErrorPlotIdx = getFractureErrorPlotIdx(schemeIdx)

        plt.figure(matrixErrorPlotIdx)
        plt.loglog(xAxis, normDataMatrix[schemeIdx, :], marker=markers[angleCount], c=prop_cycle[angleCount]['color'], linestyle=lineStyleArray[angleCount], label=angleLabel)
        plt.xlabel(r'$a/h$')
        plt.ylabel(r'$\varepsilon_{p_2}$')
        plt.title(getSchemeLabel(schemeIdx))
        plt.legend()

        plt.figure(fractureErrorPlotIdx)
        plt.loglog(xAxis, normDataFracture[schemeIdx, :], marker=markers[angleCount], c=prop_cycle[angleCount]['color'], linestyle=lineStyleArray[angleCount], label=angleLabel)
        plt.xlabel(r'$a/h$')
        plt.ylabel(r'$\varepsilon_{p_1}$')
        plt.title(getSchemeLabel(schemeIdx))
        plt.legend()


#######################
# save pressure plots #
#######################
def saveSolutionPlots(angle, vtuFolder):

    plotFolder = vtuFolder + "/plots"
    if not os.path.exists(plotFolder):
        subprocess.call(['mkdir', plotFolder])

    combinedMatrixPlotIdx = getCombinedMatrixPressurePlotIdx()
    combinedFracturePlotIdx = getCombinedFracturePressurePlotIdx()

    plt.figure(combinedMatrixPlotIdx)
    plt.savefig(plotFolder + "/combined_angle_" + str(to_degrees(angle)) + "_matrix.pdf", bbox_inches='tight')

    plt.figure(combinedFracturePlotIdx)
    plt.savefig(plotFolder + "/combined_angle_" + str(to_degrees(angle)) + "_fracture.pdf", bbox_inches='tight')

    for schemeIdx in range(0, numSchemes):
        schemeName = getSchemeAcronym(schemeIdx)
        matrixPlotIdx = getMatrixPressurePlotIdx(schemeIdx)
        fracturePlotIdx = getFracturePressurePlotIdx(schemeIdx)

        plt.figure(matrixPlotIdx)
        plt.savefig(plotFolder + "/" + schemeName + "_angle_" + str(to_degrees(angle)) + "_matrix.pdf", bbox_inches='tight')

        plt.figure(fracturePlotIdx)
        plt.savefig(plotFolder + "/" + schemeName + "_angle_" + str(to_degrees(angle)) + "_fracture.pdf", bbox_inches='tight')

        if schemeName != "boxdfm":
            loFluxPlotIdx = getLowTransferFluxPlotIdx(schemeIdx)
            hiFluxPlotIdx = getHighTransferFluxPlotIdx(schemeIdx)

            plt.figure(loFluxPlotIdx)
            plt.savefig(plotFolder + "/" + schemeName + "_qtransfer_lo_angle_" + str(to_degrees(angle)) + ".pdf", bbox_inches='tight')
            plt.figure(hiFluxPlotIdx)
            plt.savefig(plotFolder + "/" + schemeName + "_qtransfer_hi_angle_" + str(to_degrees(angle)) + ".pdf", bbox_inches='tight')

##################################################
# plots the convergence behaviour of the schemes #
##################################################
def saveErrorNormPlots(vtuFolder):
    plotFolder = vtuFolder + "/plots"
    if not os.path.exists(plotFolder):
        subprocess.call(['mkdir', plotFolder])

    for schemeIdx in range(0, numSchemes):
        matrixErrorPlotIdx = getMatrixErrorPlotIdx(schemeIdx)
        fractureErrorPlotIdx = getFractureErrorPlotIdx(schemeIdx)

        plt.figure(matrixErrorPlotIdx)
        plt.savefig(plotFolder + "/errors_" + getSchemeAcronym(schemeIdx) + "_matrix.pdf", bbox_inches='tight')
        plt.figure(fractureErrorPlotIdx)
        plt.savefig(plotFolder + "/errors_" + getSchemeAcronym(schemeIdx) + "_fracture.pdf", bbox_inches='tight')
