import subprocess
import os
import sys
from makeplots import to_degrees

# run reference simulation and refinement sequence for given
# fracture permeability, matrix permeability angle, aperture,
# number of elements inside the fracture in the equi-dimensional
# grid, an array of dx's used in the reduced dimensional grid.
# and the folder in which the vtus are to be saved
def runRefinementSequence(kf, angle, a, numElemsInFracture, dxArray, vtuFolder):

    geoFileEqui = "./../../grids/unitfracture_quads_equi.geo"
    geoFileLowDim = "./../../grids/unitfracture_quads.geo"

    if not os.path.exists(geoFileEqui) or not os.path.exists(geoFileLowDim):
        sys.stderr.write("Could not find the required .geo files")
        sys.exit(1)

    #####################################################
    # create Reference and all lower-dimensional meshes #
    #####################################################
    refGeoFile = "./reference.geo"
    refMshFile = "./reference.msh"
    if os.path.exists(refGeoFile):
        subprocess.call(['rm', refGeoFile])
    if os.path.exists(refGeoFile):
        subprocess.call(['rm', refGeoFile])

    # modify number of elements in fracture
    file = open(geoFileEqui, "r")

    geoLines = file.readlines()
    geoLines[0] = geoLines[0].strip("\n")
    geoLines[0] = geoLines[0].split(" ")
    if geoLines[0][0] != "numElementsInFracture":
        sys.stderr.write("Expected \"numElementsInFracture\" as first word of first line")
        sys.exit(1)
    elif len(geoLines[0]) != 3:
        sys.stderr.write("Expected \"numElementsInFracture = (value)\" in the first line of geo file")
        sys.exit(1)
    else:
        geoLines[0][2] = str(numElemsInFracture) + ";"

    geoLines[1] = geoLines[1].strip("\n")
    geoLines[1] = geoLines[1].split(" ")
    if geoLines[1][0] != "a":
        sys.stderr.write("Expected \"a\" as first word of second line")
        sys.exit(1)
    elif len(geoLines[1]) != 3:
        sys.stderr.write("Expected \"a = (value)\" in the second line of geo file")
        sys.exit(1)
    else:
        geoLines[1][2] = str(a) + ";"

    tmpFile = open(refGeoFile, "w")
    for lineIdx in range(len(geoLines)):
        if lineIdx == 0 or lineIdx == 1:
            tmpFile.write(geoLines[lineIdx][0] + " " + geoLines[lineIdx][1] + " " + geoLines[lineIdx][2] + "\n")
        else:
            tmpFile.write(geoLines[lineIdx])
    tmpFile.close()
    file.close()

    subprocess.call(['gmsh', '-2', refGeoFile])

    if not os.path.isfile(refGeoFile):
        sys.stderr.write("Could not create reference mesh")
        sys.exit(1)

    counter = 0
    meshArgArray = []
    for dx in dxArray:
        geoFile = open(geoFileLowDim, 'r')
        lines = geoFile.readlines()
        firstLine = lines[0]
        firstLine = firstLine.split(" ")

        if firstLine[0] != "dx":
            sys.stderr.write("Geo file is expected to have - dx = ... - in the first line!\n")
            sys.exit(1)

        # write a temporary geo-file using dx from array
        tmpGeoFileName = "./lowdim" + "_" + str(counter) + ".geo"
        tmpMshFileName = "./lowdim" + "_" + str(counter) + ".msh"
        tmpFile = open(tmpGeoFileName, 'w')
        lines[0] = "dx = " + str(dx) + ";\n"
        for line in lines:
            tmpFile.write(line)

        geoFile.close()
        tmpFile.close()

        subprocess.call(['gmsh', '-2', tmpGeoFileName])

        if not os.path.isfile(tmpMshFileName):
            sys.stderr.write("Could not create lower-dimensional mesh")
            sys.exit(1)

        meshArgArray.append(["-Refinement" + str(counter) + ".Grid.File", tmpMshFileName])
        counter = counter + 1

    ####################################################
    # run lower-dimensional models for all refinements #
    ####################################################
    errorNormFile = "norms_k_" + str(kf) + "_angle_" + str(to_degrees(angle))
    refVtkFileBody = vtuFolder + '/tensorial_ref_k_' + str(kf) + '_angle_' + str(to_degrees(angle))
    problemNameBody = vtuFolder + '/tensorial_k_' + str(kf) + "_angle_" + str(to_degrees(angle))
    subProcessCallArgs = ['./test_1p_tensorial_convergence',
                          './params.input',
                          '-ReferenceSolution.VtkFile', refVtkFileBody,
                          '-ReferenceGrid.Grid.File', refMshFile,
                          '-Problem.Name', problemNameBody,
                          '-LowDim.SpatialParams.Permeability', str(kf),
                          '-Bulk.SpatialParams.PermeabilityAngle', str(angle),
                          '-LowDim.Problem.ExtrusionFactor', str(a),
                          '-L2Norm.OutputFile', errorNormFile];

    for i in range(0, counter):
        subProcessCallArgs.append(meshArgArray[i][0])
        subProcessCallArgs.append(meshArgArray[i][1])

    ####################
    # This calls Dumux #
    ####################
    subprocess.call(subProcessCallArgs)

    # remove geo and msh files
    subprocess.call(['rm', refGeoFile])
    subprocess.call(['rm', refMshFile])
    for i in range(0, counter):
        subprocess.call(['rm', "./lowdim" + "_" + str(i) + ".geo"])
        subprocess.call(['rm', "./lowdim" + "_" + str(i) + ".msh"])
