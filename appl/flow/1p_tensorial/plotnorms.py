import math
import sys
import numpy as np
from utility import *

import matplotlib.pyplot as plt

# plot adjustments
plt.style.use('ggplot')

plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = 'Ubuntu'
plt.rcParams['text.usetex'] = True
plt.rcParams['font.weight'] = 'light'
plt.rcParams['font.size'] = 10
plt.rcParams['axes.labelsize'] = 22
plt.rcParams['axes.labelweight'] = 'light'
plt.rcParams['xtick.labelsize'] = 11
plt.rcParams['ytick.labelsize'] = 11
plt.rcParams['legend.fontsize'] = 11
plt.rcParams['figure.titlesize'] = 12
plt.rcParams.update( {'mathtext.default': 'regular' } )
plt.rcParams["text.latex.preamble"].append(r"\usepackage{stmaryrd}")
plt.rcParams["text.latex.preamble"].append(r"\usepackage{siunitx}")
plt.rcParams["text.latex.preamble"].append(r"\usepackage{amsmath}")
plt.rcParams["text.latex.preamble"].append(r"\usepackage{cmbright}")

prop_cycle = list(plt.rcParams['axes.prop_cycle'])
markers = ['o', '^', 'x', '.', ',', 's']

##################################################
# plots the convergence behaviour of the schemes #
##################################################
kArray = [1e-4, 1e-2, 1e2, 1e4]
angleArray = [0.0, math.pi/4.0, -math.pi/4.0]
numSchemes = 4

for angleIdx in range(0, len(angleArray)):

    angle = angleArray[angleIdx]
    angleLabel = "$\Phi = 0.0$"
    if angleIdx == 1:
        angleLabel = r"$\Phi = \pi/4$"
    elif angleIdx == 2:
        angleLabel = r"$\Phi = -\pi/4$"

    # each entry will be a list of errors (for each k) per scheme
    schemeErrorsMatrix = []
    schemeErrorsFracture = []
    for i in range(0, numSchemes):
        schemeErrorsMatrix.append([])
        schemeErrorsFracture.append([])

    for kf in kArray:

        normFileMatrix = open("norms_k_" + str(kf) + "_angle_" + str(to_degrees(angle)) + "_matrix.txt", "r")
        normFileFracture = open("norms_k_" + str(kf) + "_angle_" + str(to_degrees(angle)) + "_fracture.txt", "r")
        normDataMatrix = np.loadtxt(normFileMatrix, delimiter=',')
        normDataFracture = np.loadtxt(normFileFracture, delimiter=',')

        if (len(normDataMatrix) != numSchemes):
            sys.stderr.write("Expected " + str(numSchemes) + " schemes to plot errors for!\n")
            sys.exit(1)

        print("Matrix rates for k = ", str(kf))
        for schemeIdx in range(0, len(normDataMatrix)):
            lastError = normDataMatrix[ schemeIdx, len(normDataMatrix[schemeIdx])-1 ]
            prevError = normDataMatrix[ schemeIdx, len(normDataMatrix[schemeIdx])-2 ]
            deltaLogError = np.log(lastError) - np.log(prevError)
            deltaLogDx = np.log(2)
            print(getSchemeLabel(schemeIdx) + ": " + str(deltaLogError/deltaLogDx))
            schemeErrorsMatrix[schemeIdx].append(lastError)


        for schemeIdx in range(0, len(normDataFracture)):
            schemeErrorsFracture[schemeIdx].append(normDataFracture[ schemeIdx, len(normDataFracture[schemeIdx])-1 ])

    for schemeIdx in range(0, numSchemes):
        plt.figure(schemeIdx)
        plt.loglog(kArray, schemeErrorsMatrix[schemeIdx], marker=markers[angleIdx], c=prop_cycle[angleIdx]['color'], linestyle=lineStyleArray[angleIdx], label=angleLabel)
        plt.xlabel(r'$k$ [m$^2$]')
        plt.ylabel(r'$\varepsilon_{p_2}^m$')
        plt.legend()
        plt.title(getSchemeLabel(schemeIdx))
        # plt.ylim(1e-4, 1)

        plt.figure(numSchemes*2 + schemeIdx)
        plt.loglog(kArray, schemeErrorsFracture[schemeIdx], marker=markers[angleIdx], c=prop_cycle[angleIdx]['color'], linestyle=lineStyleArray[angleIdx], label=angleLabel)
        plt.xlabel(r'$k$ [m$^2$]')
        plt.ylabel(r'$\varepsilon_{p_2}^m$')
        plt.title(getSchemeLabel(schemeIdx))
        plt.legend()
        # plt.ylim(1e-8, 1)

for schemeIdx in range(0, numSchemes):
    plt.figure(schemeIdx)
    plt.savefig("errorplots_" + getSchemeAcronym(schemeIdx) + "_matrix.pdf", bbox_inches='tight')
    plt.figure(numSchemes*2 + schemeIdx)
    plt.savefig("errorplots_" + getSchemeAcronym(schemeIdx) + "_fracture.pdf", bbox_inches='tight')
