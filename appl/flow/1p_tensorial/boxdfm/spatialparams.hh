// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \brief The spatial parameters for the single-phase
 *        tensorial test using the box-dfm model.
 */
#ifndef DUMUX_ONEP_TENSORIAL_BOXDFM_SPATIAL_PARAMS_HH
#define DUMUX_ONEP_TENSORIAL_BOXDFM_SPATIAL_PARAMS_HH

#include <cmath>

#include <dune/common/fmatrix.hh>
#include <dumux/material/spatialparams/fv.hh>

namespace Dumux {

/*!
 * \brief The spatial parameters for the single-phase
 *        tensorial test using the box-dfm model.
 */
template<class FVGridGeometry, class Scalar>
class OnePTestSpatialParams : public FVSpatialParams< FVGridGeometry, Scalar, OnePTestSpatialParams<FVGridGeometry, Scalar> >
{
    using ThisType = OnePTestSpatialParams<FVGridGeometry, Scalar>;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, ThisType>;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;

    static constexpr int dimWorld = GridView::dimensionworld;

public:
    //! export the type used for permeabilities
    using PermeabilityType = Dune::FieldMatrix<Scalar, dimWorld, dimWorld>;

    OnePTestSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry) : ParentType(fvGridGeometry)
    {
        const auto kf = getParam<Scalar>("LowDim.SpatialParams.Permeability");
        const auto kh = getParam<Scalar>("Bulk.SpatialParams.Permeability");
        const auto ratio = getParam<Scalar>("Bulk.SpatialParams.PermeabilityAnisotropyRatio");
        const auto angle = getParam<Scalar>("Bulk.SpatialParams.PermeabilityAngle");
        const auto kv = kh/ratio;

        Scalar cost = cos(angle);
        Scalar sint = sin(angle);

        matrixPermeability_ = 0.0;
        matrixPermeability_[0][0] = cost*cost*kh + sint*sint*kv;
        matrixPermeability_[1][1] = sint*sint*kh + cost*cost*kv;
        matrixPermeability_[0][1] = matrixPermeability_[1][0] = cost*sint*(kh - kv);

        fracturePermeability_ = 0.0;
        fracturePermeability_[0][0] = kf;
        fracturePermeability_[1][1] = kf;
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     *        In this test, we use element-wise distributed permeabilities.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return permeability
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        if (scv.isOnFracture())
            return fracturePermeability_;
        else
            return matrixPermeability_;
    }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     * \param globalPos The global position
     */
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    { return 1.0; }

private:
    PermeabilityType matrixPermeability_;
    PermeabilityType fracturePermeability_;
};

} // end namespace Dumux

#endif
