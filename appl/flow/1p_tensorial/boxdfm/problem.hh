// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for the single-phase
 *        tensorial test using the box-dfm model.
 */
#ifndef DUMUX_ONEP_TENSORIAL_BOXDFM_PROBLEM_HH
#define DUMUX_ONEP_TENSORIAL_BOXDFM_PROBLEM_HH

#include <dune/alugrid/grid.hh>

#include <dumux/common/math.hh>

#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/evalgradients.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/porousmediumflow/boxdfm/model.hh>

#include "spatialparams.hh"

namespace Dumux {

// forward declarations
template<class TypeTag> class OnePBoxDfmTestProblem;

namespace Properties {
namespace TTag {
// we need to derive first from twop and then from the box-dfm Model
// because the flux variables cache type of OneP is overwritten in BoxDfmModel
struct OnePBoxDfm { using InheritsFrom = std::tuple<BoxDfmModel, OneP>; };
}

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePBoxDfm> { using type = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::OnePBoxDfm> { using type = OnePBoxDfmTestProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePBoxDfm>
{
private:
    using FVG = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = OnePTestSpatialParams<FVG, Scalar>;
};

// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePBoxDfm>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = FluidSystems::OnePLiquid< Scalar, Components::Constant<1, Scalar> >;
};

// permeability is constant
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::OnePBoxDfm> { static constexpr bool value = false; };

} // end namespace Properties

/*!
 * \brief The problem implementation.
 */
template<class TypeTag>
class OnePBoxDfmTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;

    // some indices for convenience
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum { pressureIdx = Indices::pressureIdx };

    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

public:

    //! The constructor
    OnePBoxDfmTestProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        fractureAperture_ = getParam<Scalar>("LowDim.Problem.ExtrusionFactor");
    }

    /*!
     * \brief Return how much the domain is extruded at a given sub-control volume.
     *
     * Here, we extrude the fracture scvs by half the aperture
     */
    template<class ElementSolution>
    Scalar extrusionFactor(const Element& element,
                           const SubControlVolume& scv,
                           const ElementSolution& elemSol) const
    {
        // In the box-scheme, we compute fluxes etc element-wise,
        // thus per element we compute only half a fracture !!!
        if (scv.isOnFracture())
            return fractureAperture_/2.0;
        return 1.0;
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param values Stores the value of the boundary type
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    //! Evaluate the source term at a given position
    NumEqVector sourceAtPos(const GlobalPosition& globalPos) const
    { return NumEqVector(0.0); }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet
     *        boundary segment
     *
     * \param values Stores the Dirichlet values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variable} ] \f$
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        static const Scalar deltaP = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.DeltaP");
        if (isOnInlet(globalPos))
            return initialAtPos(globalPos) + PrimaryVariables(deltaP);
        return initialAtPos(globalPos);
    }

    //! Evaluate the Neumann boundary conditions
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {

        // for box, incorporate Dirichlet Bcs weakly
        if (isOnOutlet(scvf.ipGlobal()) || isOnInlet(scvf.ipGlobal()))
        {
            // modify element solution to carry inlet/outlet head
            auto elemSol = elementSolution(element, elemVolVars, fvGeometry);
            for (const auto& curScvf : scvfs(fvGeometry))
            {
                if (curScvf.boundary())
                {
                    if (isOnOutlet(curScvf.ipGlobal()) || isOnInlet(curScvf.ipGlobal()))
                    {
                        const auto diriValues = dirichletAtPos(curScvf.ipGlobal());
                        const auto& insideScv = fvGeometry.scv(curScvf.insideScvIdx());
                        elemSol[insideScv.localDofIndex()][0] = diriValues[0];
                    }
                }
            }

            // evaluate gradients using this element solution
            const auto gradHead = evalGradients(element,
                                                element.geometry(),
                                                fvGeometry.fvGridGeometry(),
                                                elemSol,
                                                scvf.ipGlobal())[0];

            // compute the flux
            const auto& volVars = elemVolVars[scvf.insideScvIdx()];
            Scalar flux = -1.0*vtmv(gradHead, volVars.permeability(), scvf.unitOuterNormal());
            flux *= volVars.density()*volVars.mobility();
            return NumEqVector(flux);
        }

        return NumEqVector(0.0);
    }

    //! returns true if position is on inlet
    bool isOnInlet(const GlobalPosition& globalPos) const
    { return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + 1e-8 && globalPos[1] < -0.25 + 1e-8; }

    //! returns true if position is on outlet
    bool isOnOutlet(const GlobalPosition& globalPos) const
    { return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - 1e-8 && globalPos[1] > 0.25 - 1e-8; }

    /*!
     * \brief Evaluates the initial values for a control volume
     * \param values Stores the initial values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variables} ] \f$
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    { return PrimaryVariables(0.0); }

    //! Returns the temperature \f$\mathrm{[K]}\f$ for an isothermal problem.
    Scalar temperature() const { return 293.15; /* 10°C */ }

private:
    Scalar injectionRate_;
    Scalar fractureAperture_;
};

} // end namespace Dumux

#endif
