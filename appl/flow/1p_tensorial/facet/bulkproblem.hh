// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for the bulk domain in the single-phase
 *        tensorial test using the facet coupling models.
 */
#ifndef DUMUX_ONEP_TENSORIAL_FACET_BULK_PROBLEM_HH
#define DUMUX_ONEP_TENSORIAL_FACET_BULK_PROBLEM_HH

#include <dune/alugrid/grid.hh>
#include <dune/common/exceptions.hh>

#include <dumux/common/math.hh>
#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/evalgradients.hh>

#include <dumux/multidomain/facet/box/properties.hh>
#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>
#include <dumux/multidomain/facet/cellcentered/mpfa/properties.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/1p/model.hh>

#include "bulkspatialparams.hh"

namespace Dumux {
// forward declarations
template<class TypeTag> class OnePBulkProblem;

namespace Properties {
namespace TTag {

// create the type tag nodes
struct OnePBulk { using InheritsFrom = std::tuple<OneP>; };
struct OnePBulkTpfa { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, OnePBulk>; };
struct OnePBulkMpfa { using InheritsFrom = std::tuple<CCMpfaFacetCouplingModel, OnePBulk>; };
struct OnePBulkBox { using InheritsFrom = std::tuple<BoxFacetCouplingModel, OnePBulk>; };

} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePBulk> { using type = Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::OnePBulk> { using type = OnePBulkProblem<TypeTag>; };

// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePBulk>
{
private:
    using FVGridGeometry = GetPropType<TypeTag, FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Scalar>;

public:
    using type = OnePBulkSpatialParams< FVGridGeometry, Scalar >;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePBulk>
{
private:
    using Scalar = GetPropType<TypeTag, Scalar>;

public:
    using type = FluidSystems::OnePLiquid< Scalar, Components::Constant<1, Scalar> >;
};

// permeability is constant
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::OnePBulk> { static constexpr bool value = false; };

} // end namespace Properties

/*!
 * \brief The problem for the bulk domain in the single-phase
 *        tensorial test using the facet coupling models.
 */
template<class TypeTag>
class OnePBulkProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

    static constexpr bool isBox = FVGridGeometry::discMethod == DiscretizationMethod::box;

public:
    OnePBulkProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                    std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                    std::shared_ptr<CouplingManager> couplingManager,
                    const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManager)
    , useInteriorDirichletBCs_(getParamFromGroup<bool>(paramGroup, "Problem.UseInteriorDirichletBCs", false))
    {}

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();

        if ( !isBox && (isOnOutlet(globalPos) || isOnInlet(globalPos)) )
            values.setAllDirichlet();

        return values;
    }

    //! Specifies the type of interior boundary condition at a given position
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        if (useInteriorDirichletBCs_)
            values.setAllDirichlet();
        else
            values.setAllNeumann();
        return values;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        static const Scalar deltaP = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.DeltaP");
        if (isOnInlet(globalPos))
            return initialAtPos(globalPos) + PrimaryVariables(deltaP);
        return initialAtPos(globalPos);
    }

    //! Evaluate the Neumann boundary conditions
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        // for box, incorporate Dirichlet Bcs weakly
        if (isBox && (isOnOutlet(scvf.ipGlobal()) || isOnInlet(scvf.ipGlobal())))
        {
            // modify element solution to carry inlet/outlet head
            auto elemSol = elementSolution(element, elemVolVars, fvGeometry);
            for (const auto& curScvf : scvfs(fvGeometry))
            {
                if (curScvf.boundary())
                {
                    if (isOnOutlet(curScvf.ipGlobal()) || isOnInlet(curScvf.ipGlobal()))
                    {
                        const auto diriValues = dirichletAtPos(curScvf.ipGlobal());
                        const auto& insideScv = fvGeometry.scv(curScvf.insideScvIdx());
                        elemSol[insideScv.localDofIndex()][0] = diriValues[0];
                    }
                }
            }

            // evaluate gradients using this element solution
            const auto gradHead = evalGradients(element,
                                                element.geometry(),
                                                fvGeometry.fvGridGeometry(),
                                                elemSol,
                                                scvf.ipGlobal())[0];

            // compute the flux
            const auto& volVars = elemVolVars[scvf.insideScvIdx()];
            Scalar flux = -1.0*vtmv(gradHead, volVars.permeability(), scvf.unitOuterNormal());
            flux *= volVars.density()*volVars.mobility();
            return NumEqVector(flux);
        }

        return NumEqVector(0.0);
    }

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    //! returns true if position is on inlet
    bool isOnInlet(const GlobalPosition& globalPos) const
    { return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + 1e-8 && globalPos[1] < -0.25 + 1e-8; }

    //! returns true if position is on outlet
    bool isOnOutlet(const GlobalPosition& globalPos) const
    { return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - 1e-8 && globalPos[1] > 0.25 - 1e-8; }

    // writes the transfer fluxes to disk
    template<class GridVariables, class Assembler, class SolutionVector, class DomainId>
    void writeTransferFluxes(const std::string& csvFileNameLo,
                             const std::string& csvFileNameHi,
                             const GridVariables& gridVars,
                             const Assembler& assembler,
                             const SolutionVector& x,
                             DomainId domainId) const
    {
        Scalar flux_lo = 0.0;
        Scalar flux_hi = 0.0;

        std::ofstream fluxPlot_lo(csvFileNameLo, std::ios::out);
        std::ofstream fluxPlot_hi(csvFileNameHi, std::ios::out);

        using FluxVars = GetPropType<TypeTag, Properties::FluxVariables>;
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->fvGridGeometry());
            auto elemVolVars = localView(gridVars.curGridVolVars());
            auto elemFluxVarsCache = localView(gridVars.gridFluxVarsCache());

            couplingManagerPtr_->bindCouplingContext(domainId, element, assembler);
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            for (const auto& scvf : scvfs(fvGeometry))
            {
                if (!couplingManagerPtr_->isOnInteriorBoundary(element, scvf))
                    continue;

                const bool isLowerInterface = scvf.unitOuterNormal()[1] > 0.9;
                const bool isUpperInterface = scvf.unitOuterNormal()[1] < -0.9;

                FluxVars fv;
                fv.init(*this, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

                auto up = [] (const auto& vv) { return vv.mobility()*vv.density(); };
                Scalar flux = fv.advectiveFlux(/*phaseIdx*/0, up);

                if (isLowerInterface) { flux_lo += flux; fluxPlot_lo << scvf.center()[0] << "," << flux/scvf.area() << "\n"; }
                else if (isUpperInterface) { flux_hi += flux; fluxPlot_hi << scvf.center()[0] << "," << flux/scvf.area() << "\n"; }
                else DUNE_THROW(Dune::InvalidStateException, "Could not characterize interface");
            }
        }

        std::ofstream transferFluxFile(this->name() + ".csv", std::ios::out);
        transferFluxFile << flux_lo << "," << flux_hi << std::endl;
        transferFluxFile.close();
        fluxPlot_lo.close();
        fluxPlot_hi.close();
    }

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    bool useInteriorDirichletBCs_;
};

} // end namespace Dumux

#endif
