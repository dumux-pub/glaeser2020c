// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The spatial parameters for the bulk domain in the single-phase
 *        tensorial test using the facet coupling models.
 */
#ifndef DUMUX_ONEP_TENSORIAL_FACET_BULK_SPATIALPARAMS_HH
#define DUMUX_ONEP_TENSORIAL_FACET_BULK_SPATIALPARAMS_HH

#include <cmath>
#include <dune/common/fmatrix.hh>
#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux {

/*!
 * \brief The spatial parameters for the bulk domain in the single-phase
 *        tensorial test using the facet coupling models.
 */
template< class FVGridGeometry, class Scalar >
class OnePBulkSpatialParams
: public FVSpatialParamsOneP< FVGridGeometry, Scalar, OnePBulkSpatialParams<FVGridGeometry, Scalar> >
{
    using ThisType = OnePBulkSpatialParams< FVGridGeometry, Scalar >;
    using ParentType = FVSpatialParamsOneP< FVGridGeometry, Scalar, ThisType >;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    static constexpr int dimWorld = GridView::dimensionworld;

public:
    //! export the type used for permeabilities
    using PermeabilityType = Dune::FieldMatrix<Scalar, dimWorld, dimWorld>;

    //! the constructor
    OnePBulkSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry, const std::string& paramGroup)
    : ParentType(fvGridGeometry)
    {
        const auto kh = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");
        const auto ratio = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.PermeabilityAnisotropyRatio");
        const auto angle = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.PermeabilityAngle");
        const auto kv = kh/ratio;

        Scalar cost = cos(angle);
        Scalar sint = sin(angle);

        permeability_ = 0.0;
        permeability_[0][0] = cost*cost*kh + sint*sint*kv;
        permeability_[1][1] = sint*sint*kh + cost*cost*kv;
        permeability_[0][1] = permeability_[1][0] = cost*sint*(kh - kv);
    }

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    { return permeability_; }

    //! Return the porosity
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return 1.0; }

private:
    PermeabilityType permeability_;
};

} // end namespace Dumux

#endif
