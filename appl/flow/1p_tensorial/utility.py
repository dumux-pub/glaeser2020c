import math

# considered schemes are tpfa, mpfa, box, boxdfm
numSchemes = 4

# some stuff used by plot functions
lineStyleArray = ["-","--","-.",":"]

# returns a string with the scheme acronym being given an index
def getSchemeAcronym(schemeIdx):
    if schemeIdx == 0:
        return "tpfa"
    elif schemeIdx == 1:
        return "mpfa"
    elif schemeIdx == 2:
        return "box"
    elif schemeIdx == 3:
        return "boxdfm"

# returns a string with the plot label used for a scheme
def getSchemeLabel(schemeIdx):
    if schemeIdx == 0:
        return "TPFA"
    elif schemeIdx == 1:
        return "MPFA"
    elif schemeIdx == 2:
        return "BOX"
    elif schemeIdx == 3:
        return "BOX-CONT"

# returns the matrix  error plot idx of a scheme
def getMatrixErrorPlotIdx(schemeIdx):
    return schemeIdx
# returns the fracture error plot idx of a scheme
def getFractureErrorPlotIdx(schemeIdx):
    return numSchemes + schemeIdx
# returns the idx of the matrix pressure plot of a scheme
def getMatrixPressurePlotIdx(schemeIdx):
    return numSchemes*2 + schemeIdx
# returns the idx of the matrix pressure plot of a scheme
def getFracturePressurePlotIdx(schemeIdx):
    return numSchemes*3 + schemeIdx
# returns the transfer flux plot idx of a scheme
def getLowTransferFluxPlotIdx(schemeIdx):
    return numSchemes*4 + schemeIdx
# returns the transfer flux plot idx of a scheme
def getHighTransferFluxPlotIdx(schemeIdx):
    return numSchemes*5 + schemeIdx
# returns matrix plot idx of all schemes combined
def getCombinedMatrixPressurePlotIdx():
    return numSchemes*6
# returns matrix plot idx of all schemes combined
def getCombinedFracturePressurePlotIdx():
    return numSchemes*6 + 1

# transforms a given angle into degrees
# note: we round and transform the angle into an integer
def to_degrees(angle):
    return int(round(angle*180.0/math.pi, 1))
