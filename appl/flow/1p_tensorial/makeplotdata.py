import subprocess
import os
import sys
import math
import numpy as np
import matplotlib.pyplot as plt
from utility import *

try:
    from paraview.simple import *
except ImportError:
    print("`paraview.simple` not found. Make sure using pvpython instead of python.")

# indices in the csv files created by paraview
xIdx = 4
pressureIdx = 0

# creates the data of a plot on a provided vtu file
def makePlotOverLineData(vtuFileName, isPolyData, csvFileName, resolution, point1, point2):

    if not os.path.exists(vtuFileName):
        sys.stderr.write("Provided vtu file \"" + vtuFileName + "\" does not exist")
        sys.exit(1)

    print("Reading solution file " + vtuFileName)
    if isPolyData == True:
        vtkFile = XMLPolyDataReader(FileName=vtuFileName)
    else:
        vtkFile = XMLUnstructuredGridReader(FileName=vtuFileName)
    SetActiveSource(vtkFile)

    plotOverLine1 = PlotOverLine(Source="High Resolution Line Source")
    plotOverLine1.Source.Resolution = resolution
    plotOverLine1.Source.Point1 = point1
    plotOverLine1.Source.Point2 = point2

    # write output
    writer = CreateWriter(csvFileName, plotOverLine1, Precision=10)
    writer.UpdatePipeline()


#################################################
# Main script
# Creates the files containing the plot data
##################################################

if len(sys.argv) < 6:
    sys.stderr.write(str( len(sys.argv)-1 ) + " arguments provided. Please provide kf, angle, a, path to the .vtu files and number of refinements to consider\n")
    sys.exit(1)

kf = sys.argv[1]
angle = sys.argv[2]
a = float(sys.argv[3])
vtuFolderPath = sys.argv[4]
numRefinements = int(sys.argv[5])

# plot line specifics
resolution = 2000
resolutionFracture = 2000
matrixPlot_1_source = [-0.5, -0.125, 0.0]
matrixPlot_1_target = [-a/2.0, -a/2.0, 0.0]
matrixPlot_2_source = [a/2.0, a/2.0, 0.0]
matrixPlot_2_target = [0.5, 0.125, 0.0]
fracturePlot_source = [-0.25, 0.0, 0.0]
fracturePlot_target = [0.25, 0.0, 0.0]

# sub-folder to save csv files & plots in
csvFolderPath = vtuFolderPath + "/csv"
if not os.path.exists(csvFolderPath):
    subprocess.call(['mkdir', csvFolderPath])

# reference plot in matrix
referenceFileMatrix = vtuFolderPath + "/tensorial_ref_k_" + kf + "_angle_" + angle + "_matrix.vtu"
referenceFileFracture = vtuFolderPath + "/tensorial_ref_k_" + kf + "_angle_" + angle + "_fracture_average.vtu"
csvFileReference_matrix_1 = csvFolderPath + "/ref_k_" + kf + "_angle_" + angle + "_matrix_1.csv"
csvFileReference_matrix_2 = csvFolderPath + "/ref_k_" + kf + "_angle_" + angle + "_matrix_2.csv"
csvFileReference_fracture = csvFolderPath + "/ref_k_" + kf + "_angle_" + angle + "_fracture.csv"

makePlotOverLineData(referenceFileMatrix, False, csvFileReference_matrix_1, resolution, matrixPlot_1_source, matrixPlot_1_target)
makePlotOverLineData(referenceFileMatrix, False, csvFileReference_matrix_2, resolution, matrixPlot_2_source, matrixPlot_2_target)
makePlotOverLineData(referenceFileFracture, False, csvFileReference_fracture, resolutionFracture, fracturePlot_source, fracturePlot_target)

ref_matrixPlot1Data = np.loadtxt(csvFileReference_matrix_1, delimiter=',', skiprows=1)
ref_matrixPlot2Data = np.loadtxt(csvFileReference_matrix_2, delimiter=',', skiprows=1)
ref_fracturePlotData = np.loadtxt(csvFileReference_fracture, delimiter=',', skiprows=1)

# make plot data for all schemes & discretizations
for count in range(0, numRefinements):

    for schemeIdx in range(0, numSchemes):
        schemeName = getSchemeAcronym(schemeIdx)

        if schemeName == "boxdfm":
            vtuFileName_matrix = vtuFolderPath + "/tensorial_k_" + kf + "_angle_" + angle + "_boxdfm_" + str(count) + "-00001.vtu"
            vtuFileName_fracture = vtuFolderPath + "/tensorial_k_" + kf + "_angle_" + angle + "_boxdfm_" + str(count) + "_fracture-00001.vtp"
        else:
            vtuFileName_matrix = vtuFolderPath + "/tensorial_k_" + kf + "_angle_" + angle + "_bulk_" + schemeName + "_" + str(count) + "-00001.vtu"
            vtuFileName_fracture = vtuFolderPath + "/tensorial_k_" + kf + "_angle_" + angle + "_lowdim_" + schemeName + "_" + str(count) + "-00001.vtp"

        csvFile_matrix_1 = csvFolderPath + "/" + schemeName + "_k_" + kf + "_angle_" + angle + "_" + str(count) + "_matrix_1.csv"
        csvFile_matrix_2 = csvFolderPath + "/" + schemeName + "_k_" + kf + "_angle_" + angle + "_" + str(count) + "_matrix_2.csv"
        csvFile_fracture = csvFolderPath + "/" + schemeName + "_k_" + kf + "_angle_" + angle + "_" + str(count) + "_fracture.csv"

        makePlotOverLineData(vtuFileName_matrix, False, csvFile_matrix_1, resolution, matrixPlot_1_source, matrixPlot_1_target)
        makePlotOverLineData(vtuFileName_matrix, False, csvFile_matrix_2, resolution, matrixPlot_2_source, matrixPlot_2_target)
        makePlotOverLineData(vtuFileName_fracture, True, csvFile_fracture, resolutionFracture, fracturePlot_source, fracturePlot_target)
