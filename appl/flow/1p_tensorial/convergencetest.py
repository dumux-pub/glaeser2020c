import os
import sys
import math
import subprocess
from runrefinementsequence import runRefinementSequence
from makeplots import *

# set some parameters
aperture = 1e-3
numElemsInFracture = 5              # determines dx for the equi-dimensional mesh
fracPerms = [1e-4, 1e-2, 1e2, 1e4]  # fracture permeabilities to check
numRefinements = 5

# prepare array of discretization lengths
dxArray = []
dxArray.append(0.1)
for i in range(1, numRefinements+1):
    dxArray.append(dxArray[i-1]/2.0)
print("dxArray: ", dxArray)

###############
# main script #
###############

# run script
for kf in fracPerms:
    # reset all plots
    clearSolutionPlots()
    clearErrorNormPlots()

    vtuFolderPath = 'kf_' + str(kf)

    if os.path.exists(vtuFolderPath):
        sys.stderr.write("Vtu folder " + vtuFolderPath + " already exists. Please save data and erase it beforehand!\n")
        sys.exit(1)
    else:
        subprocess.call(['mkdir', vtuFolderPath])

    angleCount = 0
    for angle in [0.0, math.pi/4.0, -math.pi/4.0]:
        runRefinementSequence(kf, angle, aperture, numElemsInFracture, dxArray, vtuFolderPath)

        # now, the vtu files are available. Make plot data using pvpython.
        subprocess.call(['pvpython', 'makeplotdata.py', str(kf), str(to_degrees(angle)), str(aperture), vtuFolderPath, str( len(dxArray) )])

        # all csv data is now available. Add data to the plots.
        plotSolutions(kf, angle, dxArray, vtuFolderPath)
        plotErrorNorms(kf, angle, aperture, dxArray, vtuFolderPath, angleCount)
        angleCount += 1

        # save solution plots here
        saveSolutionPlots(angle, vtuFolderPath)
        clearSolutionPlots()

    # save error norm plots here
    saveErrorNormPlots(vtuFolderPath)
