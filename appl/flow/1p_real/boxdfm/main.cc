// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief One-phase tensorial test case using facet coupling models
 */
#include <config.h>
#include <iostream>

#include <config.h>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/foamgrid/foamgrid.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/istl/matrixmarket.hh>

#include "problem.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>
#include <dumux/common/geometry/diameter.hh>

#include <dumux/linear/amgbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>
#include <dumux/discretization/evalsolution.hh>
#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/codimonegridadapter.hh>

#include <dumux/porousmediumflow/boxdfm/vtkoutputmodule.hh>

// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // initialize parameter tree
    Parameters::init(argc, argv);

    // get type tags from CMakeLists
    using TypeTag = Properties::TTag::OnePBoxDfm;     // (must be declared in CMakeLists.txt)

    // we reuse the facet coupling grid manager to create the grid
    // from a mesh file with the fractures being incorporated as
    // lower-dimensional elements.
    using Grid = Dumux::GetPropType<TypeTag,  Dumux::Properties::Grid>;
    using FractureGrid = FRACTUREGRIDTYPE;
    using GridManager = Dumux::FacetCouplingGridManager<Grid, FractureGrid>;

    GridManager gridManager;
    gridManager.init();

    // use the grid adapter from the facet coupling framework to
    // identify the grid facets that coincide with a fracture.
    // For instantiation we extract the info on the embeddings from
    // the grid manager (info is read from the grid file)
    using MatrixFractureGridAdapter = CodimOneGridAdapter<typename GridManager::Embeddings>;
    MatrixFractureGridAdapter fractureGridAdapter(gridManager.getEmbeddings());

    // matrix grid view is the first one (index 0) inside the manager
    const auto& leafGridView = gridManager.template grid<0>().leafGridView();

    // create the finite volume grid geometry
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    auto fvGridGeometry = std::make_shared<FVGridGeometry>(leafGridView);
    fvGridGeometry->update(fractureGridAdapter);

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(fvGridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x(fvGridGeometry->numDofs());
    problem->applyInitialSolution(x);

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, fvGridGeometry);
    gridVariables->init(x);

    // intialize the vtk output module
    using VtkOutputModule = BoxDfmVtkOutputModule<GridVariables, SolutionVector, FractureGrid>;
    using VtkOutputFields = GetPropType<TypeTag, Properties::VtkOutputFields>;
    VtkOutputModule vtkWriter(*gridVariables, x, problem->name(), fractureGridAdapter, "", Dune::VTK::nonconforming);
    VtkOutputFields::initOutputModule(vtkWriter); //!< Add model specific output fields
    vtkWriter.write(0.0);

    // the assembler with time loop for instationary problem
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, fvGridGeometry, gridVariables);

    // print number of dofs & non-zero entries in matrix
    std::cout << "NumDofs: " << fvGridGeometry->numDofs() << std::endl;

    std::size_t nnz = 0;
    assembler->setLinearSystem();
    assembler->assembleJacobianAndResidual(x);
    for (const auto& row : assembler->jacobian())
        for (const auto& col : row)
            if (col != 0.0)
                nnz++;
    std::cout << "Number of non-zeroes: " << nnz << std::endl;

    // solve using specified linear solver
    const auto solverType = getParam<std::string>("LinearSolver.Type");
    if (solverType == "SuperLU")
    {
        //auto linearSolver = std::make_shared<SuperLUBackend>();
        //using NewtonSolver = Dumux::NewtonSolver<Assembler, SuperLUBackend>;
        //auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver);
        //newtonSolver->solve(x);
    }
    else if (solverType == "UMFPack")
    {
        auto linearSolver = std::make_shared<UMFPackBackend>();
        using NewtonSolver = Dumux::NewtonSolver<Assembler, UMFPackBackend>;
        auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver);
        newtonSolver->solve(x);
    }
    else if (solverType == "ILU0CG")
    {
        auto linearSolver = std::make_shared<ILU0CGBackend>();
        using NewtonSolver = Dumux::NewtonSolver<Assembler, ILU0CGBackend>;
        auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver);
        newtonSolver->solve(x);
    }
    else if (solverType == "ILU0BiCGSTAB")
    {
        auto linearSolver = std::make_shared<ILU0BiCGSTABBackend>();
        using NewtonSolver = Dumux::NewtonSolver<Assembler, ILU0BiCGSTABBackend>;
        auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver);
        newtonSolver->solve(x);
    }
    else if (solverType == "BlockDiagILU0BiCGSTAB")
    {
        return 0;
    }
    else if (solverType == "BlockDiagAMGBiCGSTAB")
    {
        return 0;
    }
    else
        DUNE_THROW(Dune::InvalidStateException, "Invalid solver type provided");

    // update variables
    gridVariables->update(x);

    // write vtk file
    vtkWriter.write(1.0);

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/false);

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
