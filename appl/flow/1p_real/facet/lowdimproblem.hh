// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for the lower-dimensional domain in the single-phase
 *        "real case" test using the facet coupling models.
 */
#ifndef DUMUX_ONEP_REAL_FACET_LOWDIM_PROBLEM_HH
#define DUMUX_ONEP_REAL_FACET_LOWDIM_PROBLEM_HH

#include <dune/foamgrid/foamgrid.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/multilineargeometry.hh>

#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>

#include <dumux/multidomain/facet/box/properties.hh>
#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>
#include <dumux/multidomain/facet/cellcentered/mpfa/properties.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/1p/model.hh>

#include "spatialparams.hh"

namespace Dumux {
// forward declarations
template<class TypeTag> class OnePLowDimProblem;

namespace Properties {
namespace TTag {

// create the type tag nodes
struct OnePLowDim { using InheritsFrom = std::tuple<OneP>; };
struct OnePLowDimTpfa { using InheritsFrom = std::tuple<OnePLowDim, CCTpfaModel>; };
struct OnePLowDimMpfa { using InheritsFrom = std::tuple<OnePLowDim, CCTpfaModel>; };
struct OnePLowDimBox { using InheritsFrom = std::tuple<OnePLowDim, BoxModel>; };

struct OnePFacetTpfa { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, OnePLowDim>; };
struct OnePFacetMpfa { using InheritsFrom = std::tuple<CCMpfaFacetCouplingModel, OnePLowDim>; };
struct OnePFacetBox { using InheritsFrom = std::tuple<BoxFacetCouplingModel, OnePLowDim>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePLowDim> { using type = FACETGRIDTYPE; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::OnePLowDim> { using type = OnePLowDimProblem<TypeTag>; };

// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePLowDim>
{
private:
    using FVGridGeometry = GetPropType<TypeTag, FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Scalar>;

public:
    using type = OnePSpatialParams< FVGridGeometry, Scalar >;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePLowDim>
{
private:
    using Scalar = GetPropType<TypeTag, Scalar>;

public:
    using type = FluidSystems::OnePLiquid< Scalar, Components::Constant<1, Scalar> >;
};

// permeability is constant
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::OnePLowDim> { static constexpr bool value = false; };

} // end namespace Properties

/*!
 * \brief The problem for the lower-dimensional domain in the single-phase
 *        tensorial test using the facet coupling models.
 */
template<class TypeTag>
class OnePLowDimProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr bool isBox = FVGridGeometry::discMethod == DiscretizationMethod::box;
    static constexpr bool is3dProblem = dimWorld == 3;

public:
    OnePLowDimProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                      std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                      std::shared_ptr<CouplingManager> couplingManager,
                      const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManager)
    , aperture_(getParamFromGroup<Scalar>(paramGroup, "Problem.ExtrusionFactor"))
    {}

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();

        // Note that this will not set Dirichlet on the nodes that
        // also are on lower-dimensional edges for box!
        if ( isOnOutlet(globalPos) || isOnInlet(globalPos) )
            values.setAllDirichlet();

        return values;
    }

    //! Specifies the type of interior boundary condition at a given position
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        static const bool useDiri = getParam<bool>("FacetCoupling.UseInteriorDirichletBCs");

        BoundaryTypes values;
        if (useDiri)
            values.setAllDirichlet();
        else
            values.setAllNeumann();
        return values;
    }

    //! Evaluate the source term at a given position
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        // evaluate sources from bulk domain
        auto source = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);
        source /= scv.volume()*elemVolVars[scv].extrusionFactor();
        return source;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        static const Scalar deltaP = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.DeltaP");
        if (isOnInlet(globalPos))
            return initialAtPos(globalPos) + PrimaryVariables(deltaP);
        return initialAtPos(globalPos);
    }

    //! Evaluate the Neumann boundary conditions
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        // for box, incorporate Dirichlet Bcs weakly
        if (isBox && (isOnOutlet(scvf.ipGlobal()) || isOnInlet(scvf.ipGlobal())))
        {
            // modify element solution to carry inlet/outlet head
            auto elemSol = elementSolution(element, elemVolVars, fvGeometry);
            for (const auto& curScvf : scvfs(fvGeometry))
            {
                if (curScvf.boundary())
                {
                    if (isOnOutlet(curScvf.ipGlobal()) || isOnInlet(curScvf.ipGlobal()))
                    {
                        const auto diriValues = dirichletAtPos(curScvf.ipGlobal());
                        const auto& insideScv = fvGeometry.scv(curScvf.insideScvIdx());
                        elemSol[insideScv.localDofIndex()][0] = diriValues[0];
                    }
                }
            }

            // evaluate gradients using this element solution
            const auto gradHead = evalGradients(element,
                                                element.geometry(),
                                                fvGeometry.fvGridGeometry(),
                                                elemSol,
                                                scvf.ipGlobal())[0];

            // compute the flux
            const auto& volVars = elemVolVars[scvf.insideScvIdx()];
            Scalar flux = -1.0*vtmv(gradHead, volVars.permeability(), scvf.unitOuterNormal());
            flux *= volVars.density()*volVars.mobility();
            return NumEqVector(flux);
        }

        return NumEqVector(0.0);
    }

    //! Set the aperture as extrusion factor.
    Scalar extrusionFactorAtPos(const GlobalPosition& globalPos) const
    { return aperture_; }

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    //! returns true if position is on inlet
    bool isOnInlet(const GlobalPosition& globalPos) const
    { return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + 1e-6; }

    //! returns true if position is on outlet
    bool isOnOutlet(const GlobalPosition& globalPos) const
    { return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - 1e-6; }

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    Scalar aperture_;
};

} // end namespace Dumux

#endif
