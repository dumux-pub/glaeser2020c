// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief One-phase "real" test case using facet coupling models
 */
#include <config.h>
#include <iostream>
#include <limits>
#include <algorithm>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/type.hh>
#include <dune/istl/matrixmarket.hh>

#include "bulkproblem.hh"
#include "lowdimproblem.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/geometry/diameter.hh>

#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/evalsolution.hh>
#include <dumux/linear/seqsolverbackend.hh>

#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/traits.hh>

#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>
#include <dumux/multidomain/facet/codimonegridadapter.hh>

#include <dumux/io/vtkoutputmodule.hh>

// updates the finite volume grid geometry. This is necessary as the finite volume
// grid geometry for the box scheme with facet coupling requires additional data for
// the update. The reason is that we have to create additional faces on interior
// boundaries, which wouldn't be created in the standard scheme.
template< class FVGridGeometry,
       class GridManager,
       class LowDimGridView,
       std::enable_if_t<FVGridGeometry::discMethod == Dumux::DiscretizationMethod::box, int> = 0 >
void updateBulkFVGridGeometry(FVGridGeometry& fvGridGeometry,
                           const GridManager& gridManager,
                           const LowDimGridView& lowDimGridView)
{
    using BulkFacetGridAdapter = Dumux::CodimOneGridAdapter<typename GridManager::Embeddings>;
    BulkFacetGridAdapter facetGridAdapter(gridManager.getEmbeddings());
    fvGridGeometry.update(lowDimGridView, facetGridAdapter);
}

// specialization for cell-centered schemes
template< class FVGridGeometry,
       class GridManager,
       class LowDimGridView,
       std::enable_if_t<FVGridGeometry::discMethod != Dumux::DiscretizationMethod::box, int> = 0 >
void updateBulkFVGridGeometry(FVGridGeometry& fvGridGeometry,
                           const GridManager& gridManager,
                           const LowDimGridView& lowDimGridView)
{
    fvGridGeometry.update();
}

// Function to print out info on interaction volumes
template<class FVGridGeometry, class Problem,
         std::enable_if_t<FVGridGeometry::discMethod == Dumux::DiscretizationMethod::ccmpfa, int> = 0>
void printInteractionVolumeInfo(const FVGridGeometry& fvGridGeometry, const Problem& problem)
{
    using std::min;
    using std::max;

    Dune::Timer timer;
    std::cout << "Gathering interaction volume info" << std::endl;

    Dune::FieldVector<double, FVGridGeometry::GridView::dimensionworld> maxPosition;
    std::size_t minNumIVUnknowns = std::numeric_limits<std::size_t>::max();
    std::size_t maxNumIVUnknowns = std::numeric_limits<std::size_t>::min();
    std::size_t avgNumIVUnknowns = 0;

    std::size_t minElementStencil = std::numeric_limits<std::size_t>::max();
    std::size_t maxElementStencil = std::numeric_limits<std::size_t>::min();
    std::size_t avgElementStencil = 0;

    // lambda that returns the number of unknowns of an interaction
    // volume and fills the provided stencil vector with the IV's stencil
    auto getIVInfo = [] (const auto& iv, auto& stencil) -> std::size_t
    {
        stencil.clear();
        stencil.insert(stencil.end(), iv.stencil().begin(), iv.stencil().end());
        return iv.numUnknowns();
    };

    std::size_t ivCount = 0;
    std::vector<bool> vertexHandled(fvGridGeometry.vertexMapper().size(), false);
    for (const auto& element : elements(fvGridGeometry.gridView()))
    {
        auto fvGeometry = localView(fvGridGeometry);
        fvGeometry.bind(element);

        std::vector<std::size_t> elementStencil;
        for (const auto& scvf : scvfs(fvGeometry))
        {
            std::size_t numDofs;
            std::vector<std::size_t> ivStencil;
            if (fvGridGeometry.vertexUsesSecondaryInteractionVolume(scvf.vertexIndex()))
            {
                typename FVGridGeometry::GridIVIndexSets::SecondaryInteractionVolume iv;
                iv.bind( fvGridGeometry.gridInteractionVolumeIndexSets().secondaryIndexSet(scvf),
                         problem,
                         fvGeometry);
                numDofs = getIVInfo(iv, ivStencil);
            }
            else
            {
                typename FVGridGeometry::GridIVIndexSets::PrimaryInteractionVolume iv;
                iv.bind( fvGridGeometry.gridInteractionVolumeIndexSets().primaryIndexSet(scvf),
                         problem,
                         fvGeometry);
                numDofs = getIVInfo(iv, ivStencil);
            }

            if (!vertexHandled[scvf.vertexIndex()])
            {
                if (maxNumIVUnknowns < numDofs)
                {
                    maxNumIVUnknowns = numDofs;
                    maxPosition = scvf.vertexCorner();
                }
                minNumIVUnknowns = min(minNumIVUnknowns, numDofs);
                avgNumIVUnknowns += numDofs;
                vertexHandled[scvf.vertexIndex()] = true;
                ivCount++;
            }

            elementStencil.insert(elementStencil.end(), ivStencil.begin(), ivStencil.end());
        }

        // make element stencil unique
        std::sort(elementStencil.begin(), elementStencil.end());
        elementStencil.erase(std::unique(elementStencil.begin(), elementStencil.end()), elementStencil.end());

        minElementStencil = min(minElementStencil, elementStencil.size());
        maxElementStencil = max(maxElementStencil, elementStencil.size());
        avgElementStencil += elementStencil.size();
    }

    if (ivCount != fvGridGeometry.gridView().size(FVGridGeometry::GridView::dimension))
        DUNE_THROW(Dune::InvalidStateException, "Unexpected number of interaction volumes visited");

    avgNumIVUnknowns /= ivCount;
    avgElementStencil /= fvGridGeometry.gridView().size(0);

    // print info
    std::cout << "Max number of unknowns in IV:     " << maxNumIVUnknowns << ", at " << maxPosition << std::endl
              << "Min number of unknowns in IV:     " << minNumIVUnknowns << std::endl
              << "Average number of unknowns in IV: " << avgNumIVUnknowns << std::endl
              << "Max element stencil size:         " << maxElementStencil << std::endl
              << "Min element stencil size:         " << minElementStencil << std::endl
              << "Average element stencil size:     " << avgElementStencil << std::endl;
    std::cout << "This took " << timer.elapsed() << " seconds" << std::endl;

}

// Function to print out info on interaction volumes
template<class FVGridGeometry, class Problem,
         std::enable_if_t<FVGridGeometry::discMethod != Dumux::DiscretizationMethod::ccmpfa, int> = 0>
void printInteractionVolumeInfo(const FVGridGeometry& fvGridGeometry, const Problem& problem)
{}

// obtain/define some types to be used below in the property definitions and in main
template< class BulkTypeTag, class LowDimTypeTag >
class TestTraits
{
    using BulkFVGridGeometry = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::FVGridGeometry>;
    using LowDimFVGridGeometry = Dumux::GetPropType<LowDimTypeTag, Dumux::Properties::FVGridGeometry>;
public:
    using MDTraits = Dumux::MultiDomainTraits<BulkTypeTag, LowDimTypeTag>;
    using CouplingMapper = Dumux::FacetCouplingMapper<BulkFVGridGeometry, LowDimFVGridGeometry>;
    using CouplingManager = Dumux::FacetCouplingManager<MDTraits, CouplingMapper>;
};

// set the coupling manager property for all available type tags
namespace Dumux {
namespace Properties {

// set cm property for the tests
using BoxTraits = TestTraits<TTag::OnePBulkBox, TTag::OnePLowDimBox>;
using TpfaTraits = TestTraits<TTag::OnePBulkTpfa, TTag::OnePLowDimTpfa>;
using MpfaTraits = TestTraits<TTag::OnePBulkMpfa, TTag::OnePLowDimMpfa>;

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePBulkBox> { using type = typename BoxTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePLowDimBox> { using type = typename BoxTraits::CouplingManager; };

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePBulkTpfa> { using type = typename TpfaTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePLowDimTpfa> { using type = typename TpfaTraits::CouplingManager; };

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePBulkMpfa> { using type = typename MpfaTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePLowDimMpfa> { using type = typename MpfaTraits::CouplingManager; };

} // end namespace Properties
} // end namespace Dumux

// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // initialize parameter tree
    Parameters::init(argc, argv);

    // get type tags from CMakeLists
    using BulkProblemTypeTag = Dumux::Properties::TTag::BULKTYPETAG;     // (must be declared in CMakeLists.txt)
    using LowDimProblemTypeTag = Dumux::Properties::TTag::LOWDIMTYPETAG; // (must be declared in CMakeLists.txt)

    //////////////////////////////////////////////////////
    // try to create the grids (from the given grid file)
    //////////////////////////////////////////////////////
    using BulkGrid = Dumux::GetPropType<BulkProblemTypeTag,  Dumux::Properties::Grid>;
    using LowDimGrid = Dumux::GetPropType<LowDimProblemTypeTag,  Dumux::Properties::Grid>;

    Dumux::FacetCouplingGridManager<BulkGrid, LowDimGrid> gridManager;
    gridManager.init();
    gridManager.loadBalance();

    ////////////////////////////////////////////////////////////
    // run stationary, non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid views
    const auto& bulkGridView = gridManager.template grid<0>().leafGridView();
    const auto& lowDimGridView = gridManager.template grid<1>().leafGridView();

    // create the finite volume grid geometries
    using BulkFVGridGeometry = GetPropType<BulkProblemTypeTag, Properties::FVGridGeometry>;
    using LowDimFVGridGeometry = GetPropType<LowDimProblemTypeTag, Properties::FVGridGeometry>;
    auto bulkFvGridGeometry = std::make_shared<BulkFVGridGeometry>(bulkGridView);
    auto lowDimFvGridGeometry = std::make_shared<LowDimFVGridGeometry>(lowDimGridView);
    updateBulkFVGridGeometry(*bulkFvGridGeometry, gridManager, lowDimGridView);
    lowDimFvGridGeometry->update();

    // the coupling mapper
    using TestTraits = TestTraits<BulkProblemTypeTag, LowDimProblemTypeTag>;
    auto couplingMapper = std::make_shared<typename TestTraits::CouplingMapper>();
    couplingMapper->update(*bulkFvGridGeometry, *lowDimFvGridGeometry, gridManager.getEmbeddings());

    // the coupling manager
    using CouplingManager = typename TestTraits::CouplingManager;
    auto couplingManager = std::make_shared<CouplingManager>();

    // the problems (boundary conditions)
    using BulkProblem = GetPropType<BulkProblemTypeTag, Properties::Problem>;
    using LowDimProblem = GetPropType<LowDimProblemTypeTag, Properties::Problem>;
    auto bulkSpatialParams = std::make_shared<typename BulkProblem::SpatialParams>(bulkFvGridGeometry, "Bulk");
    auto bulkProblem = std::make_shared<BulkProblem>(bulkFvGridGeometry, bulkSpatialParams, couplingManager, "Bulk");
    auto lowDimSpatialParams = std::make_shared<typename LowDimProblem::SpatialParams>(lowDimFvGridGeometry, "LowDim");
    auto lowDimProblem = std::make_shared<LowDimProblem>(lowDimFvGridGeometry, lowDimSpatialParams, couplingManager, "LowDim");

    // the solution vector
    using MDTraits = typename TestTraits::MDTraits;
    using SolutionVector = typename MDTraits::SolutionVector;
    SolutionVector x;

    static const auto bulkId = typename MDTraits::template SubDomain<0>::Index();
    static const auto lowDimId = typename MDTraits::template SubDomain<1>::Index();
    x[bulkId].resize(bulkFvGridGeometry->numDofs());
    x[lowDimId].resize(lowDimFvGridGeometry->numDofs());
    bulkProblem->applyInitialSolution(x[bulkId]);
    lowDimProblem->applyInitialSolution(x[lowDimId]);

    // initialize coupling manager
    couplingManager->init(bulkProblem, lowDimProblem, couplingMapper, x);

    // (maybe) print interaction volume info
    printInteractionVolumeInfo(*bulkFvGridGeometry, *bulkProblem);

    // the grid variables
    using BulkGridVariables = GetPropType<BulkProblemTypeTag, Properties::GridVariables>;
    using LowDimGridVariables = GetPropType<LowDimProblemTypeTag, Properties::GridVariables>;
    auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkProblem, bulkFvGridGeometry);
    auto lowDimGridVariables = std::make_shared<LowDimGridVariables>(lowDimProblem, lowDimFvGridGeometry);
    bulkGridVariables->init(x[bulkId]);
    lowDimGridVariables->init(x[lowDimId]);

    // intialize the vtk output module
    using BulkVtkWriter = VtkOutputModule<BulkGridVariables, GetPropType<BulkProblemTypeTag, Properties::SolutionVector>>;
    using LowDimVtkWriter = VtkOutputModule<LowDimGridVariables, GetPropType<LowDimProblemTypeTag, Properties::SolutionVector>>;
    const auto bulkDM = BulkFVGridGeometry::discMethod == DiscretizationMethod::box ? Dune::VTK::nonconforming : Dune::VTK::conforming;
    BulkVtkWriter bulkVtkWriter(*bulkGridVariables, x[bulkId], bulkProblem->name(), "Bulk", bulkDM);
    LowDimVtkWriter lowDimVtkWriter(*lowDimGridVariables, x[lowDimId], lowDimProblem->name(), "LowDim");

    // Add model specific output fields
    using BulkVtkOutputFields = GetPropType<BulkProblemTypeTag, Properties::VtkOutputFields>;
    using LowDimVtkOutputFields = GetPropType<LowDimProblemTypeTag, Properties::VtkOutputFields>;
    BulkVtkOutputFields::initOutputModule(bulkVtkWriter);
    LowDimVtkOutputFields::initOutputModule(lowDimVtkWriter);

    // write initial solution
    bulkVtkWriter.write(0.0);
    lowDimVtkWriter.write(0.0);

    // the assembler
    using Assembler = MultiDomainFVAssembler<MDTraits, CouplingManager, DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>( std::make_tuple(bulkProblem, lowDimProblem),
                                                  std::make_tuple(bulkFvGridGeometry, lowDimFvGridGeometry),
                                                  std::make_tuple(bulkGridVariables, lowDimGridVariables),
                                                  couplingManager);

    // print number of dofs & non-zero entries in matrix
    std::cout << "NumDofsMatrix: " << bulkFvGridGeometry->numDofs() << std::endl
              << "NumDofsFracture: " << lowDimFvGridGeometry->numDofs() << std::endl
              << "NumDofsTotal: " << bulkFvGridGeometry->numDofs()+lowDimFvGridGeometry->numDofs() << std::endl;

    std::size_t nnz = 0;
    using namespace Dune::Hybrid;
    assembler->setLinearSystem();
    Dune::Timer assemblyTimer;
    assembler->assembleJacobianAndResidual(x);
    std::cout << "Assembly took " << assemblyTimer.elapsed() << " seconds" << std::endl;

    const auto& J = assembler->jacobian();
    forEach(integralRange(Dune::index_constant<2>()), [&](auto&& i)
    {
        forEach(integralRange(Dune::index_constant<2>()), [&](auto&& j)
        {
            for (const auto& row : J[i][j])
                for (const auto& col : row)
                    if (col != 0.0)
                        nnz++;
        });
    });
    std::cout << "Number of non-zeroes: " << nnz << std::endl;

    // solve using specified linear solver
    const auto solverType = getParam<std::string>("LinearSolver.Type");
    if (solverType == "SuperLU")
    {
        //auto linearSolver = std::make_shared<SuperLUBackend>();
        //using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, SuperLUBackend, CouplingManager>;
        //auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);
        //newtonSolver->solve(x);
    }
    else if (solverType == "UMFPack")
    {
        auto linearSolver = std::make_shared<UMFPackBackend>();
        using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, UMFPackBackend, CouplingManager>;
        auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);
        newtonSolver->solve(x);
    }
    else if (solverType == "ILU0CG")
    {
        auto linearSolver = std::make_shared<ILU0CGBackend>();
        using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, ILU0CGBackend, CouplingManager>;
        auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);
        newtonSolver->solve(x);
    }
    else if (solverType == "ILU0BiCGSTAB")
    {
        auto linearSolver = std::make_shared<ILU0BiCGSTABBackend>();
        using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, ILU0BiCGSTABBackend, CouplingManager>;
        auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);
        newtonSolver->solve(x);
    }
    else if (solverType == "BlockDiagILU0BiCGSTAB")
    {
        auto linearSolver = std::make_shared<BlockDiagILU0BiCGSTABSolver>();
        using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, BlockDiagILU0BiCGSTABSolver, CouplingManager>;
        auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);
        newtonSolver->solve(x);
    }
    else if (solverType == "BlockDiagAMGBiCGSTAB")
    {
        auto linearSolver = std::make_shared<BlockDiagAMGBiCGSTABSolver>();
        using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, BlockDiagAMGBiCGSTABSolver, CouplingManager>;
        auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);
        newtonSolver->solve(x);
    }
    else
        DUNE_THROW(Dune::InvalidStateException, "Invalid solver type provided");

    // update grid variables for output
    bulkGridVariables->update(x[bulkId]);
    lowDimGridVariables->update(x[lowDimId]);

    // write vtk output
    bulkVtkWriter.write(1.0);
    lowDimVtkWriter.write(1.0);

    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/false);

    return 0;
}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
