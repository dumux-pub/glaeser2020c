import math
import sys
import subprocess
import numpy as np
from utility import *
from makeplots import *

import matplotlib.pyplot as plt

# use plot style
sys.path.append("./../../common")
import plotstyles

prop_cycle = list(plt.rcParams['axes.prop_cycle'])
markers = ['o', '^', 'x', '.', ',', 's']

####################################################
# runs all schemes and produces two pressure plots #
####################################################
for i in range(0, numSchemes):
    scheme = getSchemeAcronym(i)
    print("\nUMFPACK SOLVE USING " + getSchemeLabel(i))
    subprocess.call(["./" + getSchemeSubFolder(i) + "/" + "test_1p_real_" + getSchemeAcronym(i),
                     "-Problem.Name", "1p_real_" + scheme,
                     "-Bulk.Problem.Name", "1p_real_bulk_" + scheme,
                     "-LowDim.Problem.Name", "1p_real_lowdim_" + scheme,
                     "-Grid.File", "grids/real.msh",
                     "-LinearSolver.Type", "UMFPack"])

print("\n\nMaking plot data")
subprocess.call(['pvpython', 'makeplotdata.py', 'false'])

print("\n\nMaking plots")
makePlots(False)
#
# for i in range(0, numSchemes):
#     scheme = getSchemeAcronym(i)
#     print("\nSUPERLU SOLVE USING " + getSchemeLabel(i))
#     subprocess.call(["./" + getSchemeSubFolder(i) + "/" + "test_1p_real_" + getSchemeAcronym(i),
#                      "-Problem.Name", "1p_real_" + scheme,
#                      "-Bulk.Problem.Name", "1p_real_bulk_" + scheme,
#                      "-LowDim.Problem.Name", "1p_real_lowdim_" + scheme,
#                      "-LinearSolver.Type", "SuperLU"])
#
# for i in range(0, numSchemes):
#     scheme = getSchemeAcronym(i)
#     print("\nBiCGSTAB SOLVE USING " + getSchemeLabel(i))
#     subprocess.call(["./" + getSchemeSubFolder(i) + "/" + "test_1p_real_" + getSchemeAcronym(i),
#                      "-Problem.Name", "1p_real_" + scheme,
#                      "-Bulk.Problem.Name", "1p_real_bulk_" + scheme,
#                      "-LowDim.Problem.Name", "1p_real_lowdim_" + scheme,
#                      "-LinearSolver.Type", "BiCGSTAB"])
