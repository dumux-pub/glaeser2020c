// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Runs the reference solution and various refinements
 *        for the lower-dimensional approaches and computes the errors.
 */
#include <config.h>
#include <iostream>

#include <dune/functions/gridfunctions/analyticgridviewfunction.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/subgrid/subgrid.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/istl/bvector.hh>

#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/dumuxmessage.hh>

#include "reference/solver.hh"
#include "facet/solver.hh"
#include "boxdfm/solver.hh"

#include <dumux/io/vtk/vtkreader.hh>
#include <dumux/io/grid/gridmanager_sub.hh>

#include <dumux/discretization/functionspacebasis.hh>
#include <dumux/common/integrate.hh>
#include <dumux/multidomain/glue.hh>
#include <dumux/discretization/projection/projector.hh>

#include "averagefracturesolution.hh"

// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // initialize parameter tree
    Parameters::init(argc, argv);

    // the type tag used for the reference solution
    using RefTypeTag = Dumux::Properties::TTag::OnePMpfa;
    using ReferenceGrid = GetPropType<RefTypeTag, Properties::Grid>;
    using ReferenceGridManager = Dumux::GridManager<ReferenceGrid>;
    using ReferenceSolution = GetPropType<RefTypeTag, Properties::SolutionVector>;

    using SubGrid = Dune::SubGrid<ReferenceGrid::dimension, ReferenceGrid>;
    using SubGridManager = Dumux::GridManager<SubGrid>;
    using SubFVGridGeometry = Dumux::CCTpfaFVGridGeometry<typename SubGrid::LeafGridView>;

    // compute and parse reference sol + geometry
    ReferenceGridManager refGridManager;
    SubGridManager subGridManagerMatrix, subGridManagerFracture;
    ReferenceSolution matrixSolution, fractureSolution;
    std::shared_ptr<SubFVGridGeometry> matrixFvGridGeometry, fractureFvGridGeometry;

    // run reference
    const std::string problemNameBody = getParam<std::string>("Problem.Name");
    auto assignRefName = [&problemNameBody] (auto& paramTree)
                           {
                               paramTree["Problem.Name"] = problemNameBody + "_ref";
                           };
    Parameters::init(assignRefName);
    {
        // run simulation for reference solution
        auto refData = solveReference<RefTypeTag>("ReferenceGrid");

        // create sub-grids for matrix & fracture
        const auto& refProblem = *refData.problem;

        auto matrixElemSelector = [&refProblem] (const auto& element) -> bool
        { return !refProblem.spatialParams().isInFracture(element.geometry().center()); };

        auto fractureElemSelector = [&refProblem] (const auto& element) -> bool
        { return refProblem.spatialParams().isInFracture(element.geometry().center()); };

        std::cout << "\nMaking sub-grids" << std::endl;
        refGridManager = refData.gridManager;
        auto& refGrid = refGridManager.grid();
        subGridManagerMatrix.init(refGrid, matrixElemSelector);
        subGridManagerFracture.init(refGrid, fractureElemSelector);

        const auto& matrixGridView = subGridManagerMatrix.grid().leafGridView();
        const auto& fractureGridView = subGridManagerFracture.grid().leafGridView();

        // project solution onto the sub-grids
        std::cout << "Making sub-grid geometries" << std::endl;
        matrixFvGridGeometry = std::make_shared<SubFVGridGeometry>(matrixGridView);
        fractureFvGridGeometry = std::make_shared<SubFVGridGeometry>(fractureGridView);

        matrixFvGridGeometry->update();
        fractureFvGridGeometry->update();

        std::cout << "Making projectors" << std::endl;
        const auto matrixGlue = makeGlue(*refData.fvGridGeometry, *matrixFvGridGeometry);
        const auto fractureGlue = makeGlue(*refData.fvGridGeometry, *fractureFvGridGeometry);

        const auto& refBasis = getFunctionSpaceBasis(*refData.fvGridGeometry);
        const auto& matrixBasis = getFunctionSpaceBasis(*matrixFvGridGeometry);
        const auto& fractureBasis = getFunctionSpaceBasis(*fractureFvGridGeometry);

        const auto matrixProjector = Dumux::makeProjector(refBasis, matrixBasis, matrixGlue);
        const auto fractureProjector = Dumux::makeProjector(refBasis, fractureBasis, fractureGlue);

        auto params = matrixProjector.defaultParams();
        params.residualReduction = 1e-16;

        matrixSolution = matrixProjector.project(refData.solution, params);
        fractureSolution = fractureProjector.project(refData.solution, params);
        std::cout << "Finished projection" << std::endl;
    }

    const auto& matrixBasis = getFunctionSpaceBasis(*matrixFvGridGeometry);
    const auto& fractureBasis = getFunctionSpaceBasis(*fractureFvGridGeometry);

    // Make grid functions for pressure in both matrix & fracture
    using namespace Dune::Functions;
    using RefBlockType = typename ReferenceSolution::block_type;
    auto gfMatrix = makeDiscreteGlobalBasisFunction<RefBlockType>(matrixBasis, matrixSolution);
    auto gfFracture = makeDiscreteGlobalBasisFunction<RefBlockType>(fractureBasis, fractureSolution);

    // average fracture solution
    fractureSolution = computeAveragedFractureSolution(*fractureFvGridGeometry, fractureSolution);

    // write out (averaged) fracture solution and matrix solution separately
    Dune::VTKWriter< std::decay_t<decltype(fractureBasis.gridView())> > avgFracSolWriter(fractureBasis.gridView());
    avgFracSolWriter.addCellData(fractureSolution, "p");
    avgFracSolWriter.write(getParamFromGroup<std::string>("ReferenceSolution", "VtkFile") + "_fracture_average");

    Dune::VTKWriter< std::decay_t<decltype(matrixBasis.gridView())> > matrixSolWriter(matrixBasis.gridView());
    matrixSolWriter.addCellData(matrixSolution, "p");
    matrixSolWriter.write(getParamFromGroup<std::string>("ReferenceSolution", "VtkFile") + "_matrix");

    // integrate the squared functions define relative errors later
    // we abuse integrateL2Error() here and define a zero-function as
    // second function w.r.t. which to compute the relative errors
    auto zeroF = [] (const auto& pos) { return 0.0; };
    auto zeroFunctionMatrix = makeAnalyticGridViewFunction(zeroF, matrixBasis.gridView());
    auto zeroFunctionFracture = makeAnalyticGridViewFunction(zeroF, fractureBasis.gridView());

    const int intOrder = getParam<int>("L2Norm.IntegrationOrder");
    const auto aperture = getParam<double>("LowDim.Problem.ExtrusionFactor");
    const auto integralMatrix = Dumux::integrateL2Error(matrixBasis.gridView(), gfMatrix, zeroFunctionMatrix, intOrder);
    const auto integralFracture = Dumux::integrateL2Error(fractureBasis.gridView(), gfFracture, zeroFunctionFracture, intOrder)/aperture;

    std::cout << "Integral matrix: " << integralMatrix << std::endl;
    std::cout << "Integral fracture: " << integralFracture << std::endl;

    ////////////////////////////
    // Compute L2 error norms //
    ////////////////////////////
    const auto l2FileBody = getParam<std::string>("L2Norm.OutputFile");
    std::ofstream l2ErrorsMatrix(l2FileBody + "_matrix.txt", std::ios::out);
    std::ofstream l2ErrorsFracture(l2FileBody + "_fracture.txt", std::ios::out);

    // run tpfa
    int refinementCount = 0;
    auto assignTpfaNames = [&problemNameBody] (auto& paramTree)
                           {
                               paramTree["Problem.Name"] = problemNameBody + "_tpfa";
                               paramTree["Bulk.Problem.Name"] = problemNameBody + "_bulk_tpfa";
                               paramTree["LowDim.Problem.Name"] = problemNameBody + "_lowdim_tpfa";
                           };
    Parameters::init(assignTpfaNames);
    while (hasParam("Refinement" + std::to_string(refinementCount) + ".Grid.File"))
    {
        std::cout << "\n\n"
                  << "################################\n"
                  << "# Solving Tpfa for refinement " << refinementCount << "#\n\n";

        const std::string gridParamGroup = "Refinement" + std::to_string(refinementCount);

        using namespace Dumux::Properties::TTag;
        const auto storage = solveFacet<OnePBulkTpfa, OnePLowDimTpfa>(gridParamGroup, refinementCount);

        // intersect this and the reference grid
        std::cout << "\nIntersecting the grids" << std::endl;
        const auto bulkGlue = makeGlue(*matrixFvGridGeometry, *storage.bulkFvGridGeometry);
        std::cout << "\nIntersecting the grids" << std::endl;
        const auto lowDimGlue = makeGlue(*fractureFvGridGeometry, *storage.lowDimFvGridGeometry);

        const auto& bulkBasis = getFunctionSpaceBasis(*storage.bulkFvGridGeometry);
        const auto& lowDimBasis = getFunctionSpaceBasis(*storage.lowDimFvGridGeometry);

        std::cout << "\nProjecting reference solutions" << std::endl;
        // we project the bulk solution into the matrix solution space
        // and the fracture reference solution onto the low dim solution space
        const auto bulkProjector = Dumux::makeProjectorPair(matrixBasis, bulkBasis, bulkGlue).second;
        const auto lowDimProjector = Dumux::makeProjectorPair(fractureBasis, lowDimBasis, lowDimGlue).first;

        auto params = bulkProjector.defaultParams();
        params.residualReduction = 1e-16;

        auto projectedBulkSol = bulkProjector.project(storage.bulkSolution, params);
        auto projectedFractureSol = lowDimProjector.project(fractureSolution, params);

        std::cout << "\nComputing discrete l2 error norm" << std::endl;

        // grid function for bulk solution on matrix grid
        auto bulkMatrixGridFunction = makeDiscreteGlobalBasisFunction<RefBlockType>(matrixBasis, projectedBulkSol);
        // grid function for ref fracture sol on low dim Grid
        auto fractureLowDimGridFunction = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, projectedFractureSol);
        // grid function for solution on low dim grid
        auto gfLowDim = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, storage.lowDimSolution);

        // compute l2 norms
        auto matrixL2norm = Dumux::integrateL2Error(matrixBasis.gridView(), gfMatrix, bulkMatrixGridFunction, intOrder);
        auto fractureL2norm = Dumux::integrateL2Error(lowDimBasis.gridView(), gfLowDim, fractureLowDimGridFunction, intOrder);
        std::cout << "\t -> matrix: " << matrixL2norm << "\n"
                  << "\t -> fracture: " << fractureL2norm << std::endl;

        // write projection of bulk solution in reference grid
        Dune::VTKWriter< std::decay_t<decltype(matrixBasis.gridView())> > matrixProjectionWriter(matrixBasis.gridView());
        matrixProjectionWriter.addCellData(gfMatrix, Dune::VTK::FieldInfo("p", Dune::VTK::FieldInfo::Type::scalar, 1));
        matrixProjectionWriter.addCellData(bulkMatrixGridFunction, Dune::VTK::FieldInfo("p_projected", Dune::VTK::FieldInfo::Type::scalar, 1));
        matrixProjectionWriter.write(getParam<std::string>("Bulk.Problem.Name") + "_projection" + "_" + std::to_string(refinementCount));

        // write projection of fracture solution in low dim grid
        Dune::VTKWriter< std::decay_t<decltype(lowDimBasis.gridView())> > fractureProjectionWriter(lowDimBasis.gridView());
        fractureProjectionWriter.addCellData(gfLowDim, Dune::VTK::FieldInfo("p_lowDim", Dune::VTK::FieldInfo::Type::scalar, 1));
        fractureProjectionWriter.addCellData(fractureLowDimGridFunction, Dune::VTK::FieldInfo("p_projected", Dune::VTK::FieldInfo::Type::scalar, 1));
        fractureProjectionWriter.write(getParam<std::string>("LowDim.Problem.Name") + "_projection" + "_" + std::to_string(refinementCount));

        if (refinementCount > 0) { l2ErrorsMatrix << ","; l2ErrorsFracture << ","; }
        l2ErrorsMatrix << matrixL2norm/integralMatrix;
        l2ErrorsFracture << fractureL2norm/integralFracture;

        refinementCount++;
    }

    // go to next line in error norm file
    l2ErrorsMatrix << std::endl;
    l2ErrorsFracture << std::endl;

    // run mpfa
    refinementCount = 0;
    auto assignMpfaNames = [&problemNameBody] (auto& paramTree)
                           {
                               paramTree["Problem.Name"] = problemNameBody + "_mpfa";
                               paramTree["Bulk.Problem.Name"] = problemNameBody + "_bulk_mpfa";
                               paramTree["LowDim.Problem.Name"] = problemNameBody + "_lowdim_mpfa";
                           };
    Parameters::init(assignMpfaNames);
    while (hasParam("Refinement" + std::to_string(refinementCount) + ".Grid.File"))
    {
        std::cout << "\n\n"
                  << "################################\n"
                  << "# Solving Mpfa for refinement " << refinementCount << "#\n\n";

        const std::string gridParamGroup = "Refinement" + std::to_string(refinementCount);

        using namespace Dumux::Properties::TTag;
        const auto storage = solveFacet<OnePBulkMpfa, OnePLowDimMpfa>(gridParamGroup, refinementCount);

        // intersect this and the reference grid
        std::cout << "\nIntersecting the grids" << std::endl;
        const auto bulkGlue = makeGlue(*matrixFvGridGeometry, *storage.bulkFvGridGeometry);
        std::cout << "\nIntersecting the grids" << std::endl;
        const auto lowDimGlue = makeGlue(*fractureFvGridGeometry, *storage.lowDimFvGridGeometry);

        const auto& bulkBasis = getFunctionSpaceBasis(*storage.bulkFvGridGeometry);
        const auto& lowDimBasis = getFunctionSpaceBasis(*storage.lowDimFvGridGeometry);

        std::cout << "\nProjecting reference solutions" << std::endl;
        // we project the bulk solution into the matrix solution space
        // and the fracture reference solution onto the low dim solution space
        const auto bulkProjector = Dumux::makeProjectorPair(matrixBasis, bulkBasis, bulkGlue).second;
        const auto lowDimProjector = Dumux::makeProjectorPair(fractureBasis, lowDimBasis, lowDimGlue).first;

        auto params = bulkProjector.defaultParams();
        params.residualReduction = 1e-16;

        auto projectedBulkSol = bulkProjector.project(storage.bulkSolution, params);
        auto projectedFractureSol = lowDimProjector.project(fractureSolution, params);

        std::cout << "\nComputing discrete l2 error norm" << std::endl;

        // grid function for bulk solution on matrix grid
        auto bulkMatrixGridFunction = makeDiscreteGlobalBasisFunction<RefBlockType>(matrixBasis, projectedBulkSol);
        // grid function for ref fracture sol on low dim Grid
        auto fractureLowDimGridFunction = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, projectedFractureSol);
        // grid function for solution on low dim grid
        auto gfLowDim = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, storage.lowDimSolution);

        // compute l2 norms
        auto matrixL2norm = Dumux::integrateL2Error(matrixBasis.gridView(), gfMatrix, bulkMatrixGridFunction, intOrder);
        auto fractureL2norm = Dumux::integrateL2Error(lowDimBasis.gridView(), gfLowDim, fractureLowDimGridFunction, intOrder);
        std::cout << "\t -> matrix: " << matrixL2norm << "\n"
                  << "\t -> fracture: " << fractureL2norm << std::endl;

        // write projection of bulk solution in reference grid
        Dune::VTKWriter< std::decay_t<decltype(matrixBasis.gridView())> > matrixProjectionWriter(matrixBasis.gridView());
        matrixProjectionWriter.addCellData(gfMatrix, Dune::VTK::FieldInfo("p", Dune::VTK::FieldInfo::Type::scalar, 1));
        matrixProjectionWriter.addCellData(bulkMatrixGridFunction, Dune::VTK::FieldInfo("p_projected", Dune::VTK::FieldInfo::Type::scalar, 1));
        matrixProjectionWriter.write(getParam<std::string>("Bulk.Problem.Name") + "_projection" + "_" + std::to_string(refinementCount));

        // write projection of fracture solution in low dim grid
        Dune::VTKWriter< std::decay_t<decltype(lowDimBasis.gridView())> > fractureProjectionWriter(lowDimBasis.gridView());
        fractureProjectionWriter.addCellData(gfLowDim, Dune::VTK::FieldInfo("p_lowDim", Dune::VTK::FieldInfo::Type::scalar, 1));
        fractureProjectionWriter.addCellData(fractureLowDimGridFunction, Dune::VTK::FieldInfo("p_projected", Dune::VTK::FieldInfo::Type::scalar, 1));
        fractureProjectionWriter.write(getParam<std::string>("LowDim.Problem.Name") + "_projection" + "_" + std::to_string(refinementCount));

        if (refinementCount > 0) { l2ErrorsMatrix << ","; l2ErrorsFracture << ","; }
        l2ErrorsMatrix << matrixL2norm/integralMatrix;
        l2ErrorsFracture << fractureL2norm/integralFracture;

        refinementCount++;
    }

    // go to next line in error norm file
    l2ErrorsMatrix << std::endl;
    l2ErrorsFracture << std::endl;

    // run box
    refinementCount = 0;
    auto assignBoxNames = [&problemNameBody] (auto& paramTree)
                           {
                               paramTree["Problem.Name"] = problemNameBody + "_box";
                               paramTree["Bulk.Problem.Name"] = problemNameBody + "_bulk_box";
                               paramTree["LowDim.Problem.Name"] = problemNameBody + "_lowdim_box";
                           };
    Parameters::init(assignBoxNames);
    while (hasParam("Refinement" + std::to_string(refinementCount) + ".Grid.File"))
    {
        std::cout << "\n\n"
                  << "################################\n"
                  << "# Solving Box for refinement " << refinementCount << "##\n\n";

        const std::string gridParamGroup = "Refinement" + std::to_string(refinementCount);

        using namespace Dumux::Properties::TTag;
        const auto storage = solveFacet<OnePBulkBox, OnePLowDimBox>(gridParamGroup, refinementCount);

        // first, average solution in each element -> create DG0 space coefficients
        const auto& bulkFvGridGeometry = *storage.bulkFvGridGeometry;
        const auto& bulkGridView = bulkFvGridGeometry.gridView();
        ReferenceSolution bulkSolAveraged(bulkGridView.size(0));
        for (const auto& element : elements(bulkGridView))
        {
            const auto eg = element.geometry();
            const auto elemSol = elementSolution(element, storage.bulkSolution, bulkFvGridGeometry);

            double avgSol = 0.0;
            for (auto&& qp : Dune::QuadratureRules<double, 2>::rule(eg.type(), 2))
                avgSol += evalSolution(element, eg, elemSol, eg.global(qp.position()))
                          *qp.weight()*eg.integrationElement(qp.position());
            avgSol /= eg.volume();

            bulkSolAveraged[bulkFvGridGeometry.elementMapper().index(element)] = avgSol;
        }

        const auto& lowDimFvGridGeometry = *storage.lowDimFvGridGeometry;
        const auto& lowDimGridView = lowDimFvGridGeometry.gridView();
        ReferenceSolution lowDimSolAveraged(lowDimGridView.size(0));
        for (const auto& element : elements(lowDimGridView))
        {
            const auto eg = element.geometry();
            const auto elemSol = elementSolution(element, storage.lowDimSolution, lowDimFvGridGeometry);

            double avgSol = 0.0;
            for (auto&& qp : Dune::QuadratureRules<double, 1>::rule(eg.type(), 2))
                avgSol += evalSolution(element, eg, elemSol, eg.global(qp.position()))
                          *qp.weight()*eg.integrationElement(qp.position());
            avgSol /= eg.volume();

            lowDimSolAveraged[lowDimFvGridGeometry.elementMapper().index(element)] = avgSol;
        }

        // intersect this and the reference grid
        std::cout << "\nIntersecting the grids" << std::endl;
        const auto bulkGlue = makeGlue(*matrixFvGridGeometry, *storage.bulkFvGridGeometry);
        std::cout << "\nIntersecting the grids" << std::endl;
        const auto lowDimGlue = makeGlue(*fractureFvGridGeometry, *storage.lowDimFvGridGeometry);

        // create DG0 Basis for the two grids
        Dune::Functions::LagrangeBasis<std::decay_t<decltype(bulkGridView)>, 0> bulkBasis(bulkGridView);
        Dune::Functions::LagrangeBasis<std::decay_t<decltype(lowDimGridView)>, 0> lowDimBasis(lowDimGridView);

        std::cout << "\nProjecting reference solutions" << std::endl;
        // we project the bulk solution into the matrix solution space
        // and the fracture reference solution onto the low dim solution space
        const auto bulkProjector = Dumux::makeProjectorPair(matrixBasis, bulkBasis, bulkGlue).second;
        const auto lowDimProjector = Dumux::makeProjectorPair(fractureBasis, lowDimBasis, lowDimGlue).first;

        auto params = bulkProjector.defaultParams();
        params.residualReduction = 1e-16;

        auto projectedBulkSol = bulkProjector.project(bulkSolAveraged, params);
        auto projectedFractureSol = lowDimProjector.project(fractureSolution, params);

        std::cout << "\nComputing discrete l2 error norm" << std::endl;

        // grid function for bulk solution on matrix grid
        auto bulkMatrixGridFunction = makeDiscreteGlobalBasisFunction<RefBlockType>(matrixBasis, projectedBulkSol);
        // grid function for ref fracture sol on low dim Grid
        auto fractureLowDimGridFunction = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, projectedFractureSol);
        // grid function for solution on low dim grid
        auto gfLowDim = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, lowDimSolAveraged);

        // compute l2 norms
        auto matrixL2norm = Dumux::integrateL2Error(matrixBasis.gridView(), gfMatrix, bulkMatrixGridFunction, intOrder);
        auto fractureL2norm = Dumux::integrateL2Error(lowDimBasis.gridView(), gfLowDim, fractureLowDimGridFunction, intOrder);
        std::cout << "\t -> matrix: " << matrixL2norm << "\n"
                  << "\t -> fracture: " << fractureL2norm << std::endl;

        // write projection of bulk solution in reference grid
        Dune::VTKWriter< std::decay_t<decltype(matrixBasis.gridView())> > matrixProjectionWriter(matrixBasis.gridView());
        matrixProjectionWriter.addCellData(gfMatrix, Dune::VTK::FieldInfo("p", Dune::VTK::FieldInfo::Type::scalar, 1));
        matrixProjectionWriter.addCellData(bulkMatrixGridFunction, Dune::VTK::FieldInfo("p_projected", Dune::VTK::FieldInfo::Type::scalar, 1));
        matrixProjectionWriter.write(getParam<std::string>("Bulk.Problem.Name") + "_projection" + "_" + std::to_string(refinementCount));

        // write projection of fracture solution in low dim grid
        Dune::VTKWriter< std::decay_t<decltype(lowDimBasis.gridView())> > fractureProjectionWriter(lowDimBasis.gridView());
        fractureProjectionWriter.addCellData(gfLowDim, Dune::VTK::FieldInfo("p_lowDim", Dune::VTK::FieldInfo::Type::scalar, 1));
        fractureProjectionWriter.addCellData(fractureLowDimGridFunction, Dune::VTK::FieldInfo("p_projected", Dune::VTK::FieldInfo::Type::scalar, 1));
        fractureProjectionWriter.write(getParam<std::string>("LowDim.Problem.Name") + "_projection" + "_" + std::to_string(refinementCount));

        if (refinementCount > 0) { l2ErrorsMatrix << ","; l2ErrorsFracture << ","; }
        l2ErrorsMatrix << matrixL2norm/integralMatrix;
        l2ErrorsFracture << fractureL2norm/integralFracture;

        refinementCount++;
    }

    // go to next line in error norm file
    l2ErrorsMatrix << std::endl;
    l2ErrorsFracture << std::endl;

    // run box-dfm
    refinementCount = 0;
    auto assignBoxDfmNames = [&problemNameBody] (auto& paramTree)
                           {
                               paramTree["Problem.Name"] = problemNameBody + "_boxdfm";
                               paramTree["Bulk.Problem.Name"] = problemNameBody + "_bulk_boxdfm";
                               paramTree["LowDim.Problem.Name"] = problemNameBody + "_lowdim_boxdfm";
                           };
    Parameters::init(assignBoxDfmNames);
    while (hasParam("Refinement" + std::to_string(refinementCount) + ".Grid.File"))
    {
        std::cout << "\n\n"
                  << "###################################\n"
                  << "# Solving BoxDfm for refinement " << refinementCount << "##\n\n";

        const std::string gridParamGroup = "Refinement" + std::to_string(refinementCount);

        using namespace Dumux::Properties::TTag;
        const auto storage = solveBoxDfm<OnePBoxDfm>(gridParamGroup, refinementCount);

        // first, average solution in each element -> create DG0 space coefficients
        const auto& fvGridGeometry = *storage.fvGridGeometry;
        const auto& gridView = fvGridGeometry.gridView();
        ReferenceSolution bulkSolAveraged(gridView.size(0));
        for (const auto& element : elements(gridView))
        {
            const auto eg = element.geometry();
            const auto elemSol = elementSolution(element, storage.solution, fvGridGeometry);

            double avgSol = 0.0;
            for (auto&& qp : Dune::QuadratureRules<double, 2>::rule(eg.type(), 2))
                avgSol += evalSolution(element, eg, elemSol, eg.global(qp.position()))
                          *qp.weight()*eg.integrationElement(qp.position());
            avgSol /= eg.volume();
            bulkSolAveraged[fvGridGeometry.elementMapper().index(element)] = avgSol;
        }

        // read in fracture solution from vtk file
        const std::string vtpFile = getParam<std::string>("Problem.Name") + "_" + std::to_string(refinementCount) + "_fracture-00001.vtp";

        auto vtkReader = VTKReader(vtpFile);
        using FractureGrid = Dune::FoamGrid<1, 2>;
        using FractureGridView = typename FractureGrid::LeafGridView;

        VTKReader::Data cellData, pointData;
        Dune::GridFactory<FractureGrid> gridFactory;
        auto fractureGrid = vtkReader.readGrid(gridFactory, cellData, pointData, /*verbose=*/true);
        const auto& fractureGridView = fractureGrid->leafGridView();
        CCTpfaFVGridGeometry<FractureGridView> fractureGridGeometry(fractureGridView);

        // to write out we need to reorder as the simple addCellData interface expects MCMG mapper indices
        Dune::MultipleCodimMultipleGeomTypeMapper<FractureGrid::LeafGridView> elementMapper(fractureGridView, Dune::mcmgElementLayout());
        Dune::MultipleCodimMultipleGeomTypeMapper<FractureGrid::LeafGridView> vertexMapper(fractureGridView, Dune::mcmgVertexLayout());
        std::vector<std::size_t> vertexIndex(fractureGridView.size(FractureGrid::dimension));
        for (const auto& v : vertices(fractureGridView))
            vertexIndex[gridFactory.insertionIndex(v)] = vertexMapper.index(v);

        Dumux::VTKReader::Data reorderedPointData = pointData;
        for (const auto& data : pointData)
        {
            auto& reorderedData = reorderedPointData[data.first];
            for (unsigned int i = 0; i < data.second.size(); ++i)
                reorderedData[vertexIndex[i]] = data.second[i];
        }

        ReferenceSolution lowDimSolAveraged(fractureGridView.size(0));
        for (const auto& element : elements(fractureGridView))
        {
            const auto eg = element.geometry();
            const auto elemSol = elementSolution(element, reorderedPointData["p"], fractureGridGeometry);

            double avgSol = 0.0;
            for (auto&& qp : Dune::QuadratureRules<double, 1>::rule(eg.type(), 2))
                avgSol += evalSolution(element, eg, elemSol, eg.global(qp.position()))
                          *qp.weight()*eg.integrationElement(qp.position());
            avgSol /= eg.volume();

            lowDimSolAveraged[fractureGridGeometry.elementMapper().index(element)] = avgSol;
        }

        // intersect this and the reference grid
        std::cout << "\nIntersecting the grids" << std::endl;
        const auto bulkGlue = makeGlue(*matrixFvGridGeometry, fvGridGeometry);
        std::cout << "\nIntersecting the grids" << std::endl;
        const auto lowDimGlue = makeGlue(*fractureFvGridGeometry, fractureGridGeometry);

        // create DG0 Basis for the two grids
        Dune::Functions::LagrangeBasis<std::decay_t<decltype(gridView)>, 0> bulkBasis(gridView);
        Dune::Functions::LagrangeBasis<std::decay_t<decltype(fractureGridView)>, 0> lowDimBasis(fractureGridView);

        std::cout << "\nProjecting reference solutions" << std::endl;
        // we project the bulk solution into the matrix solution space
        // and the fracture reference solution onto the low dim solution space
        const auto bulkProjector = Dumux::makeProjectorPair(matrixBasis, bulkBasis, bulkGlue).second;
        const auto lowDimProjector = Dumux::makeProjectorPair(fractureBasis, lowDimBasis, lowDimGlue).first;

        auto params = bulkProjector.defaultParams();
        params.residualReduction = 1e-16;

        auto projectedBulkSol = bulkProjector.project(bulkSolAveraged, params);
        auto projectedFractureSol = lowDimProjector.project(fractureSolution, params);

        std::cout << "\nComputing discrete l2 error norm" << std::endl;

        // grid function for bulk solution on matrix grid
        auto bulkMatrixGridFunction = makeDiscreteGlobalBasisFunction<RefBlockType>(matrixBasis, projectedBulkSol);
        // grid function for ref fracture sol on low dim Grid
        auto fractureLowDimGridFunction = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, projectedFractureSol);
        // grid function for solution on low dim grid
        auto gfLowDim = makeDiscreteGlobalBasisFunction<RefBlockType>(lowDimBasis, lowDimSolAveraged);

        // compute l2 norms
        auto matrixL2norm = Dumux::integrateL2Error(matrixBasis.gridView(), gfMatrix, bulkMatrixGridFunction, intOrder);
        auto fractureL2norm = Dumux::integrateL2Error(lowDimBasis.gridView(), gfLowDim, fractureLowDimGridFunction, intOrder);
        std::cout << "\t -> matrix: " << matrixL2norm << "\n"
                  << "\t -> fracture: " << fractureL2norm << std::endl;

        // write projection of bulk solution in reference grid
        Dune::VTKWriter< std::decay_t<decltype(matrixBasis.gridView())> > matrixProjectionWriter(matrixBasis.gridView());
        matrixProjectionWriter.addCellData(gfMatrix, Dune::VTK::FieldInfo("p", Dune::VTK::FieldInfo::Type::scalar, 1));
        matrixProjectionWriter.addCellData(bulkMatrixGridFunction, Dune::VTK::FieldInfo("p_projected", Dune::VTK::FieldInfo::Type::scalar, 1));
        matrixProjectionWriter.write(getParam<std::string>("Bulk.Problem.Name") + "_projection" + "_" + std::to_string(refinementCount));

        // write projection of fracture solution in low dim grid
        Dune::VTKWriter< std::decay_t<decltype(lowDimBasis.gridView())> > fractureProjectionWriter(lowDimBasis.gridView());
        fractureProjectionWriter.addCellData(gfLowDim, Dune::VTK::FieldInfo("p_lowDim", Dune::VTK::FieldInfo::Type::scalar, 1));
        fractureProjectionWriter.addCellData(fractureLowDimGridFunction, Dune::VTK::FieldInfo("p_projected", Dune::VTK::FieldInfo::Type::scalar, 1));
        fractureProjectionWriter.write(getParam<std::string>("LowDim.Problem.Name") + "_projection" + "_" + std::to_string(refinementCount));

        if (refinementCount > 0) { l2ErrorsMatrix << ","; l2ErrorsFracture << ","; }
        l2ErrorsMatrix << matrixL2norm/integralMatrix;
        l2ErrorsFracture << fractureL2norm/integralFracture;

        refinementCount++;
    }

    l2ErrorsMatrix.close();
    l2ErrorsFracture.close();

    // print dumux good bye message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/false);

    return 0;
}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
