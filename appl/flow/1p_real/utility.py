import math

# considered schemes are tpfa, mpfa, box, boxdfm
numSchemes = 4

# some stuff used by plot functions
lineStyleArray = ["-","--","-.",":"]

# returns a string with the scheme acronym being given an index
def getSchemeAcronym(schemeIdx):
    if schemeIdx == 0:
        return "tpfa"
    elif schemeIdx == 1:
        return "mpfa"
    elif schemeIdx == 2:
        return "box"
    elif schemeIdx == 3:
        return "boxdfm"

# returns a string with the plot label used for a scheme
def getSchemeLabel(schemeIdx):
    if schemeIdx == 0:
        return "TPFA"
    elif schemeIdx == 1:
        return "MPFA"
    elif schemeIdx == 2:
        return "BOX"
    elif schemeIdx == 3:
        return "BOX-CONT"

# returns the sub-folder in which the executable of a scheme lies
def getSchemeSubFolder(schemeIdx):
    if schemeIdx < 3:
        return "facet"
    elif schemeIdx == 3:
        return "boxdfm"
