import subprocess
import os
import sys
import math
import numpy as np
import matplotlib.pyplot as plt
from utility import *

# use plot style
sys.path.append("./../../common")
import plotstyles

prop_cycle = list(plt.rcParams['axes.prop_cycle'])
markers = ['o', '^', 'x', '.', ',', 's']

# adds data to a plot
def addDataToPlot(plotIdx, data, xIdx, yIdx, color, legend, style='-'):
    plt.figure(plotIdx)
    plt.plot(data[:, xIdx], data[:, yIdx], c=color, label=legend, linestyle=style)
    plt.xlabel(r'$\mathrm{arc \, length} \,\, [\si{\meter}]$')
    plt.ylabel(r'$p \,\, [\si{\pascal}]$')
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.legend()

#################################################
# plots the schemes' solutions vs the reference #
#################################################
def makePlots(is3dCase):

    # indices in the csv files created by paraview
    xIdx = 3
    pressureIdx = 0

    for i in range(0, numSchemes):
        csvFile1 = "plot1_" + getSchemeAcronym(i) + ".csv"
        csvFile2 = "plot2_" + getSchemeAcronym(i) + ".csv"

        data1 = np.loadtxt(csvFile1, delimiter=',', skiprows=1)
        data2 = np.loadtxt(csvFile2, delimiter=',', skiprows=1)

        addDataToPlot(1, data1, xIdx, pressureIdx, prop_cycle[i]['color'], getSchemeLabel(i))
        addDataToPlot(2, data2, xIdx, pressureIdx, prop_cycle[i]['color'], getSchemeLabel(i))

        if is3dCase == True and i < numSchemes-1:
            csvFile1 = "plot1_is_" + getSchemeAcronym(i) + ".csv"
            csvFile2 = "plot2_is_" + getSchemeAcronym(i) + ".csv"

            data1 = np.loadtxt(csvFile1, delimiter=',', skiprows=1)
            data2 = np.loadtxt(csvFile2, delimiter=',', skiprows=1)

            addDataToPlot(1, data1, xIdx, pressureIdx, prop_cycle[i]['color'], getSchemeLabel(i) + "(with is)", '--')
            addDataToPlot(2, data2, xIdx, pressureIdx, prop_cycle[i]['color'], getSchemeLabel(i) + "(with is)", '--')

    plt.figure(1)
    plt.savefig("plot1.pdf", bbox_inches='tight')
    plt.figure(2)
    plt.savefig("plot2.pdf", bbox_inches='tight')
