import subprocess
import os
import sys
import math
import numpy as np
import matplotlib.pyplot as plt
from utility import *

try:
    from paraview.simple import *
except ImportError:
    print("`paraview.simple` not found. Make sure using pvpython instead of python.")

# indices in the csv files created by paraview
xIdx = 4
pressureIdx = 0

# creates the data of a plot on a provided vtu file
def makePlotOverLineData(vtuFileName, isPolyData, csvFileName, resolution, point1, point2):

    if not os.path.exists(vtuFileName):
        sys.stderr.write("Provided vtu file \"" + vtuFileName + "\" does not exist")
        sys.exit(1)

    print("Reading solution file " + vtuFileName)
    if isPolyData == True:
        vtkFile = XMLPolyDataReader(FileName=vtuFileName)
    else:
        vtkFile = XMLUnstructuredGridReader(FileName=vtuFileName)
    SetActiveSource(vtkFile)

    plotOverLine1 = PlotOverLine(Source="High Resolution Line Source")
    plotOverLine1.Source.Resolution = resolution
    plotOverLine1.Source.Point1 = point1
    plotOverLine1.Source.Point2 = point2

    # write output
    writer = CreateWriter(csvFileName, plotOverLine1, Precision=10)
    writer.UpdatePipeline()


#################################################
# Main script
# Creates the files containing the plot data
##################################################

if len(sys.argv) < 2:
    sys.stderr("Please provide if 3d case is considered (true/false)")
    sys.exit(1)

if sys.argv[1] == "True" or sys.argv[1] == "true" or sys.argv[1] == 1:
    is3dCase = True
elif sys.argv[1] == "False" or sys.argv[1] == "false" or sys.argv[1] == 0:
    is3dCase = False
else:
    sys.stderr("Invalid runtime argument specified. Please provide either True, true, 1, False, false or 0")
    sys.exit(1)

# plot line specifics
resolution = 2000
if is3dCase == False:
    plot1_source = [0.0, 500.0, 0.0]
    plot1_target = [700.0, 500.0, 0.0]
    plot2_source = [625.0, 0.0, 0.0]
    plot2_target = [625.0, 600.0, 0.0]
else:
    plot1_source = [0.0, 0.0, 0.0]
    plot1_target = [700.0, 600.0, 400.0]
    plot2_source = [0.0, 600.0, 400.0]
    plot2_target = [700.0, 0.0, 0.0]

# create plot data
for i in range(0, numSchemes):
    vtuFile = "1p_real_"
    if i < 3:
        vtuFile += "bulk_"
    vtuFile += getSchemeAcronym(i) + "-00001.vtu"
    csvFile1 = "plot1_" + getSchemeAcronym(i) + ".csv"
    csvFile2 = "plot2_" + getSchemeAcronym(i) + ".csv"

    makePlotOverLineData(vtuFile, False, csvFile1, resolution, plot1_source, plot1_target)
    makePlotOverLineData(vtuFile, False, csvFile2, resolution, plot2_source, plot2_target)

if is3dCase == True:
    for i in range(0, numSchemes-1):
        vtuFile = "1p_real_bulk_is_"
        vtuFile += getSchemeAcronym(i) + "-00001.vtu"
        csvFile1 = "plot1_is_" + getSchemeAcronym(i) + ".csv"
        csvFile2 = "plot2_is_" + getSchemeAcronym(i) + ".csv"

        makePlotOverLineData(vtuFile, False, csvFile1, resolution, plot1_source, plot1_target)
        makePlotOverLineData(vtuFile, False, csvFile2, resolution, plot2_source, plot2_target)
