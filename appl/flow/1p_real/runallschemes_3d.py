import math
import sys
import subprocess
import numpy as np
from utility import *
from makeplots import *

import matplotlib.pyplot as plt

# use plot style
sys.path.append("./../../common")
import plotstyles

prop_cycle = list(plt.rcParams['axes.prop_cycle'])
markers = ['o', '^', 'x', '.', ',', 's']

####################################################
# runs all schemes and produces two pressure plots #
####################################################
for i in range(0, numSchemes):
    scheme = getSchemeAcronym(i)
    print("\nUMFPACK SOLVE USING " + getSchemeLabel(i))
    subprocess.call(["./" + getSchemeSubFolder(i) + "/" + "test_1p_real_3d_" + getSchemeAcronym(i),
                     "-Problem.Name", "1p_real_" + scheme,
                     "-Bulk.Problem.Name", "1p_real_bulk_" + scheme,
                     "-LowDim.Problem.Name", "1p_real_lowdim_" + scheme,
                     "-LinearSolver.Type", "UMFPack"])

    if i < numSchemes-1:
        print("\nUMFPACK SOLVE (WITH INTERSECTIONS) USING " + getSchemeLabel(i))
        subprocess.call(["./" + getSchemeSubFolder(i) + "/" + "test_1p_real_3d_is_" + getSchemeAcronym(i),
                         "-Problem.Name", "1p_real_is_" + scheme,
                         "-Bulk.Problem.Name", "1p_real_bulk_is_" + scheme,
                         "-LowDim.Problem.Name", "1p_real_lowdim_is_s" + scheme,
                         "-LinearSolver.Type", "UMFPack"])

# skip superlu for now
# for i in range(0, numSchemes):
#    scheme = getSchemeAcronym(i)
#    print("\nSUPERLU SOLVE USING " + getSchemeLabel(i))
#    subprocess.call(["./" + getSchemeSubFolder(i) + "/" + "test_1p_real_3d_" + getSchemeAcronym(i),
#                     "-Problem.Name", "1p_real_" + scheme,
#                     "-Bulk.Problem.Name", "1p_real_bulk_" + scheme,
#                     "-LowDim.Problem.Name", "1p_real_lowdim_" + scheme,
#                     "-LinearSolver.Type", "SuperLU",
#                     "-Grid.File", "grids/real_3d.msh"])
#
#    if i < numSchemes-1:
#        print("\nSUPERLU SOLVE (WITH INTERSECTIONS) USING " + getSchemeLabel(i))
#        subprocess.call(["./" + getSchemeSubFolder(i) + "/" + "test_1p_real_3d_is_" + getSchemeAcronym(i),
#                         "-Problem.Name", "1p_real_is_" + scheme,
#                         "-Bulk.Problem.Name", "1p_real_bulk_is_" + scheme,
#                         "-LowDim.Problem.Name", "1p_real_lowdim_is_" + scheme,
#                         "-LinearSolver.Type", "SuperLU",
#                         "-Grid.File", "grids/real_3d.msh"])

# CG solver
for i in range(0, numSchemes):
   scheme = getSchemeAcronym(i)
   print("\n CG SOLVE USING " + getSchemeLabel(i))
   subprocess.call(["./" + getSchemeSubFolder(i) + "/" + "test_1p_real_3d_" + getSchemeAcronym(i),
                    "-Problem.Name", "1p_real_" + scheme,
                    "-Bulk.Problem.Name", "1p_real_bulk_" + scheme,
                    "-LowDim.Problem.Name", "1p_real_lowdim_" + scheme,
                    "-LinearSolver.Type", "ILU0CG",
                    "-Grid.File", "grids/real_3d.msh"])


   if i < numSchemes-1:
       print("\n CG SOLVE (WITH INTERSECTIONS) USING " + getSchemeLabel(i))
       subprocess.call(["./" + getSchemeSubFolder(i) + "/" + "test_1p_real_3d_is_" + getSchemeAcronym(i),
                        "-Problem.Name", "1p_real_is_" + scheme,
                        "-Bulk.Problem.Name", "1p_real_bulk_is_" + scheme,
                        "-LowDim.Problem.Name", "1p_real_lowdim_is_" + scheme,
                        "-Edge.Problem.Name", "1p_real_edge_is_" + scheme,
                        "-LinearSolver.Type", "ILU0CG",
                        "-Grid.File", "grids/real_3d.msh"])

# BiCGSTAB solver
for i in range(0, numSchemes):
   scheme = getSchemeAcronym(i)
   print("\n BiCGSTAB SOLVE USING " + getSchemeLabel(i))
   subprocess.call(["./" + getSchemeSubFolder(i) + "/" + "test_1p_real_3d_" + getSchemeAcronym(i),
                    "-Problem.Name", "1p_real_" + scheme,
                    "-Bulk.Problem.Name", "1p_real_bulk_" + scheme,
                    "-LowDim.Problem.Name", "1p_real_lowdim_" + scheme,
                    "-LinearSolver.Type", "ILU0BiCGSTAB",
                    "-Grid.File", "grids/real_3d.msh"])


   if i < numSchemes-1:
       print("\n BiCGSTAB SOLVE (WITH INTERSECTIONS) USING " + getSchemeLabel(i))
       subprocess.call(["./" + getSchemeSubFolder(i) + "/" + "test_1p_real_3d_is_" + getSchemeAcronym(i),
                        "-Problem.Name", "1p_real_is_" + scheme,
                        "-Bulk.Problem.Name", "1p_real_bulk_is_" + scheme,
                        "-LowDim.Problem.Name", "1p_real_lowdim_is_" + scheme,
                        "-Edge.Problem.Name", "1p_real_edge_is_" + scheme,
                        "-LinearSolver.Type", "ILU0BiCGSTAB",
                        "-Grid.File", "grids/real_3d.msh"])

# BlockDiag ILU BiCGSTAB solver
for i in range(0, numSchemes):
   scheme = getSchemeAcronym(i)
   print("\n BlockDiagILU0BiCGSTAB SOLVE USING " + getSchemeLabel(i))
   subprocess.call(["./" + getSchemeSubFolder(i) + "/" + "test_1p_real_3d_" + getSchemeAcronym(i),
                    "-Problem.Name", "1p_real_" + scheme,
                    "-Bulk.Problem.Name", "1p_real_bulk_" + scheme,
                    "-LowDim.Problem.Name", "1p_real_lowdim_" + scheme,
                    "-LinearSolver.Type", "BlockDiagILU0BiCGSTAB",
                    "-Grid.File", "grids/real_3d.msh"])


   if i < numSchemes-1:
       print("\n BlockDiagILU0BiCGSTAB SOLVE (WITH INTERSECTIONS) USING " + getSchemeLabel(i))
       subprocess.call(["./" + getSchemeSubFolder(i) + "/" + "test_1p_real_3d_is_" + getSchemeAcronym(i),
                        "-Problem.Name", "1p_real_is_" + scheme,
                        "-Bulk.Problem.Name", "1p_real_bulk_is_" + scheme,
                        "-LowDim.Problem.Name", "1p_real_lowdim_is_" + scheme,
                        "-Edge.Problem.Name", "1p_real_edge_is_" + scheme,
                        "-LinearSolver.Type", "BlockDiagILU0BiCGSTAB",
                        "-Grid.File", "grids/real_3d.msh"])

# BlockDiag AMG BiCGSTAB solver
for i in range(0, numSchemes):
   scheme = getSchemeAcronym(i)
   print("\n BlockDiagAMGBiCGSTAB SOLVE USING " + getSchemeLabel(i))
   subprocess.call(["./" + getSchemeSubFolder(i) + "/" + "test_1p_real_3d_" + getSchemeAcronym(i),
                    "-Problem.Name", "1p_real_" + scheme,
                    "-Bulk.Problem.Name", "1p_real_bulk_" + scheme,
                    "-LowDim.Problem.Name", "1p_real_lowdim_" + scheme,
                    "-LinearSolver.Type", "BlockDiagAMGBiCGSTAB",
                    "-Grid.File", "grids/real_3d.msh"])


   if i < numSchemes-1:
       print("\n BlockDiagAMGBiCGSTAB SOLVE (WITH INTERSECTIONS) USING " + getSchemeLabel(i))
       subprocess.call(["./" + getSchemeSubFolder(i) + "/" + "test_1p_real_3d_is_" + getSchemeAcronym(i),
                        "-Problem.Name", "1p_real_is_" + scheme,
                        "-Bulk.Problem.Name", "1p_real_bulk_is_" + scheme,
                        "-LowDim.Problem.Name", "1p_real_lowdim_is_" + scheme,
                        "-Edge.Problem.Name", "1p_real_edge_is_" + scheme,
                        "-LinearSolver.Type", "BlockDiagAMGBiCGSTAB",
                        "-Grid.File", "grids/real_3d.msh"])

print("\n\nMaking plot data")
subprocess.call(['pvpython', 'makeplotdata.py', 'true'])

print("\n\nMaking plots")
makePlots(True)
