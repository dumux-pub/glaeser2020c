// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup MultiDomain
 * \ingroup FacetCoupling
 * \brief The problem for the lower-dimensional domain in the single-phase facet coupling test
 */
#ifndef DUMUX_TEST_TPFAFACETCOUPLING_ONEP_LOWDIMPROBLEM_HH
#define DUMUX_TEST_TPFAFACETCOUPLING_ONEP_LOWDIMPROBLEM_HH

#include <dune/foamgrid/foamgrid.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/multilineargeometry.hh>

#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/1p/model.hh>

#include "spatialparams.hh"

namespace Dumux {
// forward declarations
template<class TypeTag> class OnePLowDimProblem;

namespace Properties {
namespace TTag {

// create the type tag nodes
struct OnePLowDim { using InheritsFrom = std::tuple<OneP>; };
struct OnePLowDimTpfa { using InheritsFrom = std::tuple<OnePLowDim, CCTpfaModel>; };
struct OnePLowDimMpfa { using InheritsFrom = std::tuple<OnePLowDim, CCTpfaModel>; };
struct OnePLowDimBox { using InheritsFrom = std::tuple<OnePLowDim, BoxModel>; };

} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePLowDim> { using type = Dune::FoamGrid<1, 2>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::OnePLowDim> { using type = OnePLowDimProblem<TypeTag>; };

// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePLowDim>
{
private:
    using FVGridGeometry = GetPropType<TypeTag, FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Scalar>;
public:
    using type = OnePSpatialParams< FVGridGeometry, Scalar >;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePLowDim>
{
private:
    using Scalar = GetPropType<TypeTag, Scalar>;
public:
    using type = FluidSystems::OnePLiquid< Scalar, Components::Constant<1, Scalar> >;
};

//! Parameters are solution-independent
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::OnePLowDim> { static constexpr bool value = false; };

} // end namespace Properties

/*!
 * \ingroup OnePTests
 * \brief The lower-dimensional test problem for the incompressible
 *        one-phase model with coupling across the bulk grid facets
 */
template<class TypeTag>
class OnePLowDimProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

public:
    OnePLowDimProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                      std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                      std::shared_ptr<CouplingManager> couplingManager,
                      const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManager)
    , aperture_(getParamFromGroup<Scalar>(paramGroup, "Problem.ExtrusionFactor"))
    , useSecondaryAnalyticSolution_(getParamFromGroup<bool>(paramGroup, "Problem.UseSecondaryAnalyticSolution"))
    {}

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;

        static const bool useLateralNeumann = getParam<bool>("Problem.UseLateralNeumannBCFracture");
        if (useLateralNeumann)
            values.setAllNeumann();
        else
            values.setAllDirichlet();

        return values;
    }

    //! Evaluate the source term at a given position
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        // evaluate sources from bulk domain
        auto source = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);
        source /= scv.volume()*elemVolVars[scv].extrusionFactor();

        if (useSecondaryAnalyticSolution_)
        {
            using std::cos;
            using std::cosh;
            const auto& k = this->spatialParams().permeabilityAtPos(scv.center());
            // const auto invK = 1.0/k;
            source += (k -1.0)*cos(scv.center()[0])*cosh(aperture_/2.0);
        }

        return source;
    }

    //! evaluates the exact solution at a given position.
    Scalar exact(const GlobalPosition& globalPos) const
    {
        using std::cos;
        using std::cosh;

        const auto x = globalPos[0];
        const auto y = globalPos[1];

        if (!useSecondaryAnalyticSolution_)
            return cos(x)*cosh(y);
        else
        {
            const auto& k = this->spatialParams().permeabilityAtPos(globalPos);
            const auto invK = 1.0/k;
            return invK*cos(x)*cosh(y) + (1.0 - invK)*cos(x)*cosh(aperture_/2.0);
        }
    }

    //! evaluates the exact gradient at a given position.
    GlobalPosition exactGradient(const GlobalPosition& globalPos) const
    {
        using std::cos;
        using std::sin;
        using std::cosh;
        using std::sinh;

        const auto x = globalPos[0];
        const auto y = globalPos[1];

        GlobalPosition gradU;
        if (!useSecondaryAnalyticSolution_)
        {
            gradU[0] = -1.0*sin(x)*cosh(y);
            gradU[1] = cos(x)*sinh(y);
        }
        else
        {
            const auto& k = this->spatialParams().permeabilityAtPos(globalPos);
            const auto invK = 1.0/k;
            gradU[0] = -1.0*invK*sin(x)*cosh(y) + (invK - 1.0)*sin(x)*cosh(aperture_/2);
            gradU[1] = invK*cos(x)*sinh(y);
        }

        return gradU;
    }

    //! evaluates the exact gradient at a given position.
    //! see getIntegrationPoint_() for details on the parameter forceIpMovement
    GlobalPosition exactVelocity(const GlobalPosition& globalPos) const
    {
        // This assumes viscosity of 1.0
        const auto& k = this->spatialParams().permeabilityAtPos(globalPos);
        auto vel = exactGradient(globalPos);
        vel *= -this->spatialParams().permeabilityAtPos(globalPos);
        return vel;
    }

    //! evaluates the exact flux density at a given position.
    Scalar exactFlux(const GlobalPosition& globalPos, const GlobalPosition& normalVector) const
    {
        const auto& k = this->spatialParams().permeabilityAtPos(globalPos);
        const auto gradU = exactGradient(globalPos);
        return -1.0*vtmv(normalVector, k, gradU);
    }

    //! evaluates the exact flux density at a given position.
    Scalar exactSourceTerm(const GlobalPosition& globalPos) const
    {
        GlobalPosition x_lo({globalPos[0], -aperture_/2.0});
        GlobalPosition x_hi({globalPos[0], aperture_/2.0});

        return exactFlux(x_lo, GlobalPosition({0.0, 1.0}))
               + exactFlux(x_hi, GlobalPosition({0.0, -1.0}));
    }

    //! evaluates the cross-section averaged exact solution at a given position.
    Scalar exact_crossSectionAverage(const GlobalPosition& globalPos) const
    {
        const auto crossSection = makeCrossSectionSegment_(globalPos);

        static const auto intOrder = getParam<Scalar>("Averaging.IntegrationOrder");
        const auto& rule = Dune::QuadratureRules<Scalar, dim>::rule(crossSection.type(), intOrder);

        Scalar avg = 0.0;
        for (auto&& qp : rule)
            avg += exact( crossSection.global(qp.position()) )
                   *crossSection.integrationElement( qp.position() )
                   *qp.weight();
        avg /= crossSection.volume();

        return avg;
    }

    //! evaluates the cross-section averaged exact flux at a given position.
    Scalar exactFlux_crossSectionAverage(const GlobalPosition& globalPos, const GlobalPosition& normalVector) const
    {
        const auto crossSection = makeCrossSectionSegment_(globalPos);

        static const auto intOrder = getParam<Scalar>("Averaging.IntegrationOrder");
        const auto& rule = Dune::QuadratureRules<Scalar, dim>::rule(crossSection.type(), intOrder);

        Scalar avg = 0.0;
        for (auto&& qp : rule)
            avg += exactFlux( crossSection.global(qp.position()), normalVector )
                   *crossSection.integrationElement( qp.position() )
                   *qp.weight();
        avg /= crossSection.volume();

        return avg;
    }

    //! evaluates the Dirichlet boundary condition for a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(exact_crossSectionAverage(globalPos)); }

    //! evaluates the Neumann boundary conditions for a given position
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        static const bool useZeroNeumann = getParam<bool>("Problem.UseZeroFractureNeumann");
        if (useZeroNeumann)
            return NumEqVector(0.0);
        else
            return NumEqVector(exactFlux_crossSectionAverage(scvf.ipGlobal(), scvf.unitOuterNormal()));
    }

    //! Set the aperture as extrusion factor.
    Scalar extrusionFactorAtPos(const GlobalPosition& globalPos) const
    { return aperture_; }

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(1.0); }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

private:
    //! Creates the segment representing the cross section at a fracture position
    Dune::MultiLinearGeometry<Scalar, dim, dimWorld>
    makeCrossSectionSegment_(const GlobalPosition& globalPos) const
    {
        if (globalPos[1] > 1e-6 || globalPos[1] < -1e-6)
            DUNE_THROW(Dune::InvalidStateException, "Provided position " << globalPos << " is not on fracture centerline!");

        std::vector<GlobalPosition> corners({ globalPos + GlobalPosition({0.0, aperture_/2.0}),
                                              globalPos - GlobalPosition({0.0, aperture_/2.0}) });

        return Dune::MultiLinearGeometry<Scalar, dim, dimWorld>(Dune::GeometryTypes::simplex(dim), corners);
    }

    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    Scalar aperture_;
    bool useSecondaryAnalyticSolution_;
};

} // end namespace Dumux

#endif
