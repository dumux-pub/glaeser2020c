// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief test for the one-phase facet coupling model
 */
#include <config.h>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/type.hh>
#include <dune/istl/matrixmarket.hh>

#include "bulkproblem.hh"
#include "lowdimproblem.hh"
#include "computenorm.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/geometry/diameter.hh>

#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/evalsolution.hh>
#include <dumux/linear/seqsolverbackend.hh>

#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/traits.hh>

#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>
#include <dumux/multidomain/facet/codimonegridadapter.hh>

#include <dumux/io/vtkoutputmodule.hh>

// obtain/define some types to be used below in the property definitions and in main
template< class BulkTypeTag, class LowDimTypeTag >
class TestTraits
{
    using BulkFVGridGeometry = Dumux::GetPropType<BulkTypeTag, Dumux::Properties::FVGridGeometry>;
    using LowDimFVGridGeometry = Dumux::GetPropType<LowDimTypeTag, Dumux::Properties::FVGridGeometry>;

public:
    using MDTraits = Dumux::MultiDomainTraits<BulkTypeTag, LowDimTypeTag>;
    using CouplingMapper = Dumux::FacetCouplingMapper<BulkFVGridGeometry, LowDimFVGridGeometry>;
    using CouplingManager = Dumux::FacetCouplingManager<MDTraits, CouplingMapper>;
};

// set the coupling manager property for all available type tags
namespace Dumux {
namespace Properties {

// set cm property for the tests
using BoxTraits = TestTraits<TTag::OnePBulkBox, TTag::OnePLowDimBox>;
using TpfaTraits = TestTraits<TTag::OnePBulkTpfa, TTag::OnePLowDimTpfa>;
using MpfaTraits = TestTraits<TTag::OnePBulkMpfa, TTag::OnePLowDimMpfa>;

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePBulkBox> { using type = typename BoxTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePLowDimBox> { using type = typename BoxTraits::CouplingManager; };

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePBulkTpfa> { using type = typename TpfaTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePLowDimTpfa> { using type = typename TpfaTraits::CouplingManager; };

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePBulkMpfa> { using type = typename MpfaTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::OnePLowDimMpfa> { using type = typename MpfaTraits::CouplingManager; };

} // end namespace Properties
} // end namespace Dumux


// updates the finite volume grid geometry. This is necessary as the finite volume
// grid geometry for the box scheme with facet coupling requires additional data for
// the update. The reason is that we have to create additional faces on interior
// boundaries, which wouldn't be created in the standard scheme.
template< class FVGridGeometry,
          class GridManager,
          class LowDimGridView,
          std::enable_if_t<FVGridGeometry::discMethod == Dumux::DiscretizationMethod::box, int> = 0 >
void updateBulkFVGridGeometry(FVGridGeometry& fvGridGeometry,
                              const GridManager& gridManager,
                              const LowDimGridView& lowDimGridView)
{
    using BulkFacetGridAdapter = Dumux::CodimOneGridAdapter<typename GridManager::Embeddings>;
    BulkFacetGridAdapter facetGridAdapter(gridManager.getEmbeddings());
    fvGridGeometry.update(lowDimGridView, facetGridAdapter);
}

// specialization for cell-centered schemes
template< class FVGridGeometry,
          class GridManager,
          class LowDimGridView,
          std::enable_if_t<FVGridGeometry::discMethod != Dumux::DiscretizationMethod::box, int> = 0 >
void updateBulkFVGridGeometry(FVGridGeometry& fvGridGeometry,
                              const GridManager& gridManager,
                              const LowDimGridView& lowDimGridView)
{ fvGridGeometry.update(); }


// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // initialize parameter tree
    Parameters::init(argc, argv);

    //////////////////////////////////////////////////////
    // try to create the grids (from the given grid file)
    //////////////////////////////////////////////////////

    using BulkProblemTypeTag = Dumux::Properties::TTag::BULKTYPETAG;     // (must be declared in CMakeLists.txt)
    using LowDimProblemTypeTag = Dumux::Properties::TTag::LOWDIMTYPETAG; // (must be declared in CMakeLists.txt)
    using BulkGrid = GetPropType<BulkProblemTypeTag, Properties::Grid>;
    using LowDimGrid = GetPropType<LowDimProblemTypeTag, Properties::Grid>;

    using GridManager = FacetCouplingGridManager<BulkGrid, LowDimGrid>;
    GridManager gridManager;
    gridManager.init();
    gridManager.loadBalance();

    ////////////////////////////////////////////////////////////
    // run stationary, non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    const auto& bulkGridView = gridManager.template grid<0>().leafGridView();
    const auto& lowDimGridView = gridManager.template grid<1>().leafGridView();

    // create the finite volume grid geometries
    using BulkFVGridGeometry = GetPropType<BulkProblemTypeTag, Properties::FVGridGeometry>;
    using LowDimFVGridGeometry = GetPropType<LowDimProblemTypeTag, Properties::FVGridGeometry>;
    auto bulkFvGridGeometry = std::make_shared<BulkFVGridGeometry>(bulkGridView);
    auto lowDimFvGridGeometry = std::make_shared<LowDimFVGridGeometry>(lowDimGridView);
    updateBulkFVGridGeometry(*bulkFvGridGeometry, gridManager, lowDimGridView);
    lowDimFvGridGeometry->update();

    // the coupling mapper
    using TestTraits = TestTraits<BulkProblemTypeTag, LowDimProblemTypeTag>;
    auto couplingMapper = std::make_shared<typename TestTraits::CouplingMapper>();
    couplingMapper->update(*bulkFvGridGeometry, *lowDimFvGridGeometry, gridManager.getEmbeddings());

    // the coupling manager
    using CouplingManager = typename TestTraits::CouplingManager;
    auto couplingManager = std::make_shared<CouplingManager>();

    // the problems (boundary conditions)
    using BulkProblem = GetPropType<BulkProblemTypeTag, Properties::Problem>;
    using LowDimProblem = GetPropType<LowDimProblemTypeTag, Properties::Problem>;
    auto bulkSpatialParams = std::make_shared<typename BulkProblem::SpatialParams>(bulkFvGridGeometry, "Bulk");
    auto bulkProblem = std::make_shared<BulkProblem>(bulkFvGridGeometry, bulkSpatialParams, couplingManager, "Bulk");
    auto lowDimSpatialParams = std::make_shared<typename LowDimProblem::SpatialParams>(lowDimFvGridGeometry, "LowDim");
    auto lowDimProblem = std::make_shared<LowDimProblem>(lowDimFvGridGeometry, lowDimSpatialParams, couplingManager, "LowDim");

    // the solution vector
    using MDTraits = typename TestTraits::MDTraits;
    using SolutionVector = typename MDTraits::SolutionVector;
    SolutionVector x;

    static const auto bulkId = typename MDTraits::template SubDomain<0>::Index();
    static const auto lowDimId = typename MDTraits::template SubDomain<1>::Index();
    x[bulkId].resize(bulkFvGridGeometry->numDofs());
    x[lowDimId].resize(lowDimFvGridGeometry->numDofs());
    bulkProblem->applyInitialSolution(x[bulkId]);
    lowDimProblem->applyInitialSolution(x[lowDimId]);

    // initialize coupling manager
    couplingManager->init(bulkProblem, lowDimProblem, couplingMapper, x);

    // the grid variables
    using BulkGridVariables = GetPropType<BulkProblemTypeTag, Properties::GridVariables>;
    using LowDimGridVariables = GetPropType<LowDimProblemTypeTag, Properties::GridVariables>;
    auto bulkGridVariables = std::make_shared<BulkGridVariables>(bulkProblem, bulkFvGridGeometry);
    auto lowDimGridVariables = std::make_shared<LowDimGridVariables>(lowDimProblem, lowDimFvGridGeometry);
    bulkGridVariables->init(x[bulkId]);
    lowDimGridVariables->init(x[lowDimId]);

    // the assembler
    using Assembler = MultiDomainFVAssembler<MDTraits, CouplingManager, DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>( std::make_tuple(bulkProblem, lowDimProblem),
                                                  std::make_tuple(bulkFvGridGeometry, lowDimFvGridGeometry),
                                                  std::make_tuple(bulkGridVariables, lowDimGridVariables),
                                                  couplingManager);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);

    // linearize & solve
    newtonSolver->solve(x);

    // update grid variables for output
    bulkGridVariables->update(x[bulkId]);
    lowDimGridVariables->update(x[lowDimId]);

    // maybe write out matrix in matrix market format
    if (getParam<bool>("Output.WriteMatrix", false))
    {
        using Converter = MatrixConverter<typename Assembler::JacobianMatrix, typename Assembler::Scalar>;
        auto matrix = Converter::multiTypeToBCRSMatrix(assembler->jacobian());
        std::ofstream mat;
        mat.open("matrix.mtx", std::ios::out);
        writeMatrixMarket(matrix, mat);
    }

    // maybe write vtk output
    if (getParam<bool>("Output.EnableVTK"))
    {
        // intialize the vtk output module
        using BulkVtkWriter = VtkOutputModule<BulkGridVariables, GetPropType<BulkProblemTypeTag, Properties::SolutionVector>>;
        using LowDimVtkWriter = VtkOutputModule<LowDimGridVariables, GetPropType<LowDimProblemTypeTag, Properties::SolutionVector>>;
        const auto bulkDM = BulkFVGridGeometry::discMethod == DiscretizationMethod::box ? Dune::VTK::nonconforming : Dune::VTK::conforming;
        BulkVtkWriter bulkVtkWriter(*bulkGridVariables, x[bulkId], bulkProblem->name(), "Bulk", bulkDM);
        LowDimVtkWriter lowDimVtkWriter(*lowDimGridVariables, x[lowDimId], lowDimProblem->name(), "LowDim");

        using BulkVtkOutputFields = GetPropType<BulkProblemTypeTag, Properties::VtkOutputFields>;
        using LowDimVtkOutputFields = GetPropType<LowDimProblemTypeTag, Properties::VtkOutputFields>;
        BulkVtkOutputFields::initOutputModule(bulkVtkWriter);
        LowDimVtkOutputFields::initOutputModule(lowDimVtkWriter);

        // container for the output of the exact solutions
        std::vector< GetPropType<BulkProblemTypeTag, Properties::Scalar> > bulkExact(bulkFvGridGeometry->numDofs());
        std::vector< GetPropType<LowDimProblemTypeTag, Properties::Scalar> > lowDimExact(lowDimFvGridGeometry->numDofs());
        std::vector< GetPropType<LowDimProblemTypeTag, Properties::Scalar> > lowDimExactAvg(lowDimFvGridGeometry->numDofs());
        for (const auto& element : elements(bulkFvGridGeometry->gridView()))
        {
            if (BulkFVGridGeometry::discMethod == DiscretizationMethod::box)
            {
                const auto eg = element.geometry();
                int movementDir = eg.center()[1] > 0.0 ? 1 : -1;
                for (int i = 0; i < element.geometry().corners(); ++i)
                    bulkExact[ bulkFvGridGeometry->vertexMapper().subIndex(element, i, BulkGrid::dimension) ]
                              = bulkProblem->exact( element.template subEntity<BulkGrid::dimension>(i).geometry().center(), movementDir );
            }
            else
                bulkExact[ bulkFvGridGeometry->elementMapper().index(element) ] = bulkProblem->exact( element.geometry().center() );
        }

        for (const auto& element : elements(lowDimFvGridGeometry->gridView()))
        {
            if (LowDimFVGridGeometry::discMethod == DiscretizationMethod::box)
            {
                for (int i = 0; i < element.geometry().corners(); ++i)
                {
                    const auto vIdx = lowDimFvGridGeometry->vertexMapper().subIndex(element, i, LowDimGrid::dimension);
                    const auto position = element.template subEntity<LowDimGrid::dimension>(i).geometry().center();
                    lowDimExact[vIdx] = lowDimProblem->exact( position );
                    lowDimExactAvg[vIdx] = lowDimProblem->exact_crossSectionAverage( position );
                }
            }
            else
            {
                lowDimExact[ lowDimFvGridGeometry->elementMapper().index(element) ] = lowDimProblem->exact( element.geometry().center() );
                lowDimExactAvg[ lowDimFvGridGeometry->elementMapper().index(element) ] = lowDimProblem->exact_crossSectionAverage( element.geometry().center() );
            }
        }

        bulkVtkWriter.addField(bulkExact, "pressure_exact");
        lowDimVtkWriter.addField(lowDimExact, "pressure_exact");
        lowDimVtkWriter.addField(lowDimExact, "pressure_exact_average");

        bulkVtkWriter.write(1.0);
        lowDimVtkWriter.write(1.0);
    }

    // lambdas to bind the coupling context prior to flux computations
    auto bindBulkContext = [&] (const auto& element) -> void { couplingManager->bindCouplingContext(bulkId, element, *assembler); };
    auto bindLowDimContext = [&] (const auto& element) -> void { couplingManager->bindCouplingContext(lowDimId, element, *assembler); };

    // norm of the errors in the two domains
    using namespace Dumux::FractureConvergenceTest;
    const auto m = computeL2Norm<BulkProblemTypeTag>(bulkFvGridGeometry->gridView(), *bulkProblem, *bulkGridVariables, x[bulkId], bindBulkContext);
    const auto f = computeL2Norm<LowDimProblemTypeTag>(lowDimFvGridGeometry->gridView(), *lowDimProblem, *lowDimGridVariables, x[lowDimId], bindLowDimContext);

    // output to terminal
    std::cout << "Matrix - dx/uNorm/qNorm/transferNorm: " << m.dx << ", " << m.uErrorNorm << ", " << m.qErrorNorm << ", " << m.qTransferErrorNorm << std::endl;
    std::cout << "Fracture - dx/uNorm/qNorm/transferNorm: " << f.dx << ", " << f.uErrorNorm << ", " << f.qErrorNorm << ", " << f.qTransferErrorNorm <<  std::endl;

    // append results to output file
    std::ofstream file(getParam<std::string>("Problem.OutputFileName"), std::ios::app);
    file << m.dx << ","                    // idx 0
         << m.uNorm << ","                 // idx 1
         << m.uErrorNorm << ","            // idx 2
         << m.qNorm << ","                 // idx 3
         << m.qErrorNorm << ","            // idx 4
         << m.qTransferNorm << ","         // idx 5
         << m.qTransferErrorNorm << ","    // idx 6
         << f.dx << ","                    // idx 7
         << f.uNorm << ","                 // idx 8
         << f.uErrorNorm << ","            // idx 9
         << f.qNorm << ","                 // idx 10
         << f.qErrorNorm << ","            // idx 11
         << f.qTransferNorm << ","         // idx 12
         << f.qTransferErrorNorm << "\n";  // idx 13
    file.close();

    // write .csv file with source distribution
    std::ofstream sourceDistro(getParam<std::string>("Problem.SourceCsvFile"), std::ios::out);
    for (const auto& element : elements(lowDimFvGridGeometry->gridView()))
    {
        auto fvGeometry = localView(*lowDimFvGridGeometry);
        auto elemVolVars = localView(lowDimGridVariables->curGridVolVars());

        couplingManager->bindCouplingContext(lowDimId, element, *assembler);
        fvGeometry.bindElement(element);
        elemVolVars.bindElement(element, fvGeometry, x[lowDimId]);

        for (const auto& scv: scvs(fvGeometry))
            sourceDistro << scv.center()[0] << ","
                         << lowDimProblem->source(element, fvGeometry, elemVolVars, scv)
                            *elemVolVars[scv].extrusionFactor() << ","
                         << lowDimProblem->exactSourceTerm(scv.center())
                         << std::endl;
    }

    // // The following writes out the lower-interface transfer fluxes instead of the source
    // for (const auto& element : elements(bulkFvGridGeometry->gridView()))
    // {
    //     if (element.geometry().center()[1] > 0.0)
    //         continue;
    //
    //     auto fvGeometry = localView(*bulkFvGridGeometry);
    //     auto elemVolVars = localView(bulkGridVariables->curGridVolVars());
    //     auto elemFluxVarsCache = localView(bulkGridVariables->gridFluxVarsCache());
    //
    //     couplingManager->bindCouplingContext(bulkId, element, *assembler);
    //     fvGeometry.bind(element);
    //     elemVolVars.bind(element, fvGeometry, x[bulkId]);
    //     elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);
    //
    //     for (const auto& scvf : scvfs(fvGeometry))
    //     {
    //         if (couplingManager->isOnInteriorBoundary(element, scvf))
    //         {
    //             using FluxVars = GetPropType<BulkProblemTypeTag, Properties::FluxVariables>;
    //             FluxVars fv;
    //             fv.init(*bulkProblem, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);
    //
    //             static const double a = getParam<double>("LowDim.Problem.ExtrusionFactor");
    //             static const bool useExact = getParam<bool>("Bulk.Problem.UseExactGeometry");
    //             auto pos = scvf.ipGlobal();
    //             if (!useExact) pos[1] -= a/2.0;
    //
    //             sourceDistro << scvf.ipGlobal()[0] << ","
    //                          << fv.advectiveFlux(0, [](auto& vv){return 1.0;})/scvf.area() << ","
    //                          << bulkProblem->exactFlux(pos, scvf.unitOuterNormal())
    //                          << std::endl;
    //         }
    //     }
    // }

    sourceDistro.close();

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/false);

    return 0;
}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
