// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup MultiDomain
 * \ingroup FacetCoupling
 * \brief The problem for the bulk domain in the single-phase facet coupling test
 */
#ifndef DUMUX_TEST_TPFAFACETCOUPLING_ONEP_BULKPROBLEM_HH
#define DUMUX_TEST_TPFAFACETCOUPLING_ONEP_BULKPROBLEM_HH

#include <dune/alugrid/grid.hh>

#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/multidomain/facet/box/properties.hh>
#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>
#include <dumux/multidomain/facet/cellcentered/mpfa/properties.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/1p/model.hh>

#include "spatialparams.hh"

namespace Dumux {
// forward declarations
template<class TypeTag> class OnePBulkProblem;

namespace Properties {
namespace TTag {

// create the type tag nodes
struct OnePBulk { using InheritsFrom = std::tuple<OneP>; };
struct OnePBulkTpfa { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, OnePBulk>; };
struct OnePBulkMpfa { using InheritsFrom = std::tuple<CCMpfaFacetCouplingModel, OnePBulk>; };
struct OnePBulkBox { using InheritsFrom = std::tuple<BoxFacetCouplingModel, OnePBulk>; };

} // end namespace TTag

// Set the grid type (must be declared in CMakeLists.txt)
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePBulk> { using type = BULKGRIDTYPE; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::OnePBulk> { using type = OnePBulkProblem<TypeTag>; };

// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePBulk>
{
private:
    using FVGridGeometry = GetPropType<TypeTag, FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Scalar>;

public:
    using type = OnePSpatialParams< FVGridGeometry, Scalar >;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePBulk>
{
private:
    using Scalar = GetPropType<TypeTag, Scalar>;

public:
    using type = FluidSystems::OnePLiquid< Scalar, Components::Constant<1, Scalar> >;
};

//! Parameters are solution-independent
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::OnePBulk> { static constexpr bool value = false; };

} // end namespace Properties
/*!
 * \ingroup OnePTests
 * \brief Test problem for the incompressible one-phase model
 *        with coupling across the bulk grid facets
 */
template<class TypeTag>
class OnePBulkProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

    static constexpr bool isBox = FVGridGeometry::discMethod == DiscretizationMethod::box;

public:
    OnePBulkProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                    std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                    std::shared_ptr<CouplingManager> couplingManager,
                    const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManager)
    , lowDimPermeability_(getParam<Scalar>("LowDim.SpatialParams.Permeability"))
    , aperture_(getParam<Scalar>("LowDim.Problem.ExtrusionFactor"))
    , useInteriorDirichletBCs_(getParamFromGroup<bool>(paramGroup, "Problem.UseInteriorDirichletBCs"))
    , useExactGeometry_(getParamFromGroup<bool>(paramGroup, "Problem.UseExactGeometry", false))
    , rescaleSources_(getParamFromGroup<bool>(paramGroup, "Problem.RescaleSources"))
    , rescaleBoundaryFluxes_(getParamFromGroup<bool>(paramGroup, "Problem.RescaleBoundaryFluxes"))
    , useSecondaryAnalyticSolution_(getParamFromGroup<bool>(paramGroup, "Problem.UseSecondaryAnalyticSolution"))
    {}

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllDirichlet();

        static const bool useLateralNeumann = getParam<bool>("Problem.UseLateralNeumannBC");
        if (useLateralNeumann)
        {
            if (globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + 1e-6
                || globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - 1e-6)
                values.setAllNeumann();
        }

        return values;
    }

    //! Specifies the type of interior boundary condition at a given position
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        if (useInteriorDirichletBCs_)
            values.setAllDirichlet();
        else
            values.setAllNeumann();
        return values;
    }

    //! evaluates the exact flux density at a given position.
    Scalar exactSourceTerm(const GlobalPosition& globalPos) const
    {
        const auto ip = getIntegrationPoint_(globalPos);

        using std::cos;
        using std::cosh;
        if (!useSecondaryAnalyticSolution_)
            return (1.0 - lowDimPermeability_)*cos(ip[0])*cosh(aperture_/2.0);
        else
            return 0.0;
    }

    //! Evaluate the source term at a given position
    template<class ElementVolumeVariables>
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        const auto& scvGeometry = scv.geometry();
        if (!useExactGeometry_ && rescaleSources_ && touchesFracture_(element, fvGeometry))
        {
            using std::min;
            using std::max;
            Scalar yMin = 1e12;
            Scalar xMin = 1e12;
            Scalar yMax = -1e12;
            Scalar xMax = -1e12;

            for (unsigned int i = 0; i < 4; ++i)
            {
                yMin = min(scv.corner(i)[1], yMin);
                yMax = max(scv.corner(i)[1], yMax);

                xMin = min(scv.corner(i)[0], xMin);
                xMax = max(scv.corner(i)[0], xMax);
            }

            bool scvOnFrac = false;
            if (yMin < 1e-7 && yMin > -1e-7)
                scvOnFrac = true;
            if (yMax < 1e-7 && yMax > -1e-7)
                scvOnFrac = true;

            if (scvOnFrac)
            {
                const auto dx = xMax-xMin;
                auto actualVolume = scv.volume();
                actualVolume -= dx*aperture_/2.0;
                actualVolume = max(actualVolume, 0.0);

                // create the four corners of the quadrilateral outside the fracture
                if (actualVolume > 0.0)
                {
                    if (scvGeometry.type() != Dune::GeometryTypes::quadrilateral)
                        DUNE_THROW(Dune::InvalidStateException, "Source correction only works for quads!");

                    std::vector<GlobalPosition> corners;
                    if (scv.center()[1] > 0.0)
                    {
                        corners.push_back( GlobalPosition({xMin, aperture_/2.0}) );
                        corners.push_back( GlobalPosition({xMax, aperture_/2.0}) );
                        corners.push_back( GlobalPosition({xMin, yMax}) );
                        corners.push_back( GlobalPosition({xMax, yMax}) );
                    }
                    else
                    {
                        corners.push_back( GlobalPosition({xMin, yMin}) );
                        corners.push_back( GlobalPosition({xMax, yMin}) );
                        corners.push_back( GlobalPosition({xMin, -aperture_/2.0}) );
                        corners.push_back( GlobalPosition({xMax, -aperture_/2.0}) );
                    }

                    Dune::MultiLinearGeometry<Scalar, 2, 2> exactGeometry(scvGeometry.type(), corners);
                    return NumEqVector( integrateExactSource_(exactGeometry)/scvGeometry.volume() );
                }
                else
                    return NumEqVector(0.0);
            }
        }

        return NumEqVector( integrateExactSource_(scvGeometry)/scvGeometry.volume() );
    }

    //! evaluates the exact solution at a given position.
    Scalar exact(const GlobalPosition& globalPos, int forceIpMovement = 0) const
    {
        const auto ip = getIntegrationPoint_(globalPos, forceIpMovement);

        using std::cos;
        using std::cosh;
        const auto x = ip[0];
        const auto y = ip[1];

        if (!useSecondaryAnalyticSolution_)
            return lowDimPermeability_*cos(x)*cosh(y) + (1.0 - lowDimPermeability_)*cos(x)*cosh(aperture_/2.0);
        else
            return cos(x)*cosh(y);
    }

    //! evaluates the exact gradient at a given position.
    //! see getIntegrationPoint_() for details on the parameter forceIpMovement
    GlobalPosition exactGradient(const GlobalPosition& globalPos, int forceIpMovement = 0) const
    {
        const auto ip = getIntegrationPoint_(globalPos, forceIpMovement);

        using std::cos;
        using std::sin;
        using std::cosh;
        using std::sinh;

        const auto x = ip[0];
        const auto y = ip[1];

        GlobalPosition gradU;
        if (!useSecondaryAnalyticSolution_)
        {
            gradU[0] = -1.0*lowDimPermeability_*sin(x)*cosh(y) + (lowDimPermeability_ - 1.0)*sin(x)*cosh(aperture_/2);
            gradU[1] = lowDimPermeability_*cos(x)*sinh(y);
        }
        else
        {
            gradU[0] = -1.0*sin(x)*cosh(y);
            gradU[1] = cos(x)*sinh(y);
        }

        return gradU;
    }

    //! evaluates the exact gradient at a given position.
    //! see getIntegrationPoint_() for details on the parameter forceIpMovement
    GlobalPosition exactVelocity(const GlobalPosition& globalPos, int forceIpMovement = 0) const
    {
        // This assumes viscosity of 1.0
        const auto& k = this->spatialParams().permeabilityAtPos(globalPos);
        auto vel = exactGradient(globalPos, forceIpMovement);
        vel *= -this->spatialParams().permeabilityAtPos(globalPos);
        return vel;
    }

    //! evaluates the exact flux density at a given position.
    Scalar exactFlux(const GlobalPosition& globalPos, const GlobalPosition& normalVector) const
    {
        const auto& k = this->spatialParams().permeabilityAtPos(globalPos);

        // For fluxes on the interface, we need to manually determine the movement direction
        if (globalPos[1] < 1e-8 && globalPos[1] > -1e-8)
        {
            if (normalVector[1] > 0.0)
                return -1.0*vtmv(normalVector, k, exactGradient(globalPos, -1));
            else
                return -1.0*vtmv(normalVector, k, exactGradient(globalPos, 1));
        }

        return -1.0*vtmv(normalVector, k, exactGradient(globalPos));
    }

    //! evaluates the Dirichlet boundary condition for a given scv (box schemes)
    PrimaryVariables dirichlet(const Element& element, const SubControlVolume& scv) const
    {
        // If the point touches the interface we have
        // to manually force the movement direction
        const auto y = scv.dofPosition()[1];
        if (y < 1e-8 && y > -1e-8)
        {
            if (element.geometry().center()[1] > 0.0)
                return PrimaryVariables( exact(scv.dofPosition(), 1) );
            else
                return PrimaryVariables( exact(scv.dofPosition(), -1) );
        }

        return PrimaryVariables( exact(scv.dofPosition()) );
    }

    //! evaluates the Dirichlet boundary condition for a given scvf (cc schemes)
    PrimaryVariables dirichlet(const Element& element, const SubControlVolumeFace& scvf) const
    {
        // integrate and average the exact solution
        Scalar p = 0.0;
        const auto& scvfGeometry = scvf.geometry();
        const auto& rule = Dune::QuadratureRules<Scalar, 1>::rule(scvfGeometry.type(), 2);
        for (const auto& qp : rule)
        {
            const auto local = qp.position();
            const auto global = scvfGeometry.global(local);
            p += exact(global)*qp.weight()*scvfGeometry.integrationElement(qp.position());
        }

        return PrimaryVariables(p/scvfGeometry.volume());
    }

    //! evaluates the Neumann boundary condition for a boundary segment
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        const auto& scvfGeometry = scvf.geometry();
        if (!useExactGeometry_ && rescaleBoundaryFluxes_ && touchesFracture_(element, fvGeometry, scvf))
        {
            // make Geometry of the boundary segment not overlapping with fracture
            std::vector<GlobalPosition> corners;
            if (scvf.corner(0)[1] < 1e-6 && scvf.corner(0)[1] > -1e-6)
            {
                corners.push_back(scvf.corner(1));
                if (scvf.center()[1] > 0.0)
                {
                    if (scvf.corner(1)[1] <= aperture_/2.0)
                        return NumEqVector(0.0);
                    corners.push_back(scvf.corner(0) + GlobalPosition({0.0, aperture_/2.0}));
                }
                else if (scvf.center()[1] < 0.0)
                {
                    if (scvf.corner(1)[1] >= -aperture_/2.0)
                        return NumEqVector(0.0);
                    corners.push_back(scvf.corner(0) - GlobalPosition({0.0, aperture_/2.0}));
                }
                else DUNE_THROW(Dune::InvalidStateException, "Could not identify face position");
            }
            else if (scvf.corner(1)[1] < 1e-6 && scvf.corner(1)[1] > -1e-6)
            {
                corners.push_back(scvf.corner(0));
                if (scvf.center()[1] > 0.0)
                {
                    if (scvf.corner(0)[1] <= aperture_/2.0)
                        return NumEqVector(0.0);
                    corners.push_back(scvf.corner(1) + GlobalPosition({0.0, aperture_/2.0}));
                }
                else if (scvf.center()[1] < 0.0)
                {
                    if (scvf.corner(0)[1] >= -aperture_/2.0)
                        return NumEqVector(0.0);
                    corners.push_back(scvf.corner(1) - GlobalPosition({0.0, aperture_/2.0}));
                }
                else DUNE_THROW(Dune::InvalidStateException, "Could not identify face position");
            }
            else
                DUNE_THROW(Dune::InvalidStateException, "Could not identify fracture corner");

            Dune::MultiLinearGeometry<Scalar, 1, 2> exactGeometry(scvfGeometry.type(), corners);
            return NumEqVector( integrateExactFlux_(exactGeometry, scvf.unitOuterNormal())/scvfGeometry.volume() );
        }

        return NumEqVector( integrateExactFlux_(scvfGeometry, scvf.unitOuterNormal())/scvfGeometry.volume() );
    }

    //! evaluate the initial conditions
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(1.0); }

    //! returns the temperature in \f$\mathrm{[K]}\f$ in the domain
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

private:
    //! Integrate the exact flux over a 1d geometry
    template<class Geometry>
    Scalar integrateExactFlux_(const Geometry& geometry, const GlobalPosition& normal) const
    {
        static_assert(Geometry::mydimension == 1, "one-dimensional geometry expected in flux integration");

        Scalar flux = 0.0;
        const auto& rule = Dune::QuadratureRules<Scalar, 1>::rule(geometry.type(), 2);
        for (const auto& qp : rule)
        {
            const auto local = qp.position();
            const auto global = geometry.global(local);
            const auto f = exactFlux(global, normal);
            flux += f*qp.weight()*geometry.integrationElement(local);
        }

        return flux;
    }

    //! Integrate the exact flux over a 2d geometry
    template<class Geometry>
    Scalar integrateExactSource_(const Geometry& geometry) const
    {
        static_assert(Geometry::mydimension == 2, "two-dimensional geometry expected in source integration");

        Scalar source = 0.0;
        const auto& rule = Dune::QuadratureRules<Scalar, 2>::rule(geometry.type(), 2);
        for (const auto& qp : rule)
        {
            const auto local = qp.position();
            const auto global = geometry.global(local);
            const auto f = exactSourceTerm(global);
            source += f*qp.weight()*geometry.integrationElement(local);
        }

        return source;
    }

    //! returns true if an element touches a fracture
    bool touchesFracture_(const Element& element, const FVElementGeometry& fvGeometry) const
    {
        for (const auto& scvf : scvfs(fvGeometry))
            if (couplingManager().isOnInteriorBoundary(element, scvf))
                return true;
        return false;
    }

    //! returns true if a sub-control volume face touches a fracture (box implementation)
    template<bool isB = isBox, std::enable_if_t<isB, int> = 0>
    bool touchesFracture_(const Element& element, const FVElementGeometry& fvGeometry, const SubControlVolumeFace& scvf) const
    {
        using std::min;
        using std::max;
        Scalar yMin = 1e12;
        Scalar yMax = -1e12;
        for (unsigned int i = 0; i < 2; ++i)
        {
            yMin = min(scvf.corner(i)[1], yMin);
            yMax = max(scvf.corner(i)[1], yMax);
        }

        bool scvfOnFrac = false;
        if (yMin < 1e-7 && yMin > -1e-7)
            scvfOnFrac = true;
        if (yMax < 1e-7 && yMax > -1e-7)
            scvfOnFrac = true;
        return scvfOnFrac;
    }

    //! returns true if a sub-control volume face touches a fracture (cc implementation)
    template<bool isB = isBox, std::enable_if_t<!isB, int> = 0>
    bool touchesFracture_(const Element& element, const FVElementGeometry& fvGeometry, const SubControlVolumeFace& scvf) const
    {
        if (!scvf.boundary())
            DUNE_THROW(Dune::InvalidStateException, "This query is designed for boundary faces");
        return touchesFracture_(element, fvGeometry);
    }

    //! If we use the modified grid, which meshes a domain
    //! in which aperture/2 is cut on the top and bottom, we
    //! are able to remove the volume error introduced by the
    //! scheme by evaluating BCs etc at modified positions,
    //! i.e. by moving the y-coordinate by aperture/2.
    //! The integer forceIpMovement enables forcing of the
    //! movement in positive/negative coordinate direction,
    //! where +1 means positive, -1 negative and 0 means that
    //! the direction is determined based on the given position.
    //! This is necessary for flux evaluation on faces at the interface
    //! where otherwise it wouldn't be possible to determine in
    //! which direction the point is to be moved
    GlobalPosition getIntegrationPoint_(const GlobalPosition& pos, int forceIpMovement = 0) const
    {
        if (!useExactGeometry_) return pos;
        else if (forceIpMovement == 0 && pos[1] > 0.0) return pos + GlobalPosition({0.0, aperture_/2.0});
        else if (forceIpMovement == 0 && pos[1] < 0.0) return pos - GlobalPosition({0.0, aperture_/2.0});
        else if (forceIpMovement == 1) return pos + GlobalPosition({0.0, aperture_/2.0});
        else if (forceIpMovement == -1) return pos - GlobalPosition({0.0, aperture_/2.0});
        else DUNE_THROW(Dune::InvalidStateException, "Invalid function argument combination");
    }

    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    Scalar lowDimPermeability_;
    Scalar aperture_;
    bool useInteriorDirichletBCs_;
    bool useExactGeometry_;
    bool rescaleSources_;
    bool rescaleBoundaryFluxes_;
    bool useSecondaryAnalyticSolution_;
};

} // end namespace Dumux

#endif
