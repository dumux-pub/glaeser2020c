// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Functionality to compute the norms in the fracture test.
 */
#ifndef DUMUX_FRACTURE_CONVERGENCETEST_FACET_COMPUTENORM_HH
#define DUMUX_FRACTURE_CONVERGENCETEST_FACET_COMPUTENORM_HH

#include <dune/common/fvector.hh>
#include <dune/geometry/type.hh>
#include <dune/geometry/multilineargeometry.hh>

#include <dumux/common/properties/propertysystem.hh>
#include <dumux/common/geometry/diameter.hh>
#include <dumux/common/parameters.hh>
#include <dumux/discretization/evalsolution.hh>
#include <dumux/discretization/evalgradients.hh>

namespace Dumux {

namespace FractureConvergenceTest {

//! computes a measure for the "length" of a geometry
template<class Geometry>
typename Geometry::ctype length(const Geometry& geo)
{
    if (geo.type() == Dune::GeometryTypes::line)
        return geo.volume();
    if (geo.type() == Dune::GeometryTypes::triangle)
        return Dumux::diameter(geo);
    if (geo.type() == Dune::GeometryTypes::quadrilateral)
    {
        // take the minimum of all sides
        const auto dx1 = (geo.corner(1) - geo.corner(0)).two_norm();
        const auto dx2 = (geo.corner(2) - geo.corner(0)).two_norm();
        const auto dx3 = (geo.corner(3) - geo.corner(2)).two_norm();
        const auto dx4 = (geo.corner(3) - geo.corner(1)).two_norm();

        using std::min;
        return min( min( min(dx1, dx2), dx3 ), dx4 );
    }

    DUNE_THROW(Dune::InvalidStateException, "Unexpected geometry type");
}

//! Base class for the data types storing l2 error norms
template<class Scalar>
struct L2NormData
{
    Scalar dx;        // measure for the discretization length of the grid

    Scalar uNorm;      // integrated squared exact solution
    Scalar uErrorNorm; // L2 norm of the error in solution

    Scalar qNorm;      // integrated squared exact flux
    Scalar qErrorNorm; // L2 norm of the error in fluxes

    Scalar qTransferNorm;      // Integral of the squared exact transfer flux
    Scalar qTransferErrorNorm; // L2 norm of the error in matrix-fracture transfer fluxes

    // Constructor
    L2NormData()
    : uNorm(0.0), uErrorNorm(0.0)
    , qNorm(0.0), qErrorNorm(0.0)
    , qTransferNorm(0.0), qTransferErrorNorm(0.0)
    {}
};

namespace Detail {

//! Evaluates the exact solution in a domain (overload for matrix domain)
template< class Problem, class Element,
          std::enable_if_t< int(Element::Geometry::mydimension) ==
                            int(Element::Geometry::coorddimension), int > = 0 >
double evaluateExactSolution(const Problem& problem,
                             const Element& element,
                             const typename Element::Geometry::GlobalCoordinate& ip)
{ return problem.exact(ip); }

//! Evaluates the exact solution in a domain (overload for fracture domain)
template< class Problem, class Element,
          std::enable_if_t< int(Element::Geometry::mydimension) <
                            int(Element::Geometry::coorddimension), int > = 0 >
double evaluateExactSolution(const Problem& problem,
                             const Element& element,
                             const typename Element::Geometry::GlobalCoordinate& ip)
{ return problem.exact_crossSectionAverage(ip); }

//! Evaluates the exact flux in a domain (overload for matrix domain)
template< class Problem, class Element, class SCVF,
          std::enable_if_t< int(Element::Geometry::mydimension) ==
                            int(Element::Geometry::coorddimension), int > = 0 >
double evaluateExactFlux(const Problem& problem,
                         const Element& element,
                         const SCVF& scvf,
                         const typename Element::Geometry::GlobalCoordinate& ip)
{ return problem.exactFlux(ip, scvf.unitOuterNormal()); }

//! Evaluates the exact flux in a domain (overload for matrix domain)
template< class Problem, class Element, class SCVF,
          std::enable_if_t< int(Element::Geometry::mydimension) <
                            int(Element::Geometry::coorddimension), int > = 0 >
double evaluateExactFlux(const Problem& problem,
                         const Element& element,
                         const SCVF& scvf,
                         const typename Element::Geometry::GlobalCoordinate& ip)
{ return problem.exactFlux_crossSectionAverage(ip, scvf.unitOuterNormal()); }

//! Evaluates the exact flux in a domain (overload for matrix domain)
template< class Problem, class Element, class SCVF,
          std::enable_if_t< int(Element::Geometry::mydimension) ==
                            int(Element::Geometry::coorddimension), int > = 0 >
double evaluateExactTransferFlux(const Problem& problem,
                                 const Element& element,
                                 const SCVF& scvf,
                                 const typename Element::Geometry::GlobalCoordinate& ip)
{
    static const bool exactGeometry = getParam<bool>("Bulk.Problem.UseExactGeometry");
    static const double a = getParam<double>("LowDim.Problem.ExtrusionFactor");

    // is the exact geometry is not used, make sure the transfer flux
    // is evaluated at a valid position on the interface
    auto pos = ip;
    if (!exactGeometry)
    {
        if (element.geometry().center()[1] > 0.0)
            pos[1] += a/2.0;
        else
            pos[1] -= a/2.0;
    }

    return problem.exactFlux(pos, scvf.unitOuterNormal());
}

//! Evaluates the exact flux in a domain (overload for matrix domain)
template< class Problem, class Element, class SCVF,
          std::enable_if_t< int(Element::Geometry::mydimension) <
                            int(Element::Geometry::coorddimension), int > = 0 >
double evaluateExactTransferFlux(const Problem& problem,
                                 const Element& element,
                                 const SCVF& scvf,
                                 const typename Element::Geometry::GlobalCoordinate& ip)
{ DUNE_THROW(Dune::InvalidStateException, "Transfer flux on low dim domain makes no sensse"); }

} // end namespace Detail


//! computes the L2 norm of the error of a sub-problem
template< class TypeTag, class BindCouplingManagerFunc >
L2NormData< Dumux::GetPropType<TypeTag, Dumux::Properties::Scalar> >
computeL2Norm(const Dumux::GetPropType<TypeTag, Dumux::Properties::GridView>& gridView,
              const Dumux::GetPropType<TypeTag, Dumux::Properties::Problem>& problem,
              const Dumux::GetPropType<TypeTag, Dumux::Properties::GridVariables>& gridVariables,
              const Dumux::GetPropType<TypeTag, Dumux::Properties::SolutionVector>& x,
              BindCouplingManagerFunc&& bindCouplingManager)
{
    using Scalar = Dumux::GetPropType<TypeTag, Dumux::Properties::Scalar>;
    using GridView = Dumux::GetPropType<TypeTag, Dumux::Properties::GridView>;
    using GridGeometry = Dumux::GetPropType<TypeTag, Dumux::Properties::FVGridGeometry>;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr bool isMatrixDomain = dim == dimWorld;
    static constexpr bool isBox = GridGeometry::discMethod == Dumux::DiscretizationMethod::box;

    const auto uOrder = Dumux::getParam<int>("L2Norm.SolutionIntegrationOrder");
    const auto qOrder = Dumux::getParam<int>("L2Norm.FluxIntegrationOrder");

    L2NormData<Scalar> l2Data;
    l2Data.dx = std::numeric_limits<Scalar>::max();

    Scalar domainIntegrationArea = 0.0;
    Scalar faceIntegrationArea = 0.0;
    Scalar transferFluxIntegrationArea = 0.0;

    // integrate error in each cell
    for (const auto& element : elements(gridView))
    {
        const auto eg = element.geometry();

        using std::min;
        l2Data.dx = min(length(eg), l2Data.dx);

        // make discrete element solution
        const auto elemSol = elementSolution(element, x, problem.fvGridGeometry());

        auto fvGeometry = localView(problem.fvGridGeometry());
        auto elemVolVars = localView(gridVariables.curGridVolVars());
        auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());

        bindCouplingManager(element);
        fvGeometry.bind(element);
        elemVolVars.bind(element, fvGeometry, x);
        elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

        ///////////////////////////////////////////////////
        // integrate the solution error over the element //
        ///////////////////////////////////////////////////
        Scalar fractureAvgSource = 0.0;
        domainIntegrationArea += eg.volume();
        if (!isMatrixDomain)
        {
            transferFluxIntegrationArea += eg.volume();
            for (const auto& scv : scvs(fvGeometry))
                fractureAvgSource += problem.source(element, fvGeometry, elemVolVars, scv)
                                     *scv.volume()
                                     *elemVolVars[scv].extrusionFactor();
            fractureAvgSource /= eg.volume();
        }

        const auto& rule = Dune::QuadratureRules<Scalar, dim>::rule(eg.type(), uOrder);
        for (auto&& qp : rule)
        {
            // exact and discrete solution at integration point
            const auto ip = eg.global(qp.position());
            const auto u = Detail::evaluateExactSolution(problem, element, ip);
            const auto uh = evalSolution(element, eg, elemSol, ip);

            const auto wIntElement = qp.weight()*eg.integrationElement(qp.position());
            l2Data.uNorm      +=       u * u      *wIntElement;
            l2Data.uErrorNorm += (uh - u)*(uh - u)*wIntElement;

            // for the fracture domain, add error in sources
            if (!isMatrixDomain)
            {
                const auto sourceExact = problem.exactSourceTerm(ip);
                const auto deltaSource = sourceExact - fractureAvgSource;
                l2Data.qTransferNorm += sourceExact*sourceExact*wIntElement;
                l2Data.qTransferErrorNorm += deltaSource*deltaSource*wIntElement;
            }
        }

        // compute contribution to flux error for this element
        Scalar qNormContribution = 0.0;
        Scalar qErrorNormContribution = 0.0;
        for (const auto& scvf : scvfs(fvGeometry))
        {
            const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
            const auto& insideVolVars = elemVolVars[insideScv];

            using FluxVars = GetPropType<TypeTag, Properties::FluxVariables>;
            FluxVars fluxVars;
            fluxVars.init(problem, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

            auto up = [] (const auto& vv) { return 1.0; };

            double qh;
            if (!scvf.boundary())
                qh = fluxVars.advectiveFlux(/*phaseIdx*/0, up)/(scvf.area()*insideVolVars.extrusionFactor());
            else
            {
                const auto bcTypes = isBox ? problem.boundaryTypes(element, insideScv)
                                           : problem.boundaryTypes(element, scvf);

                if (bcTypes.hasOnlyDirichlet() && !isBox)
                    qh = fluxVars.advectiveFlux(/*phaseIdx*/0, up)/(scvf.area()*insideVolVars.extrusionFactor());
                else if (bcTypes.hasOnlyNeumann())
                    qh = problem.neumann(element, fvGeometry, elemVolVars, scvf)[0];
                else // skip Dirichlet faces for box (it's hard to evaluate the boundary fluxes)
                    continue;
            }

            using std::abs;
            auto isOnFracture = [] (const auto& pos) { return abs(pos[1]) < 1e-8; };
            const bool isFluxToFracture = !isMatrixDomain ? false : isOnFracture(scvf.ipGlobal());

            // for cc schemes, scale interior fluxes with 0.5 as we compute them from both sides
            using FVGG = std::decay_t<decltype(fvGeometry.fvGridGeometry())>;
            const Scalar factor = FVGG::discMethod == DiscretizationMethod::box ? 1.0 : (isFluxToFracture ? 1.0 : 0.5);

            if (isFluxToFracture)
                transferFluxIntegrationArea += scvf.area();
            else
                faceIntegrationArea += factor*scvf.area();

            Scalar qNormScvf = 0.0;
            Scalar qErrorNormScvf = 0.0;
            const auto scvfGeometry = scvf.geometry();
            const auto& rule = Dune::QuadratureRules<Scalar, FVGG::GridView::dimension-1>::rule(scvfGeometry.type(), qOrder);
            for (auto&& qp : rule)
            {
                // exact flux at integration point
                const auto ip = scvfGeometry.global(qp.position());
                const auto q = isFluxToFracture ? Detail::evaluateExactTransferFlux(problem, element, scvf, ip)
                                                : Detail::evaluateExactFlux(problem, element, scvf, ip);

                const auto intElement = qp.weight()*scvfGeometry.integrationElement(qp.position());
                const auto qValue = factor*q*q*intElement;
                const auto qErrorValue = factor*(q - qh)*(q - qh)*intElement;

                if (isFluxToFracture)
                {
                    l2Data.qTransferNorm += qValue;
                    l2Data.qTransferErrorNorm += qErrorValue;
                }
                else
                {
                    qNormScvf += factor*qValue;
                    qErrorNormScvf += factor*qErrorValue;
                }
            }

            qNormContribution += qNormScvf/scvfGeometry.volume();
            qErrorNormContribution += qErrorNormScvf/scvfGeometry.volume();
        }

        l2Data.qNorm += qNormContribution*eg.volume();
        l2Data.qErrorNorm += qErrorNormContribution*eg.volume();
    }

    // return result
    return l2Data;
}

} // end namespace FractureConvergenceTest

} // end namespace Dumux

#endif
