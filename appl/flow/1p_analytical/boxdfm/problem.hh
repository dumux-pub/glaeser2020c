// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \brief The properties for the incompressible 1p-boxdfm test
 */
#ifndef DUMUX_INCOMPRESSIBLE_1PBOXDFM_TEST_PROBLEM_HH
#define DUMUX_INCOMPRESSIBLE_1PBOXDFM_TEST_PROBLEM_HH

#include <dune/alugrid/grid.hh>

#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/porousmediumflow/boxdfm/model.hh>

#include "spatialparams.hh"

namespace Dumux {

// forward declarations
template<class TypeTag> class OnePBoxDfmTestProblem;

namespace Properties {
namespace TTag {
// we need to derive first from twop and then from the box-dfm Model
// because the flux variables cache type of OneP is overwritten in BoxDfmModel
struct OnePBoxDfm { using InheritsFrom = std::tuple<BoxDfmModel, OneP>; };
}

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePBoxDfm> { using type = GRIDTYPE; };
// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::OnePBoxDfm> { using type = OnePBoxDfmTestProblem<TypeTag>; };
// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePBoxDfm>
{
private:
    using FVG = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = OnePTestSpatialParams<FVG, Scalar>;
};
// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePBoxDfm>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = FluidSystems::OnePLiquid< Scalar, Components::Constant<1, Scalar> >;
};
} // end namespace Properties

/*!
 * \ingroup TwoPTests
 * \brief The incompressible 2p-boxdfm test problem.
 */
template<class TypeTag>
class OnePBoxDfmTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;

    // some indices for convenience
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    enum { pressureIdx = Indices::pressureIdx };

    static constexpr Scalar eps_ = 1e-6;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

public:

    //! The constructor
    OnePBoxDfmTestProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        fractureAperture_ = getParamFromGroup<Scalar>("LowDim", "Problem.ExtrusionFactor");
        useExactGeometry_ = getParamFromGroup<bool>("Bulk", "Problem.UseExactGeometry", false);

    }

    /*!
     * \brief Return how much the domain is extruded at a given sub-control volume.
     *
     * Here, we extrude the fracture scvs by half the aperture
     */
    template<class ElementSolution>
    Scalar extrusionFactor(const Element& element,
                           const SubControlVolume& scv,
                           const ElementSolution& elemSol) const
    {
        // In the box-scheme, we compute fluxes etc element-wise,
        // thus per element we compute only half a fracture !!!
        if (scv.isOnFracture())
            return fractureAperture_/2.0;
        return 1.0;
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param values Stores the value of the boundary type
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllDirichlet();
        return values;
    }

    //! Evaluate the source term at a given position
    template< class ElemVolVars >
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElemVolVars& elemVolVars,
                       const SubControlVolume& scv) const
    {
        if (scv.isOnFracture())
            return NumEqVector(0.0);

        Scalar factor = 1.0;

        // check if scv touches a fracture
        static const bool rescaleSource = getParam<bool>("Bulk.Problem.RescaleSources");
        if (rescaleSource)
        {
            for (const auto& scvJ : scvs(fvGeometry))
            {
                if (scvJ.isOnFracture() && scvJ.localDofIndex() == scv.localDofIndex())
                {
                    using std::max;
                    Scalar actualVolume = scv.volume();
                    actualVolume -= scvJ.volume()*fractureAperture_/2.0;
                    actualVolume = max(actualVolume, 0.0);
                    factor = actualVolume/scv.volume();
                }
            }
        }

        using std::cos;
        using std::cosh;
        const auto ip = scv.center();
        Scalar u = factor*(1.0 - this->spatialParams().fracturePermeability())*cos(ip[0])*cosh(fractureAperture_/2);
        return NumEqVector(u);
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet
     *        boundary segment
     *
     * \param values Stores the Dirichlet values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variable} ] \f$
     * \param globalPos The global position
     */
    PrimaryVariables dirichlet(const Element& element,
                               const SubControlVolume& scv) const
    {
        using std::abs;
        if ( !scv.isOnFracture() )
            return exact(scv.dofPosition());
        return exact_crossSectionAverage(scv.dofPosition());
    }

    //! evaluates the exact solution at a given position.
    Scalar exact(const GlobalPosition& globalPos, bool evalInFracture = false) const
    {
        using std::abs;
        using std::cos;
        using std::cosh;

        const auto p = evalInFracture ? globalPos : getIntegrationPoint_(globalPos);
        const auto x = p[0];
        const auto y = p[1];

        // lower-dimensional fracture domain
        if (abs(globalPos[1]) < 1e-6)
            return cos(x)*cosh(y);

        // matrix domain
        return this->spatialParams().fracturePermeability()*cos(x)*cosh(y)
               + (1.0 - this->spatialParams().fracturePermeability())*cos(x)*cosh(fractureAperture_/2);
    }

    //! evaluates the exact gradient at a given position.
    GlobalPosition exactGradient(const GlobalPosition& globalPos, bool evalInFracture = false) const
    {
        using std::cos;
        using std::sin;
        using std::cosh;
        using std::sinh;

        const auto p = evalInFracture ? globalPos : getIntegrationPoint_(globalPos);
        const auto x = p[0];
        const auto y = p[1];

        GlobalPosition gradU;
        // lower-dimensional fracture domain
        if (evalInFracture)
        {
            gradU[0] = -1.0*sin(x)*cosh(y);
            gradU[1] = cos(x)*sinh(y);
        }
        else
        {
            const auto kf = this->spatialParams().fracturePermeability();
            gradU[0] = -1.0 * kf *sin(x)*cosh(y)
                           + (kf - 1.0)*sin(x)*cosh(fractureAperture_/2);
            gradU[1] = kf*cos(x)*sinh(y);
        }

        return gradU;
    }

    //! evaluates the exact flux density at a given position.
    Scalar exactFlux(const GlobalPosition& globalPos, const GlobalPosition& normalVector, bool evalInFracture = false) const
    {
        const auto& k = abs(globalPos[1]) < 1e-6 ? this->spatialParams().fracturePermeability()
                                                 : this->spatialParams().matrixPermeability();
        const auto gradU = exactGradient(globalPos, evalInFracture);
        return -1.0*vtmv(normalVector, k, gradU);
    }

    //! evaluates the cross-section averaged exact solution at a given position.
    Scalar exact_crossSectionAverage(const GlobalPosition& globalPos) const
    {
        const auto crossSection = makeCrossSectionSegment_(globalPos);

        static const auto intOrder = getParam<Scalar>("Averaging.IntegrationOrder");
        const auto& rule = Dune::QuadratureRules<Scalar, dim-1>::rule(crossSection.type(), intOrder);

        Scalar avg = 0.0;
        for (auto&& qp : rule)
            avg += exact( crossSection.global(qp.position()), true )
                   *crossSection.integrationElement( qp.position() )
                   *qp.weight();
        avg /= crossSection.volume();

        return avg;
    }

    //! evaluates the cross-section averaged exact flux at a given position.
    Scalar exactFlux_crossSectionAverage(const GlobalPosition& globalPos, const GlobalPosition& normalVector) const
    {
        const auto crossSection = makeCrossSectionSegment_(globalPos);

        static const auto intOrder = getParam<Scalar>("Averaging.IntegrationOrder");
        const auto& rule = Dune::QuadratureRules<Scalar, dim-1>::rule(crossSection.type(), intOrder);

        Scalar avg = 0.0;
        for (auto&& qp : rule)
            avg += exactFlux( crossSection.global(qp.position()), normalVector, true )
                   *crossSection.integrationElement( qp.position() )
                   *qp.weight();
        avg /= crossSection.volume();

        return avg;
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    { return NumEqVector(exactFlux(scvf.ipGlobal(), scvf.unitOuterNormal(), false)); }

    /*!
     * \brief Evaluates the initial values for a control volume
     *
     * \param values Stores the initial values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variables} ] \f$
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    { return PrimaryVariables(1.0); }

    //! Returns the temperature \f$\mathrm{[K]}\f$ for an isothermal problem.
    Scalar temperature() const
    { return 293.15; /* 10°C */ }

private:
    //! If we use the modified grid, which meshes a domain
    //! in which aperture/2 is cut on the top and bottom, we
    //! are able to remove the volume error introduced by the
    //! scheme by evaluating BCs etc at modified positions,
    //! i.e. by moving the y-coordinate by aperture/2.
    GlobalPosition getIntegrationPoint_(const GlobalPosition& pos) const
    {
        if (!useExactGeometry_) return pos;
        else if (pos[1] > 0.0) return pos + GlobalPosition({0.0, fractureAperture_/2.0});
        else return pos - GlobalPosition({0.0, fractureAperture_/2.0});
    }

    //! Creates the segment representing the cross section at a fracture position
    Dune::MultiLinearGeometry<Scalar, dim-1, dimWorld>
    makeCrossSectionSegment_(const GlobalPosition& globalPos) const
    {
        if (globalPos[1] > 1e-6 || globalPos[1] < -1e-6)
            DUNE_THROW(Dune::InvalidStateException, "Provided position " << globalPos << " is not on fracture centerline!");

        const auto p1 = globalPos + GlobalPosition({0.0, fractureAperture_/2.0});
        const auto p2 = globalPos - GlobalPosition({0.0, fractureAperture_/2.0});

        using Segment = Dune::MultiLinearGeometry<Scalar, dim-1, dimWorld>;
        return Segment(Dune::GeometryTypes::simplex(dim), std::vector<GlobalPosition>({p1, p2}));
    }

    bool useExactGeometry_;
    Scalar fractureAperture_;
};

} // end namespace Dumux

#endif
