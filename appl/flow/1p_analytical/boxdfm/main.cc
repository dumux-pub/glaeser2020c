// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief test for the one-phase box-dfm porous medium flow model
 */
#include <config.h>

#include <ctime>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/foamgrid/foamgrid.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/istl/matrixmarket.hh>

#include "problem.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>
#include <dumux/common/geometry/diameter.hh>

#include <dumux/linear/amgbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>
#include <dumux/discretization/evalsolution.hh>
#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/codimonegridadapter.hh>

#include <dumux/porousmediumflow/boxdfm/vtkoutputmodule.hh>

#include "../facet/computenorm.hh"

//! computes a measure for the "length" of a geometry
template<class Geometry>
typename Geometry::ctype length(const Geometry& geo)
{
    if (geo.type() == Dune::GeometryTypes::line)
        return geo.volume();
    if (geo.type() == Dune::GeometryTypes::triangle)
        return Dumux::diameter(geo);
    if (geo.type() == Dune::GeometryTypes::quadrilateral)
    {
        // take the minimum of all sides
        const auto dx1 = (geo.corner(1) - geo.corner(0)).two_norm();
        const auto dx2 = (geo.corner(2) - geo.corner(0)).two_norm();
        const auto dx3 = (geo.corner(3) - geo.corner(2)).two_norm();
        const auto dx4 = (geo.corner(3) - geo.corner(1)).two_norm();

        using std::min;
        return min( min( min(dx1, dx2), dx3 ), dx4 );
    }

    DUNE_THROW(Dune::InvalidStateException, "Unexpected geometry type");
}

//! computes the L2 norm of the error
template< class TypeTag >
std::array< Dumux::FractureConvergenceTest::L2NormData<Dumux::GetPropType<TypeTag, Dumux::Properties::Scalar>>, 2 >
computeL2Norm(const Dumux::GetPropType<TypeTag, Dumux::Properties::GridView>& gridView,
              const Dumux::GetPropType<TypeTag, Dumux::Properties::Problem>& problem,
              const Dumux::GetPropType<TypeTag, Dumux::Properties::GridVariables>& gridVariables,
              const Dumux::GetPropType<TypeTag, Dumux::Properties::SolutionVector>& x)
{
    // container to store results in
    using GridView = Dumux::GetPropType<TypeTag, Dumux::Properties::GridView>;
    using Scalar = Dumux::GetPropType<TypeTag, Dumux::Properties::Scalar>;
    using L2Data = Dumux::FractureConvergenceTest::L2NormData<Scalar>;
    using ResultType = std::array<L2Data, 2>;

    // some necessary parameters
    const auto uOrder = Dumux::getParam<int>("L2Norm.SolutionIntegrationOrder");
    const auto fluxOrder = Dumux::getParam<int>("L2Norm.FluxIntegrationOrder");

    ResultType result;
    auto& matrixL2Data = result[0];
    auto& fractureL2Data = result[1];

    matrixL2Data.dx = std::numeric_limits<Scalar>::max();
    fractureL2Data.dx = std::numeric_limits<Scalar>::max();

    Scalar matrixIntegrationArea = 0.0;
    Scalar fractureIntegrationArea = 0.0;

    Scalar matrixFaceIntegrationArea = 0.0;
    Scalar fractureFaceIntegrationArea = 0.0;

    // integrate error in each cell
    for (const auto& element : elements(gridView))
    {
        using std::min;
        using std::max;

        const auto eg = element.geometry();
        matrixL2Data.dx = min(length(eg), matrixL2Data.dx);

        // make discrete element solution
        const auto elemSol = elementSolution(element, x, problem.fvGridGeometry());

        // integrate the pressure error over the element
        matrixIntegrationArea += eg.volume();
        const auto& rule = Dune::QuadratureRules<Scalar, GridView::dimension>::rule(eg.type(), uOrder);
        for (auto&& qp : rule)
        {
            // integration point in global coordinates
            const auto ip = eg.global(qp.position());

            // exact and discrete solution at integration point
            const auto u = problem.exact(ip);
            const auto uh = evalSolution(element, eg, elemSol, ip);

            matrixL2Data.uNorm += u*u*qp.weight()*eg.integrationElement(qp.position());
            matrixL2Data.uErrorNorm += (uh - u)*(uh - u)*qp.weight()*eg.integrationElement(qp.position());
        }

        // compute flux error contribution and fracture errors
        auto fvGeometry = localView(problem.fvGridGeometry());
        auto elemVolVars = localView(gridVariables.curGridVolVars());
        auto elemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());

        fvGeometry.bind(element);
        elemVolVars.bind(element, fvGeometry, x);
        elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

        for (const auto& scvf : scvfs(fvGeometry))
        {
            // skip boundary faces
            if (scvf.boundary())
                continue;

            using ScvfType = std::decay_t<decltype(scvf)>;
            using ScvfGeometry = typename ScvfType::Traits::Geometry;
            using GlobalPosition = typename ScvfType::Traits::GlobalPosition;
            using FluxVars = Dumux::GetPropType<TypeTag, Dumux::Properties::FluxVariables>;

            FluxVars fluxVars;
            fluxVars.init(problem, element, fvGeometry, elemVolVars, scvf, elemFluxVarsCache);

            auto up = [] (const auto& vv) { return 1.0; };
            const auto& insideVolVars = elemVolVars[fvGeometry.scv(scvf.insideScvIdx())];
            const auto qh = fluxVars.advectiveFlux(/*phaseIdx*/0, up)/(scvf.area()*insideVolVars.extrusionFactor());

            if (scvf.isOnFracture())
            {
                // factor 0.5 because we compute this from both sides of the fracture
                const auto q = problem.exactFlux_crossSectionAverage(scvf.ipGlobal(), scvf.unitOuterNormal());
                fractureL2Data.qNorm += 0.5*q*q;
                fractureL2Data.qErrorNorm += 0.5*(q - qh)*(q - qh);
            }

            else
            {
                const auto scvfGeometry = scvf.geometry();
                const auto& rule = Dune::QuadratureRules<Scalar, GridView::dimension-1>::rule(scvfGeometry.type(), fluxOrder);
                for (auto&& qp : rule)
                {
                    // exact flux at integration point
                    const auto ip = scvfGeometry.global(qp.position());
                    const auto q = problem.exactFlux(ip, scvf.unitOuterNormal(), false);
                    matrixL2Data.qNorm += q*q*qp.weight()*scvfGeometry.integrationElement(qp.position());
                    matrixL2Data.qErrorNorm += (q - qh)*(q - qh)*qp.weight()*scvfGeometry.integrationElement(qp.position());
                }
            }

            // update integration area
            if (scvf.isOnFracture())
                fractureFaceIntegrationArea += 0.5*scvf.area();
            else
                matrixFaceIntegrationArea += scvf.area();

            // error in solution within the fracture
            if (scvf.isOnFracture())
            {
                // integrate error on fracture facet
                const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
                const auto& outsideScv = fvGeometry.scv(scvf.outsideScvIdx());
                Dune::ReservedVector<GlobalPosition, 2> corners({insideScv.dofPosition(), outsideScv.dofPosition()});
                std::vector<Dune::FieldVector<Scalar, 1>> cornerValues({ elemSol[insideScv.localDofIndex()], elemSol[outsideScv.localDofIndex()] });

                static constexpr int dim = GridView::dimension;
                ScvfGeometry fractureSegment(Dune::GeometryTypes::line, corners);
                Dune::MultiLinearGeometry<Scalar, GridView::dimension-1, 1> interpolation(Dune::GeometryTypes::line, cornerValues);

                fractureL2Data.dx = min(length(fractureSegment), fractureL2Data.dx);
                fractureIntegrationArea += fractureSegment.volume();

                const auto& rule = Dune::QuadratureRules<Scalar, dim-1>::rule(fractureSegment.type(), uOrder);
                for (auto&& qp : rule)
                {
                    // integration point in global coordinates
                    const auto ip = fractureSegment.global(qp.position());

                    // exact and discrete solution at integration point
                    const auto u = problem.exact_crossSectionAverage(ip);
                    const auto uh = interpolation.global(qp.position())[0];

                    // use 1/2 because we do the same from the other side of the fracture
                    fractureL2Data.uNorm += 0.5*u*u*qp.weight()*fractureSegment.integrationElement(qp.position());
                    fractureL2Data.uErrorNorm += 0.5*(uh - u)*(uh - u)*qp.weight()*fractureSegment.integrationElement(qp.position());
                }
            }
        }
    }

    return result;
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::OnePBoxDfm;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // we reuse the facet coupling grid manager to create the grid
    // from a mesh file with the fractures being incorporated as
    // lower-dimensional elements.
    using Grid = GetPropType<TypeTag, Properties::Grid>;
    using FractureGrid = Dune::FoamGrid<1, 2>;
    using GridManager = FacetCouplingGridManager<Grid, FractureGrid>;
    GridManager gridManager;
    gridManager.init();

    // use the grid adapter from the facet coupling framework to
    // identify the grid facets that coincide with a fracture.
    // For instantiation we extract the info on the embeddings from
    // the grid manager (info is read from the grid file)
    using MatrixFractureGridAdapter = CodimOneGridAdapter<typename GridManager::Embeddings>;
    MatrixFractureGridAdapter fractureGridAdapter(gridManager.getEmbeddings());

    // matrix grid view is the first one (index 0) inside the manager
    const auto& leafGridView = gridManager.template grid<0>().leafGridView();

    // create the finite volume grid geometry
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    auto fvGridGeometry = std::make_shared<FVGridGeometry>(leafGridView);
    fvGridGeometry->update(fractureGridAdapter);
    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(fvGridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x(fvGridGeometry->numDofs());
    problem->applyInitialSolution(x);

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, fvGridGeometry);
    gridVariables->init(x);

    // intialize the vtk output module
    using VtkOutputModule = BoxDfmVtkOutputModule<GridVariables, SolutionVector, FractureGrid>;
    using VtkOutputFields = GetPropType<TypeTag, Properties::VtkOutputFields>;
    using FractureGrid = Dune::FoamGrid<1, 2>;
    VtkOutputModule vtkWriter(*gridVariables, x, problem->name(), fractureGridAdapter, "", Dune::VTK::nonconforming);
    std::vector< GetPropType<TypeTag, Properties::Scalar> > exact;

    const bool writeVTK = getParam<bool>("Output.EnableVTK");
    if (writeVTK)
    {
        VtkOutputFields::initOutputModule(vtkWriter); //!< Add model specific output fields

        exact.resize(fvGridGeometry->numDofs());
        for (const auto& v : vertices(fvGridGeometry->gridView()))
            exact[fvGridGeometry->vertexMapper().index(v)] = problem->exact(v.geometry().center());

        vtkWriter.addField(exact, "pressure_exact");
        vtkWriter.write(0.0);
    }

    // the assembler with time loop for instationary problem
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, fvGridGeometry, gridVariables);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    // solve the non-linear system with time step control
    nonLinearSolver.solve(x);

    // write out matrix
    const auto writeMatrix = getParam<bool>("Output.WriteMatrix", false);
    if (writeMatrix)
    {
        std::ofstream mat;
        mat.open("matrix.mtx", std::ios::out);
        writeMatrixMarket(assembler->jacobian(), mat);
    }

    // update variables
    gridVariables->update(x);

    // write vtk output
    if (writeVTK)
        vtkWriter.write(1.0);

    // norm of the errors in the two domains
    const auto norm = computeL2Norm<TypeTag>(fvGridGeometry->gridView(), *problem, *gridVariables, x);
    const auto& m = norm[0];
    const auto& f = norm[1];

    // output to terminal
    std::cout << "Matrix - dx/uNorm/qNorm/transferNorm: " << m.dx << ", " << m.uErrorNorm << ", " << m.qErrorNorm << ", " << m.qTransferErrorNorm << std::endl;
    std::cout << "Fracture - dx/uNorm/qNorm/transferNorm: " << f.dx << ", " << f.uErrorNorm << ", " << f.qErrorNorm << ", " << f.qTransferErrorNorm <<  std::endl;

    // append results to output file
    std::ofstream file(getParam<std::string>("Problem.OutputFileName"), std::ios::app);
    file << m.dx << ","                    // idx 0
         << m.uNorm << ","                 // idx 1
         << m.uErrorNorm << ","            // idx 2
         << m.qNorm << ","                 // idx 3
         << m.qErrorNorm << ","            // idx 4
         << 1e12 << ", "                   // idx 5 (not computable for boxdfm, will be 0.0)
         << 1e12 << ", "                   // idx 6 (not computable for boxdfm, will be 0.0)
         << f.dx << ","                    // idx 7
         << f.uNorm << ","                 // idx 8
         << f.uErrorNorm << ","            // idx 9
         << f.qNorm << ","                 // idx 10
         << f.qErrorNorm << ","            // idx 11
         << 1e12 << ", "                   // idx 12 (not computable for boxdfm, will be 0.0)
         << 1e12 << "\n";                  // idx 13 (not computable for boxdfm, will be 0.0)
    file.close();

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
