// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup OnePTests
 * \brief The spatial params for the incompressible 1p test
 */
#ifndef DUMUX_INCOMPRESSIBLE_1PBOXDFM_TEST_SPATIAL_PARAMS_HH
#define DUMUX_INCOMPRESSIBLE_1PBOXDFM_TEST_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/fv.hh>

namespace Dumux {

/*!
 * \brief The spatial params for the incompressible 1p test
 */
template<class FVGridGeometry, class Scalar>
class OnePTestSpatialParams : public FVSpatialParams< FVGridGeometry, Scalar, OnePTestSpatialParams<FVGridGeometry, Scalar> >
{
    using ThisType = OnePTestSpatialParams<FVGridGeometry, Scalar>;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, ThisType>;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;

    static constexpr int dimWorld = GridView::dimensionworld;

public:
    using PermeabilityType = Scalar;

    OnePTestSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry) : ParentType(fvGridGeometry)
    {
        matrixPermeability_ = getParamFromGroup<Scalar>("Bulk", "SpatialParams.Permeability");
        fracturePermeability_ = getParamFromGroup<Scalar>("LowDim", "SpatialParams.Permeability");
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     *        In this test, we use element-wise distributed permeabilities.
     *
     * \param element The current element
     * \param scv The sub-control volume inside the element.
     * \param elemSol The solution at the dofs connected to the element.
     * \return permeability
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        if (scv.isOnFracture())
            return fracturePermeability_;
        else
            return matrixPermeability_;
    }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param globalPos The global position
     */
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    { return 1.0; }

    // return matrix&fracture permeabilities
    Scalar matrixPermeability() const { return matrixPermeability_; }
    Scalar fracturePermeability() const { return fracturePermeability_; }

private:
    Scalar matrixPermeability_;
    Scalar fracturePermeability_;
};

} // end namespace Dumux

#endif
