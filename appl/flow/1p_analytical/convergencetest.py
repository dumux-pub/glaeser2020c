#!/usr/bin/env python

from math import *
import subprocess
import os
import sys
import matplotlib.pyplot as plt

sys.path.append("./utility")
from runrefinementsequence import runRefinementSequence
from parseresultsfile import parseResultsFile
from plotrates import *

sys.path.append("../../utility")
from getparam import getParam
from setparam import setParam


##############################
# User definitions
##############################
# define the permeabilities for which to run this
k = [1e-4, 1e4]

# define sequence of discretization lengths
dx = [0.1, 0.05, 0.025, 0.0125, 0.00759, 0.00375, 0.001875]

##############################
# schemes to be considered ###
##############################
schemeNames = ["TPFA", "BOX", "BOX-CONT"]
exeNames = ["facet/test_facet_convergence_tpfa",
            "facet/test_facet_convergence_box",
            "boxdfm/test_convergence_boxdfm"]

#############################
# color and marker cycles ###
#############################
prop_cycle = list(plt.rcParams['axes.prop_cycle'])
markers = ['o', '^', 'x', '.', ',', 's']


# function to run refinement sequence for an executable
def plotRefinementSequence(schemeIdx, k, useExactGeometry, plotQTransfer, plotSource):

    # run simulations using the given dx's
    exeName = exeNames[schemeIdx]
    inputFileName = "params.input"
    logFileName = exeNames[schemeIdx] + ".log"
    runRefinementSequence(exeName, inputFileName, logFileName,
                          dx, useExactGeometry, k)

    # parse results into array of dictionaries
    results = parseResultsFile(logFileName)

    # plot results with acronym as legend
    color = prop_cycle[schemeIdx]['color']
    schemeAcronym = schemeNames[schemeIdx]
    a = getParam(inputFileName, "LowDim", "Problem.ExtrusionFactor")
    plotResults(results, schemeAcronym, a, k,
                color, markers[schemeIdx], plotQTransfer, plotSource)

    if plotSource:
        plotSourceDistribution(inputFileName, exeName, schemeAcronym, dx, color)



############################
# Main body of this script #
############################
if len(sys.argv) != 2:
    sys.stderr.write("Please provide True/False if you want to compute on the exact geometry or not\n")
    sys.exit(1)

# parse command line argument
if sys.argv[1] == "1" or sys.argv[1] == "true" or sys.argv[1] == "True":
    useExactGeometry = True
elif sys.argv[1] == "0" or sys.argv[1] == "false" or sys.argv[1] == "False":
    useExactGeometry = False
else:
    sys.stderr.write("Invalid argument specifying usage of exact/inexact geometry")
    sys.exit(1)


# for each permeability, compute rates for all schemes
plotQTransfer = True
for permIndex in range(0, len(k)):
    for schemeIdx in range(0, len(exeNames)):
        schemePlotTransfer = True
        schemePlotSource = True
        if schemeNames[schemeIdx] == "BOX-CONT":
            schemePlotTransfer = False
            schemePlotSource = False

        plotRefinementSequence(schemeIdx,
                               k[permIndex],
                               useExactGeometry,
                               schemePlotTransfer,
                               schemePlotSource)
    saveAndClearPlots("k_" + str(k[permIndex]), plotQTransfer, True)
