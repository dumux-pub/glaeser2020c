#!/usr/bin/env python

import subprocess
import os
import sys

sys.path.append("../../../utility")
from setparam import setParam

# run a sequence of simulations
def runRefinementSequence(exeName,
                          iniFileName,
                          outputFileName,
                          dxArray,
                          useExactGeom,
                          lowDimK):

    if len(dxArray) == 0:
        sys.stderr("Provided dx array has zero length")
        sys.exit(1)

    # maybe remove previous output file
    if os.path.isfile(outputFileName):
        subprocess.call(['rm', outputFileName])
        print("Found and removed old log file ( " + outputFileName + " )!")

    # open the right geo file
    if useExactGeom:
        geoFileName = "./grids/unitfracture_quads_exact.geo"
    else:
        geoFileName = "./../../grids/unitfracture_quads.geo"

    # run simulation for each discretization length
    for dx in dxArray:
        # file to store the source term plots in
        sourceTermCsvFile = exeName + "_source_" + str(dx) + ".csv"
        if os.path.isfile(sourceTermCsvFile):
            subprocess.call(['rm', sourceTermCsvFile])
            print("Found and removed old source term csv file ( " + sourceTermCsvFile + " )!")

        geoFile = open(geoFileName, 'r')
        lines = geoFile.readlines()
        firstLine = lines[0]
        firstLine = firstLine.split(" ")

        if firstLine[0] != "dx":
            sys.stderr.write("Geo file is expected to have - dx = ... - in the first line!\n")
            sys.exit(1)

        # write a temporary geo-file using dx from array
        tmpGeoFile = open("grids/tmp.geo", 'w')
        lines[0] = "dx = " + str(dx) + ";\n"
        for line in lines:
            tmpGeoFile.write(line)

        geoFile.close()
        tmpGeoFile.close()

        # create mesh
        subprocess.call(['gmsh', '-2', 'grids/tmp.geo'])
        if not os.path.isfile("grids/tmp.msh"):
            sys.stderr.write("Could not create mesh for dx = " + str(dx))
            sys.exit(1)

        # run simulation
        subprocess.call(['./' + exeName, iniFileName,
                         '-Grid.File', 'grids/tmp.msh',
                         '-Problem.Name', exeName,
                         '-Bulk.Problem.UseExactGeometry', str(useExactGeom),
                         '-Bulk.Problem.Name', exeName + "_bulk",
                         '-LowDim.Problem.Name', exeName + "_lowdim",
                         '-LowDim.SpatialParams.Permeability', str(lowDimK),
                         '-Problem.OutputFileName', outputFileName,
                         '-Problem.SourceCsvFile', sourceTermCsvFile])

        # remove geo and msh file
        subprocess.call(['rm', 'grids/tmp.geo'])
        subprocess.call(['rm', 'grids/tmp.msh'])
