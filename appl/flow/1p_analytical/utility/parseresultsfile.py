#!/usr/bin/env python

from math import *
import sys

# parses the results of refinement sequence
def parseResultsFile(logFileName):

    results = []
    logFile = open(logFileName, "r+")

    dx_matrix = 1e12
    for line in logFile:
        line = line.strip("\n")
        line = line.split(",")

        # maybe update reference norms
        if float(line[0]) < dx_matrix:
            dx_matrix = float(line[0])
            uNorm_matrix_fine = float(line[1])
            qNorm_matrix_fine = float(line[3])
            qtransferNorm_matrix_fine = float(line[5])
            uNorm_fracture_fine = float(line[8])
            qNorm_fracture_fine = float(line[10])
            sourceTerm_fracture_fine = float(line[12])

        results.append( dict([ ("dx_matrix", float(line[0])),
                               ("uError_normalized_matrix", float(line[2])),
                               ("qError_normalized_matrix", float(line[4])),
                               ("qTransfer_normalized_matrix", float(line[6])),
                               ("dx_fracture", float(line[7])),
                               ("uError_normalized_fracture", float(line[9])),
                               ("qError_normalized_fracture", float(line[11])),
                               ("sourceNorm_normalized_fracture", float(line[13])) ]) )
    for r in results:
        r["uError_normalized_matrix"] = sqrt(r["uError_normalized_matrix"]/uNorm_matrix_fine)
        r["qError_normalized_matrix"] = sqrt(r["qError_normalized_matrix"]/qNorm_matrix_fine)
        r["uError_normalized_fracture"] = sqrt(r["uError_normalized_fracture"]/uNorm_fracture_fine)
        r["qError_normalized_fracture"] = sqrt(r["qError_normalized_fracture"]/qNorm_fracture_fine)
        # if qtransferNorm_matrix_fine != 0.0:
        r["qTransfer_normalized_matrix"] = sqrt(r["qTransfer_normalized_matrix"]/qtransferNorm_matrix_fine)
        # else:
        #     r["qTransfer_normalized_matrix"] = sqrt(r["qTransfer_normalized_matrix"])
        # if sourceTerm_fracture_fine != 0.0:
        r["sourceNorm_normalized_fracture"] = sqrt(r["sourceNorm_normalized_fracture"]/sourceTerm_fracture_fine)
        # else:
        #     r["sourceNorm_normalized_fracture"] = sqrt(r["sourceNorm_normalized_fracture"])

    return results;
