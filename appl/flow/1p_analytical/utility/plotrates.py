#!/usr/bin/env python

from math import *
import subprocess
import os
import sys
import math
import numpy as np
import matplotlib.pyplot as plt

sys.path.append("../../../utility")
from getparam import getParam

# use plot style
sys.path.append(os.path.join(
    os.path.dirname(os.path.abspath(__file__)), 
    "../../../../../appl/common/"
))
import plotstyles

# function to add results to plot
def plotResults(results, schemeName, aperture, k, color, marker, plotQTransfer = True, plotSource = True):

    uNorm_matrix_rates = []
    qNorm_matrix_rates = []
    uNorm_fracture_rates = []
    qNorm_fracture_rates = []
    qTransfer_matrix_rates = []
    sourceTerm_fracture_rates = []

    # we use eps = aperture/dx_matrix as x-axis here
    eps = []
    for r in results:
        eps.append(aperture/r["dx_matrix"])

    for i in range(len(results)-1):
        deltaLogU_matrix = log(results[i]["uError_normalized_matrix"]) - log(results[i+1]["uError_normalized_matrix"])
        deltaLogQ_matrix = log(results[i]["qError_normalized_matrix"]) - log(results[i+1]["qError_normalized_matrix"])
        deltaLogU_fracture = log(results[i]["uError_normalized_fracture"]) - log(results[i+1]["uError_normalized_fracture"])
        deltaLogQ_fracture = log(results[i]["qError_normalized_fracture"]) - log(results[i+1]["qError_normalized_fracture"])
        deltaLogQTransfer_matrix = log(results[i]["qTransfer_normalized_matrix"]) - log(results[i+1]["qTransfer_normalized_matrix"])
        deltaLogSource_fracture = log(results[i]["sourceNorm_normalized_fracture"]) - log(results[i+1]["sourceNorm_normalized_fracture"])

        deltaLogEps = log(eps[i])-log(eps[i+1])
        uNorm_matrix_rates.append(deltaLogU_matrix/deltaLogEps)
        qNorm_matrix_rates.append(deltaLogQ_matrix/deltaLogEps)
        uNorm_fracture_rates.append(deltaLogU_fracture/deltaLogEps)
        qNorm_fracture_rates.append(deltaLogQ_fracture/deltaLogEps)
        qTransfer_matrix_rates.append(deltaLogQTransfer_matrix/deltaLogEps)
        sourceTerm_fracture_rates.append(deltaLogSource_fracture/deltaLogEps)

    print("\n")
    print("Computed the following convergence rates for the scheme \"" + schemeName + "\":\n")
    print("u - matrix \t|\t q - matrix \t|\t u - fracture \t|\t q - fracture \t|\t qTransfer \t|\t fracture source term")
    for i in range(len(uNorm_matrix_rates)):
        print( str(uNorm_matrix_rates[i]) + "\t|\t"
               + str(qNorm_matrix_rates[i]) + "\t|\t"
               + str(uNorm_fracture_rates[i]) + "\t|\t"
               + str(qNorm_fracture_rates[i]) + "\t|\t"
               + str(qTransfer_matrix_rates[i]) + "\t|\t"
               + str(sourceTerm_fracture_rates[i]) )

    # plot stuff
    print("\nplotting...")

    # parse results from dictionary in plottable array
    uNorm_matrix_rel = []
    qNorm_matrix_rel = []
    uNorm_fracture_rel = []
    qNorm_fracture_rel = []
    qTransfer_matrix_rel = []
    source_fracture_rel = []
    for r in results:
        uNorm_matrix_rel.append(r["uError_normalized_matrix"])
        qNorm_matrix_rel.append(r["qError_normalized_matrix"])
        uNorm_fracture_rel.append(r["uError_normalized_fracture"])
        qNorm_fracture_rel.append(r["qError_normalized_fracture"])
        qTransfer_matrix_rel.append(r["qTransfer_normalized_matrix"])
        source_fracture_rel.append(r["sourceNorm_normalized_fracture"])

    # quadratically converging reference plots
    pref = []
    qref = []
    qTransferRef = []
    for i in range(0, len(eps)):
        pref.append(pow(10, log10(uNorm_matrix_rel[0]) - 2*(log10(eps[i]) - log10(eps[0]))))
        qref.append(pow(10, log10(qNorm_matrix_rel[0]) - 2*(log10(eps[i]) - log10(eps[0]))))
        qTransferRef.append(pow(10, log10(qTransfer_matrix_rel[0]) - 2*(log10(eps[i]) - log10(eps[0]))))

    # plot matrix results
    plt.figure(1)
    plt.loglog(eps, uNorm_matrix_rel, label=schemeName, c=color, marker=marker)
    plt.loglog(eps, pref, c=color, linestyle='--', label=r"$\mathcal{O}\,(2)$")
    plt.xlabel(r'$a/h$', fontsize=22)
    plt.ylabel(r'$\varepsilon_{p_2}$', fontsize=22)
    plt.legend(ncol=3)

    plt.figure(2)
    plt.loglog(eps, qNorm_matrix_rel, label=schemeName, c=color, marker=marker)
    plt.loglog(eps, qref, c=color, linestyle='--', label=r"$\mathcal{O}\,(2)$")
    plt.xlabel(r'$a/h$', fontsize=22)
    plt.ylabel(r'$\varepsilon_{F_2}$', fontsize=22)
    plt.legend(ncol=3)

    # quadratically converging reference plots
    p_fref = []
    q_fref = []
    sourceRef = []
    for i in range(0, len(eps)):
        p_fref.append(pow(10, log10(uNorm_fracture_rel[0]) - 2*(log10(eps[i]) - log10(eps[0]))))
        q_fref.append(pow(10, log10(qNorm_fracture_rel[0]) - 2*(log10(eps[i]) - log10(eps[0]))))
        sourceRef.append(pow(10, log10(source_fracture_rel[0]) - 2*(log10(eps[i]) - log10(eps[0]))))


    # plot fracture results
    plt.figure(3)
    plt.loglog(eps, uNorm_fracture_rel, label=schemeName, c=color, marker=marker)
    plt.loglog(eps, p_fref, c=color, linestyle='--', label=r"$\mathcal{O}\,(2)$")
    plt.xlabel(r'$a/h$', fontsize=22)
    plt.ylabel(r'$\varepsilon_{p_1}$', fontsize=22)
    plt.legend(ncol=3)

    plt.figure(4)
    plt.loglog(eps, qNorm_fracture_rel, label=schemeName, c=color, marker=marker)
    plt.loglog(eps, q_fref, c=color, linestyle='--', label=r"$\mathcal{O}\,(2)$")
    plt.xlabel(r'$a/h$', fontsize=22)
    plt.ylabel(r'$\varepsilon_{F_1}$', fontsize=22)
    plt.legend(ncol=3)

    if plotSource:
        plt.figure(7)
        plt.loglog(eps, source_fracture_rel, label=schemeName, c=color, marker=marker)
        plt.loglog(eps, sourceRef, c=color, linestyle='--', label=r"$\mathcal{O}\,(2)$")
        plt.xlabel(r'$a/h$', fontsize=22)
        plt.ylabel(r'$\varepsilon_{ \llbracket F_2^\gamma \rrbracket }$', fontsize=22)
        plt.legend(ncol=2)

    # transfer flux
    if plotQTransfer:
        plt.figure(8)
        plt.loglog(eps, qTransfer_matrix_rel, label=schemeName, c=color, marker=marker)
        plt.loglog(eps, qTransferRef, c=color, linestyle='--', label=r"$\mathcal{O}\,(2)$")
        plt.xlabel(r'$a/h$', fontsize=22)
        plt.ylabel(r'$\varepsilon_{F_2^\gamma}$', fontsize=22)
        plt.legend(ncol=2)

# plots the source term along the fracture line
def plotSourceDistribution(inputFileName, exeName, schemeName, dx, color):
    # todo plot all curves of dx for differnt schemes
    csvFileName = exeName + "_source_" + str(dx[len(dx)-1]) + ".csv"
    data = np.loadtxt(csvFileName, delimiter=',')

    order = np.argsort(data[:, 0])
    xValues = np.array(data[:, 0])[order]
    source_discrete = np.array(data[:, 1])[order]
    source_exact = np.array(data[:, 2])[order]

    plt.figure(9)
    plt.plot(xValues, source_exact, c='k', label='exact')
    plt.plot(xValues, source_discrete, label=schemeName, c=color)
    plt.xlabel(r'$x \,\, [\si{\meter}]$', fontsize=22)
    plt.ylabel(r'$\llbracket \rho \left( \mathbf{q}_2 \cdot \mathbf{n} \right) \rrbracket \,\, [\si{\kilogram/\meter/\second}]$', fontsize=22)
    # plt.ylim([5, 15])
    plt.legend()

# function to save the created plots
def saveAndClearPlots(name, plotQTransfer = True, plotSource = True):

    if not os.path.exists("plots"):
        subprocess.call(['mkdir', 'plots'])

    plt.figure(1)
    plt.savefig("./plots/" + name + "_u_matrix.pdf", bbox_inches='tight')
    plt.clf()
    plt.figure(2)
    plt.savefig("./plots/" + name + "_q_matrix.pdf", bbox_inches='tight')
    plt.clf()
    plt.figure(3)
    plt.savefig("./plots/" + name + "_u_fracture.pdf", bbox_inches='tight')
    plt.clf()
    plt.figure(4)
    plt.savefig("./plots/" + name + "_q_fracture.pdf", bbox_inches='tight')
    plt.clf()

    if plotSource:
        plt.figure(7)
        plt.savefig("./plots/" + name + "_source.pdf", bbox_inches='tight')
        plt.clf()

        plt.figure(9)
        plt.savefig("./plots/" + name + "_source_distribution.pdf", bbox_inches='tight')
        plt.clf()

    if plotQTransfer:
        plt.figure(8)
        plt.savefig("./plots/" + name + "_q_transfer.pdf", bbox_inches='tight')
        plt.clf()
