import matplotlib.pyplot as plt
import numpy as np

massTpfa = np.loadtxt('no_is/oilspill_tpfa.csv', delimiter=',')
# massTpfa_is = np.loadtxt('soil_withis/waterflooding_tpfa.csv', delimiter=',')

plt.figure(1)
plt.plot(massTpfa[:, 0], massTpfa[:, 4], label='middle bulk block')
plt.plot(massTpfa[:, 0], massTpfa[:, 8], label='facet')
plt.legend()

for i in range(1, len(massTpfa)):
    print("dt = ", massTpfa[i, 0] - massTpfa[i-1, 0])

plt.show()
