// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The spatial parameters class for the two-phase facet coupling test.
 */
#ifndef DUMUX_TWOP_GRAVITY_SPATIALPARAMS_HH
#define DUMUX_TWOP_GRAVITY_SPATIALPARAMS_HH

#include <dumux/discretization/elementsolution.hh>
#include <dumux/material/spatialparams/fv.hh>

#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux {

/*!
 * \brief The spatial parameters for the two-phase facet coupling test.
 */
template<class FVGridGeometry, class Scalar>
class TwoPSpatialParams : public FVSpatialParams<FVGridGeometry, Scalar,
                                                 TwoPSpatialParams<FVGridGeometry, Scalar>>
{
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;

    using ThisType = TwoPSpatialParams<FVGridGeometry, Scalar>;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, ThisType>;

    // use a regularized van-genuchten material law
    using EffectiveLaw = RegularizedVanGenuchten<Scalar>;

public:
    //! Export the type used for permeability
    using PermeabilityType = Scalar;

    //! export the material law and parameters used
    using MaterialLaw = EffToAbsLaw< EffectiveLaw >;
    using MaterialLawParams = typename MaterialLaw::Params;

    //! Constructor
    TwoPSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                      const std::string& paramGroup)
    : ParentType(fvGridGeometry)
    , porosity_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Porosity"))
    , permeability_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability"))
    {
        // set the material law parameters
        materialLawParams_.setSnr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Snr"));
        materialLawParams_.setSwr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Swr"));
        materialLawParams_.setVgAlpha(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.VGAlpha"));
        materialLawParams_.setVgn(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.VGN"));
    }

    //! Return the material law parameters
    const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition& globalPos) const
    { return materialParameters(); }

    //! Return the material law parameters
    const MaterialLawParams& materialParameters() const
    { return materialLawParams_; }

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    template<class ElementSolution>
    Scalar permeability(const Element& element,
                        const SubControlVolume& scv,
                        const ElementSolution& elemSol) const
    { return permeability_; }

    //! Returns the porosity for a sub-control volume.
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return porosity_; }

    //! Water is the wetting phase
    template< class FluidSystem >
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    {
        // we set water as the wetting phase here
        // which is phase0Idx in the our chosen fluid system
        return FluidSystem::phase0Idx;
    }

private:
    Scalar porosity_;
    Scalar permeability_;
    MaterialLawParams materialLawParams_;
};

} // end namespace Dumux

#endif // DUMUX_TEST_FACETCOUPLING_ELTWOP_TWOP_FACET_FLOW_SPATIALPARAMS_HH
