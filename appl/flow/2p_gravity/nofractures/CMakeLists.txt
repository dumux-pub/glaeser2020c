dune_add_test(NAME test_2p_gravity_nofracture_tpfa
              COMPILE_DEFINITIONS TYPETAG=TwoPBulkTpfa
              SOURCES main.cc
              CMAKE_GUARD "( dune-alugrid_FOUND )")

dune_add_test(NAME test_2p_gravity_nofracture_box
              COMPILE_DEFINITIONS TYPETAG=TwoPBulkBox
              SOURCES main.cc
              CMAKE_GUARD "( dune-foamgrid_FOUND )")

set(CMAKE_BUILD_TYPE Release)
