// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for a two-phase test.
 */
#ifndef DUMUX_TWOP_GRAVITY_PROBLEM_HH
#define DUMUX_TWOP_GRAVITY_PROBLEM_HH

#include <dune/alugrid/grid.hh>

#include <dumux/material/fluidsystems/2pimmiscible.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/trichloroethene.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/tabulatedcomponent.hh>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>
#include <dumux/discretization/method.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p/model.hh>

#include "../spatialparams.hh"

namespace Dumux {
// forward declarations
template<class TypeTag> class TwoPBulkProblem;

namespace Properties {
// create the type tag nodes
namespace TTag {
struct TwoPBulk { using InheritsFrom = std::tuple<TwoP>; };
struct TwoPBulkTpfa { using InheritsFrom = std::tuple<TwoPBulk, CCTpfaModel>; };
struct TwoPBulkMpfa { using InheritsFrom = std::tuple<TwoPBulk, CCMpfaModel>; };
struct TwoPBulkBox { using InheritsFrom = std::tuple<TwoPBulk, BoxModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TwoPBulk> { using type = Dune::ALUGrid<3, 3, Dune::simplex, Dune::nonconforming>; };
// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::TwoPBulk> { using type = TwoPBulkProblem<TypeTag>; };
// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TwoPBulk>
{
private:
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

public:
    using type = TwoPSpatialParams<FVGridGeometry, Scalar>;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPBulk>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using TCE = Components::Trichloroethene<Scalar>;
    using Water = Components::H2O<Scalar>;
    using TabulatedWater = Components::TabulatedComponent<Water>;
    using WaterPhase = FluidSystems::OnePLiquid<Scalar, TabulatedWater>;
    using NaplPhase = FluidSystems::OnePLiquid<Scalar, TCE>;

public:
    using type = Dumux::FluidSystems::TwoPImmiscible< Scalar, WaterPhase, NaplPhase>;
};

// permeability is constant
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::TwoPBulk>
{ static constexpr bool value = false; };

} // end namespace Properties

/*!
 * \brief The problem for the bulk domain in the two-phase facet coupling test.
 */
template<class TypeTag>
class TwoPBulkProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    static constexpr bool isBox = FVGridGeometry::discMethod == DiscretizationMethod::box;

public:
    //! The constructor
    TwoPBulkProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                    std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                    const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    {
        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");

        using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
        using Indices = typename ModelTraits::Indices;
        const GlobalPosition min({this->fvGridGeometry().bBoxMin()[0], this->fvGridGeometry().bBoxMin()[1], this->fvGridGeometry().bBoxMin()[2]});
        const GlobalPosition max({this->fvGridGeometry().bBoxMax()[0], this->fvGridGeometry().bBoxMax()[1], this->fvGridGeometry().bBoxMax()[2]});
        const auto minPressure = initialAtPos(min)[Indices::pressureIdx];
        const auto maxPressure = initialAtPos(max)[Indices::pressureIdx];

        // initialize the fluid system (tabulation)
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        FluidSystem::init(/*tMin*/0.8*temperature(), /*tMax*/1.2*temperature(), /*n*/50,
                          /*pMin*/minPressure*0.8, /*pMax*/maxPressure*10.0, /*n*/100);
    }

    //! The problem name.
    const std::string& name() const
    { return problemName_; }

    //! Specifies the kind of boundary condition on a given boundary position.
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();

        if (globalPos[2] > this->fvGridGeometry().bBoxMax()[2] - 1e-6)
            values.setAllDirichlet();

        // // use Neumann on the bottom boundary
        // if (globalPos[2] < this->fvGridGeometry().bBoxMin()[2] + 1e-3)
        //     values.setAllNeumann();

        return values;
    }

    //! evaluates the Neumann boundary condition for a given position
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    { return NumEqVector(0.0); }

    //! Evaluates the Dirichlet boundary conditions at a given position.
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
        using Indices = typename ModelTraits::Indices;

        static const Scalar waterSat = getParam<double>("Problem.InflowPatchWaterSaturation");
        auto values = initialAtPos(globalPos);

        // check if position is on inflow patch
        if (isOnInflowPatch(globalPos))
            values[Indices::saturationIdx] = 1.0 - waterSat;

        return values;
    }

    //! Evaluates the initial conditions.
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        static const Scalar initSat = getParam<double>("Problem.InitialMatrixOilSaturation");
        static const Scalar rhoW = 1000.0;

        const auto depth = this->fvGridGeometry().bBoxMax()[2] - globalPos[2];
        return PrimaryVariables({1.0e5 + rhoW*9.81*depth, initSat});
    }

    //! Returns the temperature in \f$\mathrm{[K]}\f$ in the domain.
    Scalar temperature() const
    { return 283.15; /*10°C*/ }

    //! Returns true if position is on inflow patch
    bool isOnInflowPatch(const GlobalPosition& globalPos) const
    {
        return globalPos[0] > 2.5 && globalPos[0] < 7.5
               && globalPos[1] > 2.5 && globalPos[1] < 7.5
               && globalPos[2] > this->fvGridGeometry().bBoxMax()[2] - 1e-6;
    }

private:
    std::string problemName_;
};

} // end namespace Dumux

#endif
