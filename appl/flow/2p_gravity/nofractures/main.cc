// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Test for a two-phase flow model.
 */
#include <config.h>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>
#include <dumux/common/timeloop.hh>

#include "problem.hh"

#include <dumux/assembly/diffmethod.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/evalgradients.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>

//! Function to compute the component masses contained in a sub-domain
template<class A, class P, class GV, class Sol>
std::array<double, 6> computeMassDistribution(const A& assembler,
                                              const P& problem,
                                              const GV& gridVariables,
                                              const Sol& x)
{
    std::array<double, 6> masses({0.0, 0.0, 0.0, 0.0, 0.0, 0.0});

    for (const auto& element : elements(problem.fvGridGeometry().gridView()))
    {
        auto fvGeometry = localView(problem.fvGridGeometry());
        auto elemVolVars = localView(gridVariables.curGridVolVars());

        fvGeometry.bind(element);
        elemVolVars.bindElement(element, fvGeometry, x);

        for (const auto& scv : scvs(fvGeometry))
        {
            const auto& vv = elemVolVars[scv];
            const auto mass_w = vv.saturation(0)*vv.density(0)*vv.porosity()*scv.volume()*vv.extrusionFactor();
            const auto mass_napl = vv.saturation(1)*vv.density(1)*vv.porosity()*scv.volume()*vv.extrusionFactor();

            if (scv.center()[2] < -6.0) { masses[0] += mass_w; masses[1] += mass_napl; }
            else if (scv.center()[2] < -3.0) { masses[2] += mass_w; masses[3] += mass_napl; }
            else { masses[4] += mass_w; masses[5] += mass_napl; }
        }
    }

    return masses;
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::TYPETAG;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // run stationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    auto fvGridGeometry = std::make_shared<FVGridGeometry>(leafGridView);
    fvGridGeometry->update();

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;

    auto spatialParams = std::make_shared<typename Problem::SpatialParams>(fvGridGeometry, "TwoPBulk");
    auto problem = std::make_shared<Problem>(fvGridGeometry, spatialParams, "TwoPBulk");

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x, xOld;
    problem->applyInitialSolution(x);
    xOld = x;

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVars = std::make_shared<GridVariables>(problem, fvGridGeometry);
    gridVars->init(x);

    // intialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;

    // use non-conforming output for the test with interface solver
    VtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVars, x, problem->name());
    using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
    vtkWriter.addVelocityOutput(std::make_shared<VelocityOutput>(*gridVars));
    IOFields::initOutputModule(vtkWriter); // Add model specific output fields
    vtkWriter.write(0.0);

    // instantiate time loop
    auto timeLoop = std::make_shared<CheckPointTimeLoop<double>>(getParam<double>("Restart.Time", 0),
                                                                 getParam<double>("TimeLoop.DtInitial"),
                                                                 getParam<double>("TimeLoop.TEnd"));
    timeLoop->setMaxTimeStepSize(getParam<double>("TimeLoop.MaxTimeStepSize"));

    // the assembler with time loop for instationary problem
    using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, fvGridGeometry, gridVars, timeLoop);

    // the linear solver
    using LinearSolver = AMGBackend<TypeTag>;
    auto linearSolver = std::make_shared<LinearSolver>(leafGridView, fvGridGeometry->dofMapper());

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver newtonSolver(assembler, linearSolver);

    // set previous solution for storage evaluations
    assembler->setPreviousSolution(xOld);

    // open file to write outfluxes in
    std::ofstream outputFile(getParam<std::string>("IO.MassOutputFile"), std::ios::out);

    // time loop
    timeLoop->start(); do
    {
        // linearize & solve
        newtonSolver.solve(x, *timeLoop);

        // make the new solution the old solution
        xOld = x;
        gridVars->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // write vtk output
        vtkWriter.write(timeLoop->time());

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by the Newton solver
        timeLoop->setTimeStepSize(newtonSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

        // compute mass distribution
        const auto matrixMasses = computeMassDistribution(*assembler, *problem, *gridVars, x);
        outputFile << timeLoop->time() << "," << matrixMasses[0] << "," << matrixMasses[1] << "," << matrixMasses[2]
                                       << "," << matrixMasses[3] << "," << matrixMasses[4] << "," << matrixMasses[5] << std::endl;
    } while (!timeLoop->finished());

    // output some Newton & time loopt statistics
    newtonSolver.report();
    timeLoop->finalize();

    // print parameter usage
    Parameters::print();

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/false);

    return 0;

}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
