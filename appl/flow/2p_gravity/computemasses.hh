// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Function to compute the masses contained in a sub-domain.
 */
#ifndef DUMUX_TWOP_GRAVITY_COMPUTEMASS_HH
#define DUMUX_TWOP_GRAVITY_COMPUTEMASS_HH

#include <dune/common/indices.hh>

namespace Dumux {

//! Function to compute the component masses contained in a sub-domain
template<std::size_t id, class CM, class A, class P, class GV, class Sol>
std::array<double, 2> computeMassInDomain(Dune::index_constant<id> domainId,
                                          CM& couplingManager,
                                          const A& assembler,
                                          const P& problem,
                                          const GV& gridVariables,
                                          const Sol& x)
{
    std::array<double, 2> masses({0.0, 0.0});

    for (const auto& element : elements(problem.fvGridGeometry().gridView()))
    {
        auto fvGeometry = localView(problem.fvGridGeometry());
        auto elemVolVars = localView(gridVariables.curGridVolVars());

        couplingManager.bindCouplingContext(domainId, element, assembler);
        fvGeometry.bind(element);
        elemVolVars.bindElement(element, fvGeometry, x);

        for (const auto& scv : scvs(fvGeometry))
        {
            const auto& vv = elemVolVars[scv];
            masses[0] += vv.saturation(0)*vv.density(0)*vv.porosity()*scv.volume()*vv.extrusionFactor();
            masses[1] += vv.saturation(1)*vv.density(1)*vv.porosity()*scv.volume()*vv.extrusionFactor();
        }
    }

    return masses;
}

//! Function to compute the component masses contained in a sub-domain
template<std::size_t id, class CM, class A, class P, class GV, class Sol>
std::array<double, 6> computeBulkMassDistribution(Dune::index_constant<id> domainId,
                                                  CM& couplingManager,
                                                  const A& assembler,
                                                  const P& problem,
                                                  const GV& gridVariables,
                                                  const Sol& x)
{
    std::array<double, 6> masses({0.0, 0.0, 0.0, 0.0, 0.0, 0.0});

    for (const auto& element : elements(problem.fvGridGeometry().gridView()))
    {
        auto fvGeometry = localView(problem.fvGridGeometry());
        auto elemVolVars = localView(gridVariables.curGridVolVars());

        couplingManager.bindCouplingContext(domainId, element, assembler);
        fvGeometry.bind(element);
        elemVolVars.bindElement(element, fvGeometry, x);

        for (const auto& scv : scvs(fvGeometry))
        {
            const auto& vv = elemVolVars[scv];
            const auto mass_w = vv.saturation(0)*vv.density(0)*vv.porosity()*scv.volume()*vv.extrusionFactor();
            const auto mass_air = vv.saturation(1)*vv.density(1)*vv.porosity()*scv.volume()*vv.extrusionFactor();

            if (scv.center()[2] < -6.0) { masses[0] += mass_w; masses[1] += mass_air; }
            else if (scv.center()[2] < -3.0) { masses[2] += mass_w; masses[3] += mass_air; }
            else { masses[4] += mass_w; masses[5] += mass_air; }

        }
    }

    return masses;
}

} // end namespace Dumux

#endif
