// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Test for a two-phase flow facet coupling model.
 */
#include <config.h>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>
#include <dumux/common/timeloop.hh>

#include "problem_bulk_twop.hh"
#include "problem_facet_twop.hh"
#include "problem_is_twop.hh"

#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/evalgradients.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/traits.hh>
#include <dumux/multidomain/fvgridgeometry.hh>
#include <dumux/multidomain/fvproblem.hh>
#include <dumux/multidomain/fvgridvariables.hh>

#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/codimonegridadapter.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>
#include <dumux/multidomain/io/vtkoutputmodule.hh>

#include "../../common/computevelocities.hh"
#include "computemasses.hh"

// type tags of this test
using BulkFlowTypeTag = Dumux::Properties::TTag::BULKTYPETAG;
using FacetFlowTypeTag = Dumux::Properties::TTag::FACETTYPETAG;
using IntersectionFlowTypeTag = Dumux::Properties::TTag::INTERSECTIONTYPETAG;

// obtain/define some types to be used below in the property definitions and in main
class TestTraits
{
    using BulkFlowFVG = Dumux::GetPropType<BulkFlowTypeTag, Dumux::Properties::FVGridGeometry>;
    using FacetFlowFVG = Dumux::GetPropType<FacetFlowTypeTag, Dumux::Properties::FVGridGeometry>;
    using IntersectionFlowFVG = Dumux::GetPropType<IntersectionFlowTypeTag, Dumux::Properties::FVGridGeometry>;
public:
    using MDTraits = Dumux::MultiDomainTraits< BulkFlowTypeTag,
                                               FacetFlowTypeTag,
                                               IntersectionFlowTypeTag >;
    using CouplingMapper = Dumux::FacetCouplingThreeDomainMapper<BulkFlowFVG, FacetFlowFVG, IntersectionFlowFVG>;
    using CouplingManager = Dumux::FacetCouplingThreeDomainManager<MDTraits, CouplingMapper>;
};

// set the coupling manager property in the sub-problems
namespace Dumux {
namespace Properties {

template<class TypeTag>
struct CouplingManager<TypeTag, BulkFlowTypeTag> { using type = typename TestTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, FacetFlowTypeTag> { using type = typename TestTraits::CouplingManager; };
template<class TypeTag>
struct CouplingManager<TypeTag, IntersectionFlowTypeTag> { using type = typename TestTraits::CouplingManager; };

} // end namespace Properties
} // end namespace Dumux

// updates the finite volume grid geometry. This is necessary as the finite volume
// grid geometry for the box scheme with facet coupling requires additional data for
// the update. The reason is that we have to create additional faces on interior
// boundaries, which wouldn't be created in the standard scheme.
template< class FVGridGeometry,
          class GridManager,
          class FractureGridView,
          std::enable_if_t<FVGridGeometry::discMethod == Dumux::DiscretizationMethod::box, int> = 0 >
void updateFVGridGeometry(FVGridGeometry& fvGridGeometry,
                          const GridManager& gridManager,
                          const FractureGridView& lowDimGridView)
{
    static constexpr int higherGridId = int(FVGridGeometry::GridView::dimension) == 3 ? 0 : 1;
    using BulkFacetGridAdapter = Dumux::CodimOneGridAdapter<typename GridManager::Embeddings, higherGridId, higherGridId+1>;
    BulkFacetGridAdapter facetGridAdapter(gridManager.getEmbeddings());
    fvGridGeometry.update(lowDimGridView, facetGridAdapter);
}

// specialization for cell-centered schemes
template< class FVGridGeometry,
          class GridManager,
          class FractureGridView,
          std::enable_if_t<FVGridGeometry::discMethod != Dumux::DiscretizationMethod::box, int> = 0 >
void updateFVGridGeometry(FVGridGeometry& fvGridGeometry,
                          const GridManager& gridManager,
                          const FractureGridView& fractureGidView)
{
    fvGridGeometry.update();
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // initialize parameter tree
    Parameters::init(argc, argv);

    // the multidomain traits and some indices
    using Traits = typename TestTraits::MDTraits;
    constexpr auto bulkFlowId = Traits::template SubDomain<0>::Index{};
    constexpr auto facetFlowId = Traits::template SubDomain<1>::Index{};
    constexpr auto isFlowId = Traits::template SubDomain<2>::Index{};

    // try to create a grid (from the given grid file or the input file)
    using GridManager = FacetCouplingGridManager<Traits::template SubDomain<bulkFlowId>::Grid,
                                                 Traits::template SubDomain<facetFlowId>::Grid,
                                                 Traits::template SubDomain<isFlowId>::Grid>;

    GridManager gridManager;
    gridManager.init();
    gridManager.loadBalance();

    ////////////////////////////////////////////////////////////
    // run stationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid views
    const auto& bulkGridView = gridManager.template grid<0>().leafGridView();
    const auto& facetGridView = gridManager.template grid<1>().leafGridView();
    const auto& isGridView = gridManager.template grid<2>().leafGridView();

    // create the finite volume grid geometries
    using MDGridGeometry = MultiDomainFVGridGeometry<Traits>;
    MDGridGeometry fvGridGeometry(std::make_tuple(bulkGridView, facetGridView, isGridView));
    updateFVGridGeometry(fvGridGeometry[bulkFlowId], gridManager, facetGridView);
    updateFVGridGeometry(fvGridGeometry[facetFlowId], gridManager, isGridView);
    fvGridGeometry[isFlowId].update();

    // the coupling manager
    using CouplingManager = typename TestTraits::CouplingManager;
    auto couplingManager = std::make_shared<CouplingManager>();

    // the problems (boundary conditions)
    MultiDomainFVProblem<Traits> problem;

    using BulkFlowProblem = MultiDomainFVProblem<Traits>::template Type<bulkFlowId>;
    using FacetFlowProblem = MultiDomainFVProblem<Traits>::template Type<facetFlowId>;
    using IntersectionFlowProblem = MultiDomainFVProblem<Traits>::template Type<isFlowId>;

    auto bulkFlowSpatialParams = std::make_shared<typename BulkFlowProblem::SpatialParams>(fvGridGeometry.get(bulkFlowId), "TwoPBulk");
    auto facetFlowSpatialParams = std::make_shared<typename FacetFlowProblem::SpatialParams>(fvGridGeometry.get(facetFlowId), "TwoPFacet");
    auto isFlowSpatialParams = std::make_shared<typename IntersectionFlowProblem::SpatialParams>(fvGridGeometry.get(isFlowId), "TwoPIntersection");

    problem.set(std::make_shared<BulkFlowProblem>(fvGridGeometry.get(bulkFlowId), bulkFlowSpatialParams, couplingManager, "TwoPBulk"), bulkFlowId);
    problem.set(std::make_shared<FacetFlowProblem>(fvGridGeometry.get(facetFlowId), facetFlowSpatialParams, couplingManager, "TwoPFacet"), facetFlowId);
    problem.set(std::make_shared<IntersectionFlowProblem>(fvGridGeometry.get(isFlowId), isFlowSpatialParams, couplingManager, "TwoPIntersection"), isFlowId);

    // the coupling mappers
    using CouplingMapper = typename TestTraits::CouplingMapper;
    auto couplingMapper = std::make_shared<CouplingMapper>();
    couplingMapper->update(fvGridGeometry[bulkFlowId], fvGridGeometry[facetFlowId], fvGridGeometry[isFlowId], gridManager.getEmbeddings());

    // initialize the coupling manager
    typename Traits::SolutionVector x, xOld;
    couplingManager->init(problem.get(bulkFlowId),
                          problem.get(facetFlowId),
                          problem.get(isFlowId),
                          couplingMapper,
                          x);

    // the solution vector
    problem.applyInitialSolution(x);
    xOld = x;

    // the grid variables
    using GridVariables = MultiDomainFVGridVariables<Traits>;
    GridVariables gridVars(fvGridGeometry.getTuple(), problem.getTuple());
    gridVars.init(x);

    // intialize the vtk output module
    MultiDomainVtkOutputModule<Traits> vtkWriter;

    using BulkFlowVtkOutputModule = typename MultiDomainVtkOutputModule<Traits>::template Type<bulkFlowId>;
    using FacetFlowVtkOutputModule = typename MultiDomainVtkOutputModule<Traits>::template Type<facetFlowId>;
    using IntersectionFlowVtkOutputModule = typename MultiDomainVtkOutputModule<Traits>::template Type<isFlowId>;

    const auto dmBulk = MDGridGeometry::template Type<bulkFlowId>::discMethod == DiscretizationMethod::box ? Dune::VTK::nonconforming : Dune::VTK::conforming;
    const auto dmFacet = MDGridGeometry::template Type<facetFlowId>::discMethod == DiscretizationMethod::box ? Dune::VTK::nonconforming : Dune::VTK::conforming;
    vtkWriter.set(std::make_shared<BulkFlowVtkOutputModule>(gridVars[bulkFlowId], x[bulkFlowId], problem[bulkFlowId].name(), "", dmBulk), bulkFlowId);
    vtkWriter.set(std::make_shared<FacetFlowVtkOutputModule>(gridVars[facetFlowId], x[facetFlowId], problem[facetFlowId].name(), "", dmFacet), facetFlowId);
    vtkWriter.set(std::make_shared<IntersectionFlowVtkOutputModule>(gridVars[isFlowId], x[isFlowId], problem[isFlowId].name()), isFlowId);

    // velocity output
    using BulkVelocityOutput = GetPropType<typename Traits::template SubDomain<bulkFlowId>::TypeTag, Properties::VelocityOutput>;
    using FacetVelocityOutput = GetPropType<typename Traits::template SubDomain<facetFlowId>::TypeTag, Properties::VelocityOutput>;
    using IntersectionVelocityOutput = GetPropType<typename Traits::template SubDomain<isFlowId>::TypeTag, Properties::VelocityOutput>;

    BulkVelocityOutput bulkVelocityOutput(gridVars[bulkFlowId]);
    FacetVelocityOutput facetVelocityOutput(gridVars[facetFlowId]);
    IntersectionVelocityOutput isVelocityOutput(gridVars[isFlowId]);

    static constexpr int dimWorld = GridManager::template Grid<0>::dimensionworld;
    using VelocityVector = Dune::FieldVector<double, dimWorld>;
    std::vector< VelocityVector > bulkVelocity_w(fvGridGeometry[bulkFlowId].gridView().size(0), VelocityVector(0.0));
    std::vector< VelocityVector > bulkVelocity_n(fvGridGeometry[bulkFlowId].gridView().size(0), VelocityVector(0.0));
    std::vector< VelocityVector > facetVelocity_w(fvGridGeometry[facetFlowId].gridView().size(0), VelocityVector(0.0));
    std::vector< VelocityVector > facetVelocity_n(fvGridGeometry[facetFlowId].gridView().size(0), VelocityVector(0.0));
    std::vector< VelocityVector > isVelocity_w(fvGridGeometry[isFlowId].gridView().size(0), VelocityVector(0.0));
    std::vector< VelocityVector > isVelocity_n(fvGridGeometry[isFlowId].gridView().size(0), VelocityVector(0.0));
    vtkWriter[bulkFlowId].addField(bulkVelocity_w, "velocity_w", BulkFlowVtkOutputModule::FieldType::element);
    vtkWriter[bulkFlowId].addField(bulkVelocity_n, "velocity_n", BulkFlowVtkOutputModule::FieldType::element);
    vtkWriter[facetFlowId].addField(facetVelocity_w, "velocity_w", FacetFlowVtkOutputModule::FieldType::element);
    vtkWriter[facetFlowId].addField(facetVelocity_n, "velocity_n", FacetFlowVtkOutputModule::FieldType::element);
    vtkWriter[isFlowId].addField(isVelocity_w, "velocity_w", IntersectionFlowVtkOutputModule::FieldType::element);
    vtkWriter[isFlowId].addField(isVelocity_n, "velocity_n", IntersectionFlowVtkOutputModule::FieldType::element);

    // instantiate time loop
    auto timeLoop = std::make_shared<CheckPointTimeLoop<double>>(getParam<double>("Restart.Time", 0),
                                                                 getParam<double>("TimeLoop.DtInitial"),
                                                                 getParam<double>("TimeLoop.TEnd"));
    timeLoop->setMaxTimeStepSize(getParam<double>("TimeLoop.MaxTimeStepSize"));

    // the assembler
    using Assembler = MultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>( problem.getTuple(), fvGridGeometry.getTuple(), gridVars.getTuple(), couplingManager, timeLoop);

    // add marker to fracture output
    auto facetGridData = gridManager.getGridData()->template getSubDomainGridData<facetFlowId>();
    std::vector<int> elementMarkers(fvGridGeometry[facetFlowId].gridView().size(0), 0);
    for (const auto& element : elements(fvGridGeometry[facetFlowId].gridView()))
        elementMarkers[fvGridGeometry[facetFlowId].elementMapper().index(element)]
            = facetGridData->getElementDomainMarker(element);

    vtkWriter[facetFlowId].addField(elementMarkers, "marker");
    vtkWriter.initDefaultOutputFields();
    vtkWriter.write(0.0);

    // the linear solver
    // using LinearSolver = UMFPackBackend;
    using LinearSolver = BlockDiagILU0BiCGSTABSolver; //BlockDiagILU0BiCGSTABSolver;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);

    // open file to write outfluxes in
    std::ofstream outputFile(getParam<std::string>("IO.MassOutputFile"), std::ios::out);

    // set previous solution for storage evaluations
    assembler->setPreviousSolution(xOld);

    // time loop
    timeLoop->start(); do
    {
        // linearize & solve
        newtonSolver->solve(x, *timeLoop);

        // make the new solution the old solution
        xOld = x;
        gridVars.advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // update velocities
        computeTwoPhaseVelocities<BulkFlowTypeTag>(bulkFlowId, *assembler, *couplingManager, fvGridGeometry[bulkFlowId],
                                                   gridVars[bulkFlowId], x[bulkFlowId], bulkVelocity_w, bulkVelocity_n);
        computeTwoPhaseVelocities<FacetFlowTypeTag>(facetFlowId, *assembler, *couplingManager, fvGridGeometry[facetFlowId],
                                                    gridVars[facetFlowId], x[facetFlowId], facetVelocity_w, facetVelocity_n);
        computeTwoPhaseVelocities<IntersectionFlowTypeTag>(isFlowId, *assembler, *couplingManager, fvGridGeometry[isFlowId],
                                                           gridVars[isFlowId], x[isFlowId], isVelocity_w, isVelocity_n);

        // write vtk output
        vtkWriter.write(timeLoop->time());

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by the Newton solver
        timeLoop->setTimeStepSize(newtonSolver->suggestTimeStepSize(timeLoop->timeStepSize()));

        // compute mass distributions
        const auto matrixMasses = computeBulkMassDistribution(bulkFlowId, *couplingManager, *assembler,
                                                              problem[bulkFlowId], gridVars[bulkFlowId], x[bulkFlowId]);
        const auto facetMasses = computeMassInDomain(facetFlowId, *couplingManager, *assembler,
                                                     problem[facetFlowId], gridVars[facetFlowId], x[facetFlowId]);
        const auto isMasses = computeMassInDomain(isFlowId, *couplingManager, *assembler,
                                                  problem[isFlowId], gridVars[isFlowId], x[isFlowId]);

        outputFile << timeLoop->time() << "," << matrixMasses[0] << "," << matrixMasses[1] << "," << matrixMasses[2]
                                       << "," << matrixMasses[3] << "," << matrixMasses[4] << "," << matrixMasses[5]
                                       << "," << facetMasses[0] << "," << facetMasses[1]
                                       << "," << isMasses[0] << "," << isMasses[1] << std::endl;
    } while (!timeLoop->finished());

    // output some Newton & time loopt statistics
    newtonSolver->report();
    timeLoop->finalize();

    // print parameter usage
    Parameters::print();

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/false);

    return 0;

}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
