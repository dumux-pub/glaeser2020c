// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem for the (d-2)-dimensional intersection domain in the
 *        two-phase facet coupling test.
 */
#ifndef DUMUX_TWOP_EXPERIMENT_INTERSECTION_FLOW_PROBLEM_HH
#define DUMUX_TWOP_EXPERIMENT_INTERSECTION_FLOW_PROBLEM_HH

#include <dune/foamgrid/foamgrid.hh>

#include <dumux/material/fluidsystems/2pimmiscible.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/trichloroethene.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/tabulatedcomponent.hh>

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p/model.hh>

#include "spatialparams.hh"

namespace Dumux {
// forward declarations
template<class TypeTag> class TwoPIntersectionProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct TwoPIntersection { using InheritsFrom = std::tuple<TwoP>; };
struct TwoPIntersectionTpfa { using InheritsFrom = std::tuple<TwoPIntersection, CCTpfaModel>; };
struct TwoPIntersectionBox { using InheritsFrom = std::tuple<TwoPIntersection, BoxModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TwoPIntersection> { using type = Dune::FoamGrid<1, 3>; };
// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::TwoPIntersection> { using type = TwoPIntersectionProblem<TypeTag>; };
// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TwoPIntersection>
{
private:
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

public:
    using type = TwoPSpatialParams<FVGridGeometry, Scalar>;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPIntersection>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using TCE = Components::Trichloroethene<Scalar>;
    using Water = Components::H2O<Scalar>;
    using TabulatedWater = Components::TabulatedComponent<Water>;
    using WaterPhase = FluidSystems::OnePLiquid<Scalar, TabulatedWater>;
    using NaplPhase = FluidSystems::OnePLiquid<Scalar, TCE>;

public:
    using type = Dumux::FluidSystems::TwoPImmiscible< Scalar, WaterPhase, NaplPhase>;
};

} // end namespace Properties

/*!
 * \brief The problem for the (d-2)-dimensional intersection domain in the
 *        two-phase facet coupling test.
 */
template<class TypeTag>
class TwoPIntersectionProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using PrimaryVariables = typename GridVariables::PrimaryVariables;
    using Scalar = typename GridVariables::Scalar;

    using FVGridGeometry = typename GridVariables::GridGeometry;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

public:
    TwoPIntersectionProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                            std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                            std::shared_ptr<CouplingManager> couplingManagerPtr,
                            const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManagerPtr)
    {
        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
        crossSectionArea_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.IntersectionArea");
    }

    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    { return problemName_; }

    //! Specifies the kind of boundary condition at a boundary position.
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    //! evaluates the Neumann boundary condition for a given position
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    { return NumEqVector(0.0); }

    /*!
     * \brief Evaluates the source term for all phases within a given
     *        sub-control volume.
     */
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        // obtain the sources stemming from the bulk flow domain
        auto source = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);
        source /= scv.volume()*elemVolVars[scv].extrusionFactor();
        return source;
    }

    //! Sets the aperture as extrusion factor.
    template<class ElementSolution>
    Scalar extrusionFactor(const Element& element,
                           const SubControlVolume& scv,
                           const ElementSolution& elemSol) const
    { return crossSectionArea_; }

    //! Evaluates the initial conditions.
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        using MaterialLaw = typename ParentType::SpatialParams::MaterialLaw;
        const auto& params = this->spatialParams().materialParameters();
        const auto& bulkParams = couplingManager().problem(Dune::index_constant<0>()).spatialParams().materialParameters();
        const auto& bulkFvGridGeometry = couplingManager().problem(Dune::index_constant<0>()).fvGridGeometry();

        static const Scalar initSat = getParam<double>("Problem.InitialMatrixOilSaturation");
        static const Scalar initPc = MaterialLaw::pc(bulkParams, 1.0 - initSat);
        static const Scalar initFacetWaterSat = MaterialLaw::sw(params, initPc);
        static const Scalar rhoW = 1000.0;

        using std::min;
        const auto depth = bulkFvGridGeometry.bBoxMax()[2] - globalPos[2];
        return PrimaryVariables({ 1.0e5 + rhoW*9.81*depth,
                                  min(1.0 - initFacetWaterSat, 1.0) });
    }

    //! Evaluates the Dirichlet boundary conditions at a given position.
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return initialAtPos(globalPos); }

    //! Returns the temperature \f$\mathrm{[K]}\f$ for an isothermal problem.
    Scalar temperature() const
    { return 283.15; /*10°*/ }

    //! Returns const reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }


private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    std::string problemName_;
    Scalar crossSectionArea_;
};

} // end namespace Dumux

#endif
