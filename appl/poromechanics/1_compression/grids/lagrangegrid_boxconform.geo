a = 1;             // width of the domain
b = 1;             // height of the domain

numCells = 50;
dx_m = (a+b)/2.0/numCells; // discretization length in the matrix
dx_f = dx_m*0.5;       // discretization length in the fracture

h = 0.5/numCells;
numSupportPoints = numCells+2;

// Points
Point(1) = {a/2.0, 0.25*b, 0.0, dx_f};
Point(2) = {a/2.0, 0.25*b+h/2.0, 0.0, dx_f};
Line(1) = {1, 2};
For i In {3:numSupportPoints-1}
    Point(i) = {a/2.0, 0.25*b + h/2.0 + (i-2)*h, 0.0, dx_f};
    Line(1+i-2) = {i-1, i};
EndFor

Point(numSupportPoints) = {a/2.0, 0.75*b, 0.0, dx_f};
Line(numCells+1) = {numSupportPoints-1, numSupportPoints};

Transfinite Line{1:numCells+1} = 2;
