import matplotlib.pyplot as plt
import numpy as np
import sys

# use plot style
sys.path.append("./../../common")
import plotstyles

dataFile = "plotdata_fem_2nd_tpfa_DG1.csv"
data = np.loadtxt(dataFile, skiprows=1, delimiter=',')

yIdx = 18
apertureIdx = 3 #LINEAR 3 #CONSTANT 0
tractionIdx = 0 #LINEAR 0 #CONSTANT 1

fig, ax1 = plt.subplots()

color1 = 'tab:red'
ax1.set_xlabel(r'$y$')
ax1.set_ylabel(r'$a$', color=color1, rotation=0)
# ax1.grid(False)
ax1.tick_params(axis='y', labelcolor=color1)
ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
ax1.yaxis.set_major_locator(plt.MaxNLocator(4))
ax1.yaxis.set_label_coords(-0.125, 0.4625)
ax1.set_ylim([-1e-5, 5e-4])

color2 = 'tab:blue'
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
ax2.set_ylabel(r'$| \tau^\perp |$', color=color2, rotation=0)
ax2.grid(False)
ax2.tick_params(axis='y', labelcolor=color2)
ax2.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
ax2.yaxis.set_major_locator(plt.MaxNLocator(4))
ax2.yaxis.set_label_coords(1.125, 0.55)
ax2.set_ylim([-0.02*8e4, 8e4])

# make ticks align
ax1.set_yticks(np.linspace(0.0, 5e-4, 5))
ax2.set_yticks(np.linspace(0.0, 8e4, 5))

ax1.plot(data[:,yIdx], data[:,apertureIdx], color=color1)
ax2.plot(data[:,yIdx], abs(data[:,tractionIdx]), color=color2)

fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.savefig("plot_fem_2nd_tpfa_DG1.pdf", bbox_inches='tight')
