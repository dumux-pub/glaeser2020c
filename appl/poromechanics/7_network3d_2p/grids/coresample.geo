SetFactory("OpenCASCADE");

Cylinder(1) = {0, 0, 0, 0, 0, 0.15, 0.05, 2*Pi};
Disk(4) = {0, 0, 0, 0.06, 0.03};

Rotate {{0, 1, 0}, {0, 0, 0}, Pi/3.5} { Surface{4}; }
Translate {0.0, 0.0, 0.075} { Surface{4}; }

BooleanIntersection{ Volume{1}; } { Surface{4}; Delete; }
BooleanFragments{ Volume{1}; Delete; } { Surface{4}; }
Physical Surface(1) = {4};
Physical Volume(1) = {1};

size = 0.01;
coarsenFactor = 1.0;
Characteristic Length {3} = size;
Characteristic Length {4,5} = size*coarsenFactor;
