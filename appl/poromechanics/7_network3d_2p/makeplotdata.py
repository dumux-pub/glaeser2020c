import subprocess
import os
import sys
import math
import numpy as np
import matplotlib.pyplot as plt

try:
    from paraview.simple import *
except ImportError:
    print("`paraview.simple` not found. Make sure using pvpython instead of python.")

# creates the data of a plot on a provided vtu file
def makePlotOverLineData(vtuFileName, isPolyData, csvFileName, resolution, point1, point2):

    if not os.path.exists(vtuFileName):
        sys.stderr.write("Provided vtu file \"" + vtuFileName + "\" does not exist")
        sys.exit(1)

    print("Reading solution file " + vtuFileName)
    if isPolyData == True:
        vtkFile = XMLPolyDataReader(FileName=vtuFileName)
    else:
        vtkFile = XMLUnstructuredGridReader(FileName=vtuFileName)
    SetActiveSource(vtkFile)

    plotOverLine1 = PlotOverLine(Source="High Resolution Line Source")
    plotOverLine1.Source.Resolution = resolution
    plotOverLine1.Source.Point1 = point1
    plotOverLine1.Source.Point2 = point2

    # write output
    writer = CreateWriter(csvFileName, plotOverLine1, Precision=10)
    writer.UpdatePipeline()


#################################################
# Main script
# Creates the files containing the plot data
##################################################

# plot line specifics
resolution = 2000
frac1_source = [0.15, 0.15, 0.0]
frac1_target = [0.55, 0.45, 0.0]
frac2_source = [0.65, 0.5, 0.0]
frac2_target = [1.00, 0.5, 0.0]

if len(sys.argv) != 3:
    sys.stderr.write("Wring number of arguments")
    sys.exit(1)

vtuFile = sys.argv[1]
csvFileBody = sys.argv[2]

makePlotOverLineData(vtuFile, True, csvFileBody + "_frac1.csv", resolution, frac1_source, frac1_target)
makePlotOverLineData(vtuFile, True, csvFileBody + "_frac2.csv", resolution, frac2_source, frac2_target)
