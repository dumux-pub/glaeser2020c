import subprocess
import sys

if len(sys.argv) < 2:
    sys.stderr.write("Please provide refinement level (integer between 0 and 4) on which to compute")
    sys.exit(1)

refLevel = sys.argv[1]

if refLevel == '0':
    print("Refinement level 0 chosen. Only second order approaches will be considered!")

schemes = [
           'box_mpfa',
           'box_tpfa',
           'fem_1storder_box',
           'fem_1storder_mpfa',
           'fem_1storder_tpfa',
           'fem_2ndorder_box',
           'fem_2ndorder_mpfa',
           'fem_2ndorder_tpfa',
          ]

grid = "./grids/network_ref" + refLevel + ".msh"
lagrangeGrid = "./grids/network_ref" + str( int(refLevel) - 1 ) + ".msh"

for scheme in schemes:
    if refLevel != '0':
        print("Calling " + scheme)
        subprocess.call(['./test_network2d_' + scheme,
                         'params.input',
                         '-OnePBulk.Problem.Name', 'onep_bulk_' + scheme,
                         '-OnePFacet.Problem.Name', 'onep_facet_' + scheme,
                         '-Lagrange.Problem.Name', 'lagrange_' + scheme,
                         '-ElasticBulk.Problem.Name', 'elastic_' + scheme,
                         '-Output.FluxFile', 'fluxes_' + scheme + '.csv',
                         '-Grid.File', grid,
                         '-Lagrange.Grid.File', lagrangeGrid])

    if (scheme == 'fem_2ndorder_box' or
        scheme == 'fem_2ndorder_mpfa' or
        scheme == 'fem_2ndorder_tpfa'):
            print("Calling conforming configuration for " + scheme)
            subprocess.call(['./test_network2d_' + scheme,
                             'params.input',
                             '-OnePBulk.Problem.Name', 'onep_bulk_' + scheme + '_conforming',
                             '-OnePFacet.Problem.Name', 'onep_facet_' + scheme + '_conforming',
                             '-Lagrange.Problem.Name', 'lagrange_' + scheme + '_conforming',
                             '-ElasticBulk.Problem.Name', 'elastic_' + scheme + '_conforming',
                             '-Output.FluxFile', 'fluxes_' + scheme + '_conforming.csv',
                             '-Grid.File', grid,
                             '-Lagrange.Grid.File', grid])
