import sys
import math
import subprocess

numElemsFinest1 = 96
numElemsFinest2 = 64
dxFinest = 0.005
numRefinements = 4

for i in range(0, numRefinements+1):
    print("Creating refinement " + str(numRefinements-i))

    numElems1 = int(numElemsFinest1/math.pow(2, i))
    numElems2 = int(numElemsFinest2/math.pow(2, i))
    dx = float(0.5/numElems1)

    print("numElems1 = " + str(numElems1))
    print("numElems2 = " + str(numElems2))
    print("dx = " + str(dx))

    geoFileLines = open("grids/network.geo", "r").readlines()

    line1 = geoFileLines[0]
    line2 = geoFileLines[1]
    line3 = geoFileLines[2]

    line1 = line1.split(" ")
    line2 = line2.split(" ")
    line3 = line3.split(" ")

    if line1[0] != "numFracVertices1":
        sys.stderr.write("Expected \"numFracVertices1\" in the first line of the geo file")
        sys.exit(1)

    if line2[0] != "numFracVertices2":
        sys.stderr.write("Expected \"numFracVertices2\" in the second line of the geo file")
        sys.exit(1)

    if line3[0] != "dx":
        sys.stderr.write("Expected \"dx\" in the third line of the geo file")
        sys.exit(1)

    tmpGeoFile = open("tmp.geo", "w")
    tmpGeoFile.write("numFracVertices1 = " + str(numElems1+1) + ";\n")
    tmpGeoFile.write("numFracVertices2 = " + str(numElems2+1) + ";\n")
    tmpGeoFile.write("dx = " + str(dx) + ";\n")

    for j in range(3, len(geoFileLines)):
        tmpGeoFile.write(geoFileLines[j])
    tmpGeoFile.close()

    # let gmsh mesh the geo file
    subprocess.call('gmsh -2 ./tmp.geo', shell=True)

    # move to i-th mesh file
    subprocess.call(['mv', 'tmp.msh', 'grids/network_ref' + str(numRefinements-i) + '.msh'])

    # clean up
    subprocess.call(['rm', 'tmp.geo'])
