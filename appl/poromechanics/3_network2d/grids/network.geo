numFracVertices1 = 97;
numFracVertices2 = 65;
dx = 0.005;

SetFactory("OpenCASCADE");

Point(5) = {0.15, 0.15, 0, 1.0};
Point(6) = {0.55, 0.45, 0, 1.0};

Point(7) = {0.15, 0.85, 0, 1.0};
Point(8) = {0.55, 0.55, 0, 1.0};

Point(13) = {0.65, 0.5, 0.0, 1.0};
Point(14) = {0.95, 0.5, 0.0, 1.0};
Point(19) = {1.0, 0.5, 0.0, 1.0};

// fracture lines
Line(5) = {5, 6};
Line(6) = {7, 8};   Transfinite Line{5,6} = numFracVertices1;
Line(9) = {13, 19}; Transfinite Line{9} = numFracVertices2;

// domain outline
Point(15) = {0.0, 0.0, 0.0, 1.0};
Point(16) = {1.0, 0.0, 0.0, 1.0};
Point(17) = {1.0, 1.0, 0.0, 1.0};
Point(18) = {0.0, 1.0, 0.0, 1.0};
Line(10) = {15, 16};
Line(11) = {16, 19};
Line(12) = {19, 17};
Line(13) = {17, 18};
Line(14) = {18, 15};

Line Loop(1) = {10, 11, 12, 13, 14};
Plane Surface(1) = {1};
Line{5,6,9} In Surface{1};

Physical Line(1) = {5};
Physical Line(2) = {6};
Physical Line(3) = {9};
Physical Surface(1) = {1};

Characteristic Length {5,6,7,8,13,14,15,16,17,18,19} = dx;
