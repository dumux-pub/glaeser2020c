SetFactory("OpenCASCADE");

Cylinder(1) = {0, 0, 0, 0, 0, 0.15, 0.05, 2*Pi};
Disk(4) = {0, 0, 0, 0.06, 0.03};
Disk(5) = {0, 0, 0, 0.055, 0.03};

Disk(6) = {0, 0, 0, 0.025, 0.025};
Disk(7) = {0, 0, 0, 0.025, 0.025};

Rotate {{0, 1, 0}, {0, 0, 0}, Pi/4.0} { Surface{4}; }
Translate {0.03, 0.001, 0.1} { Surface{4}; }

Rotate {{0, 1, 0}, {0, 0, 0}, Pi/3.5} { Surface{5}; }
Translate {-0.02, -0.005, 0.055} { Surface{5}; }

Rotate {{0, 1, 0}, {0, 0, 0}, 3.0*Pi/3.5} { Surface{6}; }
Translate {-0.015, -0.015, 0.1025} { Surface{6}; }

Rotate {{0, 1, 0}, {0, 0, 0}, 3.0*Pi/4.5} { Surface{7}; }
Translate {0.025, 0.005, 0.055} { Surface{7}; }

BooleanIntersection{ Volume{1}; } { Surface{4,5,6,7}; Delete; }
BooleanFragments{ Volume{1}; Delete; } { Surface{4,5,6,7}; }
Physical Surface(1) = {4, 5, 6, 7};
Physical Volume(1) = {1};

Characteristic Length {9:18} = 0.0075;
