import sys
import os
import subprocess
import matplotlib.pyplot as plt
import numpy as np
import math

# use plot style
sys.path.append("./../../common")
import plotstyles

# function to multiply the number of elements used on fracture
def multiplyNumFractureElements(file, multiplicationFactor):
    lines = open(file).read().splitlines()
    if (lines[0].split(" ")[0] != "numCellsOnFracture"):
        sys.stderr.write("Expected \"numCellsOnFracture\" as first word of the first line!\n")
        sys.exit(1)

    numElemsStr = lines[0].split(" ")[2]
    numElemsStr = numElemsStr.strip("\n")
    numElemsStr = numElemsStr.strip(";")
    newNumElems = float(numElemsStr)*multiplicationFactor
    lines[0] = "numCellsOnFracture = " + str(newNumElems) + ";"
    open(file,'w').write('\n'.join(lines))

##############################
# Start of the actual script #
##############################
if len(sys.argv) < 2:
    sys.stderr.write("Please provide number of refinements to be considered and true/false if you want to do refinement by splitting\n")
    sys.exit(1)

numRefinements = int(sys.argv[1])

# set this to true if you want to refine the entire grid in each step
refineBySplitting = bool(sys.argv[2])

# call dumux for each scheme
schemes = ['box_tpfa', 'fem_1storder_tpfa', 'fem_2ndorder_tpfa_halve', 'fem_2ndorder_tpfa']
for suffix in schemes:

    # for first order scheme, coarsen mortar grid
    doCoarsen = True
    if suffix == 'fem_2ndorder_tpfa':
        doCoarsen = False

    # prepare grids
    print("Creating temporary grid files\n")
    subprocess.call(['cp', 'grids/inclinedcrack.geo', 'tmp.geo'])
    subprocess.call(['gmsh', '-2', 'tmp.geo'])
    subprocess.call(['mv', 'tmp.msh', 'tmp_0.msh'])
    subprocess.call(['cp', 'tmp_0.msh', 'tmp_mortar_0.msh'])
    if doCoarsen == True:
        if refineBySplitting == True:
            subprocess.call(['gmsh', '-refine', 'tmp_0.msh'])
        else:
            subprocess.call(['cp', 'tmp.geo', 'tmp_0.geo'])
            multiplyNumFractureElements('tmp_0.geo', 2.0)
            subprocess.call(['gmsh', '-2', 'tmp_0.geo'])

    for i in range(1, numRefinements+1):
        if refineBySplitting == True:
            subprocess.call(['cp', 'tmp_mortar_' + str(i-1) + '.msh', 'tmp_mortar_' + str(i) + '.msh'])
            subprocess.call(['gmsh', '-refine', 'tmp_mortar_' + str(i) + '.msh'])
            subprocess.call(['cp', 'tmp_mortar_' + str(i) + '.msh', 'tmp_' + str(i) + '.msh'])
            if doCoarsen == True:
                subprocess.call(['gmsh', '-refine', 'tmp_' + str(i) + '.msh'])
        else:
            # double the number of elements used on the mortar
            subprocess.call(['cp', 'tmp.geo', 'tmp_' + str(i) + '.geo'])
            multiplyNumFractureElements('tmp_' + str(i) + '.geo', math.pow(2.0, i))
            subprocess.call(['gmsh', '-2', 'tmp_' + str(i) + '.geo'])
            subprocess.call(['cp', 'tmp_' + str(i) + '.msh', 'tmp_mortar_' + str(i) + '.msh'])
            if doCoarsen == True:
                subprocess.call(['gmsh', '-refine', 'tmp_' + str(i) + '.msh'])

    errorFileName = suffix + "_errors.csv"

    # deduce scheme plot label
    if suffix == "fem_1storder_tpfa":
        schemeLabel = "CG1 (h/2)"
        problemNameSuffix = "fem1st"
    elif suffix == "fem_2ndorder_tpfa":
        schemeLabel = "CG2"
        problemNameSuffix = "fem2nd"
    elif suffix == "fem_2ndorder_tpfa_halve":
        schemeLabel = "CG2 (h/2)"
        problemNameSuffix = "fem2nd_halve"
    elif suffix == "box_tpfa":
        schemeLabel = "BOX (h/2)"
        problemNameSuffix = "box"
    else:
        sys.stderr.write("Unsupported scheme suffix\n")
        sys.exit(1)

    if suffix == 'fem_2ndorder_tpfa_halve':
        suffix = 'fem_2ndorder_tpfa'
    exeName = "test_analytic_crack_" + suffix

    if os.path.isfile(errorFileName):
        print("Found and removed old error file")
        subprocess.call(['rm', errorFileName])

    # make sure executable is up to date
    print("\nRecompiling executable")
    subprocess.call(['make', exeName])

    for i in range(0, numRefinements+1):
        subprocess.call(['./' + exeName,
                         '-Grid.File', 'tmp_' + str(i) + '.msh',
                         '-OnePBulk.Problem.Name', 'onep_bulk_' + problemNameSuffix + '_' + str(i),
                         '-OnePFacet.Problem.Name', 'onep_facet_' + problemNameSuffix + '_' + str(i),
                         '-ElasticBulk.Problem.Name', 'elastic_' + problemNameSuffix + '_' + str(i),
                         '-Lagrange.Problem.Name', 'lagrange_' + problemNameSuffix + '_' + str(i),
                         '-Lagrange.Grid.File', 'tmp_mortar_' + str(i) + '.msh',
                         '-L2Error.OutputFile', errorFileName])

    # add data to plot
    errorData = np.loadtxt(errorFileName, delimiter=',')

    # print rate
    print("printing rates for scheme " + schemeLabel)
    for refIdx in range(0, len(errorData)-1):
        print( (np.log(errorData[refIdx+1, 1]) - np.log(errorData[refIdx, 1])) / (np.log(errorData[refIdx+1, 0]) - np.log(errorData[refIdx, 0])) )

    # add first order reference plot first
    if suffix == "box_tpfa":
        firstOrder = []
        firstOrder.append(errorData[0,1]*2.0)
        for i in range(1, len(errorData[:])):
            firstOrder.append(firstOrder[i-1]*(errorData[i, 0]/errorData[i-1, 0]))
        plt.loglog(1/errorData[:, 0], firstOrder, label=r"$\mathcal{O} \left( 1 \right)$", color='k', linestyle='--', alpha=0.5)

    # use transparent curve for box (fem lies below it)
    alphaValue = 1
    lineStyle = '-'
    if suffix == "box_tpfa":
        alphaValue = 1.0
    if suffix == "fem_1storder_tpfa":
        alphaValue = 0.75
        lineStyle = '--'

    plt.figure(1)
    plt.loglog(1/errorData[:, 0], errorData[:,1], label=schemeLabel, alpha=alphaValue, linestyle=lineStyle, marker='*')
    plt.ylabel(r"$\varepsilon_{\llbracket u \rrbracket^\parallel}$")
    plt.xlabel(r"$1/h_\tau$")
    plt.xlim([0.1, 2])

    plt.legend()

# save the figure
plt.savefig("convergence.pdf", bbox_inches='tight')

# remove all created grids and geo files
print("Removing temporary grid files\n")
subprocess.call(['rm', 'tmp.geo'])
for i in range(0, numRefinements+1):
    subprocess.call(['rm', 'tmp_' + str(i) + '.geo'])
    subprocess.call(['rm', 'tmp_' + str(i) + '.msh'])
    subprocess.call(['rm', 'tmp_mortar_' + str(i) + '.msh'])
