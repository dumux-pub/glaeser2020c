import numpy as np
import matplotlib.pyplot as plt
import sys

# use plot style
sys.path.append("./../../common")
import plotstyles

dataFile = "plotdata_fem_1st_finest.csv"
data = np.loadtxt(dataFile, skiprows=1, delimiter=',')

arcIdx = 21
deltaUtIdx = 8
exactDeltaUtIdx = 12
tractionIdx = 13
exactTractionIdx = 10

# tangential displacement jump
plt.figure(1)
plt.xlabel(r'$x\,\, \left[ \si{\meter} \right]$')
plt.ylabel(r'$\llbracket \mathbf{u} \rrbracket^\parallel \,\, \left[ \si{\meter} \right]$')
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.ylim([0.0, 1e-2])

plt.plot(data[:,arcIdx], data[:,deltaUtIdx], label=r'CG1')
plt.plot(data[:,arcIdx], data[:,exactDeltaUtIdx], alpha=0.5, label='exact')
plt.legend()
plt.savefig("fem_1st_finest_deltaut.pdf", bbox_inches='tight')

plt.figure(2)
plt.xlabel(r'$x\,\, \left[ \si{\meter} \right]$')
plt.ylabel(r'$| \tau^\perp | \,\, \left[ \si{\pascal/\meter} \right]$')
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.plot(data[:,arcIdx], np.sqrt(data[:,tractionIdx]*data[:,tractionIdx] + data[:,tractionIdx+1]*data[:,tractionIdx+1]), label=r'CG1')
plt.plot(data[:,arcIdx], abs(data[:,exactTractionIdx]), label='exact')
plt.legend()
plt.savefig("fem_1st_finest_normaltraction.pdf", bbox_inches='tight')
