import numpy as np
import matplotlib.pyplot as plt

# plot adjustments
plt.style.use('ggplot')

plt.rcParams['font.family'] = 'serif'
plt.rcParams['text.usetex'] = True
plt.rcParams['font.weight'] = 'light'
plt.rcParams['font.size'] = 10
plt.rcParams['axes.labelsize'] = 22
plt.rcParams['axes.labelweight'] = 'light'
plt.rcParams['xtick.labelsize'] = 11
plt.rcParams['ytick.labelsize'] = 11
plt.rcParams['legend.fontsize'] = 11
plt.rcParams['figure.titlesize'] = 12
plt.rcParams.update( {'mathtext.default': 'regular' } )

dataFile = "plotdata_fem_1st_finest.csv"
data = np.loadtxt(dataFile, skiprows=1, delimiter=',')

arcIdx = 20
deltaUtIdx = 8
exactDeltaUtIdx = 9
tractionIdx = 13
exactTractionIdx = 10

fig, ax1 = plt.subplots()

color1 = 'tab:red'
ax1.set_xlabel(r'$y$')
ax1.set_ylabel(r'$\left[ \mathbf{u} \right]^\parallel$', color=color1, rotation=0)
# ax1.grid(False)
ax1.tick_params(axis='y', labelcolor=color1)
ax1.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
ax1.yaxis.set_major_locator(plt.MaxNLocator(4))
ax1.yaxis.set_label_coords(-0.125, 0.4625)
ax1.set_ylim([0.0, 1e-2])

color2 = 'tab:blue'
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
ax2.set_ylabel(r'$\| \tau^\perp \|$', color=color2, rotation=0)
ax2.grid(False)
ax2.tick_params(axis='y', labelcolor=color2)
ax2.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
ax2.yaxis.set_major_locator(plt.MaxNLocator(4))
ax2.yaxis.set_label_coords(1.125, 0.55)
ax2.set_ylim([0.0, 8e6])

# make ticks align
ax1.set_yticks(np.linspace(0.0, 1e-2, 5))
ax2.set_yticks(np.linspace(0.0, 8e6, 5))

ax1.plot(data[:,arcIdx], data[:,deltaUtIdx], color=color1, label=r'$\left[ \mathbf{u} \right]^\parallel$')
ax1.plot(data[:,arcIdx], data[:,exactDeltaUtIdx], color='orangered', alpha=0.5, label='exact')
ax2.plot(data[:,arcIdx], np.sqrt(data[:,tractionIdx]*data[:,tractionIdx] + data[:,tractionIdx+1]*data[:,tractionIdx+1]), color=color2)
ax2.plot(data[:,arcIdx], abs(data[:,exactTractionIdx]), color='mediumslateblue', label='exact')

ax1.legend(loc='upper left')
ax2.legend()

fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.savefig("plot_fem_1st_finest.pdf", bbox_inches='tight')
