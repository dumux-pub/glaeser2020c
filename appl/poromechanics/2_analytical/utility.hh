// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Some utility functions for the fracture mechanics analytic test case.
 */
#ifndef DUMUX_ANALYTIC_CRACK_UTILITY_HH
#define DUMUX_ANALYTIC_CRACK_UTILITY_HH

#include <cmath>
#include <dune/common/fvector.hh>
#include <dumux/common/parameters.hh>

namespace Dumux {
namespace AnalyticCrackTest {

    // computes the center of the fracture
    Dune::FieldVector<double, 2> fractureEnd()
    {
        static const double alpha = getParam<double>("Problem.FractureInclinationAngle")*M_PI/180.0;
        static const double df = getParam<double>("Problem.FractureLength");
        static const double b = df/2.0;

        using std::sin;
        using std::cos;
        return {{-cos(alpha)*b, sin(alpha)*b}};
    }

    // evaluate exact normal traction
    double exactNormalTraction()
    {
        static const double alpha = getParam<double>("Problem.FractureInclinationAngle")*M_PI/180.0;
        static const double s = getParam<double>("ElasticBulk.Problem.BoundaryStress");

        using std::sin;
        return s*sin(alpha)*sin(alpha);
    };

    // evaluate exact shear traction
    double exactShearTraction()
    {
        static const double alpha = getParam<double>("Problem.FractureInclinationAngle")*M_PI/180.0;
        static const double s = getParam<double>("ElasticBulk.Problem.BoundaryStress");
        static const double f = getParam<double>("Lagrange.FrictionCoefficient");

        using std::sin;
        using std::cos;
        return s*sin(alpha)*(cos(alpha) - sin(alpha)*f);
    }

    // computes the exact displacement jump at a position
    template<class GlobalPosition>
    double exactDeltaUt(const GlobalPosition& globalPos)
    {
        static const double nu = getParam<double>("ElasticBulk.SpatialParams.Nu");
        static const double E = getParam<double>("ElasticBulk.SpatialParams.EModule");
        static const double df = getParam<double>("Problem.FractureLength");
        static const double b = df/2.0;

        using std::sqrt;
        const auto eta = (globalPos - fractureEnd()).two_norm();
        return 4.0*(1.0 - nu*nu)*exactShearTraction()/E*sqrt(b*b - (eta - b)*(eta - b));
    }

} // end namespace AnalyticCrackTest
} // end namespace Dumux

#endif
