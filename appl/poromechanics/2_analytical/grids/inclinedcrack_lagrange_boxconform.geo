a = 500;        // width of the domain
b = 500;        // height of the domain
alpha = 20.0;   // fracture inclination angle [degrees]
df = 10;        // length of the fracture

numCellsOnFracture = 20;
h = df/numCellsOnFracture;

numSupportPoints = numCellsOnFracture+2;

// fracture points
x0 = -df*0.5*Cos(alpha*Pi/180.0);
y0 = df*0.5*Sin(alpha*Pi/180.0);

dx = h*Cos(alpha*Pi/180.0);
dy = h*Sin(alpha*Pi/180.0);

Point(1) = {x0, y0, 0.0};
Point(2) = {x0+dx/2.0, y0-dy/2.0, 0.0};

Line(1) = {1, 2};
For i In {3:numSupportPoints-1}
    Point(i) = {x0+dx/2.0 + (i-2)*dx, y0-dy/2.0 - (i-2)*dy, 0.0};
    Line(1+i-2) = {i-1, i};
EndFor

Point(numSupportPoints) = {-x0, -y0, 0.0};
Line(numCellsOnFracture+1) = {numSupportPoints-1, numSupportPoints};

Transfinite Line{1:numCellsOnFracture+1} = 2;
