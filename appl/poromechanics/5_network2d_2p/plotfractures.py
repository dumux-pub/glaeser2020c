import numpy as np
import matplotlib.pyplot as plt
import sys

# use plot style
sys.path.append("./../../common")
import plotstyles

prop_cycle = list(plt.rcParams['axes.prop_cycle'])

if (len(sys.argv)-1)%2 != 0:
    sys.stderr.write("Wrong number of arguments provided\n")
    sys.exit(1)

numSchemes = (len(sys.argv)-1)/2
schemeColorOffset = 0

# friction coefficient
F = 0.5

for schemeIdx in range(0, numSchemes):
    schemeLabel = sys.argv[schemeIdx*2 + 2]
    dataFileBody = sys.argv[schemeIdx*2 + 1]
    dataFile1 = dataFileBody + "_frac1.csv"
    dataFile2 = dataFileBody + "_frac2.csv"

    data1 = np.loadtxt(dataFile1, skiprows=1, delimiter=',')
    data2 = np.loadtxt(dataFile2, skiprows=1, delimiter=',')

    deltaUtIdx = 1
    fricCoeffIdx = 8
    gapIdx = 9
    xIdx = 18

    normTracIdx = 10
    tangTracIdx = 13

    # tangential displacement jump (fracture 1)
    plt.figure(1)
    plt.xlabel(r'$x\,\, \left[ \si{\meter} \right]$')
    plt.ylabel(r'$\llbracket \mathbf{u} \rrbracket^\parallel \,\, \left[ \si{\meter} \right]$')
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(data1[:,xIdx], data1[:,deltaUtIdx], color=prop_cycle[schemeIdx+schemeColorOffset]['color'], linestyle='-', marker='*', markersize=0, label=schemeLabel)
    plt.legend()

    # gap (fracture 1)
    plt.figure(2)
    plt.xlabel(r'$x\,\, \left[ \si{\meter} \right]$')
    plt.ylabel(r'$\varrho \,\, \left[ \si{\meter} \right]$')
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(data1[:,xIdx], data1[:,gapIdx], color=prop_cycle[schemeIdx+schemeColorOffset]['color'], linestyle='-', marker='*', markersize=0, label=schemeLabel)
    plt.legend()

    # friction bound vs tangential traction
    plt.figure(3)
    plt.xlabel(r'$x\,\, \left[ \si{\meter} \right]$')
    plt.ylabel(r'$| \tau^\parallel | \,\, \left[ \si{\newton/\meter^2} \right]$')
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(data1[:,xIdx], np.sqrt(data1[:,tangTracIdx]*data1[:,tangTracIdx] + data1[:,tangTracIdx+1]*data1[:,tangTracIdx+1]), color=prop_cycle[schemeIdx+schemeColorOffset]['color'], linestyle='-', marker='*', markersize=0, label=schemeLabel)
    plt.plot(data1[:,xIdx], data1[:,fricCoeffIdx]*np.sqrt(data1[:,normTracIdx]*data1[:,normTracIdx] + data1[:,normTracIdx+1]*data1[:,normTracIdx+1]), color=prop_cycle[schemeIdx+schemeColorOffset]['color'], linestyle='--', marker='*', markersize=0, alpha=0.5)
    plt.legend()

    # normal traction (fracture 2)
    plt.figure(4)
    plt.xlabel(r'$x\,\, \left[ \si{\meter} \right]$')
    plt.ylabel(r'$| \tau^\perp | \,\, \left[ \si{\newton/\meter^2} \right]$')
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.plot(data2[:,xIdx], np.sqrt(data2[:,normTracIdx]*data2[:,normTracIdx] + data2[:,normTracIdx+1]*data2[:,normTracIdx+1]), color=prop_cycle[schemeIdx+schemeColorOffset]['color'], linestyle='-', marker='*', markersize=0, label=schemeLabel)
    plt.legend()

plt.figure(1)
plt.title("Fracture 1")
plt.savefig("deltaUt_frac1.pdf", bbox_inches='tight')

plt.figure(2)
plt.title("Fracture 1")
plt.savefig("gap_frac2.pdf", bbox_inches='tight')

plt.figure(3)
plt.title("Fracture 1")
plt.savefig("tangtraction_frac1.pdf", bbox_inches='tight')

plt.figure(4)
plt.title("Fracture 2")
plt.savefig("normtraction_frac2.pdf", bbox_inches='tight')
