// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The spatial parameters class for the bulk flow domain in the
 *        elastic tow-phase facet coupling test.
 */
#ifndef DUMUX_2D_NETWORK_TWOP_FLOW_SPATIALPARAMS_HH
#define DUMUX_2D_NETWORK_TWOP_FLOW_SPATIALPARAMS_HH

#include <dumux/discretization/elementsolution.hh>

#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/porositydeformation.hh>

#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux {

/*!
 * \brief The spatial parameters for the bulk flow domain in the single-phase facet coupling test.
 */
template<class FVGridGeometry, class Scalar, class CouplingManager>
class TwoPBulkSpatialParams : public FVSpatialParams<FVGridGeometry, Scalar,
                                                     TwoPBulkSpatialParams<FVGridGeometry, Scalar, CouplingManager>>
{
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;

    using ThisType = TwoPBulkSpatialParams<FVGridGeometry, Scalar, CouplingManager>;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, ThisType>;

    // use a regularized van-genuchten material law
    using EffectiveLaw = RegularizedVanGenuchten<Scalar>;

public:
    //! Export the type used for permeability
    using PermeabilityType = Scalar;

    //! export the material law and parameters used
    using MaterialLaw = EffToAbsLaw< EffectiveLaw >;
    using MaterialLawParams = typename MaterialLaw::Params;

    TwoPBulkSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                          std::shared_ptr<const CouplingManager> couplingManagerPtr,
                          const std::string& paramGroup = "")
    : ParentType(fvGridGeometry)
    , couplingManagerPtr_(couplingManagerPtr)
    , initialPorosity_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.InitialPorosity"))
    , initialPermeability_(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.InitialPermeability"))
    , useConstantPorosity_(getParamFromGroup<bool>(paramGroup, "SpatialParams.UseConstantPorosity"))
    {
        // set the material law parameters
        matLawParams_.setSnr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Snr"));
        matLawParams_.setSwr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Swr"));
        matLawParams_.setVgAlpha(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.VGAlpha"));
        matLawParams_.setVgn(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.VGN"));
    }

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    template<class ElementSolution>
    Scalar permeability(const Element& element,
                        const SubControlVolume& scv,
                        const ElementSolution& elemSol) const
    { return initialPermeability_; }

    //! Returns the porosity for a sub-control volume.
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    {
        if (useConstantPorosity_)
            return initialPorosity_;

        static constexpr auto mechId = CouplingManager::poroMechId;
        static constexpr auto matrixFlowId = CouplingManager::matrixFlowId;

        const auto eIdx = this->fvGridGeometry().elementMapper().index(element);
        const auto poroMechElementIdx = couplingManagerPtr_->bulkIndexMap().map(matrixFlowId, eIdx);
        const auto& poroMechGridGeom = couplingManagerPtr_->problem(mechId).gridGeometry();
        const auto poroMechElement = poroMechGridGeom.element(poroMechElementIdx);

        const auto& sol = couplingManagerPtr_->usePrevSol() ? couplingManagerPtr_->getSol()[mechId] : couplingManagerPtr_->curSol()[mechId];
        auto poroMechElemSol = elementSolution(poroMechElement, sol, poroMechGridGeom);

        // evaluate the deformation-dependent porosity at the scv center
        return PorosityDeformation<Scalar>::evaluatePorosity(poroMechGridGeom, poroMechElement, element.geometry().center(), poroMechElemSol, initialPorosity_);
    }

    //! Return the material law parameters
    const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition& globalPos) const
    { return matLawParams_; }

    //! Water is the wetting phase
    template< class FluidSystem >
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    {
        // we set water as the wetting phase here
        // which is phase0Idx in the our chosen fluid system
        return FluidSystem::phase0Idx;
    }

private:
    std::shared_ptr<const CouplingManager> couplingManagerPtr_;
    Scalar initialPorosity_;
    Scalar initialPermeability_;
    bool useConstantPorosity_;

    MaterialLawParams matLawParams_;
};

} // end namespace Dumux

#endif
