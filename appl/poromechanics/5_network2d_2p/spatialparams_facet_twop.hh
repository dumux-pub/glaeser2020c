// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The spatial parameters class for the facet flow domain in the
 *        elastic two-phase facet coupling test.
 */
#ifndef DUMUX_2D_NETWOR_TWOP_FACET_FLOW_SPATIALPARAMS_HH
#define DUMUX_2D_NETWOR_TWOP_FACET_FLOW_SPATIALPARAMS_HH

#include <dumux/discretization/elementsolution.hh>
#include <dumux/material/spatialparams/fv.hh>

#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux {

/*!
 * \brief The spatial parameters for the single-phase facet coupling test.
 */
template<class FVGridGeometry, class Scalar, class CouplingManager>
class TwoPFacetSpatialParams : public FVSpatialParams<FVGridGeometry, Scalar,
                                                      TwoPFacetSpatialParams<FVGridGeometry, Scalar, CouplingManager>>
{
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;

    using ThisType = TwoPFacetSpatialParams<FVGridGeometry, Scalar, CouplingManager>;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar, ThisType>;

    // use a regularized van-genuchten material law
    using EffectiveLaw = RegularizedVanGenuchten<Scalar>;

public:
    //! Export the type used for permeability
    using PermeabilityType = Scalar;

    //! export the material law and parameters used
    using MaterialLaw = EffToAbsLaw< EffectiveLaw >;
    using MaterialLawParams = typename MaterialLaw::Params;

    TwoPFacetSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                           std::shared_ptr<CouplingManager> couplingManagerPtr,
                           const std::string& paramGroup = "")
    : ParentType(fvGridGeometry)
    , paramGroup_(paramGroup)
    , couplingManagerPtr_(couplingManagerPtr)
    {
        initialAperture_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.InitialGap");
        initialAperture_ += getParamFromGroup<Scalar>(paramGroup, "SpatialParams.MinHydraulicAperture");

        // set the material law parameters
        materialLawParams_.setSnr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Snr"));
        materialLawParams_.setSwr(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Swr"));
        materialLawParams_.setVgAlpha(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.VGAlpha"));
        materialLawParams_.setVgn(getParamFromGroup<Scalar>(paramGroup, "SpatialParams.VGN"));

        alphaRefAperture_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.AlphaReferenceAperture");
        vgAlpha_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.VGAlpha");
    }

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    template<class ElementSolution>
    Scalar permeability(const Element& element,
                        const SubControlVolume& scv,
                        const ElementSolution& elemSol) const
    {
        static const Scalar gapZeroThreshold = getParam<Scalar>("SpatialParams.ZeroGapThreshold");
        static const Scalar minA = getParam<Scalar>("SpatialParams.MinHydraulicAperture");
        static const Scalar initGap = getParam<Scalar>("SpatialParams.InitialGap");

        const auto& mechSol = couplingManagerPtr_->getSol()[Dune::Indices::_2];
        auto a = couplingManagerPtr_->usePrevSol() ? couplingManagerPtr_->computeAperture(element, scv, initGap, mechSol)
                                                   : couplingManagerPtr_->computeAperture(element, scv, initGap);

        a = a < gapZeroThreshold ? minA : minA + a - gapZeroThreshold;

        return a*a/12.0;
    }

    //! Returns the porosity for a sub-control volume.
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return 1.0; }

    //! Return the material law parameters
    template<class ElementSolution>
    const MaterialLawParams& materialLawParams(const Element& element,
                                               const SubControlVolume& scv,
                                               const ElementSolution& elemSol) const
    {
        static const bool hasApertureDependency = getParam<bool>("SpatialParams.ApertureDependentParams");
        static const Scalar gapZeroThreshold = getParam<Scalar>("SpatialParams.ZeroGapThreshold");
        static const Scalar minA = getParam<Scalar>("SpatialParams.MinHydraulicAperture");
        static const Scalar initGap = getParam<Scalar>("SpatialParams.InitialGap");
        if (hasApertureDependency)
        {

            auto a = couplingManagerPtr_->computeAperture(element, scv, initGap);
            a = a < gapZeroThreshold ? minA : minA + a - gapZeroThreshold;

            materialLawParams_.setVgAlpha(vgAlpha_*a/alphaRefAperture_);
            return materialLawParams_;
        }
        else
        {
            static const Scalar constAlpha = vgAlpha_*(minA + initGap)/alphaRefAperture_;
            materialLawParams_.setVgAlpha(constAlpha);
        }

        return materialLawParams_;
    }

    //! Water is the wetting phase
    template< class FluidSystem >
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    {
        // we set water as the wetting phase here
        // which is phase0Idx in the our chosen fluid system
        return FluidSystem::phase0Idx;
    }

private:
    std::string paramGroup_;
    Scalar initialAperture_;

    Scalar vgAlpha_;
    Scalar alphaRefAperture_;
    mutable MaterialLawParams materialLawParams_;
    std::shared_ptr<const CouplingManager> couplingManagerPtr_;
};

} // end namespace Dumux

#endif
