dune_symlink_to_source_files(FILES "grids" "params.input" "preparegrids.py" "runallschemes.py" "makeplotdata.py" "plotfractures.py")
set(CMAKE_BUILD_TYPE Release)

# using FEM (1st order) and TPFA
dune_add_test(NAME test_network2d_2p_fem_1storder_tpfa
              SOURCES main_fem_fv.cc
              COMPILE_DEFINITIONS BULKFLOWTYPETAG=TwoPBulkTpfa
              COMPILE_DEFINITIONS FACETFLOWTYPETAG=TwoPFacetTpfa
              CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )")

# using FEM (1st order) and MPFA
dune_add_test(NAME test_network2d_2p_fem_1storder_mpfa
              SOURCES main_fem_fv.cc
              COMPILE_DEFINITIONS FEMORDER=1
              COMPILE_DEFINITIONS BULKFLOWTYPETAG=TwoPBulkMpfa
              COMPILE_DEFINITIONS FACETFLOWTYPETAG=TwoPFacetTpfa
              CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )")

# using FEM (1st order) and BOX
dune_add_test(NAME test_network2d_2p_fem_1storder_box
              SOURCES main_fem_fv.cc
              COMPILE_DEFINITIONS FEMORDER=1
              COMPILE_DEFINITIONS BULKFLOWTYPETAG=TwoPBulkBox
              COMPILE_DEFINITIONS FACETFLOWTYPETAG=TwoPFacetBox
              CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )")


# using FEM (2nd order) and TPFA
dune_add_test(NAME test_network2d_2p_fem_2ndorder_tpfa
              SOURCES main_fem_fv.cc
              COMPILE_DEFINITIONS FEMORDER=2
              COMPILE_DEFINITIONS BULKFLOWTYPETAG=TwoPBulkTpfa
              COMPILE_DEFINITIONS FACETFLOWTYPETAG=TwoPFacetTpfa
              CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )")

# using FEM (2nd order) and TPFA
dune_add_test(NAME test_network2d_2p_fem_2ndorder_mpfa
              SOURCES main_fem_fv.cc
              COMPILE_DEFINITIONS FEMORDER=2
              COMPILE_DEFINITIONS BULKFLOWTYPETAG=TwoPBulkMpfa
              COMPILE_DEFINITIONS FACETFLOWTYPETAG=TwoPFacetTpfa
              CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )")

# using FEM (2nd order) and BOX
dune_add_test(NAME test_network2d_2p_fem_2ndorder_box
             SOURCES main_fem_fv.cc
             COMPILE_DEFINITIONS FEMORDER=2
             COMPILE_DEFINITIONS BULKFLOWTYPETAG=TwoPBulkBox
             COMPILE_DEFINITIONS FACETFLOWTYPETAG=TwoPFacetBox
             CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )")

# using Box and TPFA
dune_add_test(NAME test_network2d_2p_box_tpfa
              SOURCES main_box_fv.cc
              COMPILE_DEFINITIONS BULKFLOWTYPETAG=TwoPBulkTpfa
              COMPILE_DEFINITIONS FACETFLOWTYPETAG=TwoPFacetTpfa
              CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )")

# using Box and MPFA
dune_add_test(NAME test_network2d_2p_box_mpfa
              SOURCES main_box_fv.cc
              COMPILE_DEFINITIONS BULKFLOWTYPETAG=TwoPBulkMpfa
              COMPILE_DEFINITIONS FACETFLOWTYPETAG=TwoPFacetTpfa
              CMAKE_GUARD "( dune-foamgrid_FOUND AND dune-alugrid_FOUND )")
