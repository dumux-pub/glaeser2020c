numElementsInFracture = 5;
a = 1e-3;
dx_f = a/numElementsInFracture;
dx_m = dx_f*5.0;

Point(1) = {0.5, -0.5, 0.0};
Point(2) = {0.5, -0.25, 0.0};
Point(3) = {0.5, -a/2.0, 0.0};
Point(4) = {0.5, a/2.0, 0.0};
Point(5) = {0.5, 0.25, 0.0};
Point(6) = {0.5, 0.5, 0.0};
Point(7) = {-0.5, 0.5, 0.0};
Point(8) = {-0.5, 0.25, 0.0};
Point(9) = {-0.5, a/2.0, 0.0};
Point(10) = {-0.5, -a/2.0, 0.0};
Point(11) = {-0.5, -0.25, 0.0};
Point(12) = {-0.5, -0.5, 0.0};

// horizontal lines
Line(1) = {7, 6};
Line(2) = {8, 5};
Line(3) = {9, 4};
Line(4) = {10, 3};
Line(5) = {11, 2};
Line(6) = {12, 1};
Transfinite Line{1:6} = 1.0/dx_m + 1;

// vertical matrix outlines
Line(7) = {7, 8};
Line(8) = {8, 9};
Line(9) = {10, 11};
Line(10) = {11, 12};
Line(11) = {1, 2};
Line(12) = {2, 3};
Line(13) = {4, 5};
Line(14) = {5, 6};
Transfinite Line{7:14} = 0.25/dx_m + 1;

// vertical fracture outlines
Line(15) = {9, 10};
Line(16) = {4, 3};
Transfinite Line{15, 16} = a/dx_f + 1;

// domain surfaces
Line Loop(1) = {10, 6, 11, -5};
Plane Surface(1) = {1};
Line Loop(2) = {5, 12, -4, 9};
Plane Surface(2) = {2};
Line Loop(3) = {4, -16, -3, 15};
Plane Surface(3) = {3};
Line Loop(4) = {3, 13, -2, 8};
Plane Surface(4) = {4};
Line Loop(5) = {2, 14, -1, 7};
Plane Surface(5) = {5};
Transfinite Surface "*";
Recombine Surface "*";
