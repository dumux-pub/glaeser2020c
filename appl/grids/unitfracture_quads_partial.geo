dx = 0.0125;
a = 1e-2;
numElemsPerFracture = 0.5/dx;
numElemsPerExtension = 0.25/dx;

Point(1) = {0.5, -0.5, 0, dx};
Point(2) = {0.5, 0, 0, dx};
Point(3) = {0.5, 0.5, 0, dx};
Point(4) = {-0.5, 0.5, 0, dx};
Point(5) = {-0.5, 0, 0, dx};
Point(6) = {-0.5, -0.5, 0, dx};

// points for defining inlet & outlet
Point(7) = {-0.5, -0.25, 0.0, dx};
Point(8) = {0.5, 0.25, 0.0, dx};
Point(9) = {-0.5, 0.25, 0.0, dx};
Point(10) = {0.5, -0.25, 0.0, dx};

// points for fracture (plus extension to boundaries)
Point(11) = {-0.25, 0.0, 0.0, dx};
Point(12) = {0.25, 0.0, 0.0, dx};
Point(13) = {-0.25, -0.5, 0.0, dx};
Point(14) = {-0.25, 0.5, 0.0, dx};
Point(15) = {0.25, -0.5, 0.0, dx};
Point(16) = {0.25, 0.5, 0.0, dx};
Point(17) = {0.25, 0.25, 0.0, dx};
Point(18) = {0.25, -0.25, 0.0, dx};
Point(19) = {-0.25, 0.25, 0.0, dx};
Point(20) = {-0.25, -0.25, 0.0, dx};

Line(1) = {4, 9};
Line(2) = {9, 5};
Line(3) = {5, 7};
Line(4) = {7, 6};
Line(5) = {6, 13};
Line(6) = {13, 20};
Line(7) = {20, 11};
Line(8) = {11, 19};
Line(9) = {19, 14};
Line(10) = {14, 16};
Line(11) = {16, 17};
Line(12) = {17, 12};
Line(13) = {12, 18};
Line(14) = {18, 15};
Line(15) = {15, 1};
Line(16) = {1, 10};
Line(17) = {10, 2};
Line(18) = {2, 8};
Line(19) = {8, 3};
Line(20) = {3, 16};
Line(21) = {14, 4};
Line(22) = {9, 19};
Line(23) = {19, 17};
Line(24) = {17, 8};
Line(25) = {2, 12};
Line(26) = {12, 11};
Line(27) = {11, 5};
Line(28) = {7, 20};
Line(29) = {20, 18};
Line(30) = {18, 10};
Line(31) = {15, 13};

Curve Loop(1) = {22, 9, 21, 1};
Plane Surface(1) = {1};
Curve Loop(2) = {9, 10, 11, -23};
Plane Surface(2) = {2};
Curve Loop(3) = {24, 19, 20, 11};
Plane Surface(3) = {3};
Curve Loop(4) = {18, -24, 12, -25};
Plane Surface(4) = {4};
Curve Loop(5) = {12, 26, 8, 23};
Plane Surface(5) = {5};
Curve Loop(6) = {8, -22, 2, -27};
Plane Surface(6) = {6};
Curve Loop(7) = {27, 3, 28, 7};
Plane Surface(7) = {7};
Curve Loop(8) = {7, -26, 13, -29};
Plane Surface(8) = {8};
Curve Loop(9) = {30, 17, 25, 13};
Plane Surface(9) = {9};
Curve Loop(10) = {16, -30, 14, 15};
Plane Surface(10) = {10};
Curve Loop(11) = {14, 31, 6, 29};
Plane Surface(11) = {11};
Curve Loop(12) = {6, -28, 4, 5};
Plane Surface(12) = {12};

Transfinite Line{10, 23, 26, 29, 31} = numElemsPerFracture;
Transfinite Line{1, 2, 3, 4, 5, 28, 27, 22, 21, 9, 8, 7, 6, 14, 13, 12, 11, 20, 24, 25, 30, 15, 16, 17, 18, 19} = numElemsPerExtension;

Transfinite Surface "*";
Recombine Surface "*";

// frature line
Physical Line(1) = {26};
Physical Surface(1) = {1:12};
