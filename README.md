# Welcome to the dumux pub table Glaeser2020c

This module contains the source code for the examples shown in the PhD thesis:

Gläser, Dennis. <em>Discrete fracture modeling of multi-phase flow and deformation in fractured poroelastic media.</em> https://dx.doi.org/10.18419/opus-11040

This code is based on a large number of unpublished changes to Dumux, that were necessary to circumvent limitations that made it impossible to implement the presented models work. These changes are shipped as part of the installation script (see below), and are incorporated while executing it. Therefore, it is important that you install this module __only__ with the provided installation script. We are working on incorporating the required changes into Dumux in a clean way in order to bring the models available in this repository into the mainline of Dumux.

## License

This project is licensed under the terms and conditions of the GNU General Public
License (GPL) version 3 or - at your option - any later version.
The GPL can be found under [GPL-3.0-or-later.txt](LICENSES/GPL-3.0-or-later.txt)
provided in the `LICENSES` directory located at the topmost of the source code tree.


## Version Information

|      module name      |      branch name      |                 commit sha                 |         commit date         |
|-----------------------|-----------------------|--------------------------------------------|-----------------------------|
|      dune-common      |  origin/releases/2.7  |  aa689abba532f40db8f5663fa379ea77211c1953  |  2020-11-10 13:36:21 +0000  |
|     dune-geometry     |  origin/releases/2.7  |  15e67ecffd1e37407dc489a5afc54adb1a683c22  |  2023-05-23 12:14:25 +0000  |
|       dune-grid       |  origin/releases/2.7  |  e8b460122a411cdd92f180dcca81ad8b2ac8d5ee  |  2021-08-31 13:04:20 +0000  |
|  dune-localfunctions  |  origin/releases/2.7  |  68f1bcf32d9068c258707d241624a08b771b6fde  |  2020-11-26 23:45:36 +0000  |
|       dune-istl       |  origin/releases/2.7  |  761b28aa1deaa786ec55584ace99667545f1b493  |  2020-11-26 23:29:21 +0000  |
|         dumux         |     origin/master     |  d9efb9d80b1f794d976f6b5ae1ffb9f71a7dcdf9  |  2019-07-31 02:20:02 +0200  |
|      dune-alugrid     |  origin/releases/2.7  |  51bde29a2dfa7cfac4fb73d40ffd42b9c1eb1d3d  |  2021-04-22 15:10:17 +0200  |
|     dune-foamgrid     |     origin/master     |  d49187be4940227c945ced02f8457ccc9d47536a  |  2020-01-06 15:36:03 +0000  |
|     dune-functions    |  origin/releases/2.7  |  534900aa256aeef9d025d5b8776ff1c0f0e144da  |  2020-10-29 19:47:28 +0000  |
|     dune-typetree     |  origin/releases/2.7  |  50603353e821c1139f74871b575011fa44d13701  |  2020-08-11 11:56:10 +0000  |
|      dune-subgrid     |  origin/releases/2.7  |  45d1ee9f3f711e209695deee97912f4954f7f280  |  2020-05-28 13:21:59 +0000  |

## Installation

The installation procedure is done as follows:
Create a root folder, e.g. `DUMUX`, enter the previously created folder,
clone this repository and use the install script `install_Glaeser2020c.py`
provided in this repository to install all dependent modules.

```sh
mkdir DUMUX
cd DUMUX
git clone https://git.iws.uni-stuttgart.de/dumux-pub/glaeser2020c.git Glaeser2020c
./Glaeser2020c/install_Glaeser2020c.py
```

This will clone all modules into the directory `DUMUX`,
configure your module with `dunecontrol` and build tests.

